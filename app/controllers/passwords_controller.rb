class PasswordsController < Devise::PasswordsController
  prepend_before_filter :require_no_authentication

  #layout "no_sidebar"


  def initialize
    @model = User
    super
  end

  # GET /resource/password/new
  def new
    super
  end

  # POST /resource/password
  def create
    super
  end

  # GET /resource/password/edit?reset_password_token=abcdef
  def edit
    super
  end

  # PUT /resource/password
  def update
    super
  end

  protected

  # The path used after sending reset password instructions
  def after_sending_reset_password_instructions_path_for(resource_name)
    super
  end

  def get_tabs

  end

end