class ReviewsController < BaseCrudController

  def initialize
    @model = Review
    super
  end

  def index
    @body_class = 'about'
    super
  end

end
