class BaseCrudController < BaseFrontController

  before_filter :find_model, :only =>  [:show, :edit, :update]

  ### SEO

  before_filter do |c|
    [:title, :keywords, :description, :robots].each do |tag|
      c.send("get_#{action_name}_#{tag}") if c.respond_to?("get_#{action_name}_#{tag}")
    end
  end

  ### SEO

  include Modules::CollectionModule
  include Modules::CrudModule
  include Modules::SeoModule

end