require 'mongo'

class GridfsController < ApplicationController

  def file
    find_file
  end

  def mini_thumb_24x24
    find_file(:mini_thumb_24x24)
  end

  def mini_thumb_30x30
    find_file(:mini_thumb_30x30)
  end

  def mini_thumb_70x70
    find_file(:mini_thumb_70x70)
  end

  def thumb_file
    find_file(:thumb)
  end

  def middle_thumb
    find_file(:middle_thumb)
  end

  def middle_thumb_142x163
    find_file(:middle_thumb_142x163)
  end

  def middle_thumb_173x182
    find_file(:middle_thumb_173x182)
  end

  def middle_thumb_182x182
    find_file(:middle_thumb_182x182)
  end

  def big_thumb_file
    find_file(:big_thumb)
  end

  def big_thumb_538x435
    find_file(:big_thumb_538x435)
  end

  def big_thumb_270x170
    find_file(:big_thumb_270x170)
  end

  def big_thumb_270x200
    find_file(:big_thumb_270x200)
  end

  def big_thumb_270x430
    find_file(:big_thumb_270x430)
  end

  def big_thumb_570x200
    find_file(:big_thumb_570x200)
  end

  def full_screen_thumb
    find_file(:full_screen_thumb)
  end

  def full_screen_thumb_1000x850
    find_file(:full_screen_thumb_1000x850)
  end


  protected

  def find_file(version = nil)
    @record = params[:model].camelcase.constantize.cached_find(params[:id])

    content = (version.nil? ? @record.file : @record.file.send(version)).read
    if (@record.nil? || content.nil?)
      render_404
    else
      if stale?(etag: content, last_modified: @record.updated_at.utc, public: true)
        send_data content, type: (version.nil? ? @record.file : @record.file.send(version)).file.content_type, disposition: "inline"
        expires_in 0, public: true
      end
    end
  end

end