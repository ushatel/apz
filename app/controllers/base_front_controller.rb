class BaseFrontController < ApplicationController

  attr_writer :model

  def clean_cache!
    expire_fragment "#{get_pluralized_model}_block"
  end

end