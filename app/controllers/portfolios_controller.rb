class PortfoliosController < BaseCrudController

  def initialize
    @model = Portfolio
    super
  end

  def show
    @records = @model.ordered(get_field, get_order).where(:category_ids.in => [@record.id]).page(params[:page]).per(per_records)

    super
  end


  protected

  def find_model
    if params[:id] == 'all'
      redirect_to portfolios_path, :status => 301
    else
      @record = Category.find_by_slug(params[:id])
      render_404  if @record.nil?
    end
  end

  def per_records
    @per_records = Settings.portfolio_page_records_count.to_i || 12
  end
end
