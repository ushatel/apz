class RegistrationsController < Devise::RegistrationsController
  #before_filter :check_permissions, :only => [:new, :create, :cancel]
  skip_before_filter :require_no_authentication
  after_filter :add_role

  #layout "no_sidebar"

  #def check_permissions
  #  authorize! :create, resource
  #end

  def initialize
    @model = User
    super
  end


  protected

  def add_role
    if params[:role] == 'worker'
      @user.roles << Role.find("52f8f90db3b53e9fc500000b")
      @user.save
    end
  end


  private

  def build_resource(*args)
    super
    if session['devise.omniauth_data']
      @user.apply_omniauth(session['devise.omniauth_data'])
      @user.valid?
    end
  end

  def get_tabs

  end


end