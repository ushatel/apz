class PeopleController < BaseCrudController

  def initialize
    @model = Person
    super
  end

  def index
    @body_class = 'about'
    super
  end

end
