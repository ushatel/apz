class TestingsController < BaseCrudController

  # Only for development

  layout "testings"

  def initialize
    @model = Project
    super
  end

  respond_to :json, :html

  def show

    #@record = Zoo.find_by_slug("3")
    #
    #@count = @record.animals.size
    #respond_with(:api, json: @records, root: "items", meta: {total: count})

    if stale?(:etag => @record, :last_modified => @record.updated_at.utc, :public => true)
      respond_with(:api, @record)
    end
  end

  def find_model

  end

end
