class ArticlesController < BaseCrudController

  def initialize
    @model = Article
    super
  end

  def show
    @body_class = 'about'
    @records = @model.ordered(get_field, get_order).where(get_search_string)
    super
  end

end
