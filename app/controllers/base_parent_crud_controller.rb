class BaseParentCrudController < BaseFrontController

  attr_writer :parent_model # :model inherited from base class

  before_filter :find_parent_model, :only => [:index, :show, :new, :create, :edit, :update, :delete, :destroy, :develop]
  before_filter :find_model, :only => [:show, :edit, :update, :delete, :destroy, :develop]

  ### Check permission

  ### Check permission

  include Modules::ParentCollectionModule
  include Modules::ParentCrudModule


end
