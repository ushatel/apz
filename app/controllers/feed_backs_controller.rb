class FeedBacksController < BaseCrudController

  def initialize
    @model = FeedBack
    super
  end

  def index
    @record = FeedBack.new
    @body_class = 'feed-back'

    render action: "new"
  end

  def subscribe

    begin
      gb = Gibbon::API.new(Settings.mailchimp_api_key)
      gb.lists.subscribe({:id => Settings.mailchimp_list_id, :email => {:email => params[:email]}, :merge_vars => {:FNAME => params[:name]}, :double_optin => false})
    rescue

    end

    FeedBack.create(:name => params[:name], :email => params[:email], :topic => Settings.subscribe_form_topic)

    render json: {status: 1}
  end


  protected

  def redirect_after_create
    redirect_to root_path, notice: I18n.t("models.#{@model.to_s.underscore.pluralize}.created")
  end

end
