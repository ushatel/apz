class WelcomeController < BaseCrudController

  def initialize
    @model = Page
    super
  end

  def index

    respond_to do |format|
      format.html
    end
  end

  def service
    gon.roomSpaceLessThen = Settings.room_space_less_then
    gon.roomSpaceLessValue = Settings.room_space_less_value
    gon.roomSpaceAboveValue = Settings.room_space_above_value

    gon.discountLessThen = Settings.discount_less_then
    gon.discountLessValue = Settings.discount_less_value
    gon.discountAboveValue = Settings.discount_above_value

    gon.urgency1Day = Settings.urgency_1_day
    gon.urgency2Day = Settings.urgency_2_day
    gon.urgency3Day = Settings.urgency_3_day
    gon.urgency4Day = Settings.urgency_4_day

    gon.furniturePlanCost = Settings.furniture_plan_cost
    gon.ceilingPlanCost = Settings.ceiling_plan_cost
    gon.lightingPlanCost = Settings.lighting_plan_cost
    gon.coveringsPlanCost = Settings.coverings_plan_cost
    gon.wallsPlanCost = Settings.walls_plan_cost

    gon.selectingFacilitiesCost = Settings.selecting_facilities_cost

    gon.adjustmentsCountLessThen = Settings.adjustments_count_less_then
    gon.adjustmentsCountLessValue = Settings.adjustments_count_less_value
    gon.adjustmentsCountAboveValue = Settings.adjustments_count_above_value

    gon.modelingTasksCost = Settings.modeling_tasks_cost
    gon.highValueImageCost = Settings.high_value_image_cost
    gon.initialSceneCost = Settings.initial_scene_cost
    gon.highResolutionImageCost = Settings.high_resolution_image_cost
    gon.drawingsCost = Settings.drawings_cost

    respond_to do |format|
      format.html
    end
  end

  def about
    @body_class = 'about'
  end

  def sitemap
    @records = Node.in_xml_sitemap
    @map = XmlSitemap::Map.new(Settings.hostname) do |m|
      @records.each do |record|
        m.add polymorphic_path(record)
      end
    end

    respond_to do |format|
      format.xml { render :xml => @map.render }
      format.json { render :json => Hash.from_xml(@map.render).to_json }
      format.html
    end
  end

  def robots
    respond_to do |format|
      format.txt { render :text => Settings.robots_txt }
    end
  end

  def login
    redirect_to new_user_session_path
  end

  def client_register

  end

  def worker_register

  end

  def task_set
    redirect_to root_path, notice: I18n.t("models.tasks.created")
  end


  protected

  def per_records
    Settings.main_page_records_count.to_i || 20
  end

  def get_index_title
    @title = Settings.main_page_title
  end

  def get_index_keywords
    @seo_keywords = Settings.main_page_keywords
  end

  def get_index_description
    @seo_description = Settings.main_page_description
  end

  def get_index_robots
    @seo_robots = Settings.main_page_robots
  end

end
