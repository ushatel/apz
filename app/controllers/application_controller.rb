class ApplicationController < ActionController::Base
  include ApplicationHelper
  include ViewsHelper

  protect_from_forgery

  before_filter :set_user_language
  before_filter :set_last_request_at
  before_filter :set_current_user_to_gon
  before_filter :set_gon_values
  before_filter :send_user_id_to_newrelic
  #before_filter :miniprofiler
  after_filter :prepare_unobtrusive_flash

  def miniprofiler
    #Rack::MiniProfiler.authorize_request  if current_user.role?("Admin")
  end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end

  def send_user_id_to_newrelic
    if user_signed_in?
      NewRelic::Agent.add_custom_parameters({:user_id => current_user.id.to_s})
    end
  end

  ### Deprecated
  def check_permission(permission, class_name, redirect_path = nil, message = nil)
    alert_message = message.nil? ? I18n.t('alerts.access_denied') : message

    if can? permission, class_name
      true
    else
      unless redirect_path.nil?
        redirect_to redirect_path, :alert => alert_message
        return false
      end

      if user_signed_in?
        redirect_to account_path, :alert => alert_message
        return false
      else
        session[:return_to_path] = request.url unless params[:controller] == "devise/sessions"  # Return to this page after sign in
        session[:user_return_to] = request.url unless params[:controller] == "devise/sessions"  # Return to this page after sign in
        redirect_to new_user_session_path, :alert => alert_message
        return false
      end
    end
  end

  def check_registered(options = {})
    alert_message = options[:message].nil? ? I18n.t('alerts.access_denied') : options[:message]
    redirect_to_sign_in_page(options[:redirect_path], alert_message) if current_user.nil?
  end

  def check_role(roles, options = {})
    alert_message = options[:message].nil? ? I18n.t('alerts.access_denied') : options[:message]

    if current_user.nil?
      redirect_to_sign_in_page(options[:redirect_path], alert_message)
    else
      if current_user.has_roles?(roles)
        true
      else
        redirect_to_sign_in_page(options[:redirect_path], alert_message)
      end
    end
  end

  def redirect_to_sign_in_page(redirect_path, alert_message)
    unless redirect_path.nil?
      redirect_to redirect_path, :alert => alert_message
      return false
    end

    if user_signed_in?
      redirect_to account_path, :alert => alert_message
      return false
    else
      session[:return_to_path] = request.url unless params[:controller] == "devise/sessions"  # Return to this page after sign in
      session[:user_return_to] = request.url unless params[:controller] == "devise/sessions"  # Return to this page after sign in
      redirect_to new_user_session_path, :alert => alert_message
      return false
    end
  end

  # Customize the Devise after_sign_in_path_for() for redirect to previous page after login
  def after_sign_in_path_for(resource_or_scope)
    #stored_location_for(resource) || welcome_path
    #if session[:return_to_path].nil?
    #  root_path
    #else
    #  session[:return_to_path]
    #end
    account_path
  end


  private

  def set_user_language
    I18n.locale = :ru
    gon.language = I18n.locale
  end

  def set_last_request_at
    current_user.update_attribute(:last_request_at, Time.now.utc) if user_signed_in?
  end

  def set_current_user_to_gon
    gon.current_user = current_user.nil? ? nil : current_user.login
    @current_user = current_user
  end

  def set_gon_values
    gon.notifyDelayTime = Settings.notify_delay_time.to_i
  end

  def deny_access
    store_location
    redirect_to new_user_session_path
  end

  def render_404
    #raise Mongoid::Errors::DocumentNotFound.new(params[:model].camelcase.constantize, "Document not found") if (@record.nil? || content.nil?)
    respond_to do |format|
      format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
      format.xml  { head :not_found }
      format.any  { head :not_found }
    end
  end

  # add back anyone_signed_in? method after Oliver's comment @ 2011-03-12
  def anyone_signed_in?
    !current_user.nil?
  end

  def store_location
    session[:return_to] = request.fullpath
  end

  def clear_stored_location
    session[:return_to] = nil
  end

  def get_model_sym
    (@child_model || @model).to_s.underscore.to_sym
  end

  def get_nested_model_sym
    @nested_model.to_s.underscore.to_sym
  end

  def get_pluralized_model
    model = @model || @child_model
    get_pluralized_class_string(model)
  end

  def get_pluralized_nested_model
    get_pluralized_class_string(@nested_model)
  end

  def get_parent_model
    @parent_model.to_s.underscore
  end

  def get_child_model
    @child_model.to_s.underscore
  end

  def get_search_string
    if params[:q].nil?
      if params[:filter_by] == "user_slug"
        user = User.where(:slug => params[:filter_value]).first
        {:user_id => user.id} unless user.nil?
      elsif params[:filter_by] == "visualization_slug"
          visualization = Visualization.cached_find_by_slug_or_id(params[:filter_value])
          {:visualization_id => visualization.id} unless visualization.nil?
      elsif params[:filter_by] == "facilities_set_slug"
        facilities_set = FacilitiesSet.find(params[:filter_value])
        {:facilities_set_id => facilities_set.id} unless facilities_set.nil?
      elsif params[:filter_by] == "task_slug"
        task = Task.find(params[:filter_value])
        {:task_id => task.id} unless task.nil?
      elsif params[:filter_by] == "scenes_set_slug"
        scenes_set = ScenesSet.find(params[:filter_value])
        {:scenes_set_id => scenes_set.id} unless scenes_set.nil?
      elsif !params[:filter_by].nil? && (params[:filter_by] != '')
        {params[:filter_by].to_sym => params[:filter_value]} unless params[:filter_value].nil?
      end
    else
      {:name => /#{params[:q]}/}
    end
  end

end
