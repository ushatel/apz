class SearchController < BaseCrudController

  def initialize
    @model = Node
    super
  end

  def index

    unless params[:search_string].nil?
      redirect_to search_path(params[:search_string])
      return
    end

    respond_to do |format|
      format.html
    end
  end

  def show
    @search_value = params[:id]

    respond_to do |format|
      format.html
      format.json { render :json =>  {get_pluralized_class_string(@model).to_sym => @records, :total => @records.count} }
      format.xml  { render :xml =>  {get_pluralized_class_string(@model).to_sym => @records, :total => @records.count} }
      format.js
    end
  end


  protected

  def find_model
    search_value = params[:id].to_s.chop
    category_ids = Category.where(:name => /#{search_value}/i).only(:_id).map(&:_id)
    @records = @model.ordered(get_field, get_order).any_of(
        {:name => /#{search_value}/i},
        {:title => /#{search_value}/i},
        {:body => /#{search_value}/i},
        {:teaser => /#{search_value}/i},
        {:seo_keywords => /#{search_value}/i},
        {:seo_description => /#{search_value}/i},
        {:category_ids.in => category_ids}
    ).page(params[:page]).per(per_records)
  end

  def get_search_string
    {:name => /#{params[:id]}/i, :sku => /#{params[:id]}/i, :body => /#{params[:id]}/i} unless params[:id].nil?
  end

  def get_tabs

  end

  def get_index_title
    @title = I18n.t("models.search.site_title")
  end

  def get_index_keywords
    @seo_keywords = I18n.t("models.search.site_keywords")
  end

  def get_index_description
    @seo_description = I18n.t("models.search.site_description")
  end

  def get_show_title
    @title = I18n.t("models.search.show_site_title", :phrase => params[:id])
  end

  def get_show_keywords
    @seo_keywords = I18n.t("models.search.show_site_keywords", :phrase => params[:id])
  end

  def get_show_description
    @seo_description = I18n.t("models.search.show_site_description", :phrase => params[:id])
  end

end
