class SessionsController < Devise::SessionsController

  #before_filter :check_permissions, :only => [:new, :create, :cancel]
  skip_before_filter :require_no_authentication ### Required for authorizing (VPolotskiy)

  ActionController::Base.before_filter :only => [:destroy] do |c|
    #c.check_permission :destroy, @record
    true
  end

  #layout "no_sidebar"


  def initialize
    @model = User
    super
  end

  def get_tabs

  end

end