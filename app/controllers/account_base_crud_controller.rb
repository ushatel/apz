class AccountBaseCrudController < BaseFrontController

  ### Check permission

  before_filter :only => [:index] do |c|
    c.check_permission :update, current_user
  end

  ### Check permission

  def set_gon_values
    super()

    #gon.canManageFacilities = can?(:manage, Facility)
    #gon.canManageVisualizations = can?(:manage, Visualization)

    unless current_user.nil?
      gon.current_user_role = current_user.roles.first.slug unless current_user.roles.first.nil?

      gon.current_user_object = {
          :name => current_user.get_name,
          :login => current_user.login,
          :email => current_user.email,
          :roles => current_user.roles.map { |role| role.slug },
          :last_sign_in_at => current_user.last_sign_in_at.strftime(Settings.date_time_format),
          :avatar => (current_user.image unless current_user.image.nil?),
          :updated_at => current_user.updated_at.strftime(Settings.date_time_format),
          :created_at => current_user.created_at.strftime(Settings.date_time_format)
      }
    end

    #gon.projectsIndexPageFirstLabel = Settings.projects_index_page_first_label
    #gon.visualizationsEditPageFirstLabel = Settings.visualizations_edit_page_first_label
    #gon.visualizationsEditPageSecondLabel = Settings.visualizations_edit_page_second_label
    #gon.addDrawingsInfoLabel = Settings.add_drawings_info_label
    #gon.selectFacilitiesInfoLabel = Settings.select_facilities_info_label
    #gon.furniturePlanFirstLabel = Settings.furniture_plan_first_label
    #gon.ceilingPlanFirstLabel = Settings.ceiling_plan_first_label
    #gon.lightingPlanFirstLabel = Settings.lighting_plan_first_label
    #gon.coveringsPlanFirstLabel = Settings.coverings_plan_first_label
    #gon.wallsPlanFirstLabel = Settings.walls_plan_first_label
    #gon.selectedFacilitySettingsLabel = Settings.selected_facility_settings_label
    #gon.modelingObjectTaskLightboxLabel = Settings.modeling_object_task_lightbox_label
    gon.taskLightboxLabel = Settings.task_lightbox_label
    gon.userProfileLightboxLabel = Settings.user_profile_lightbox_label

    #gon.taskStatuses = Settings.get_task_statuses

    gon.roomSpaceLessThen = Settings.room_space_less_then
    gon.roomSpaceLessValue = Settings.room_space_less_value
    gon.roomSpaceAboveValue = Settings.room_space_above_value

    #gon.discountLessThen = Settings.discount_less_then
    #gon.discountLessValue = Settings.discount_less_value
    #gon.discountAboveValue = Settings.discount_above_value

    #gon.urgency1Day = Settings.urgency_1_day
    #gon.urgency2Day = Settings.urgency_2_day
    #gon.urgency3Day = Settings.urgency_3_day
    #gon.urgency4Day = Settings.urgency_4_day

    gon.furniturePlanCost = Settings.furniture_plan_cost
    gon.ceilingPlanCost = Settings.ceiling_plan_cost
    gon.lightingPlanCost = Settings.lighting_plan_cost
    gon.coveringsPlanCost = Settings.coverings_plan_cost
    gon.wallsPlanCost = Settings.walls_plan_cost

    gon.selectingFacilitiesCost = Settings.selecting_facilities_cost

    gon.adjustmentsCountLessThen = Settings.adjustments_count_less_then
    gon.adjustmentsCountLessValue = Settings.adjustments_count_less_value
    gon.adjustmentsCountAboveValue = Settings.adjustments_count_above_value

    gon.modelingTasksCost = Settings.modeling_tasks_cost
    gon.initialSceneCost = Settings.initial_scene_cost
    gon.highResolutionImageCost = Settings.high_resolution_image_cost
    gon.highValueImageCost = Settings.high_value_image_cost
    gon.drawingsCost = Settings.drawings_cost

  end

end
