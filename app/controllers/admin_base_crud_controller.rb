class AdminBaseCrudController < BaseFrontController

  ### Check permission

  before_filter :only => [:index] do |c|
    c.check_role([:manager, :admin])
  end

  ### Check permission

  def set_gon_values
    super

    gon.furniturePlanFirstLabel = Settings.furniture_plan_first_label
    gon.ceilingPlanFirstLabel = Settings.ceiling_plan_first_label
    gon.lightingPlanFirstLabel = Settings.lighting_plan_first_label
    gon.coveringsPlanFirstLabel = Settings.coverings_plan_first_label
    gon.wallsPlanFirstLabel = Settings.walls_plan_first_label

    unless current_user.nil?
      gon.current_user_object = {
          :name => current_user.get_name,
          :login => current_user.login,
          :email => current_user.email,
          :roles => (current_user.roles.map { |role| role.slug }  || []),
          :last_sign_in_at => current_user.last_sign_in_at.strftime(Settings.date_time_format),
          :updated_at => current_user.updated_at.strftime(Settings.date_time_format),
          :created_at => current_user.created_at.strftime(Settings.date_time_format)
      }
    end
  end

end