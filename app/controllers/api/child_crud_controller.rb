class Api::ChildCrudController < BaseFrontController

  respond_to :html, :json

  before_filter :find_parent_record, :only =>  [:index, :show, :create, :update, :delete, :destroy]
  before_filter :find_child_records, :only =>  [:index]
  before_filter :find_child_record, :only =>  [:show, :update, :delete, :destroy]
  after_filter :clean_cache!, :only => [:create, :update, :destroy]

  def index

    if stale?(:etag => get_records_cache_key, :last_modified => get_records_last_modified, :public => true)
      respond_with(:api, json: @child_records, root: "items", meta: {total: get_total})
    end
  end

  def show

    if stale?(:etag => @child_record, :last_modified => @child_record.updated_at.utc, :public => true)
      respond_with(:api, @child_record)
    end
  end

  def create
    create_model!
    @child_record.save

    respond_with(:api, @parent_model, @child_record)
  end

  # PUT /role/1
  # PUT /role/1.json
  def update
    update_model!
    result = @child_record.valid? ? {json: @child_record} : @child_record
    respond_with(:api, result)
  end

  # DELETE /roles/1
  # DELETE /roles/1.json
  def delete
    respond_with(:api, @child_record)
  end


  # DELETE /roles/1
  # DELETE /roles/1.json
  def destroy
    destroy_model!
    respond_with(:api, json: {status: 1})
  end

  protected

  def find_child_records
    db_query = @child_records ||= @parent_record.send("get_ordered_#{get_pluralized_model}").where(get_search_string)
    if params[:disable_pagination] == "true"
      cached_result = Rails.cache.fetch "#{get_pluralized_model}-ordered-#{get_field}-#{get_order}-#{get_search_string}-disable_pagination", :tag => [get_pluralized_model] do
        fetch_cache_from_db_query(db_query)
      end
      fetch_from_cached_records(cached_result)
    else
      cached_result = Rails.cache.fetch "#{get_pluralized_model}-ordered-#{get_field}-#{get_order}-#{get_search_string}-#{params[:page]}-#{params[:per]}", :tag => [get_pluralized_model] do
        fetch_cache_from_db_query(db_query.page(params[:page]).per(params[:per]))
      end
      fetch_from_cached_records(cached_result)
    end
  end

  def find_parent_record
    @parent_record ||= @parent_model.cached_find_by_slug_or_id(params["#{get_parent_model}_id".to_sym]) unless @parent_model.nil?
    render_404 if @parent_record.nil?
  end

  def find_child_record
    @child_record = @child_model.cached_find_by_slug_or_id(params[:id]) unless @child_model.nil?
    render_404 if @child_record.nil?
  end

  def get_total
    @child_records_count || @child_records.count
  end

  def get_records_cache_key
    @child_records_cache_key || @child_records
  end

  def get_records_last_modified
    @child_records_last_modified || Time.now.utc
  end

  def create_model!
    @child_record = @parent_record.send(get_pluralized_model).new(params[get_model_sym])
    @child_record.user = current_user unless @child_model.associations["user"].nil?
  end

  def update_model!
    @child_record.update_attributes(params[get_model_sym])
  end

  def destroy_model!
    @child_record.destroy
  end

  def get_field
    return :created_at if params[:field].nil?
    @child_model.fields.include?(params[:field].to_s) ? params[:field].to_sym : :created_at
  end

  def get_order
    return :desc if params[:order].nil?
    [:asc, :desc].include?(params[:order].to_sym) ? params[:order].to_sym : :desc
  end

  def default_serializer_options
    {root: false}
  end

  def fetch_cache_from_db_query(db_query)
    result = {}
    result[:count] = db_query.count
    result[:cache_key] = db_query.cache_key
    result[:last_modified] = db_query.desc(:updated_at).first.updated_at.utc unless db_query.desc(:updated_at).first.nil?
    result[:records] = db_query.to_a
    result
  end

  def fetch_from_cached_records(cached_records)
    cached_records.assert_valid_keys(:records, :count, :cache_key, :last_modified)
    @child_records = cached_records[:records]
    @child_records_count = cached_records[:count]
    @child_records_cache_key = cached_records[:cache_key]
    @child_records_last_modified = cached_records[:last_modified]
  end

end