class Api::ArticlesController < Api::BaseCrudController

  def initialize
    @model = Article
    super
  end

  def create
    create_model!
    @record.save

    if !params[get_model_sym][:image_id].nil?
      @record.update_attributes(params[get_model_sym])
    end

    respond_with(:api, @record)
  end


  protected

  def create_model!
    delete_empty_values

    super
  end

  def update_model!
    delete_empty_values

    super
  end

  def delete_empty_values

    [:slideshow_image_ids].each do |param|
      params[get_model_sym].delete(param) if params[get_model_sym][param].nil? || (params[get_model_sym][param] == "") #|| (params[get_model_sym][param] == 'null')
      params[get_model_sym][param] = JSON.parse(params[get_model_sym][param]) if params[get_model_sym][param].class == String
    end
  end

end
