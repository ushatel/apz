class Api::NestedChildCrudController < BaseFrontController

  respond_to :html, :json

  before_filter :find_parent_record, :only => [:create]
  before_filter :find_child_record, :only => [:index, :create]
  before_filter :find_nested_record, :only => [:show, :update, :delete, :destroy]
  before_filter :find_nested_records, :only => [:index]


  def index
    respond_with(:api, json: {items: @nested_records, total: get_total})
  end

  def show
    respond_with(:api, @nested_record)
  end

  def create
    create_model!
    @nested_record.save

    respond_with(:api, @parent_record, @child_record, @nested_record)
  end

  # PUT /role/1
  # PUT /role/1.json
  def update
    update_model!
    result = @nested_record.valid? ? {json: @nested_record} : @nested_record
    respond_with(:api, result)
  end

  # DELETE /roles/1
  # DELETE /roles/1.json
  def delete
    respond_with(:api, @child_record)
  end


  # DELETE /roles/1
  # DELETE /roles/1.json
  def destroy
    destroy_model!
    respond_with(:api, json: {status: 1})
  end


  protected

  def find_nested_records
    @nested_records ||= @child_record.send(get_pluralized_nested_model).where(get_search_string).desc(:created_at).page(params[:page])
  end

  def find_parent_record
    @parent_record ||= @parent_model.find_by_slug(params["#{get_parent_model}_id".to_sym]) unless @parent_model.nil?
    render_404 if @parent_record.nil?
  end

  def find_child_record
    @child_record = @child_model.find_by_slug(params["#{get_child_model}_id".to_sym]) unless @child_model.nil?
    render_404 if @child_record.nil?
  end

  def find_nested_record
    @nested_record = @nested_model.find_by_slug(params[:id]) unless @nested_model.nil?
    render_404 if @nested_record.nil?
  end

  def get_total
    @nested_records.count
  end

  def create_model!
    @nested_record = @child_record.send(get_pluralized_nested_model).new(params[get_nested_model_sym])
    @nested_record.user = current_user unless @nested_model.associations["user"].nil?
  end

  def update_model!
    @nested_record.update_attributes(params[get_nested_model_sym])
  end

  def destroy_model!
    @nested_record.destroy
  end

  def get_field
    return :created_at if params[:field].nil?
    @nested_model.fields.include?(params[:field].to_s) ? params[:field].to_sym : :created_at
  end

  def get_order
    return :desc if params[:order].nil?
    [:asc, :desc].include?(params[:order].to_sym) ? params[:order].to_sym : :desc
  end

end