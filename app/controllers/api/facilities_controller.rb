class Api::FacilitiesController < Api::BaseCrudController

  before_filter :only => [:index, :show] do |c|
    c.check_registered
  end

  before_filter :only => [:create] do |c|
    c.check_role([:worker, :manager, :admin])
  end

  before_filter :only => [:update, :destroy] do |c|
    c.check_role([:manager, :admin])
  end

  def initialize
    @model = Facility
    super
  end

  def create
    create_model!
    @record.save

    if !params[get_model_sym][:image_ids].nil? || !params[get_model_sym][:max_file_ids].nil?
      @record.update_attributes(params[get_model_sym])
    end

    respond_with(:api, @record)
  end


  protected


  def create_model!
    delete_empty_values

    super()
  end

  def update_model!
    delete_empty_values

    super()
  end

  def delete_empty_values
    [:image_ids, :max_file_ids].each do |param|
      params[get_model_sym].delete(param) if (params[get_model_sym][param].nil? || (params[get_model_sym][param] == ""))
    end
  end

  def find_records

    if params[:filters].nil? || (JSON.parse(params[:filters]) == {})
      db_query = @model.includes(:user, :manufacturer, :style, :group, :materials, :images, :max_files).ordered(get_field, get_order).where(get_search_string)
      if params[:disable_pagination] == "true"
        cached_result = Rails.cache.fetch "#{get_pluralized_model}-ordered-#{get_field}-#{get_order}-#{get_search_string}-disabled_pagination", :tag => [get_pluralized_model] do
          fetch_cache_from_db_query(db_query)
        end
        fetch_from_cached_records(cached_result)
      else

        cached_result = Rails.cache.fetch "#{get_pluralized_model}-ordered-#{get_field}-#{get_order}-#{get_search_string}-#{params[:page]}-#{params[:per]}", :tag => [get_pluralized_model] do
          fetch_cache_from_db_query(db_query.page(params[:page]).per(params[:per]))
        end
        fetch_from_cached_records(cached_result)
      end

    else
      filters = JSON.parse(params[:filters])

      query = {}

      query[:group_id.in] = filters['group_ids'] if (!filters['group_ids'].nil? && (filters['group_ids'] != []))
      query[:style_id.in] = filters['style_ids'] if (!filters['style_ids'].nil? && (filters['style_ids'] != []))
      query[:manufacturer_id.in] = filters['manufacturer_ids'] if (!filters['manufacturer_ids'].nil? && (filters['manufacturer_ids'] != []))
      query[:material_ids.in] = filters['material_ids'] if (!filters['material_ids'].nil? && (filters['material_ids'] != []))

      if !filters['name'].nil? && (filters['name'] != "")
        cached_result = Rails.cache.fetch "#{get_pluralized_model}-where-#{query}-#{filters['name']}-#{get_order}-#{get_field}-#{params[:page]}-#{params[:per]}", :tag => [get_pluralized_model] do

          category_ids = Category.where(:name => /#{filters['name']}/i).only(:_id).map(&:_id)

          fetch_cache_from_db_query @model.includes(:user, :manufacturer, :style, :group, :materials, :images, :max_files).ordered(get_field, get_order).any_of(
            {:name => /#{filters['name']}/i},
            {:manufacturer_id.in => category_ids},
            {:style_id.in => category_ids},
            {:group_id.in => category_ids},
            {:material_ids.in => category_ids}
          ).where(query).page(params[:page]).per(params[:per])
        end
        fetch_from_cached_records(cached_result)
      else
        cached_result = Rails.cache.fetch "#{get_pluralized_model}-where-#{query}-#{get_order}-#{get_field}-#{params[:page]}-#{params[:per]}", :tag => [get_pluralized_model] do
          fetch_cache_from_db_query @model.includes(:user, :manufacturer, :style, :group, :materials, :images, :max_files).ordered(get_field, get_order).where(query).page(params[:page]).per(params[:per])
        end
        fetch_from_cached_records(cached_result)
      end


    end
  end

end
