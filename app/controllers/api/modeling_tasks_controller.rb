class Api::ModelingTasksController < Api::BaseCrudController

  def initialize
    @model = ModelingTask
    super
  end

  def create
    create_model!
    @record.save

    if !params[get_model_sym][:modeling_task_image_id].nil? || !params[get_model_sym][:modeling_task_texture_ids].nil?
      @record.update_attributes(params[get_model_sym])
    end

    respond_with(:api, @record)
  end

  protected


  def create_model!
    delete_empty_values

    super()
  end

  def update_model!
    delete_empty_values

    super()
  end

  def delete_empty_values
    [:modeling_task_image_id, :modeling_task_texture_ids].each do |param|
      params[get_model_sym].delete(param) if (params[get_model_sym][param].nil? || (params[get_model_sym][param] == ""))
    end
  end

end
