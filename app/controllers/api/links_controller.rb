class Api::LinksController < Api::ChildCrudController

  def initialize
    @child_model = Link
    @parent_model = Menu
    super
  end

  def find_child_record
    @child_record = @parent_record.links.find_by_slug(params[:id]) unless @child_model.nil?
    render_404 if @child_record.nil?
  end

end
