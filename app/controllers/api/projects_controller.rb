class Api::ProjectsController < Api::BaseCrudController

  def initialize
    @model = Project
    super
  end


  protected

  def get_index_meta
    filter = current_user.nil? ? {} : {:user_id => current_user.id}
    project_ids = Project.where(filter).distinct(:_id)

    all_dates = Visualization.where(:project_id.in => project_ids).distinct(:created_at)
    min_date = all_dates.min
    max_date = all_dates.max

    period = "#{min_date.strftime("%d/%m/%Y")}-#{max_date.strftime("%d/%m/%Y")}" if !min_date.nil? && !max_date.nil? # min_date-max_date

    statuses = Visualization.where(:project_id.in => project_ids).distinct(:status)

    {total: get_total, total_records: project_ids.size, period: period, statuses: statuses}
  end

  def find_records

    if params[:filters].nil? || (JSON.parse(params[:filters]) == {})
      @records = @model.includes(:visualizations).ordered(get_field, get_order).where(get_search_string).page(params[:page]).per(params[:per])
      @records_count = @records.count
      @records_cache_key = @records.cache_key({user_id: current_user.id})
      @records_last_modified =  @records.first.nil? ? Time.now.utc : @records.first.updated_at.utc
    else

      filters = JSON.parse(params[:filters])

      query = {}

      if !filters['visualizationsMonth'].nil? && (filters['visualizationsMonth'] != 'null') && (filters['visualizationsMonth'] != '')
        start_day = Date.strptime("01/#{filters['visualizationsMonth']}/#{Date.today.year}", "%d/%m/%Y")
        end_day = Date.strptime("#{Date.new(Date.today.year, filters['visualizationsMonth'], -1).day}/#{filters['visualizationsMonth']}/#{Date.today.year}", "%d/%m/%Y")

        project_ids = Visualization.where(:created_at.gte => start_day, :created_at.lte => end_day).where(get_search_string).distinct(:project_id)
        query[:_id.in] = project_ids
      end

      if !filters['visualizationsStatus'].nil? && (filters['visualizationsStatus'] != 'null') && (filters['visualizationsStatus'] != '')
        project_ids = Visualization.where(:status => filters['visualizationsStatus']).where(get_search_string).distinct(:project_id)
        if query[:_id.in].nil?
          query[:_id.in] = project_ids
        else
          res = []
          project_ids.each do |project_id|
            res << project_id if query[:_id.in].include?(project_id)
          end
          query[:_id.in] = res

        end

      end

      @records = @model.includes(:visualizations).where(query).where(get_search_string).send(get_order, get_field).page(params[:page]).per(params[:per])
      @records_count = @records.count
      @records_cache_key = @records.cache_key({user_id: current_user.id})
      @records_last_modified =  @records.first.nil? ? Time.now.utc : @records.first.updated_at.utc
    end
  end

  def get_search_string
    if params[:filter_by] == "user_slug"
      if !current_user.nil? && current_user.can_read_all_tasks?
        {} # show all projects
      else
        user = User.where(:slug => params[:filter_value]).first
        {:user_id => user.id} unless user.nil?
      end
    else
      super
    end
  end

end
