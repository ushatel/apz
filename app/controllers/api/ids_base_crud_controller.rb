class Api::IdsBaseCrudController < Api::BaseCrudController

  def show
    respond_with(:api, json: @records, root: "items", meta: {total: get_total})
  end

  def destroy_model!
    @records.delete_all
    Cashier.expire get_pluralized_model
  end


  protected

  def find_model
    @records = @model.where(:slug.in => params[:id].split(","))
  end

end