class Api::SettingsController < Api::BaseCrudController

  def initialize
    @model = Settings
    super
  end

  def index
    Rails.cache.clear
    Rails.cache.clear
    Rails.cache.clear

    Settings.reload

    super()
  end

  def show
    Rails.cache.clear
    Rails.cache.clear
    Rails.cache.clear

    Settings.reload

    respond_with(:api, json: Settings.json(params[:id]))
  end

  def create
    create_model!
    find_model

    respond_with(:api, json: Settings.json(params[:id]))
  end

  def update
    update_model!
    find_model
    respond_with(:api, json: Settings.json(params[:id]))
  end


  protected

  def find_records
    @records = params[:filter_value].nil? ? Settings.get_all_as_json : Settings.get_part_as_json(params[:filter_value])
  end

  def find_model
    @record = Settings.send(params[:id]) unless @model.nil?
  end

  def create_model!
    if %w"default_user_image_id default_image_id".include?(params[:id])
      params[:value] = Image.create_from_remote(params[:value])
    end
    Settings.set_value(params[:id], params[:value])
    Settings.reload
  end

  def update_model!
    if %w"default_user_image_id default_image_id header_image_id".include?(params[:id])
      params[:value] = Image.create_from_remote(params[:value])
    end
    Settings.set_value(params[:id], params[:value])
    Settings.reload
  end

  def get_total
    @records.count
  end

end
