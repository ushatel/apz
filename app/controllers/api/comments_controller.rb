class Api::CommentsController < Api::BaseCrudController

  def initialize
    @model = Comment
    super
  end

  def create
    create_model!
    @record.save

    if !params[get_model_sym][:file_attachment_ids].nil?
      @record.update_attributes(params[get_model_sym])
    end

    respond_with(:api, @record)
  end


  protected

  def create_model!
    delete_empty_values

    super
  end

  def delete_empty_values
    [:file_attachment_ids].each do |param|
      params[get_model_sym].delete(param) if params[get_model_sym][param].nil? || (params[get_model_sym][param] == "")
      params[get_model_sym][param] = JSON.parse(params[get_model_sym][param]) if params[get_model_sym][param].class == String
    end
  end
end
