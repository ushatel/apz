class Api::ResultFileAttachmentsController < Api::BaseCrudController

  def initialize
    @model = ResultFileAttachment
    super
  end


  protected

  def create_model!
    super

    @record.task_id = params[:task_id] unless params[:task_id].nil?
  end

end