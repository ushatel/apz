class Api::UsersController < Api::BaseCrudController
  include RegexHelper

  def initialize
    @model = User
    super
  end

  def update_model!
    params[get_model_sym].delete(:password) if params[get_model_sym][:password] == ""
    params[get_model_sym].delete(:password_confirmation) if params[get_model_sym][:password_confirmation] == ""
    super()
  end

  def current
    if current_user.nil?
      render :json => {errors: 'Need authorize'}, :status => 401
    else
      render :json => {
          :name => current_user.get_name,
          :surname => current_user.surname,
          :login => current_user.login,
          :email => current_user.email,
          :image => current_user.image,
          :city => current_user.city,
          :studio => current_user.studio,
          :phone => current_user.phone,
          :roles => (current_user.roles.map { |role| role.slug }  || []),
          :last_sign_in_at => current_user.last_sign_in_at.strftime(Settings.date_time_format),
          :updated_at => current_user.updated_at.strftime(Settings.date_time_format),
          :created_at => current_user.created_at.strftime(Settings.date_time_format)
      }
    end
  end


  protected

  def find_records
    if params[:filters].nil? || (JSON.parse(params[:filters]) == {})
      super
    else
      filters = JSON.parse(params[:filters])

      query = {}

      query[:name] = name_surname_regexp(filters['name_start_with']) if (!filters['name_start_with'].nil? && (filters['name_start_with'] != []))
      query[:role_ids.in] = filters['role_ids'].to_a if (!filters['role_ids'].nil? && (filters['role_ids'] != []))

      if params[:disable_pagination] == "true"
        cached_result = Rails.cache.fetch "#{get_pluralized_model}-where-#{query}-#{get_order}-#{get_field}", :tag => [get_pluralized_model] do
          fetch_cache_from_db_query @model.includes(:roles).where(query).send(get_order, get_field)
        end
        fetch_from_cached_records(cached_result)
      else
        cached_result = Rails.cache.fetch "#{get_pluralized_model}-where-#{query}-#{get_order}-#{get_field}-#{params[:page]}-#{params[:per]}", :tag => [get_pluralized_model] do
          fetch_cache_from_db_query @model.includes(:roles).where(query).send(get_order, get_field).page(params[:page]).per(params[:per])
        end
        fetch_from_cached_records(cached_result)
      end
    end
  end

end
