class Api::ScenesController < Api::BaseCrudController

  before_filter :only => [:index, :show] do |c|
    c.check_registered
  end

  before_filter :only => [:create] do |c|
    c.check_role([:worker, :manager, :admin])
  end

  before_filter :only => [:update, :destroy] do |c|
    c.check_role([:manager, :admin])
  end

  def initialize
    @model = Scene
    super
  end

  def create
    create_model!
    @record.save

    if !params[get_model_sym][:image_ids].nil? || !params[get_model_sym][:max_file_ids].nil?
      @record.update_attributes(params[get_model_sym])
    end

    respond_with(:api, @record)
  end

  protected


  def create_model!
    delete_empty_values

    super()
  end

  def update_model!
    delete_empty_values

    super()
  end

  def delete_empty_values
    [:image_ids, :max_file_ids].each do |param|
      params[get_model_sym].delete(param) if (params[get_model_sym][param].nil? || (params[get_model_sym][param] == ""))
    end
  end

  def find_records

    if params[:filters].nil? || (JSON.parse(params[:filters]) == {})
      db_query = @model.includes(:user, :project_name, :performer, :room, :style, :tone, :gamma, :main_colors, :accent_colors, :materials, :images, :max_files).ordered(get_field, get_order).where(get_search_string)
      if params[:disable_pagination] == "true"
        cached_result = Rails.cache.fetch "#{get_pluralized_model}-ordered-#{get_field}-#{get_order}-#{get_search_string}-disabled_pagination", :tag => [get_pluralized_model] do
          fetch_cache_from_db_query(db_query)
        end
        fetch_from_cached_records(cached_result)
      else

        cached_result = Rails.cache.fetch "#{get_pluralized_model}-ordered-#{get_field}-#{get_order}-#{get_search_string}-#{params[:page]}-#{params[:per]}", :tag => [get_pluralized_model] do
          fetch_cache_from_db_query(db_query.page(params[:page]).per(params[:per]))
        end
        fetch_from_cached_records(cached_result)
      end

    else
      filters = JSON.parse(params[:filters])

      query = {}

      [:project_name, :performer, :room, :style, :tone, :gamma].each do |field|
        query["#{field}_id".to_sym.in] = filters["#{field}_ids"] if (!filters["#{field}_ids"].nil? && (filters["#{field}_ids"] != []))
      end

      [:main_color, :accent_color, :material].each do |field|
        query["#{field}_ids".to_sym.in] = filters["#{field}_ids"] if (!filters["#{field}_ids"].nil? && (filters["#{field}_ids"] != []))
      end


      if !filters['name'].nil? && (filters['name'] != "")
        cached_result = Rails.cache.fetch "#{get_pluralized_model}-where-#{query}-#{filters['name']}-#{get_order}-#{get_field}-#{params[:page]}-#{params[:per]}", :tag => [get_pluralized_model] do

          category_ids = Category.where(:name => /#{filters['name']}/i, :section_id => Settings.project_names_section).only(:_id).map(&:_id)

          fetch_cache_from_db_query @model.includes(:user, :project_name, :performer, :room, :style, :tone, :gamma, :main_colors, :accent_colors, :materials, :images, :max_files).ordered(get_field, get_order).any_of(
                                        {:name => /#{filters['name']}/i},
                                        {:project_name_id.in => category_ids}
                                    ).where(query).page(params[:page]).per(params[:per])
        end
        fetch_from_cached_records(cached_result)
      else
        cached_result = Rails.cache.fetch "#{get_pluralized_model}-where-#{query}-#{get_order}-#{get_field}-#{params[:page]}-#{params[:per]}", :tag => [get_pluralized_model] do
          fetch_cache_from_db_query @model.includes(:user, :project_name, :performer, :room, :style, :tone, :gamma, :main_colors, :accent_colors, :materials, :images, :max_files).ordered(get_field, get_order).where(query).page(params[:page]).per(params[:per])
        end
        fetch_from_cached_records(cached_result)
      end
    end
  end

end
