class Api::CategoriesController < Api::ChildCrudController
  include RegexHelper

  def initialize
    @child_model = Category
    @parent_model = Section
    super
  end


  protected

  def find_child_records
    if params[:filters].nil? || (JSON.parse(params[:filters]) == {})

      if @parent_record == Section.where(:_id => Settings.facilities_manufacturers_section).first
        if params[:disable_pagination] == "true"
          @child_records ||= @parent_record.categories.asc(:name).where(get_search_string)
        else
          @child_records ||= @parent_record.categories.asc(:name).where(get_search_string).page(params[:page]).per(params[:per])
        end
      else
        if params[:disable_pagination] == "true"
          @child_records ||= @parent_record.send("get_ordered_#{get_pluralized_model}").where(get_search_string)
        else
          @child_records ||= @parent_record.send("get_ordered_#{get_pluralized_model}").where(get_search_string).page(params[:page]).per(params[:per])
        end
      end

    else
      filters = JSON.parse(params[:filters])

      query = {}
      query[:name] = alphabetic_regexp(filters['name_start_with']) if (!filters['name_start_with'].nil? && (filters['name_start_with'] != []))
      query[:group_category_ids.in] = filters['group_category_ids'] if (!filters['group_category_ids'].nil? && (filters['group_category_ids'] != []))

      if params[:disable_pagination] == "true"
        @child_records ||= @parent_record.categories.asc(:name).where(query)
      else
        @child_records ||= @parent_record.categories.asc(:name).where(query).page(params[:page]).per(params[:per])
      end
    end
  end

  def get_search_string
    {:name => /^#{params[:q]}/i}
  end

end
