class Api::TranslationsController < Api::BaseCrudController

  def create
    value = params[:value].gsub(/\r\n$/, "")
    I18n.backend.store_translations(params[:locale], {params[:id] => value}, :escape => false)
    respond_with(:api, json: {:locale => params[:locale], :key => params[:id], :value => value})
  end

  def dump
    @translations = get_store.keys.map { |key| [key, Yajl::Parser.parse(TRANSLATION_STORE[key]).first.to_s.gsub(/\r\n$/, "")] if TRANSLATION_STORE[key].class == String }

    to_file = ""

    @translations.each do |key, value|
      to_file += "#{key}#{params[:separator]}#{value}\r\n"
    end

    respond_to do |format|
      format.html { send_data(to_file, :type => 'text/plain; charset=utf8; header=present', :disposition => "attachment; filename=translations_#{@translations.count}.txt") }
    end
  end

  def import

    lines = 0
    params[:files] ||= []
    params[:files].each do |file|
      file.read.split("\r\n").each do |line|
        lines = lines + 1
        slices = line.force_encoding(Encoding::UTF_8).split(params[:separator])
        unless slices[0].nil?
          locale = slices[0].split(".")[0]
          key = slices[0].gsub(/^..\./, "")
          value = slices[1].nil? ? "" : slices[1].gsub(/\r\n$/, "")

          I18n.backend.store_translations(locale, Settings.get_hash_form_key(key, value))
        end
      end
    end

    respond_with(:api, json: {:status => 1, :lines => lines, files: params[:files].count})
  end


  protected

  def get_store
    TRANSLATION_STORE
  end

  def find_records
    @records = []
    return if ((params[:filter_by] == "") || (params[:filter_value] == ""))

    get_store.keys.each do |key|
      value = {key: key.to_s, value: Yajl::Parser.parse(get_store[key]).first.to_s}

      value = nil if ((params[:filter_by] == "key") && !value[:key].include?(params[:filter_value])) # Filter by keys
      value = nil if ((params[:filter_by] == "value") && !value[:value].include?(params[:filter_value])) # Filter by keys

      @records << value unless value.nil?
    end
    @records
  end

  def get_total
    get_store.keys.count
  end

end

