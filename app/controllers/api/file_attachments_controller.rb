class Api::FileAttachmentsController < Api::BaseCrudController

  def initialize
    @model = FileAttachment
    super
  end


  protected

  def create_model!
    super

    @record.task_id = params[:task_id] unless params[:task_id].nil?
  end

end