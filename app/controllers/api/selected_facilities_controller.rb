class Api::SelectedFacilitiesController < Api::BaseCrudController

  def initialize
    @model = SelectedFacility
    super
  end

  def empty
    if (params[:filter_by] == "facilities_set_slug") && (params[:filter_value] != '') && !params[:filter_value].nil?
      search_string = get_search_string

      @model.where(search_string).delete_all unless search_string.nil?
    end

    if (params[:filter_by] == "task_slug") && (params[:filter_value] != '') && !params[:filter_value].nil?
      search_string = get_search_string
      @model.where(search_string).delete_all unless search_string.nil?
    end

    render json: {status: 1}
  end

  protected


  def create_model!
    delete_empty_values

    super()
  end

  def update_model!
    delete_empty_values

    super()
  end

  def delete_empty_values
    [:selected_facility_texture_ids].each do |param|
      params[get_model_sym][param] = [] if params[get_model_sym][param].nil? || (params[get_model_sym][param] == "")
    end
  end

end
