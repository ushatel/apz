class Api::ScenesSetsController < Api::BaseCrudController

  def initialize
    @model = ScenesSet
    super
  end

  def zip
    find_model

    t = Tempfile.new("#{@record.id}-#{request.remote_ip}")

    Zip::ZipOutputStream.open(t.path) do |zos|

      @record.selected_scenes.each_with_index do |selected_scene, index|
        [:images, :max_files].each do |file_part|
          selected_scene.scene.send(file_part).each do |file|
            zos.put_next_entry("#{selected_scene.scene.get_name.gsub(/\//, "_")}/#{File.basename(file.file.path)}")
            zos.print file.file.read
          end
        end

        txt_file = Tempfile.new("scenes-set-#{@record.slug}")
        txt_file.write("#{I18n.t("words.title")}: #{selected_scene.scene.get_name.gsub(/\//, "_")}\r\n")

        #:project_name,
        [:performer, :room, :style, :tone, :gamma].each do |field|
          unless selected_scene.scene.send(field).nil?
            txt_file.write("#{I18n.t("forms.fields.#{field}_id")}: #{selected_scene.scene.send(field).get_name.gsub(/\//, "_")}\r\n")
          end
        end

        [:main_color, :accent_color, :material].each do |field|
          if selected_scene.scene.send("#{field}s").size > 0
            values_array = selected_scene.scene.send("#{field}s").map { |each_field| each_field.get_name.gsub(/\//, "_") }
            txt_file.write("#{I18n.t("forms.fields.#{field}_ids")}: #{values_array.join(", ")}\r\n")
          end
        end

        txt_file.rewind
        zos.put_next_entry("#{selected_scene.scene.get_name.gsub(/\//, "_")}/#{selected_scene.scene.get_name.gsub(/\//, "_")}.txt")
        zos.print txt_file.read
        txt_file.close
        txt_file.unlink
      end
    end

    send_file t.path, :type => 'application/zip', :disposition => 'attachment', :filename => "#{@record.slug}.zip"

    t.close
  end

  def find_model
    @record = @model.find(params[:id]) unless @model.nil?
    render_404 if @record.nil?
  end

  protected

  def get_search_string
    if params[:filter_by] == "user_slug"
      if !current_user.nil? && current_user.can_read_all_tasks?
        {} # show all projects
      else
        user = User.where(:slug => params[:filter_value]).first
        {:user_id => user.id} unless user.nil?
      end
    else
      super
    end
  end

end
