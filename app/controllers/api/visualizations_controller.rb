class Api::VisualizationsController < Api::BaseCrudController
  require 'zip/zip'
  require 'zip/zipfilesystem'

  def initialize
    @model = Visualization
    super
  end

  def zip
    find_model

    t = Tempfile.new("#{@record.slug}-#{request.remote_ip}")

    Zip::ZipOutputStream.open(t.path) do |zos|
      [:furniture_plans, :ceiling_plans, :lighting_plans, :coverings_plans, :coverings_textures, :walls_plans, :walls_textures].each do |file_part|
        @record.send(file_part).each do |file|
          zos.put_next_entry("#{file_part}/#{File.basename(file.file.path)}")
          zos.print file.file.read
        end
      end

      @record.selected_facilities.each_with_index do |selected_facility, index|
        [:images, :max_files].each do |file_part|
          selected_facility.facility.send(file_part).each do |file|
            zos.put_next_entry("selected_facilities/#{index}/#{File.basename(file.file.path)}")
            zos.print file.file.read
          end
        end
      end
    end

    send_file t.path, :type => 'application/zip', :disposition => 'attachment', :filename => "#{@record.slug}.zip"

    t.close
  end

  def sketch
    find_model

    t = Tempfile.new("#{@record.slug}-#{request.remote_ip}")

    Zip::ZipOutputStream.open(t.path) do |zos|

      @record.sketches.where(:type => params[:type]).each_with_index do |sketch, index|
        zos.put_next_entry(File.basename(sketch.file.path)) unless sketch.file.nil?
        zos.print sketch.file.read
      end

    end

    send_file t.path, :type => 'application/zip', :disposition => 'attachment', :filename => "#{@record.slug}-#{params[:type]}.zip"

    t.close

  end

  def order
    find_model

    @record.order()

    respond_with(:api, json: {status: 1})
  end

  def update
    super

    @record.create_update_comment(current_user) if @record.ordered?
  end


  protected

  def create_model!
    delete_empty_values

    ### Project
    project_name = params[:project_name]

    project = Project.where(:name => project_name, :user_id => current_user.id).first

    if project.nil?
      new_project = Project.new(:name => project_name)
      new_project.user = current_user unless current_user.nil?
      new_project.save

      params[get_model_sym][:project_id] = new_project.id
    else
      params[get_model_sym][:project_id] = project.id
    end

    ### Discount
    unless params[:discount].nil? || (params[:discount] == '')
      discount = Discount.where(:code => params[:discount]).first
      params[get_model_sym][:discount_id] = (discount.id unless discount.nil?)
    end

    super()
  end

  def update_model!
    unless params[:discount].nil? || (params[:discount] == '')
      discount = Discount.where(:code => params[:discount]).first
      params[get_model_sym][:discount_id] = (discount.id unless discount.nil?)
    end
    delete_empty_values

    super()
  end

  def delete_empty_values

    [:sketch_ids, :sample_project_style_ids, :furniture_plan_ids, :ceiling_plan_ids, :lighting_plan_ids, :coverings_plan_ids, :walls_plan_ids, :modeling_task_ids].each do |param|
      params[get_model_sym].delete(param) if params[get_model_sym][param].nil? || (params[get_model_sym][param] == "") #|| (params[get_model_sym][param] == 'null')
      params[get_model_sym][param] = JSON.parse(params[get_model_sym][param]) if params[get_model_sym][param].class == String
    end
  end

end
