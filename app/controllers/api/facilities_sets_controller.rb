class Api::FacilitiesSetsController < Api::BaseCrudController

  def initialize
    @model = FacilitiesSet
    super
  end

  def zip
    find_model

    t = Tempfile.new("#{@record.id}-#{request.remote_ip}")

    Zip::ZipOutputStream.open(t.path) do |zos|

      @record.selected_facilities.each_with_index do |selected_facility, index|
        [:images, :max_files].each do |file_part|
          selected_facility.facility.send(file_part).each do |file|
            zos.put_next_entry("#{selected_facility.facility.get_name.gsub(/\//, "_")}/#{File.basename(file.file.path)}")
            zos.print file.file.read
          end
        end

        txt_file = Tempfile.new("facilities-set-#{@record.slug}")
        txt_file.write("#{I18n.t("words.title")}: #{selected_facility.facility.get_name.gsub(/\//, "_")}\r\n")

        unless selected_facility.facility.manufacturer.nil?
          txt_file.write("#{I18n.t("forms.fields.manufacturer_id")}: #{selected_facility.facility.manufacturer.get_name.gsub(/\//, "_")}\r\n")
        end

        unless selected_facility.facility.style.nil?
          txt_file.write("#{I18n.t("forms.fields.style_id")}: #{selected_facility.facility.style.get_name.gsub(/\//, "_")}\r\n")
        end

        unless selected_facility.facility.group.nil?
          txt_file.write("#{I18n.t("forms.fields.group_id")}: #{selected_facility.facility.group.get_name.gsub(/\//, "_")}\r\n")
        end

        txt_file.rewind
        zos.put_next_entry("#{selected_facility.facility.get_name.gsub(/\//, "_")}/#{selected_facility.facility.get_name.gsub(/\//, "_")}.txt")
        zos.print txt_file.read
        txt_file.close
        txt_file.unlink
      end
    end

    send_file t.path, :type => 'application/zip', :disposition => 'attachment', :filename => "#{@record.slug}.zip"

    t.close
  end

  def find_model
    @record = @model.find(params[:id]) unless @model.nil?
    render_404 if @record.nil?
  end

  protected

  def get_search_string
    if params[:filter_by] == "user_slug"
      if !current_user.nil? && current_user.can_read_all_tasks?
        {} # show all projects
      else
        user = User.where(:slug => params[:filter_value]).first
        {:user_id => user.id} unless user.nil?
      end
    else
      super
    end
  end

end
