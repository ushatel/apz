class Api::SectionsController < Api::BaseCrudController

  def initialize
    @model = Section
    @rebased_model = Category
    super
  end

  def root_categories
    find_model
    if params[:disable_pagination] == "true"
      @records = @record.get_root_categories
    else
      @records = @record.get_root_categories.page(params[:page]).per(params[:per])
    end

    respond_with(:api, json: {items: @records, total: @records.count})
  end

end
