class Api::TasksController < Api::BaseCrudController

  def initialize
    @model = Task
    super
  end

  def create
    create_model!
    @record.save

    @record.update_attributes(params[get_model_sym]) unless params[get_model_sym][:file_attachment_ids].nil?

    respond_with(:api, @record)
  end

  def set
    find_model
    @record.set!(params)

    respond_with(:api, json: {status: 1})
  end

  def toggle_selected
    find_model
    if @record.selected?(current_user)
      @record.unselect!(current_user)
    else
      @record.select!(current_user)
    end

    respond_with(:api, json: {status: 1, selected: @record.selected?(current_user)})
  end

  def file_attachments_zip
    find_model

    t = Tempfile.new("#{@record.id}-#{request.remote_ip}")

    Zip::ZipOutputStream.open(t.path) do |zos|
      ### Add attachments
      @record.file_attachments.each do |file_attachment, index|
        zos.put_next_entry(File.basename(file_attachment.file.path))
        zos.print file_attachment.file.read
      end
    end

    send_file t.path, :type => 'application/zip', :disposition => 'attachment', :filename => "#{@record.get_name}-attachments.zip"

    t.close
  end

  def result_file_attachments_zip
    find_model

    t = Tempfile.new("#{@record.id}-#{request.remote_ip}")

    Zip::ZipOutputStream.open(t.path) do |zos|
      ### Add attachments
      @record.result_file_attachments.each do |result_file_attachment, index|
        zos.put_next_entry(File.basename(result_file_attachment.file.path))
        zos.print result_file_attachment.file.read
      end
    end

    send_file t.path, :type => 'application/zip', :disposition => 'attachment', :filename => "#{@record.get_name}-results.zip"

    t.close
  end

  def selected_facilities_zip
    find_model

    t = Tempfile.new("#{@record.id}-#{request.remote_ip}")

    Zip::ZipOutputStream.open(t.path) do |zos|
      ### Add selected facilities
      @record.selected_facilities.each_with_index do |selected_facility, index|
        [:images, :max_files].each do |file_part|
          selected_facility.facility.send(file_part).each do |file|
            zos.put_next_entry("#{selected_facility.facility.get_name.gsub(/\//, "_")}/#{File.basename(file.file.path)}")
            zos.print file.file.read
          end
        end

        selected_facility.selected_facility_textures.each do |selected_facility_texture|
          zos.put_next_entry("#{selected_facility.facility.get_name.gsub(/\//, "_")}/#{I18n.t("words.textures")}/#{File.basename(selected_facility_texture.file.path)}")
          zos.print selected_facility_texture.file.read
        end

        txt_file = Tempfile.new("facilities-set-#{@record.slug}")
        txt_file.write("#{I18n.t("words.title")}: #{selected_facility.facility.get_name.gsub(/\//, "_")}\r\n")

        unless selected_facility.facility.manufacturer.nil?
          txt_file.write("#{I18n.t("forms.fields.manufacturer_id")}: #{selected_facility.facility.manufacturer.get_name.gsub(/\//, "_")}\r\n")
        end

        unless selected_facility.facility.style.nil?
          txt_file.write("#{I18n.t("forms.fields.style_id")}: #{selected_facility.facility.style.get_name.gsub(/\//, "_")}\r\n")
        end

        unless selected_facility.facility.group.nil?
          txt_file.write("#{I18n.t("forms.fields.group_id")}: #{selected_facility.facility.group.get_name.gsub(/\//, "_")}\r\n")
        end

        if !selected_facility.comment.nil? && (selected_facility.comment != '')
          txt_file.write("#{I18n.t("words.comment")}: #{selected_facility.comment.gsub(/\//, "_")}\r\n")
        end

        txt_file.rewind
        zos.put_next_entry("#{selected_facility.facility.get_name.gsub(/\//, "_")}/#{selected_facility.facility.get_name.gsub(/\//, "_")}.txt")
        zos.print txt_file.read
        txt_file.close
        txt_file.unlink
      end
    end

    send_file t.path, :type => 'application/zip', :disposition => 'attachment', :filename => "#{@record.get_name}-facilities.zip"

    t.close
  end

  def selected_scenes_zip
    find_model

    t = Tempfile.new("#{@record.id}-#{request.remote_ip}")

    Zip::ZipOutputStream.open(t.path) do |zos|
      ### Add selected facilities
      @record.selected_scenes.each_with_index do |selected_scene, index|
        [:images, :max_files].each do |file_part|
          selected_scene.scene.send(file_part).each do |file|
            zos.put_next_entry("#{selected_scene.scene.get_name.gsub(/\//, "_")}/#{File.basename(file.file.path)}")
            zos.print file.file.read
          end
        end

        selected_scene.selected_scene_textures.each do |selected_scene_texture|
          zos.put_next_entry("#{selected_scene.scene.get_name.gsub(/\//, "_")}/#{I18n.t("words.textures")}/#{File.basename(selected_scene_texture.file.path)}")
          zos.print selected_scene_texture.file.read
        end

        txt_file = Tempfile.new("scenes-set-#{@record.slug}")
        txt_file.write("#{I18n.t("words.title")}: #{selected_scene.scene.get_name.gsub(/\//, "_")}\r\n")

        #:project_name,
        [:performer, :room, :style, :tone, :gamma].each do |field|
          unless selected_scene.scene.send(field).nil?
            txt_file.write("#{I18n.t("forms.fields.#{field}_id")}: #{selected_scene.scene.send(field).get_name.gsub(/\//, "_")}\r\n")
          end
        end

        [:main_color, :accent_color, :material].each do |field|
          if selected_scene.scene.send("#{field}s").size > 0
            values_array = selected_scene.scene.send("#{field}s").map { |each_field| each_field.get_name.gsub(/\//, "_") }
            txt_file.write("#{I18n.t("forms.fields.#{field}_ids")}: #{values_array.join(", ")}\r\n")
          end
        end

        if !selected_scene.comment.nil? && (selected_scene.comment != '')
          txt_file.write("#{I18n.t("words.comment")}: #{selected_scene.comment.gsub(/\//, "_")}\r\n")
        end

        txt_file.rewind
        zos.put_next_entry("#{selected_scene.scene.get_name.gsub(/\//, "_")}/#{selected_scene.scene.get_name.gsub(/\//, "_")}.txt")
        zos.print txt_file.read
        txt_file.close
        txt_file.unlink
      end
    end

    send_file t.path, :type => 'application/zip', :disposition => 'attachment', :filename => "#{@record.get_name}-scenes.zip"

    t.close
  end

  def zip
    find_model

    t = Tempfile.new("#{@record.id}-#{request.remote_ip}")

    Zip::ZipOutputStream.open(t.path) do |zos|

      ### Add name and description
      txt_file = Tempfile.new("task-#{@record.slug}")
      txt_file.write("#{I18n.t("words.name")}: #{@record.get_name.gsub(/\//, "_")}\r\n")
      txt_file.write("#{I18n.t("words.description")}: #{@record.description.gsub(/\//, "_")}\r\n")
      txt_file.rewind
      zos.put_next_entry("#{@record.get_name.gsub(/\//, "_")}.txt")
      zos.print txt_file.read
      txt_file.close
      txt_file.unlink

      ### Add attachments
      @record.file_attachments.each do |file_attachment, index|
        zos.put_next_entry("#{I18n.t("words.uploaded_files")}/#{File.basename(file_attachment.file.path)}")
        zos.print file_attachment.file.read
      end

      ### Add selected facilities
      @record.selected_facilities.each_with_index do |selected_facility, index|
        [:images, :max_files].each do |file_part|
          selected_facility.facility.send(file_part).each do |file|
            zos.put_next_entry("#{I18n.t("models.facilities.selected")}/#{selected_facility.facility.get_name.gsub(/\//, "_")}/#{File.basename(file.file.path)}")
            zos.print file.file.read
          end
        end

        selected_facility.selected_facility_textures.each do |selected_facility_texture|
          zos.put_next_entry("#{I18n.t("models.facilities.selected")}/#{selected_facility.facility.get_name.gsub(/\//, "_")}/#{I18n.t("words.textures")}/#{File.basename(selected_facility_texture.file.path)}")
          zos.print selected_facility_texture.file.read
        end

        txt_file = Tempfile.new("facilities-set-#{@record.slug}")
        txt_file.write("#{I18n.t("words.title")}: #{selected_facility.facility.get_name.gsub(/\//, "_")}\r\n")

        unless selected_facility.facility.manufacturer.nil?
          txt_file.write("#{I18n.t("forms.fields.manufacturer_id")}: #{selected_facility.facility.manufacturer.get_name.gsub(/\//, "_")}\r\n")
        end

        unless selected_facility.facility.style.nil?
          txt_file.write("#{I18n.t("forms.fields.style_id")}: #{selected_facility.facility.style.get_name.gsub(/\//, "_")}\r\n")
        end

        unless selected_facility.facility.group.nil?
          txt_file.write("#{I18n.t("forms.fields.group_id")}: #{selected_facility.facility.group.get_name.gsub(/\//, "_")}\r\n")
        end

        if !selected_facility.comment.nil? && (selected_facility.comment != '')
          txt_file.write("#{I18n.t("words.comment")}: #{selected_facility.comment.gsub(/\//, "_")}\r\n")
        end

        txt_file.rewind
        zos.put_next_entry("#{I18n.t("models.facilities.selected")}/#{selected_facility.facility.get_name.gsub(/\//, "_")}/#{selected_facility.facility.get_name.gsub(/\//, "_")}.txt")
        zos.print txt_file.read
        txt_file.close
        txt_file.unlink
      end
    end

    send_file t.path, :type => 'application/zip', :disposition => 'attachment', :filename => "#{@record.get_name}.zip"

    t.close
  end


  protected

  def find_records

    if params[:filters].nil? || (JSON.parse(params[:filters]) == {})
      @records = @model.ordered(get_field, get_order).where(get_search_string).page(params[:page]).per(params[:per])
      @records_count = @records.count
      @records_cache_key = @records.cache_key({user_id: current_user.id})
      @records_last_modified =  @records.first.nil? ? Time.now.utc : @records.first.updated_at.utc
    else

      filters = JSON.parse(params[:filters])

      query = {}

      if !filters['favorites'].nil? && filters['favorites']
        query[:_id.in] = current_user.selected_task_ids
      end

      if !filters['name'].nil? && filters['name'] != ''
        query[:name] = /#{filters['name']}/i
      end

      if !filters['implementation_date_start_date'].nil? && (filters['implementation_date_start_date'] != '') && !filters['implementation_date_end_date'].nil? && (filters['implementation_date_end_date'] != '')
        start_day = Date.strptime(filters['implementation_date_start_date'], '%d.%m.%Y')
        end_day = Date.strptime(filters['implementation_date_end_date'], '%d.%m.%Y')

        query[:implementation_date.gte] = start_day
        query[:implementation_date.lte] = end_day
      end

      if !filters['statuses'].nil? && (filters['statuses'] != '') && (filters['statuses'] != [])
        query[:status.in] = filters['statuses']
      end

      if !filters['performer_ids'].nil? && (filters['performer_ids'] != '') && (filters['performer_ids'] != [])
        query[:users_in_discussion_ids.in] = filters['performer_ids']
      end

      if !filters['producer_ids'].nil? && (filters['producer_ids'] != '') && (filters['producer_ids'] != [])
        query[:user_id.in] = filters['producer_ids']
      end

      @records = @model.ordered(get_field, get_order).where(query).where(get_search_string).send(get_order, get_field).page(params[:page]).per(params[:per])
      @records_count = @records.count
      @records_cache_key = @records.cache_key({user_id: current_user.id})
      @records_last_modified =  @records.first.nil? ? Time.now.utc : @records.first.updated_at.utc
    end
  end

  def get_search_string
    if params[:filter_by] == "user_slug"
      if !current_user.nil? && current_user.can_read_all_tasks?
        {} # show all projects
      else
        user = User.where(:slug => params[:filter_value]).first
        {:user_id => user.id} unless user.nil?
      end
    else
      if !current_user.nil? && current_user.client?
        {:user_id => current_user.id}
      else
        super
      end
    end
  end

  def create_model!
    delete_empty_values

    super
  end

  def update_model!
    delete_empty_values

    super()
  end

  def delete_empty_values
    [:file_attachment_ids, :result_file_attachment_ids, :selected_facility_ids, :selected_scene_ids, :result_file_attachment_ids].each do |param|
      params[get_model_sym].delete(param) if params[get_model_sym][param].nil? || (params[get_model_sym][param] == "")
    end
  end

end
