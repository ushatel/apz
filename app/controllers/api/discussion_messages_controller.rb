class Api::DiscussionMessagesController < Api::BaseCrudController

  def initialize
    @model = DiscussionMessage
    super
  end


  protected

  def get_search_string
    if params[:filter_by] == "discussion_id"
      {:discussion_id => params[:filter_value].to_s} unless params[:filter_value].nil?
    else
      super
    end
  end

  def get_order
    return :asc if params[:order].nil?
    super
  end

  def create_model!
    delete_empty_values

    super()
  end

  def update_model!
    delete_empty_values

    super()
  end

  def delete_empty_values
    [:file_attachment_ids].each do |param|
      params[get_model_sym].delete(param) if params[get_model_sym][param].nil? || (params[get_model_sym][param] == "")
    end
  end

end
