class Api::DiscussionsController < Api::ChildCrudController

  def initialize
    @child_model = Discussion
    @parent_model = Task
    super
  end


  protected

  def create_model!
    super

    unless @child_record.nil?
      if !params[get_model_sym][:body].nil? && params[get_model_sym][:body] != ''
        discussion_message = DiscussionMessage.new(body: params[get_model_sym][:body])
        discussion_message.discussion = @child_record
        discussion_message.user = current_user
        discussion_message.save

        discussion_message.update_attributes(file_attachment_ids: params[:file_attachment_ids]) if params[:file_attachment_ids]
      end
    end
  end

  def find_child_records
    db_query = @child_records ||= @parent_record.send("get_ordered_#{get_pluralized_model}").where(get_search_string)
    if params[:disable_pagination] == "true"
      cached_result = Rails.cache.fetch "#{get_pluralized_model}-ordered-#{get_field}-#{get_order}-#{get_search_string}-disable_pagination", :tag => [get_pluralized_model] do
        fetch_cache_from_db_query(db_query)
      end
      fetch_from_cached_records(cached_result)
    else
      cached_result = Rails.cache.fetch "#{get_pluralized_model}-ordered-#{get_field}-#{get_order}-#{get_search_string}-#{params[:page]}-#{params[:per]}", :tag => [get_pluralized_model] do
        fetch_cache_from_db_query(db_query.page(params[:page]).per(params[:per]))
      end
      fetch_from_cached_records(cached_result)
    end
  end

end
