class Api::BaseCrudController < BaseFrontController

  respond_to :json

  before_filter :find_records, :only => [:index]
  before_filter :find_blocked_records, :only => [:blocked]
  before_filter :find_model, :only =>  [:show, :update, :delete, :destroy, :rebuild]
  after_filter :clean_cache!, :only => [:create, :update, :destroy, :rebuild]

  def index

    if stale?(:etag => get_records_cache_key, :last_modified => get_records_last_modified, :public => true)
      respond_with(:api, json: @records, root: "items", meta: get_index_meta)
    end
  end


  def blocked
    respond_with(:api, json: @records, root: "items", meta: {total: get_total})
  end

  def show

    #if stale?(:etag => @record, :last_modified => @record.updated_at.utc, :public => true)
      respond_with(:api, @record)
    #end
  end

  def create
    create_model!
    @record.save

    respond_with(:api, @record)
  end

  # PUT /role/1
  # PUT /role/1.json
  def update
    update_model!

    result = @record.valid? ? {json: @record} : @record
    respond_with(:api, result)
  end

  def delete
    respond_with(:api, @record)
  end

  def destroy
    destroy_model!
    respond_with(:api, json: {status: 1})
  end

  def rebuild
    rebase_models(params[:items])
    expire_fragment "categories_block"

    respond_with(:api, json: {status: 1})
  end

  protected

  def find_records

    db_query = @model.ordered(get_field, get_order).where(get_search_string)
    if params[:disable_pagination] == "true"
      cached_result = Rails.cache.fetch "#{get_pluralized_model}-ordered-#{get_field}-#{get_order}-#{get_search_string}-disable_pagination", :tag => [get_pluralized_model] do
        fetch_cache_from_db_query(db_query)
      end
      fetch_from_cached_records(cached_result)
    else
      cached_result = Rails.cache.fetch "#{get_pluralized_model}-ordered-#{get_field}-#{get_order}-#{get_search_string}-#{params[:page]}-#{params[:per]}", :tag => [get_pluralized_model] do
        fetch_cache_from_db_query(db_query.page(params[:page]).per(params[:per]))
      end
      fetch_from_cached_records(cached_result)
    end
  end

  def find_blocked_records
    @records = @model.blocked(get_field, get_order).where(get_search_string).page(params[:page]).per(params[:per])
  end

  def find_model
    @record = @model.cached_find_by_slug_or_id(params[:id]) unless @model.nil?
    render_404 if @record.nil?
  end

  def get_total
    @records_count || @records.count
  end

  def get_records_cache_key
    @records_cache_key || @records
  end

  def get_records_last_modified
    @records_last_modified || Time.now.utc
  end

  def create_model!
    @record = @model.new(params[get_model_sym])
    @record.user = current_user unless @model.associations["user"].nil?
  end

  def update_model!
    @record.update_attributes(params[get_model_sym])
  end

  def destroy_model!
    @record.destroy
  end

  def get_field
    return :created_at if params[:field].nil?
    @model.fields.include?(params[:field].to_s) ? params[:field].to_sym : :created_at
  end

  def get_order
    return :desc if params[:order].nil?
    [:asc, :desc].include?(params[:order].to_sym) ? params[:order].to_sym : :desc
  end

  def get_index_meta
    {total: get_total}
  end

  def rebase_models(items, root_id = "root", index = 0)
    items.each do |item|
      unless item[1]["id"].nil?
        root = find_rebased_record(item[1]["id"].to_s)
        root.update_attributes(:parent_id => root_id, :order => index)
        index = index + 1
        unless item[1]["children"].nil?
          item[1]["children"].each do |child_item|
            index = index + 1
            rebase_models([child_item], root.id, index)
          end
        end
      end
    end
  end

  def fetch_cache_from_db_query(db_query)
    result = {}
    result[:count] = db_query.count
    result[:cache_key] = db_query.cache_key
    result[:last_modified] = db_query.first.nil? ? Time.now.utc : db_query.first.updated_at.utc
    result[:records] = db_query.to_a
    result
  end

  def fetch_from_cached_records(cached_records)
    cached_records.assert_valid_keys(:records, :count, :cache_key, :last_modified)
    @records = cached_records[:records]
    @records_count = cached_records[:count]
    @records_cache_key = cached_records[:cache_key]
    @records_last_modified = cached_records[:last_modified]
  end

  def find_rebased_record(id)
    @record.send(get_pluralized_class_string(@rebased_model).to_sym).find(id)
  end

  def default_serializer_options
    {root: false}
  end

end