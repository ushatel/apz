class AccountController < AccountBaseCrudController

  def index

    respond_to do |format|
      format.html
    end
  end

  def show
    render action: "index"
  end
end
