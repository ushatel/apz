class FaqsController < BaseCrudController

  def initialize
    @model = Faq
    super
  end

  def index
    @body_class = 'about'
    super
  end


  protected

  def find_records
    @faq_types = %w"visualization design payment drawings"
  end

end
