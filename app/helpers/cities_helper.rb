module CitiesHelper

  def display_cities(cities, viewed = [])
    output = ""
    cities.each do |city|
      unless viewed.include?(city)
        viewed << city
        output += "<li id='city_#{city.id}'>"
        output += "<div class='city-container'>"
        output += render :partial => "/admin/countries/print_city", :locals => {city: city}
        output += "</div>"
        output += "<ol>#{display_cities(city.get_child_cities, viewed)}</ol>"
        output += "</li>"
      end
    end
    output.html_safe
  end

end
