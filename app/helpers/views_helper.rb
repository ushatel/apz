module ViewsHelper
  include ActionView::Helpers::NumberHelper

  def active_li(text, href, options = {})
    if current_page?(href)
      content_tag :li, link_to(text.html_safe, href), :class => "active #{options[:class]}"
    else
      if options[:class].nil?
        return content_tag :li, link_to(text.html_safe, href)
      else
        return content_tag :li, link_to(text.html_safe, href), :class => "#{options[:class]}"
      end
    end
  end

  def safe_html(html, symbols = nil)
    html = html.to_s.gsub(/^<p>\r\n\t&nbsp;<\/p>\r\n/, '')
    elements = {
        :elements => %w(div a span h1 h2 h3 p img ol ul li form input strong table tbody tr td blockquote sub sup strike em hr),
        :attributes => {
            'a' => %w(href title class id style name),
            'span' => %w(class id style),
            'div' => %w(id class data-font data-href data-layout data-send data-show-faces data-width style),
	          'img' => %w(src alt title class id style),
            'ol' => %w(class id style),
            'ul' => %w(class id style),
            'li' => %w(class id style),
            'form' => %w(action class method name target style),
            'input' => %w(class name type value placeholder style),
            'table' => %w(class id style),
            'tbody' => %w(class id style),
            'tr' => %w(class id style),
            'td' => %w(class id style),
            'blockquote' => %w(class id style),
            'sub' => %w(class id style),
            'sup' => %w(class id style),
            'strike' => %w(class id style),
            'em' => %w(class id style),
            'strong' => %w(class id style),
            'hr' => %w(class id style)
        },
        :protocols => {
            'a' => { :href => %w(http https mailto) },
            'form' => { :href => %w(http https) }
        }
    }
    result = html
    link_regexp = /\@link\((.*),\s?(.*)\)/i
    node_id = ""
    result.gsub link_regexp do |match|
      node_id = $2
    end

    unless node_id.nil? || (node_id == "")
      node = Node.where(:_id => node_id).first
      unless node.nil?
        result = result.gsub link_regexp do |match|
          "<a href=\"#{get_absolute_path(polymorphic_path(node))}\">#{$1}</a>"
        end
      end
    end

    if symbols.nil?
      result = Sanitize.clean(html, elements)
    else
      result = Sanitize.clean(html, elements).truncate(symbols.to_i, :omission => "...")
    end
    result = result.gsub /href="([^"]+)"/ do |match|
      path = match.gsub(/^href="([^"]+)"/, $1)
      "href=\"#{get_absolute_path(path)}\""
    end
    result.html_safe
  end

  def print_tabs(tabs, options = {})
    options[:type] ||= "pills"
    output = "<ul class='nav nav-#{options[:type]}'>"
    tabs.each do |tab|
      if !tab[:main].nil? && !tab[:links].nil?
        output += '<li class="dropdown">'
        output +=  "<a class='dropdown-toggle' data-toggle='dropdown' href='#'>#{tab[:main][:text]}<b class='caret'></b></a>"
        output +=  '<ul class="dropdown-menu">'
        tab[:links].each do |link|
          output += "<li><a href='#{link[:href]}'>#{link[:text]}</a></li>"
        end
        output += '</ul>'
        output += '</li>'
      else
        output += active_li(tab[:text], tab[:href])
      end
    end
    output += "</ul>"
    output.html_safe
  end

  def print_secondary_tabs(tabs)
    output = "<div class='btn-group'>"
    tabs.each do |tab|
      #output += "<button class='btn'>"
      output += link_to(tab[:text], tab[:href], :class => "btn")
      #output += "</button>"
    end
    output += "</div>"
    output.html_safe
  end

  def print_budget(sum, currency=nil)
    (sum == BigDecimal.new(0)) ? I18n.t('words.contract_price') :  print_money(sum, currency)
  end

  def print_money(sum, currency=nil)
    real_currency = currency.nil? ? Settings.currency : currency
    if sum.nil?
      number_with_precision(0, :precision => 2).to_s + (" " + real_currency unless currency == :no).to_s
    else
      number_with_precision(sum, :precision => 2).to_s + (" " + real_currency unless currency == :no).to_s
    end
  end

  def print_money_in_html(sum, currency=nil)
    real_currency = currency.nil? ? Settings.currency : currency
    result = ""
    if sum.nil?
      result += "<span class=\"sum\">#{number_with_precision(0, :precision => 2).to_s}</span>#{((" <span class=\"currency\">" + real_currency + "</span>") unless currency == :no).to_s}"
    else
      result += "<span class=\"sum\">#{number_with_precision(sum, :precision => 2).to_s}</span>#{((" <span class=\"currency\">" + real_currency + "</span>") unless currency == :no).to_s}"
    end
    result.html_safe
  end

  def print_full_time(time)
    return nil if time.nil?
    time.strftime(Settings.date_time_format)
  end

  def print_date(date)
    date.strftime(Settings.date_format)
  end

  def print_remain_time(date_time)
    output = ""
    days = (date_time - Time.now).to_i / 83400
    hours = ((date_time - Time.now).to_i / 3600)  - (days * 24)
    output += "#{days} #{I18n.t("letters.d")} " if days > 0
    output += "#{hours.abs} #{I18n.t("letters.ch")}"

    output
  end

  def separate_by_commas(items, print_link = true)
    count = items.count
    output = ""
    items.each_with_index do |item, index|
      if index == (count - 1) # last
        output += print_link ? link_to(item.get_name, item) : item.get_name
      else
        output += print_link ? "#{link_to(item.get_name, item)}, " : "#{item.get_name}, "
      end
    end
    output.html_safe
  end

  def separate(items, separator = " ", print_link = true)
    count = items.count
    output = ""
    items.each_with_index do |item, index|
      if index == (count - 1) # last
        output += print_link ? link_to(item.get_name, item) : item.get_name
      else
        output += print_link ? "#{link_to(item.get_name, item)}#{separator} " : "#{item.get_name}, "
      end
    end
    output.html_safe
  end

  def addresses_path(path)
    root_path
  end

  def print_external_link(link)
    if link =~ /^http:\/\/.+/
      link_to link, link, :target => "_blank"
    else
      link_to link, "http://#{link}", :target => "_blank"
    end
  end

  def get_current_user
    current_user
  end

  def russian_pluralize(n, one, few, many, other = nil)
    n % 10 == 1 && n % 100 != 11 ? one : [2, 3, 4].include?(n % 10) && ![12, 13, 14].include?(n % 100) ? few : n % 10 == 0 || [5, 6, 7, 8, 9].include?(n % 10) || [11, 12, 13, 14].include?(n % 100) ? many : other
  end

end
