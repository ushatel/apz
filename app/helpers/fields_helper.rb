module FieldsHelper

  def get_model_fields(where)
    result = []
    Settings.send("#{get_model_sym}_fields").to_a.sort! { |a, b| a.last["order"].to_i <=> b.last["order"].to_i }.each do |field|
      result << field unless field.last[where].nil?
    end
    result
  end

end
