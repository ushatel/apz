module FaqHelper

  def print_index(index)
    index < 10 ? "0#{index}" : index
  end

end
