module MenusHelper

  def print_menu(menu, options = {})
    output = ""
    output += "<" + options[:menu_left_wrap].to_s unless options[:menu_left_wrap].nil?
    output += " class='#{options[:menu_left_wrap_class]}'" unless options[:menu_left_wrap_class].nil?
    output += " id='#{options[:menu_left_wrap_id]}'" unless options[:menu_left_wrap_id].nil?
    output += ">" unless options[:menu_left_wrap].nil?
    if !menu.get_root_links.nil? && (menu.get_root_links.count > 0)
      output += print_links(menu.get_root_links, options)
    end
    output += options[:menu_right_wrap].to_s unless options[:menu_right_wrap].nil?
    output.html_safe
  end

  def print_links(links, options)
    output = ""
    separator = options[:separator].nil? ? " | " : options[:separator]
    links.each_with_index do |link, index|
      if link.get_child_links.count > 0

        output += options[:left_wrap].to_s unless options[:left_wrap].nil?
        output += print_link(link)

        output += "<#{options[:menu_left_wrap].to_s}>" unless options[:menu_left_wrap].nil?
        options[:separator] = ""
        output += print_links(link.get_child_links, options)
        output += options[:menu_right_wrap].to_s unless options[:menu_right_wrap].nil?

        output += options[:right_wrap].to_s unless options[:right_wrap].nil?

        output += separator unless index == (links.count-1)
      else
        unless options[:left_wrap].nil?
          if request.fullpath == link.get_path
            output += options[:active_left_wrap].to_s unless options[:active_left_wrap].nil?
          else
            output += options[:left_wrap].to_s
          end
        end
        output += print_link(link)
        output += options[:right_wrap].to_s unless options[:right_wrap].nil?

        output += separator unless index == (links.count-1)

      end
    end
    output.html_safe
  end

end
