module UsersHelper

  def online(user)
    user.online? ? "<span class='online'>online</span>".html_safe : "<span class='was-on-site'>#{I18n.t('words.was_on_site')}: #{print_full_time(user.last_sign_in_at)}</span>".html_safe unless user.last_sign_in_at.nil?
  end

end
