module ApplicationHelper

  def get_model_sym
    @model.to_s.underscore.to_sym
  end

  def get_pluralized_class_string(model)
    (model.to_s.split(":")[2]).nil? ? model.to_s.underscore.pluralize : model.to_s.split(":")[2].underscore.pluralize
  end

  def get_class_string(model, is_uppercase = false)
    if model.to_s.split(":")[2].nil?
      (is_uppercase) ? model.to_s : model.to_s.underscore
    else
      (is_uppercase) ? model.to_s.split(":")[2] : model.to_s.split(":")[2].underscore
    end
  end

  def in_admin?
    request.fullpath =~ /^\/admin.+/
  end

  def get_href_namespace(only_frontent = false)
    return nil if only_frontent
    if request.fullpath =~ /^\/admin.+/
      :admin
    else
      nil
    end
  end

  def get_superclasses(model)
    result = []
    unless model.superclass == Object
      result <<  model.superclass
      get_superclasses(model.superclass).each do |class_string|
        result << class_string
      end
    end
    result
  end

end
