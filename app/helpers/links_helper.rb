module LinksHelper

  def display_links(links, viewed = [])
    output = ""
    links.each do |link|
      unless viewed.include?(link)
        viewed << link
        output += "<li id='link_#{link.id}'>"
        output += "<div class='link-container'>"
        output += render :partial => "/admin/menus/print_link", :locals => {link: link}
        output += "</div>"
        output += "<ol>#{display_links(link.get_child_links, viewed)}</ol>"
        output += "</li>"
      end
    end
    output.html_safe
  end

  def print_link(link)
    result = ""
    result += "<noindex>" if link.noindex?
    result += "<a href=\"#{link.get_path}\""
    result += " class=\"#{link.class_string}\"" unless link.class_string == ""
    result += " title=\"#{link.title}\"" unless link.title == ""
    nofollow = link.nofollow? ? " nofollow" : ""
    result += " rel=\"#{link.rel}#{nofollow}\"" unless link.rel == ""
    result += " rel=\"nofollow\"" if link.rel == "" && link.nofollow?

    result += ">"
    result += "#{link.get_name}"
    result += "</a>"
    result += "</noindex>" if link.noindex?
    result#.html_safe
  end

  def get_absolute_path(path)
    return path
    #if Settings.absolute_links == "true"
    #  if path =~ /^http:\/\/#{Settings.hostname}\/.*/
    #    path
    #  else
    #    path =~ /^mailto:.*/ ? path : "http://#{Settings.hostname}#{path}"
    #  end
    ##else
    #  path
    #end
  end

end
