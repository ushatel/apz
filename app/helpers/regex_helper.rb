# encoding: utf-8
module RegexHelper

  def alphabetic_regexp(str)
    if str.to_s =~ /[A-Z]/i # latin alphabet
      /^[#{str}]/i
    elsif str.to_s == "0-9" # only numeric
      /^[0-9]/i
    elsif str.to_s =~ /\p{Cyrillic}/i # cyrillic alphabet
      #/^[\p{Cyrillic}]/i
      /\s#{str}/i
    end
  end

  def name_surname_regexp(str)
    /(^#{str})|(\s#{str})/i
  end

end