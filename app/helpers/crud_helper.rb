module CrudHelper

  def check_crud(model, action)
    if model.respond_to?(action)
      model.send(action)
    else
      true
    end
  end

end
