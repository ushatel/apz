module PaginationHelper

  def showing_from(page_number, per_records)
    page_number = 1 if page_number.to_i == 0

    per_records.to_i * (page_number.to_i - 1) + 1
  end

  def showing_to(page_number, per_records, total)
    page_number = 1 if page_number.to_i == 0

    result = per_records.to_i * page_number.to_i

    (result > total) ? total : result
  end

end
