module TagsHelper

  def print_tags(tags)
    output = ""
    tags.each_with_index do |tag, index|
      if index == (tags.count-1)
        output += link_to tag.get_name, tag
      else
        output += "#{link_to tag.get_name, tag}, "
      end
    end
    output.html_safe
  end

end
