module CategoriesHelper

  def display_categories(categories, viewed = [])
    output = ""
    categories.each do |category|
      unless viewed.include?(category)
        viewed << category
        output += "<li id='category_#{category.id}'>"
        output += "<div class='category-container'>"
        output += render :partial => "/welcome/print_category", :locals => {category: category}
        output += "</div>"
        output += "<ol>#{display_categories(category.get_child_categories, viewed)}</ol>"
        output += "</li>"
      end
    end
    output.html_safe
  end

  def print_categories_js_tree(record, categories, viewed = [])
    output = ""
    output += "<ul>" if categories.count > 0
    categories.each do |category|
      unless viewed.include?(category)
        viewed << category
        output += "<li id='category_#{category.id}' data-model='#{record.class.to_s.underscore}' #{"data-checked=\"true\"" if record.categories.include?(category)}>"
          output += link_to category.get_name, '#' if category.has_children?
          output += link_to category.get_name, [get_href_namespace, category.section, category] unless category.has_children?
          if category.has_children?
            output += print_categories_js_tree(record, category.get_child_categories, viewed)
          end
          output += "</li>"
      end
    end
    output += "</ul>" if categories.count > 0
    output.html_safe
  end

  def print_specializations_js_tree(model, categories, viewed = [])
    output = ""
    output += "<ul>" if categories.count > 0
    categories.each do |category|
      unless viewed.include?(category)
        viewed << category
        output += "<li id='category_#{category.id}' data-model='#{model.class.to_s.downcase}' data-class='#{category.class.to_s.downcase}' data-checked='#{model.specializations.include?(category)}'>"
        output += link_to category.get_name, '#' if category.has_children?
        output += link_to(category.get_name, '#') unless category.has_children?
        #output += link_to category.get_name, '#' if сategory.class == Section
        if category.has_children?
          output += print_specializations_js_tree(model, category.get_child_categories, viewed)
        end
        output += "</li>"
      end
    end
    output += "</ul>" if categories.count > 0
    output.html_safe
  end

  def print_categories_to_accordion(categories)
    output = ""
    rnd = Random.rand(15)
    start_not_children = 0
    end_not_children = 0
    output += "<div class='accordion' id='accordion#{rnd}'>"
      categories.each_with_index do |category, index|
        if category.has_not_children? and category.root?
          if index == 0
            #output += "<ul>"
            start_not_children += 1
          end
          output += "<div class='accordion-group'>"
          output += "<div class='accordion-heading'>"
          output += link_to category.get_name, category, :class => "accordion-toggle"
          output += "</div>"
          output += "</div>"
          if index == (categories.count - 1)
            #output += "</ul>"
            end_not_children += 1
          end
        else
          if category.root?
            output += "<div class='accordion-group'>"
            output += "<div class='accordion-heading'>"
            output += link_to category.get_name, "#collapse-#{category.id}-#{rnd}", :class => 'accordion-toggle', :data => {:toggle => 'collapse',  :parent => "#accordion#{rnd}"}
            output += "</div>"
            output += "<div id='collapse-#{category.id}-#{rnd}' class='accordion-body collapse'>"
            output += "<ul>"
             category.get_child_categories.each do |child_category|
               output += "<li>"
               output += link_to child_category.get_name, child_category
               output += "</li>"
             end
            output += "</ul>"
            output += "</div>"
            output += "</div>"
            end
        end
      end
    output += "</div>"
    output.html_safe
  end

  def  print_categories_to_nested_accordion(categories, accordion_number = 1, used_accordion_numbers = [])
    output = ""
    output += "<div class=\"accordion\" id=\"accordion#{accordion_number}\">"
    categories.each_with_index do |category, index|
      output += '<div class="accordion-group">'
        if category.has_children?
          output += '<div class="accordion-heading">'
          while used_accordion_numbers.include?(accordion_number) do
            accordion_number = accordion_number + 1
          end
          accordion_number_word = I18n.with_locale(:en) { accordion_number.to_words }.gsub("-", "_").camelize
          output += "<a class=\"accordion-toggle collapsed\" data-toggle=\"collapse\" data-href=\"#{polymorphic_path(category)}\" data-parent=\"#accordion#{accordion_number}\" href=\"#collapse#{accordion_number_word}\">#{category.get_name}</a>"
          output += "</div>"
          output += "<div id=\"collapse#{accordion_number_word}\" class=\"accordion-body collapse\">"
          used_accordion_numbers << accordion_number
          accordion_number = accordion_number + 1
          output += "<div class=\"accordion-inner\">"
          output += print_categories_to_nested_accordion(category.get_child_categories, (accordion_number + 1), used_accordion_numbers)
          output += "</div>"
          output += "</div>"
        else
          output += '<div class="accordion-heading">'
          output += link_to(category.get_name, category, :class => "collapsed", :data => {href: polymorphic_path(category)})
          output += "</div>"
        end

      output += "</div>"
    end
    output += "</div>"
    output.html_safe
  end

  def print_breadcrumbs(category)
    result = link_to(I18n.t("words.main"), root_path)

    category.get_parents.reverse_each do |parent_category|
      result += "<span class=\"separator\">&raquo;</span>".html_safe
      result += link_to(parent_category.get_name, parent_category)
    end

    result += "<span class=\"separator\">&raquo;</span>".html_safe
    result += link_to(category.get_name, category)
    result.html_safe
  end

  def render_child_categories(category)
    result = ""
    if category.has_children?
      result += '<div class="child-categories">'
      category.get_child_categories.each_with_index do |child_category, child_category_index|
        result += "<div class=\"child-category child-category-#{child_category_index}\">"
        result += link_to child_category.get_name, child_category
        result += "</div>"
        result += render_child_categories(child_category)
      end

      result += '</div>'
    end
    result.html_safe
  end

end
