module AdminHelper

 def sortable(field, title, model)
   order = params[:order] == "asc" ? "desc" : "asc"
   link_to title, polymorphic_path([get_href_namespace, model], order: order, field: field, page: params[:page]), :remote => true
 end

end
