class Page < Node

  def self.method_missing(method, *args, &block)
    if method.to_s =~ /_page$/
      self.find(Settings.send("#{method.to_s.gsub("get_", "")}_id"))
    else
      self.where(:category_ids => Settings.send("#{method.to_s.gsub("get_", "")}_category")).asc(:created_at)
    end

  end

end
