class Article < Node

  attr_accessible :static_image_id, :slideshow_image_ids

  belongs_to :static_image, touch: true, index: true
  has_many :slideshow_images

  def self.get_last_article
    self.recently_created.first
  end

  def self.method_missing(method, *args, &block)
    if method.to_s =~ /_page$/
      self.find(Settings.send("#{method.to_s.gsub("get_", "")}_id"))
    else
      self.where(:category_ids => Settings.send("#{method.to_s.gsub("get_", "")}_category")).asc(:created_at)
    end

  end

end
