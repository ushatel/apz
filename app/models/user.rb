class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Slugify
  include Kaminari::MongoidExtension::Criteria
  include Kaminari::MongoidExtension::Document
  include ActiveModel::Validations
  include ViewsHelper

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable, :lockable,
         :recoverable, :rememberable, :trackable, :validatable

  ## Database authenticatable
  field :email,              :type => String, :default => ""
  field :encrypted_password, :type => String, :default => ""

  ## Recoverable
  field :reset_password_token,   :type => String
  field :reset_password_sent_at, :type => Time

  ## Rememberable
  field :remember_created_at, :type => Time

  ## Trackable
  field :sign_in_count,      :type => Integer, :default => 0
  field :current_sign_in_at, :type => Time
  field :last_sign_in_at,    :type => Time
  field :current_sign_in_ip, :type => String
  field :last_sign_in_ip,    :type => String
  field :last_request_at,    :type => Time

  ## Confirmable
  field :confirmation_token,   :type => String
  field :confirmed_at,         :type => Time
  field :confirmation_sent_at, :type => Time
  field :unconfirmed_email,    :type => String # Only if using reconfirmable

  ## Lockable
  field :failed_attempts, :type => Integer, :default => 0 # Only if lock strategy is :failed_attempts
  field :unlock_token,    :type => String # Only if unlock strategy is :email or :both
  field :locked_at,       :type => Time

  ## Token authenticatable
  # field :authentication_token, :type => String

  field :login, type: String

  field :name, type: String
  field :surname, type: String
  field :city, type: String
  field :studio, type: String
  field :phone, type: String

  field :views, :type => Integer, :default => 0
  field :blocked, :type => Boolean, :default => false

  ### Associations

  belongs_to :image, index: true
  has_many :nodes, dependent: :delete
  has_many :facilities
  has_many :scenes
  has_many :projects
  has_many :visualizations
  has_many :discussions
  has_many :discussion_messages
  has_many :inbox_messages, :class_name => "Message", inverse_of: :recipient
  has_many :sent_messages, :class_name => "Message", inverse_of: :sender
  has_many :tasks, inverse_of: :user
  belongs_to :manager, :class_name => "User", touch: true, index: true
  # belongs_to :task, :class_name => "Task", touch: true, index: true, inverse_of: :user

  has_and_belongs_to_many :task_discussions, :class_name => "Task", index: true, inverse_of: :users_in_discussions
  has_and_belongs_to_many :selected_tasks, :class_name => "Task", index: true, inverse_of: :users_chosen_tasks
  has_and_belongs_to_many :roles, index: true

  ### #Associations

  attr_accessible :login, :name, :surname, :email, :password, :password_confirmation, :remember_me, :last_request_at, :role_ids, :blocked, :manager_id, :city, :studio, :phone, :image_id

  index({ email: 1 }, { unique: true, background: true })
  index({ created_at: -1 }, {  background: true })

  ### Validations

  validates_presence_of :login, :message => I18n.t('errors.messages.blank')
  validates_presence_of :email, :message => I18n.t('errors.messages.blank')
  validates_uniqueness_of :login, :case_sensitive => false, :message => I18n.t('errors.messages.taken')
  validates_uniqueness_of :email, :case_sensitive => false, :message => I18n.t('errors.messages.taken')
  validates_format_of :email,  :with =>  /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :allow_blank => true, :message => I18n.t('errors.messages.invalid')

  before_validation do |document|
    document.login = document.email.gsub(/[!@#\$%\^\&\*\(\)\[\]\.]/, "-") if document.new_record?
    document.password =  (0...8).map{ ('a'..'z').to_a[rand(26)] }.join if document.new_record?
  end

  ### #Validations

  def self.cache_key(query = {})
    Digest::MD5.hexdigest (where(query).last.nil? ? "#{Time.now.to_i}-#{scoped.count}" : "#{where(query).last.updated_at.to_i}-#{scoped.count}")
  end

  ### Scopes

  def self.recently_created(no_desc = nil)
    query = where(:blocked => false)
    no_desc.nil? ? query.desc(:created_at) : query
  end

  def self.ordered(field = :created_at, order = :desc)
    if order == :desc
      self.recently_created(false).desc(field)
    else
      self.recently_created(false).asc(field)
    end
  end

  def self.blocked(field = :created_at, order = :desc)
    if order == :desc
      self.where(:blocked => true).desc(field)
    else
      self.where(:blocked => true).asc(field)
    end
  end

  def self.get_system_user
    User.where(:_id => '52b83c70b3b53ef850000009').first
  end

  ### Scopes

  def generate_slug
    login.parameterize
  end

  ### Getters

  def get_name
    if self.name.nil? || (self.name == '')
      self.email
    else
      if self.surname.nil? || (self.surname == '')
        "#{self.name} #{self.surname}"
      else
        self.name
      end
    end
  end

  def get_name_and_login
    (self.name.nil? || (self.name == "")) ? login : "#{self.name} [#{self.login}]"
  end

  def get_name_and_email
    "#{self.get_name} - #{self.email}"
  end

  def online?
    if self.last_request_at.nil?
      false
    else
      self.last_request_at > 1.minutes.ago
    end
  end

  def get_inbox_messages
    Message.where(:recipient_id => self.id, :recipient_has_deleted => false).desc(:created_at)
  end

  def get_all_messages
    Message.any_of({:recipient_id => self.id}, {:sender_id => self.id}).desc(:created_at)
  end

  def get_sent_messages
    Message.where(:sender_id => self.id, :sender_has_deleted => false).desc(:created_at)
  end

  ### Getters

  ### Setters

  def block!
    self.blocked = true
    self.save
  end

  def unblock!
    self.blocked = false
    self.save
  end

  ### Setters

  def can_read_all_tasks?
    self.manager? || self.admin?
  end

  def role?(role)
    if self.roles.nil?
      return false
    end
    self.roles.each do |users_role|
      if users_role.name.downcase == role.downcase.to_s
        return true
      end
    end
    false
  end

  def has_roles?(roles)
    Array(roles).each do |each_role|
      return true if self.role?(each_role)
    end
    false
  end

  def client?
    self.roles.size == 0
  end

  ### Caching

  after_create :flush_cache
  after_update :flush_cache
  after_destroy :flush_cache

  def flush_cache
    Cashier.expire self.class.to_s.underscore.pluralize

    Rails.cache.delete ['User', self.id.to_s]
    Rails.cache.delete ['User', self.slug]
  end

  def self.cached_find(id)
    Rails.cache.fetch([name, id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find(id) }
  end

  def self.cached_find_by_slug_or_id(slug_or_id)
    Rails.cache.fetch([name, slug_or_id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find_by_slug_or_id(slug_or_id) }
  end

  def method_missing(method_name, *args, &block)
    if method_name =~ /^cached_(.*)_count$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_count/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_count"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).size }

    elsif method_name =~ /^cached_(.*)_records$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_records$/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_records"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).to_a }

    elsif method_name =~ /^cached_(.*)_record$/
      model_name = method_name.to_s.gsub(/cached_(.*)_record$/, $1)
      Rails.cache.fetch([self, "#{model_name}_record"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(model_name) }

    elsif method_name =~ /^cached_(.*)_ids$/
      model_name = method_name.to_s.gsub(/cached_(.*)_ids$/, $1)
      Rails.cache.fetch([self, "#{model_name}_ids"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_ids").to_a }

    elsif method_name =~ /^cached_(.*)_id$/
      model_name = method_name.to_s.gsub(/cached_(.*)_id$/, $1)
      Rails.cache.fetch([self, "#{model_name}_id"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_id") }

    elsif method_name =~ /.*\?$/
      self.role?(method_name.to_s.gsub('?', ''))
    else
      super
    end
  end

  ### Caching

end
