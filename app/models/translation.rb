class Translation
  include Mongoid::Document
  store_in collection: "i18n"

  default_scope order_by(:locale => :asc, :key => :asc)

  field :value
  field :locale
  field :key

  validates :value, presence: true
  validates :locale, presence: true
  validates :key, presence: true, uniqueness: true
  validates_uniqueness_of :key, :scope => :locale

  attr_accessible :value, :key, :locale
end