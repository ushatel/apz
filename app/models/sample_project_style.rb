class SampleProjectStyle < Image

  belongs_to :visualization, touch: true, index: true

  attr_accessible :visualization_id

  def flush_cache
    super

    unless self.visualization.nil?
      Rails.cache.delete ["Visualization", self.visualization.id]
      Rails.cache.delete ["Visualization", self.visualization.slug]
      self.visualization.save
    end
  end

end
