class Facility
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Slugify
  include Mongoid::Versioning
  include ActiveModel::Serialization
  include ActiveModel::SerializerSupport
  include Kaminari::MongoidExtension::Criteria
  include Kaminari::MongoidExtension::Document

  field :name, type: String

  field :views, :type => Integer, :default => 0
  field :blocked, :type => Boolean, :default => false
  field :used_in_visualizations, :type => Integer, :default => 0

  belongs_to :user, touch: true, index: true
  belongs_to :manufacturer, :class_name => "Category", :inverse_of => :manufacturer_facilities, touch: true, index: true
  belongs_to :style, :class_name => "Category", :inverse_of => :style_facilities, touch: true, index: true
  belongs_to :group, :class_name => "Category", :inverse_of => :group_facilities, touch: true, index: true
  has_and_belongs_to_many :materials, :class_name => "Category", :inverse_of => :material_facilities, index: true
  has_many :images, dependent: :delete
  has_many :max_files, dependent: :delete

  attr_accessible :name, :blocked, :manufacturer_id, :style_id, :group_id, :material_ids, :image_ids, :max_file_ids

  validates_presence_of :name, :message => I18n.t('errors.messages.blank')
  validates_length_of :name, :within => 1..255, :message => I18n.t('errors.messages.too_long.many')
  validate :validate_manufacturer
  validate :validate_style
  validate :validate_group
  validate :validate_materials
  validate :validate_images
  validate :validate_max_files

  index({ name: -1 }, {  background: true })
  index({ created_at: -1 }, {  background: true })

  def self.cache_key(query = {})
    Digest::MD5.hexdigest (where(query).last.nil? ? "#{Time.now.to_i}-#{scoped.count}" : "#{where(query).last.updated_at.to_i}-#{scoped.count}")
  end

  ### Validations

  def validate_manufacturer
    errors.add(:manufacturer_id, I18n.t('errors.messages.blank')) if self.manufacturer.nil?
  end

  def validate_style
    errors.add(:style_id, I18n.t('errors.messages.blank')) if self.style.nil?
  end

  def validate_group
    errors.add(:group_id, I18n.t('errors.messages.blank')) if self.group.nil?
  end

  def validate_materials
    errors.add(:material_ids, I18n.t('errors.messages.blank')) if self.materials.size < 1
  end

  def validate_images
    errors.add(:image_ids, I18n.t('errors.messages.blank')) if self.images.size < 1
  end

  def validate_max_files
    errors.add(:max_file_ids, I18n.t('errors.messages.blank')) if self.max_files.size < 1
  end

  ### Scopes

  scope :top, where(:blocked => false).desc(:created_at)

  def self.recently_created(no_desc = nil)
    query = where(:blocked => false)
    no_desc.nil? ? query.desc(:created_at) : query
  end

  def self.ordered(field = :created_at, order = :desc)
    (order == :desc) ? self.recently_created(false).desc(field) : self.recently_created(false).asc(field)
  end

  def self.blocked(field = :created_at, order = :desc)
    if order == :desc
      self.where(:blocked => true).desc(field)
    else
      self.where(:blocked => true).asc(field)
    end
  end

  ### Scopes

  def generate_slug
    Russian::transliterate(name).parameterize
  end

  def get_name
    self.name
  end

  def get_avatar
    if self.cached_images_count > 0
      self.cached_images_records.first
    else
      Image.find(Settings.get_default_image)
    end
  end

  def add_view
    self.views += 1
    self.save
  end

  ### Can?

  def can_read?(cur_user)
    true
  end

  def self.can_create?(cur_user)
    true
  end

  def can_update?(cur_user)
    true
  end

  def can_destroy?(cur_user)
    true
  end


  ### Can?

  def created_date
    created_at.strftime(Settings.date_format)
  end

  def showing_date
    created_at.strftime(Settings.date_format)
  end

  def block!
    self.blocked = true
    self.save
  end

  def unblock!
    self.blocked = false
    self.save
  end

  def author
    self.user
  end

  ### Caching

  after_create :flush_cache
  after_update :flush_cache
  after_destroy :flush_cache

  def flush_cache
    Cashier.expire self.class.to_s.underscore.pluralize

    Thread.new do
      sleep 5
      CacheWorker.perform_async(self.class.to_s, self.id.to_s)
    end

  end

  def self.cached_find(id)
    Rails.cache.fetch([name, id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find(id) }
  end

  def self.cached_find_by_slug_or_id(slug_or_id)
    Rails.cache.fetch([name, slug_or_id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find_by_slug_or_id(slug_or_id) }
  end

  def method_missing(method_name, *args, &block)
    if method_name =~ /^cached_(.*)_count$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_count/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_count"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).size }

    elsif method_name =~ /^cached_(.*)_records$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_records$/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_records"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).to_a }

    elsif method_name =~ /^cached_(.*)_record$/
      model_name = method_name.to_s.gsub(/cached_(.*)_record$/, $1)
      Rails.cache.fetch([self, "#{model_name}_record"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(model_name) }

    elsif method_name =~ /^cached_(.*)_ids$/
      model_name = method_name.to_s.gsub(/cached_(.*)_ids$/, $1)
      Rails.cache.fetch([self, "#{model_name}_ids"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_ids").to_a }

    elsif method_name =~ /^cached_(.*)_id$/
      model_name = method_name.to_s.gsub(/cached_(.*)_id$/, $1)
      Rails.cache.fetch([self, "#{model_name}_id"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_id") }

    else
      super
    end
  end

  ### Caching

end
