module Modules::CrudModule

  def show
    @record.add_view if @record.respond_to?(:add_view)

    respond_to do |format|
      format.html
    end
  end

  def new
    @record = @model.new

    respond_to do |format|
      format.html
    end
  end

  def create
    create_model!

    respond_to do |format|
      if @record.save
        format.html { redirect_after_create }
      else
        format.html { render action: "new" }
      end
    end
  end

  def edit

    respond_to do |format|
      format.html
    end
  end

  # PUT /role/1
  # PUT /role/1.json
  def update
    respond_to do |format|
      if @record.update_attributes(params[get_model_sym])
        format.html { redirect_to polymorphic_path([get_href_namespace, @record]), notice: I18n.t("models.#{@model.to_s.underscore.pluralize}.updated") }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /roles/1
  # DELETE /roles/1.json
  def delete

    respond_to do |format|
      format.html
      format.json { render json: @record }
      format.xml  { render :xml => @record }
    end
  end


  # DELETE /roles/1
  # DELETE /roles/1.json
  def destroy
    @record.destroy

    respond_to do |format|
      format.html { redirect_to polymorphic_path([get_href_namespace, @model]), notice: I18n.t("models.#{get_pluralized_class_string(@model)}.destroyed") }
      format.json { head :no_content }
      format.xml { head :no_content }
      format.js do
        @records = @model.recently_created.page(params[:page]).per(per_records)
      end
    end
  end

  def develop
    respond_to do |format|
      format.html
    end
  end


  protected

  def find_model
    @record = @model.find_by_slug(params[:id]) unless @model.nil?
    render_404 if @record.nil?
  end

  def create_model!
    @record = @model.new(params[get_model_sym])
    @record.user = current_user unless @model.associations["user"].nil?
  end

  def redirect_after_create
    redirect_to polymorphic_path([get_href_namespace, @record]), notice: I18n.t("models.#{get_pluralized_class_string(@model)}.created")
  end


end