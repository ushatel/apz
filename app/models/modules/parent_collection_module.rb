module Modules::ParentCollectionModule

  # GET /categories
  # GET /categories.json
  def index

    find_records

    respond_to do |format|
      format.html
      format.json { render json: {get_pluralized_class_string(@model).to_sym => @records, :total => @records.count} }
      format.xml { render xml: {get_pluralized_class_string(@model).to_sym => @records, :total => @records.count} }
      format.js
    end
  end


  protected

  def find_records
    @records ||= @parent_record.send(get_pluralized_model).where(get_search_string).desc(:created_at).page(params[:page])
  end

end