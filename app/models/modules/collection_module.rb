module Modules::CollectionModule

  # GET /roles
  # GET /roles.xml
  # GET /roles.json                                       HTML and AJAX
  #-----------------------------------------------------------------------
  def index
    find_records

    respond_to do |format|
      format.html
      format.json { render :json => @records } # {get_pluralized_class_string(@model).to_sym => @records, :total => @records.count}
      format.xml  { render :xml =>  {get_pluralized_class_string(@model).to_sym => @records, :total => @records.count} }
      format.js
    end
  end

  def top

    @records = @model.top.page(params[:page]).per(per_records)

    respond_to do |format|
      format.html
      format.json { render json: {get_pluralized_class_string(@model).to_sym => @records, :total => @records.count} }
      format.xml  { render :xml => {get_pluralized_class_string(@model).to_sym => @records, :total => @records.count} }
      format.js
    end
  end

  def blocked
    @records = @model.blocked.page(params[:page]).per(per_records)

    respond_to do |format|
      format.html
      format.json { render :json =>  {get_pluralized_class_string(@model).to_sym => @records, :total => @records.count} }
      format.xml  { render :xml =>  {get_pluralized_class_string(@model).to_sym => @records, :total => @records.count} }
      format.js
    end
  end

  def fields
    @view_types = Field.get_ordered_grouped_fields(@model)

    respond_to do |format|
      format.html
      format.json { render :json => @settings }
      format.xml  { render :xml => @settings }
    end
  end

  def save_fields

    params[:view_types].each do |view_type_param|
      view_type = view_type_param.first.to_sym
      fields = params[:view_types][view_type][:fields]
      unless fields.nil?
        fields.each do |field|
          field_name = field.first.to_sym

          stored_field = Field.find_or_create_by(:model => @model.to_s, :view_type => view_type, :field_name => field_name)
          stored_field.order = field.last[:order].to_i
          stored_field.visible = !field.last[:visible].nil?
          stored_field.label = field.last[:label]
          stored_field.control_type = field.last[:type]

          stored_field.save
        end
      end
    end

    respond_to do |format|
      format.html { redirect_to polymorphic_path(get_model_path, :action => "fields") }
      format.js
    end
  end

  def up_field
    Field.where(model: @model, view_type: params["view_type"].to_sym, field_name: params["field"].to_sym).first.up_field!

    respond_to do |format|
      format.html { redirect_to polymorphic_path(get_model_path, :action => "fields") }
      format.js
    end

  end

  def down_field
    Field.where(model: @model, view_type: params["view_type"].to_sym, field_name: params["field"].to_sym).first.down_field!

    respond_to do |format|
      format.html { redirect_to polymorphic_path(get_model_path, :action => "fields") }
      format.js
    end
  end


  protected

  def find_settings
    @settings = Settings.send("#{get_model_sym}_fields") if Settings.respond_to?("#{get_model_sym}_fields")
  end

  def find_records
    @records = @model.ordered(get_field, get_order).where(get_search_string).page(params[:page]).per(per_records)
  end

  def per_records
    Settings.per_records.to_i || 10
  end

  def get_field
    return :created_at if params[:field].nil?
    @model.fields.include?(params[:field].to_s) ? params[:field].to_sym : :created_at
  end

  def get_order
    return :desc if params[:order].nil?
    [:asc, :desc].include?(params[:order].to_sym) ? params[:order].to_sym : :desc
  end

end