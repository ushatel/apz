module Modules::ParentCrudModule

  # GET /categories/1
  # GET /categories/1.json
  def show

    respond_to do |format|
      format.html
      format.json { render json: @record }
      format.xml { render xml: @record }
    end
  end

  # GET /categories/new
  # GET /categories/new.json
  def new
    @record = @parent_record.send(get_pluralized_model).new

    respond_to do |format|
      format.html
      format.json { render json: @record }
      format.xml { render xml: @record }
    end
  end

  # GET /categories/1/edit
  def edit

    respond_to do |format|
      format.html
      format.json { render json: @record }
      format.xml  { render :xml => @record }
    end
  end

  # POST /categories
  # POST /categories.json
  def create
    create_model!

    respond_to do |format|
      if @record.save
        format.html { redirect_after_create }
        format.json { render json: @record, status: :created, location: [get_href_namespace, @parent_record, @model] }
      else
        format.html { render action: "new" }
        format.json { render json: @record.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /categories/1
  # PUT /categories/1.json
  def update
    respond_to do |format|
      if @record.update_attributes(params[get_model_sym])
        format.html { redirect_after_update }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @record.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @record.destroy

    respond_to do |format|
      format.html { redirect_after_destroy }
      format.json { head :no_content }
    end
  end

  protected

  def find_model
    @record ||= @model.find_by_slug(params[:id]) unless @model.nil?
    render_404 if @record.nil?
  end

  def find_parent_model
    @parent_record ||= @parent_model.find_by_slug(params["#{get_parent_model}_id".to_sym]) unless @parent_model.nil?
    render_404 if @parent_record.nil?
  end

  def get_parent_model
    @parent_model.to_s.underscore
  end

  def get_pluralized_model
    get_pluralized_class_string(@model)
  end

  def create_model!
    @record = @parent_record.send(get_pluralized_model).new(params[get_model_sym])
    @record.user = current_user unless @model.associations["user"].nil?
  end

  def redirect_after_create
    redirect_to [get_href_namespace, @parent_record, @model], notice: I18n.t("models.#{get_pluralized_model}.created")
  end

  def redirect_after_update
    redirect_to [get_href_namespace, @parent_record], notice: I18n.t("models.#{get_pluralized_model}.updated")
  end

  def redirect_after_destroy
    redirect_to [get_href_namespace, @parent_record], notice: I18n.t(".models.#{get_pluralized_model}.deleted")
  end

end