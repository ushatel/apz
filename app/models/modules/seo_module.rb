module Modules::SeoModule

  def seo

  end

  def save_seo
    params[get_model_sym]["present_in_xml_sitemap"] = params[get_model_sym]["present_in_xml_sitemap"].nil? ? false : params[get_model_sym]["present_in_xml_sitemap"]
    Settings.set_value("#{get_model_sym}_seo_settings", params[get_model_sym])

    redirect_to polymorphic_path([get_href_namespace, @model], :action => "seo"), notice: I18n.t("models.#{get_model_sym}.seo_saved")
  end

  protected

  ### Index

  def get_index_title
    @title = get_seo("index", "title")
  end

  def get_index_keywords
    @seo_keywords = get_seo("index", "keywords")
  end

  def get_index_description
    @seo_description = get_seo("index", "description")
  end

  def get_index_robots
    @seo_robots = get_seo("index", "robots")
  end

  ### Index

  ### Show

  def get_show_title
    @title = get_seo("show", "title", @record)
  end

  def get_show_keywords
    @seo_keywords = get_seo("show", "keywords", @record)
  end

  def get_show_description
    @seo_description = get_seo("show", "description", @record)
  end

  def get_show_robots
    @seo_robots = get_seo("show", "robots", @record)
  end

  ### Show

  ### New

  def get_new_title
    @title = get_seo("new", "title", @record)
  end

  def get_new_keywords
    @seo_keywords = get_seo("new", "keywords", @record)
  end

  def get_new_description
    @seo_description = get_seo("new", "description", @record)
  end

  def get_new_robots
    @seo_robots = get_seo("new", "robots", @record)
  end

  ### New

  ### Edit

  def get_edit_title
    @title = get_seo("edit", "title", @record)
  end

  def get_edit_keywords
    @seo_keywords = get_seo("edit", "keywords", @record)
  end

  def get_edit_description
    @seo_description = get_seo("edit", "description", @record)
  end

  def get_edit_robots
    @seo_robots = get_seo("edit", "robots", @record)
  end

  ### Edit

  ### Delete

  def get_delete_title
    @title = get_seo("delete", "title", @record)
  end

  def get_delete_keywords
    @seo_keywords = get_seo("delete", "keywords", @record)
  end

  def get_delete_description
    @seo_description = get_seo("delete", "description", @record)
  end

  def get_delete_robots
    @seo_robots = get_seo("delete", "robots", @record)
  end

  ### Delete


  private

  def get_seo(action_name, tag, record = nil)
    if record.nil?

    else
      if action_name == "show"
        case tag
          when "title"
            if record.respond_to?(:title)
              return (record.title.nil? || (record.title == "")) ? record.get_name : record.title
            else
              return record.get_name
            end
          when "keywords" then return record.seo_keywords if (record.respond_to?(:seo_keywords) && !(record.seo_keywords.nil? || (record.seo_keywords == "")))
          when "robots" then return record.seo_robots if (record.respond_to?(:seo_robots) && !(record.seo_robots.nil? || (record.seo_robots == "")))
          when "description"
            if record.respond_to?(:seo_description)
              return (record.seo_description.nil? || (record.seo_description == "")) ? record.get_teaser : record.seo_description
            else
              return record.get_name
            end
        end
      end
    end
  end

  def replace_string_from_record(string, record)
    result = string.to_s
    if record.nil?
      result = result.gsub(/@model_name/, I18n.t("models.#{get_pluralized_class_string(@model)}.singular"))
      result = result.gsub(/@models_name/, I18n.t("models.#{get_pluralized_class_string(@model)}.plural"))
      result = result.gsub(/@site_name/, Settings.site_name)
    else
      result = result.gsub(/@model_name/, I18n.t("models.#{get_pluralized_class_string(@model)}.singular"))
      result = result.gsub(/@models_name/, I18n.t("models.#{get_pluralized_class_string(@model)}.plural"))
      result = result.gsub(/@name/, record.get_name)
      result = result.gsub(/@body/, record.snippet.to_s) if record.respond_to?(:snippet)
      result = result.gsub(/@author_name/, record.author.get_name) if (record.respond_to?(:author) && !record.author.nil?)
      result = result.gsub(/@author_login/, record.author.login) if (record.respond_to?(:author) && !record.author.nil?)
      result = result.gsub(/@city/, record.city.get_name) if (record.respond_to?(:city) && !record.city.nil?)
      result = result.gsub(/@country/, record.city.country.get_name) if (record.respond_to?(:city) && !record.city.nil?  && !record.city.country.nil?)
      result = result.gsub(/@site_name/, Settings.site_name)
    end

  end

end