class Category
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Slugify
  include Mongoid::Versioning
  include Kaminari::MongoidExtension::Criteria
  include Kaminari::MongoidExtension::Document
  include ActiveModel::Validations

  field :name, :type => String
  field :title, type: String
  field :teaser, :type => String, :default => ""
  field :body, :type => String, :default => ""
  field :parent_id, :type => String, :default => "root"
  field :order, :type => Integer, :default => 0
  field :seo_keywords, :type => String
  field :seo_description, :type => String
  field :seo_present_in_xml_sitemap, :type => Boolean, :default => true
  field :seo_robots, :type => String, :default => "index, follow"
  field :views, :type => Integer, :default => 0

  index({ created_at: -1 }, {  background: true })

  belongs_to :section, touch: true, index: true
  has_and_belongs_to_many :nodes, index: true
  has_and_belongs_to_many :portfolios, index: true
  has_many :visualization_uses, :class_name => "Visualization", :inverse_of => :use

  has_and_belongs_to_many :group_categories, :class_name => "Category", index: true

  ### Facilities
  has_many :manufacturer_facilities, :class_name => "Facility", :inverse_of => :manufacturer
  belongs_to :style_facilities, :class_name => "Facility", :inverse_of => :style, touch: true, index: true
  belongs_to :group_facilities, :class_name => "Facility", :inverse_of => :group, touch: true, index: true
  has_and_belongs_to_many :material_facilities, :class_name => "Facility", :inverse_of => :materials, index: true

  ### Scenes
  has_many :project_name_scenes, :class_name => "Scene", :inverse_of => :project_name
  has_many :performer_scenes, :class_name => "Scene", :inverse_of => :performer
  belongs_to :room_scenes, :class_name => "Scene", :inverse_of => :room, touch: true, index: true
  belongs_to :style_scenes, :class_name => "Scene", :inverse_of => :style, touch: true, index: true
  belongs_to :tone_scenes, :class_name => "Scene", :inverse_of => :tone, touch: true, index: true
  belongs_to :gamma_scenes, :class_name => "Scene", :inverse_of => :gamma, touch: true, index: true
  has_and_belongs_to_many :main_color_scenes, :class_name => "Scene", :inverse_of => :main_colors, index: true
  has_and_belongs_to_many :accent_color_scenes, :class_name => "Scene", :inverse_of => :accent_colors, index: true
  has_and_belongs_to_many :material_scenes, :class_name => "Scene", :inverse_of => :materials, index: true

  attr_accessible :name, :slug, :order, :parent_id, :section_id, :seo_keywords, :group_category_ids

  validates_presence_of :name, :message => I18n.t('errors.messages.blank')
  validate :validates_uniqueness_of_name

  def validates_uniqueness_of_name
    if (self.new_record?) && (self.section.categories.where(:name => /#{self.name}/i).size > 0)
      errors.add(:name, I18n.t('errors.messages.taken'))
    end
  end

  def self.cache_key(query = {})
    Digest::MD5.hexdigest (where(query).last.nil? ? "#{Time.now.to_i}-#{scoped.count}" : "#{where(query).last.updated_at.to_i}-#{scoped.count}")
  end

  def self.recently_created(no_desc = nil)
    query = where(nil)
    no_desc.nil? ? query.desc(:created_at) : query
  end

  def self.ordered(field = :created_at, order = :desc)
    (order == :desc) ? self.recently_created(false).desc(field) : self.recently_created(false).asc(field)
  end

  def get_name
    self.name unless self.name.nil?
  end

  def get_name_with_count
    "#{self.get_name} (#{self.cached_manufacturer_facilities_count})"
  end

  def get_teaser
    if self.teaser.nil? || (self.teaser == "")
      self.snippet
    else
      self.teaser
    end
  end

  def snippet
    unless body.nil?
      limit = 1000
      if body.mb_chars.length > limit
        while body[limit] != " "
          limit = limit - 1
        end
        Nokogiri::HTML::DocumentFragment.parse(body.truncate(limit, :omission => "...")).to_html
      else
        Nokogiri::HTML::DocumentFragment.parse(body).to_html
      end
    end
  end

  def add_view
    self.views += 1
    self.save
  end

  def generate_slug
    name.parameterize
  end

  def has_children?
    self.get_child_categories.count > 0
  end

  def has_not_children?
    !self.has_children?
  end

  def get_child_categories
    Category.where(:parent_id => self.id).asc(:order)
  end

  def get_all_child_categories
    result = []
    self.get_child_categories.each do |child_category|
      result << child_category.id
      result = result + child_category.get_all_child_categories if child_category.has_children?
    end
    result
  end

  def root?
    self.parent_id == "root"
  end

  def get_parent
    Category.find(self.parent_id) unless self.parent_id.nil?
  end

  def get_parents
    result = []
    unless self.root?
      parent_category = Category.find(self.parent_id)
      result << parent_category
      result = result + parent_category.get_parents
    end
    result
  end

  ### Caching

  after_create :flush_cache
  after_update :flush_cache
  after_destroy :flush_cache

  def flush_cache
    Cashier.expire self.class.to_s.underscore.pluralize
  end

  def self.cached_find(id)
    Rails.cache.fetch([name, id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find(id) }
  end

  def self.cached_find_by_slug_or_id(slug_or_id)
    Rails.cache.fetch([name, slug_or_id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find_by_slug_or_id(slug_or_id) }
  end

  def method_missing(method_name, *args, &block)
    if method_name =~ /^cached_(.*)_count$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_count/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_count"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).size }

    elsif method_name =~ /^cached_(.*)_records$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_records$/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_records"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).to_a }

    elsif method_name =~ /^cached_(.*)_record$/
      model_name = method_name.to_s.gsub(/cached_(.*)_record$/, $1)
      Rails.cache.fetch([self, "#{model_name}_record"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(model_name) }

    elsif method_name =~ /^cached_(.*)_ids$/
      model_name = method_name.to_s.gsub(/cached_(.*)_ids$/, $1)
      Rails.cache.fetch([self, "#{model_name}_ids"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_ids").to_a }

    elsif method_name =~ /^cached_(.*)_id$/
      model_name = method_name.to_s.gsub(/cached_(.*)_id$/, $1)
      Rails.cache.fetch([self, "#{model_name}_id"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_id") }
    else
      super
    end
  end

  ### Caching

end