class FurniturePlan < PlanFile

  belongs_to :visualization, touch: true, index: true

  attr_accessible :visualization_id

  def flush_cache
    super
    Cashier.expire "visualizations"
  end

end
