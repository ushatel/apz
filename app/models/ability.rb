class Ability
  include CanCan::Ability
  
  def initialize(user)
    user ||= User.new # guest user

    user.roles.each do |role|
      role.permissions.each do |permission|
        can permission.action.to_sym, permission.get_subject_class
      end
    end

    unless user.new_record?
      Role.authorized.permissions.each do |permission|
        can permission.action.to_sym, permission.get_subject_class
      end

      can :update, User do |u|
        u == user
      end


    end

    Role.anonymous.permissions.each do |permission|
      can permission.action.to_sym, permission.get_subject_class
    end

  end

end