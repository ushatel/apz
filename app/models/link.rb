class Link
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Slugify
  include Kaminari::MongoidExtension::Criteria
  include Kaminari::MongoidExtension::Document

  field :name, type: String
  field :title, type: String
  field :href, type: String
  field :rel, type: String
  field :class_string, type: String
  field :nofollow, type: Boolean, :default => false
  field :noindex, type: Boolean, :default => false

  field :order, :type => Integer, :default => 0
  field :parent_id, :type => String, :default => "root"

  index({ created_at: -1 }, {  background: true })

  embedded_in :menu

  attr_accessible :name, :href, :title, :class_string, :rel, :nofollow, :noindex, :order, :parent_id
  
  validates_presence_of :name, :message => I18n.t('errors.messages.blank')
  validates_presence_of :href, :message => I18n.t('errors.messages.blank')

  def self.cache_key(query = {})
    Digest::MD5.hexdigest (where(query).last.nil? ? "#{Time.now.to_i}-#{scoped.count}" : "#{where(query).last.updated_at.to_i}-#{scoped.count}")
  end

  def self.recently_created(no_desc = nil)
    query = where(nil)
    no_desc.nil? ? query.desc(:created_at) : query
  end

  def get_name
    self.name
  end

  def generate_slug
    name.parameterize
  end

  def get_path
    return self.href
    #if Settings.absolute_links == "true"
    #  if self.href =~ /^http:\/\/#{Settings.hostname}\/.*/
    #    self.href
    #  else
    #    "http://#{Settings.hostname}#{self.href}"
    #  end
    #else
    #  self.href
    #end
  end

  def noindex?
    self.noindex
  end

  def nofollow?
    self.nofollow
  end

  def has_children?
    self.get_child_links.count > 0
  end

  def has_not_children?
    !self.has_children?
  end

  def get_child_links
    self.menu.links.where(:parent_id => self.id).asc(:order)
  end

  def root?
    self.parent_id == "root"
  end

  ### Caching

  after_create :flush_cache
  after_update :flush_cache
  after_destroy :flush_cache

  def flush_cache
    Cashier.expire self.class.to_s.underscore.pluralize
  end

  def self.cached_find(id)
    Rails.cache.fetch([name, id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find(id) }
  end

  def self.cached_find_by_slug_or_id(slug_or_id)
    Rails.cache.fetch([name, slug_or_id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find_by_slug_or_id(slug_or_id) }
  end

  def method_missing(method_name, *args, &block)
    if method_name =~ /^cached_(.*)_count$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_count/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_count"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).size }

    elsif method_name =~ /^cached_(.*)_records$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_records$/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_records"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).to_a }

    elsif method_name =~ /^cached_(.*)_record$/
      model_name = method_name.to_s.gsub(/cached_(.*)_record$/, $1)
      Rails.cache.fetch([self, "#{model_name}_record"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(model_name) }

    elsif method_name =~ /^cached_(.*)_ids$/
      model_name = method_name.to_s.gsub(/cached_(.*)_ids$/, $1)
      Rails.cache.fetch([self, "#{model_name}_ids"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_ids").to_a }

    elsif method_name =~ /^cached_(.*)_id$/
      model_name = method_name.to_s.gsub(/cached_(.*)_id$/, $1)
      Rails.cache.fetch([self, "#{model_name}_id"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_id") }

    else
      super
    end
  end

  ### Caching

end
