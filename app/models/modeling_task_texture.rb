class ModelingTaskTexture < TextureFile

  belongs_to :modeling_task, touch: true, index: true

  attr_accessible :modeling_task_id

end
