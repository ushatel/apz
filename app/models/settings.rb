class Settings
  include Mongoid::Document
  include Mongoid::Timestamps
  include Kaminari::MongoidExtension::Criteria
  include Kaminari::MongoidExtension::Document
  include Mongoid::AppSettings

  # Main

  setting :site_name
  setting :site_url
  setting :site_email
  setting :date_format
  setting :date_time_format
  setting :per_records

  MAIN_SETTING_KEYS = [:site_name, :site_url, :site_email, :date_format, :date_time_format, :per_records]

  # Other

  setting :notify_delay_time
  setting :main_menu_id
  setting :anonymous_role_id
  setting :authorized_role_id
  setting :manager_role_id
  setting :translations_file_separator

  OTHER_SETTING_KEYS = [:notify_delay_time, :main_menu_id, :anonymous_role_id, :authorized_role_id, :manager_role_id, :translations_file_separator]

  # Structure

  setting :pages_section
  setting :articles_section
  setting :portfolios_section
  setting :facilities_manufacturers_section
  setting :facilities_groups_section
  setting :facilities_styles_section
  setting :facilities_materials_section
  setting :rooms_uses_section

  setting :scenes_performer_section
  setting :scenes_rooms_section
  setting :scenes_styles_section
  setting :scenes_tones_section
  setting :scenes_gammas_section
  setting :scenes_main_colors_section
  setting :scenes_accent_colors_section
  setting :scenes_materials_section
  setting :project_names_section

  STRUCTURE_SETTING_KEYS = [:pages_section, :articles_section, :portfolios_section, :facilities_manufacturers_section, :facilities_groups_section, :facilities_styles_section, :facilities_materials_section, :rooms_uses_section,
                            :scenes_performer_section, :scenes_rooms_section, :scenes_styles_section, :scenes_tones_section, :scenes_gammas_section, :scenes_main_colors_section, :scenes_accent_colors_section, :scenes_materials_section, :project_names_section]

  ### Messages settings

  setting :devise_confirmation_instructions_message
  setting :devise_reset_password_instructions_message
  setting :devise_unlock_instructions_message
  setting :visualization_comment_after_order
  setting :visualization_comment_after_update
  setting :feed_back_topic
  setting :feed_back_message
  setting :task_topic
  setting :task_message
  setting :subscribe_form_topic

  MESSAGES_SETTING_KEYS = [:devise_confirmation_instructions_message,
                           :devise_reset_password_instructions_message,
                           :devise_unlock_instructions_message,
                           :visualization_comment_after_order,
                           :visualization_comment_after_update,
                           :feed_back_topic,
                           :feed_back_message,
                           :subscribe_form_topic,
                           :task_topic,
                           :task_message]

  ### System settings

  setting :default_image_id
  setting :task_statuses

  SYSTEM_SETTING_KEYS = [:task_statuses]

  ### SEO settings

  setting :hostname
  setting :robots_txt
  setting :main_page_title
  setting :main_page_keywords
  setting :main_page_description
  setting :main_page_robots
  setting :main_page_records_count
  setting :portfolio_page_records_count
  setting :google_analytics_id
  setting :google_analytics_host
  setting :yandex_metrika_id
  setting :mailchimp_api_key
  setting :mailchimp_list_id

  setting :base_seo_settings

  SEO_SETTING_KEYS = [:hostname, :main_page_title, :main_page_keywords, :main_page_description, :main_page_robots, :main_page_records_count, :portfolio_page_records_count, :google_analytics_id, :google_analytics_host, :yandex_metrika_id, :mailchimp_api_key, :mailchimp_list_id]


  ### Calc settings

  setting :room_space_less_then
  setting :room_space_less_value
  setting :room_space_above_value

  setting :discount_less_then
  setting :discount_less_value
  setting :discount_above_value

  setting :urgency_1_day
  setting :urgency_2_day
  setting :urgency_3_day
  setting :urgency_4_day

  setting :furniture_plan_cost
  setting :ceiling_plan_cost
  setting :lighting_plan_cost
  setting :coverings_plan_cost
  setting :walls_plan_cost

  setting :selecting_facilities_cost

  setting :adjustments_count_less_then
  setting :adjustments_count_less_value
  setting :adjustments_count_above_value

  setting :modeling_tasks_cost
  setting :high_value_image_cost
  setting :initial_scene_cost
  setting :high_resolution_image_cost
  setting :drawings_cost

  CALC_SETTING_KEYS = [:room_space_less_then, :room_space_less_value, :room_space_above_value, :discount_less_then, :discount_less_value, :discount_above_value,
                       :urgency_1_day, :urgency_2_day, :urgency_3_day, :urgency_4_day,
                       :furniture_plan_cost, :ceiling_plan_cost, :lighting_plan_cost, :coverings_plan_cost, :walls_plan_cost,
                       :selecting_facilities_cost,
                       :adjustments_count_less_then, :adjustments_count_less_value, :adjustments_count_above_value,
                       :modeling_tasks_cost, :high_value_image_cost, :initial_scene_cost, :high_resolution_image_cost, :drawings_cost]

  ### Visualization Labels settings

  setting :projects_index_page_first_label
  setting :visualizations_edit_page_first_label
  setting :visualizations_edit_page_second_label
  setting :furniture_plan_first_label
  setting :ceiling_plan_first_label
  setting :lighting_plan_first_label
  setting :coverings_plan_first_label
  setting :walls_plan_first_label
  setting :selected_facility_settings_label
  setting :add_drawings_info_label
  setting :select_facilities_info_label
  setting :modeling_object_task_lightbox_label
  setting :user_profile_lightbox_label
  setting :task_lightbox_label

  VISUALIZATION_LABELS_SETTING_KEYS = [:projects_index_page_first_label,
                                       :visualizations_edit_page_first_label, :visualizations_edit_page_second_label,
                                       :furniture_plan_first_label, :ceiling_plan_first_label, :lighting_plan_first_label, :coverings_plan_first_label,
                                       :walls_plan_first_label, :add_drawings_info_label, :select_facilities_info_label, :selected_facility_settings_label, :modeling_object_task_lightbox_label,
                                       :user_profile_lightbox_label, :task_lightbox_label]

  def self.set_value(key, value)
    self[key.to_sym] = value
  end

  def self.get_settings(symbol)
    result = {}
    self.all.each do |setting|
      result[setting[0]] = setting[1] if Settings.const_get("#{symbol.upcase}_SETTING_KEYS".upcase).include?(setting[0])
    end
    result
  end

  def self.read_categories_from_file(filename, section)
    root_id = nil
    File.open(filename).each do |line|
      if line[1] == '-'
        category = Category.new(:name => line.gsub(/^--/, ""), :parent_id => root_id)
        category.section = section
        category.save
      else #root
        category = Category.new(:name => line.gsub(/^-/, ""), :parent_id => "root")
        category.section = section
        category.save
        root_id = category.id
      end
    end
  end

  def self.get_default_image
    @default_image ||= begin
        Image.find(Settings.default_image_id) unless Settings.default_image_id.nil?
    end
  end

  def self.get_hash_form_key(key, value, hash={})
    result = hash
    if key.split(".")[1].nil?
      result[key] = value
    else
      if key.split(".")[2].nil?
        result[key.split(".")[0]] = self.get_hash_form_key(key.split(".")[1], value)
      else
        if key.split(".")[3].nil?
          result[key.split(".")[0]] = {}
          result[key.split(".")[0]][key.split(".")[1]] = self.get_hash_form_key(key.split(".")[2], value)
        else
          result[key.split(".")[0]] = {}
          result[key.split(".")[0]][key.split(".")[1]] = {}
          result[key.split(".")[0]][key.split(".")[1]][key.split(".")[2]] = self.get_hash_form_key(key.split(".")[3], value)
        end
      end
    end
    result
  end

  def self.get_hash_form_array(array)
    result = ""
    array.each do |key, value|
      result += self.get_hash_form_key(key, value).to_s
    end
    result
  end

  def self.get_all_as_json()
    Settings.all.keys.map { |key| Settings.json(key) }
  end

  def self.get_part_as_json(part)
    part = "#{part}_SETTING_KEYS".upcase.to_sym
    Settings.constants.include?(part) ? Settings.const_get(part).map { |key| Settings.json(key) } : []
  end

  def self.get_task_statuses
    result = {}
    Settings.task_statuses.split("\n").each_with_index do |task_status, index|
      result[index] = task_status
    end
    result
  end

  def self.json(key)
    {
      id: key,
      key: I18n.t("models.settings.#{key}"),
      value: Settings.send(key)
    }
  end

end