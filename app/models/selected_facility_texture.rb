class SelectedFacilityTexture < TextureFile

  belongs_to :selected_facility, touch: true, index: true

  attr_accessible :selected_facility_id

  def flush_cache
    super
    Cashier.expire "selected_facilities"
  end

end
