class Message
  include Mongoid::Document
  include Mongoid::Timestamps
  include Kaminari::MongoidExtension::Criteria
  include Kaminari::MongoidExtension::Document
  include ViewsHelper
  include ActionView::Helpers::TextHelper
  include ActionView::Helpers::TagHelper

  field :topic, :type => String
  field :body, :type => String
  field :sender_has_deleted, :type => Boolean, :default => false
  field :recipient_has_deleted, :type => Boolean, :default => false
  field :has_read, :type => Boolean, :default => false # Recipient has read message

  index({ created_at: -1 }, {  background: true })

  belongs_to :sender, :class_name => "User", :inverse_of => :inbox_messages, touch: true, index: true
  belongs_to :recipient, :class_name => "User", :inverse_of => :sent_messages, touch: true, index: true

  attr_accessible :topic, :body, :recipient_id, :has_read, :recipient_has_deleted, :sender_has_deleted

  validates_presence_of :body, :message => I18n.t('errors.messages.blank')
  validates_presence_of :sender, :message => I18n.t('errors.messages.blank')
  validates_presence_of :recipient, :message => I18n.t('errors.messages.blank')

  def self.cache_key(query = {})
    Digest::MD5.hexdigest (where(query).last.nil? ? "#{Time.now.to_i}-#{scoped.count}" : "#{where(query).last.updated_at.to_i}-#{scoped.count}")
  end

  def self.recently_created(no_desc = nil)
    query = where(nil)
    no_desc.nil? ? query.desc(:created_at) : query
  end

  def self.ordered(field = :created_at, order = :desc)
    field = :topic if field == :name
    if order == :desc
      self.recently_created(false).desc(field)
    else
      self.recently_created(false).asc(field)
    end
  end

  ### Can?

  def can_update?(cur_user)
    (self.sender == cur_user) || (self.recipient == cur_user)
  end

  def can_destroy?(cur_user)
    false
  end

  ### Can?

  def get_name
    self.topic
  end

  def snippet
    if body.length > 600
      body.truncate(600, :omission => "...") unless body.nil?
    else
      body
    end
  end

  def user=(user)
    self.sender = user
  end

  def self.find_by_slug(slug)
    self.find(slug)
  end

  def sender_delete!
    self.sender_has_deleted = true
    self.save
  end

  def recipient_delete!
    self.recipient_has_deleted = true
    self.save
  end

  def self.get_devise_confirmations_instructions_message(email, link, user)
    message = self.replace_text(Settings.devise_confirmation_instructions_message, email, link)
    message = message.gsub(/@email/, user.email)
    message = message.gsub(/@password/, user.password)
    message.html_safe
  end

  def self.get_devise_reset_password_instructions_message(email, link)
    self.replace_text(Settings.devise_reset_password_instructions_message, email, link)
  end

  def self.get_devise_unlock_instructions_message(email, link)
    self.replace_text(Settings.devise_unlock_instructions_message, email, link)
  end

  def self.get_new_feed_back_message(name, email, body, topic)
    message = Settings.feed_back_message.to_s.gsub(/@name/, name)
    message = message.gsub(/@topic/, topic)
    message = message.gsub(/@email/, email)
    message = message.gsub(/@body/, ApplicationController.helpers.simple_format(body))
    message.html_safe
  end

  def self.get_set_task_message(task)
    message = Settings.task_message.to_s.gsub(/@task_name/, task.get_name)
    message = message.gsub(/@task_description/, task.description)
    message = message.gsub(/@task_zip/, Rails.application.routes.url_helpers.zip_api_task_url(task, host: 'archivizer.com'))

    Rails.logger.info '============================'
    Rails.logger.info '============================'
    Rails.logger.info '============================'
    Rails.logger.info '============================'
    Rails.logger.info '============================'
    Rails.logger.info '============================'
    Rails.logger.info '============================'
    Rails.logger.info '============================'
    Rails.logger.info '============================'
    Rails.logger.info '============================'
    Rails.logger.info '============================'
    Rails.logger.info '============================'
    Rails.logger.info '============================'
    Rails.logger.info '============================'
    Rails.logger.info '============================'
    Rails.logger.info '============================'

    unless task.user.nil?
      message = message.gsub(/@user_name/, task.user.get_name)
      message = message.gsub(/@user_email/, task.user.email)
    end

    message.html_safe
  end

  def read!
    self.has_read = true
    self.save
  end

  ### Caching

  after_create :flush_cache
  after_update :flush_cache
  after_destroy :flush_cache

  def flush_cache
    Cashier.expire self.class.to_s.underscore.pluralize
  end

  def self.cached_find(id)
    Rails.cache.fetch([name, id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find(id) }
  end

  def self.cached_find_by_slug_or_id(slug_or_id)
    Rails.cache.fetch([name, slug_or_id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find_by_slug_or_id(slug_or_id) }
  end

  def method_missing(method_name, *args, &block)
    if method_name =~ /^cached_(.*)_count$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_count/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_count"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).size }

    elsif method_name =~ /^cached_(.*)_records$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_records$/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_records"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).to_a }

    elsif method_name =~ /^cached_(.*)_record$/
      model_name = method_name.to_s.gsub(/cached_(.*)_record$/, $1)
      Rails.cache.fetch([self, "#{model_name}_record"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(model_name) }

    elsif method_name =~ /^cached_(.*)_ids$/
      model_name = method_name.to_s.gsub(/cached_(.*)_ids$/, $1)
      Rails.cache.fetch([self, "#{model_name}_ids"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_ids").to_a }

    elsif method_name =~ /^cached_(.*)_id$/
      model_name = method_name.to_s.gsub(/cached_(.*)_id$/, $1)
      Rails.cache.fetch([self, "#{model_name}_id"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_id") }

    else
      super
    end
  end

  ### Caching

  protected

  def self.replace_text(text, email, link)
    text = text.to_s.gsub(/@email/, email)
    unless text.scan(/@link\((.*)\)/).nil?
      link_text = nil
      link_text = text.scan(/@link\((.*)\)/)[0][0] unless text.scan(/@link\((.*)\)/)[0].nil?
      text = text.gsub(/@link\((.*)\)/, ActionController::Base.helpers.link_to(link_text, link).html_safe) unless link_text.nil?
    end
    text.html_safe
  end

end
