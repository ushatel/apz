class Comment
  include Mongoid::Document
  include Mongoid::Timestamps
  include Kaminari::MongoidExtension::Criteria
  include Kaminari::MongoidExtension::Document

  field :body, type: String

  has_many :file_attachments, dependent: :delete
  belongs_to :visualization, touch: true, index: true
  belongs_to :user, touch: true, index: true

  attr_accessible :body, :visualization_id, :file_attachment_ids

  validates_presence_of :body, :message => I18n.t('errors.messages.blank')
  validates_presence_of :user_id, :message => I18n.t('errors.messages.blank')
  validates_presence_of :visualization_id, :message => I18n.t('errors.messages.blank')

  index({ created_at: -1 }, {  background: true })

  def self.cache_key(query = {})
    Digest::MD5.hexdigest (where(query).last.nil? ? "#{Time.now.to_i}-#{scoped.count}" : "#{where(query).last.updated_at.to_i}-#{scoped.count}")
  end

  def self.recently_created(no_desc = nil)
    query = where(nil)
    no_desc.nil? ? query.desc(:created_at) : query
  end

  def self.ordered(field = :created_at, order = :desc)
    if order == :desc
      self.recently_created(false).desc(field)
    else
      self.recently_created(false).asc(field)
    end
  end

  def self.find_by_slug(slug)
    self.find(slug)
  end

  def self.find_by_slug_or_id(slug)
    self.find(slug)
  end

  def self.create_order_comment(_visualization)
    comment = Comment.new
    ## Send message
    comment.visualization_id = _visualization.id
    comment.user_id = User.get_system_user.id unless User.get_system_user.nil?
    comment.body = Settings.visualization_comment_after_order
    comment.save
  end

  def author
    self.user
  end

  def type
    unless author.nil?
      if author.system?
        return 'system'
      elsif author.manager?
        return 'manager'
      else
        return 'client'
      end
      #%w"system manager content visualizer architect admin".each do |role_name|
      #   return role_name if self.author.send("#{role_name}?")
      #end
    end
  end

  ### Caching

  after_create :flush_cache
  after_update :flush_cache
  after_destroy :flush_cache

  def flush_cache
    Cashier.expire self.class.to_s.underscore.pluralize
  end

  def self.cached_find(id)
    Rails.cache.fetch([name, id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find(id) }
  end

  def self.cached_find_by_slug_or_id(slug_or_id)
    Rails.cache.fetch([name, slug_or_id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find_by_slug_or_id(slug_or_id) }
  end

  def method_missing(method_name, *args, &block)
    if method_name =~ /^cached_(.*)_count$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_count/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_count"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).size }

    elsif method_name =~ /^cached_(.*)_records$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_records$/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_records"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).to_a }

    elsif method_name =~ /^cached_(.*)_record$/
      model_name = method_name.to_s.gsub(/cached_(.*)_record$/, $1)
      Rails.cache.fetch([self, "#{model_name}_record"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(model_name) }

    elsif method_name =~ /^cached_(.*)_ids$/
      model_name = method_name.to_s.gsub(/cached_(.*)_ids$/, $1)
      Rails.cache.fetch([self, "#{model_name}_ids"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_ids").to_a }

    elsif method_name =~ /^cached_(.*)_id$/
      model_name = method_name.to_s.gsub(/cached_(.*)_id$/, $1)
      Rails.cache.fetch([self, "#{model_name}_id"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_id") }
    else
      super
    end
  end

  ### Caching

end
