class SelectedSceneTexture < TextureFile

  belongs_to :selected_scene, touch: true, index: true

  attr_accessible :selected_scene_id

  def flush_cache
    super
    Cashier.expire "selected_scenes"
  end

end
