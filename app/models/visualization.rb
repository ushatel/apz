class Visualization
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Slugify
  include Mongoid::Versioning
  include Kaminari::MongoidExtension::Criteria
  include Kaminari::MongoidExtension::Document

  field :name, type: String, :default => ''
  field :floor_area, type: String, :default => ''
  field :implementation_date, type: Date
  field :furniture_plan_comment, type: String, :default => ''
  field :ceiling_plan_comment, type: String, :default => ''
  field :lighting_plan_comment, type: String, :default => ''
  field :coverings_plan_comment, type: String, :default => ''
  field :walls_plan_comment, type: String, :default => ''
  field :status, type: Integer, :default => 0 # 0 = default, 1 - ordered
  field :adjustments_count, type: Integer, :default => 0
  field :high_resolution_image, type: Boolean, :default => false
  field :initial_scene_with_settings, type: Boolean, :default => false
  field :drawings, type: Boolean, :default => false

  field :views, :type => Integer, :default => 0
  field :blocked, :type => Boolean, :default => false

  belongs_to :user, touch: true, index: true
  belongs_to :project, touch: true, index: true
  belongs_to :use, :class_name => "Category", :inverse_of => :visualization_uses, touch: true, index: true
  belongs_to :discount, touch: true, index: true

  has_many :sample_project_styles, dependent: :delete
  has_many :comments, dependent: :delete
  has_many :furniture_plans, dependent: :delete
  has_many :ceiling_plans, dependent: :delete
  has_many :lighting_plans, dependent: :delete
  has_many :coverings_plans, dependent: :delete
  has_many :coverings_textures, dependent: :delete
  has_many :walls_plans, dependent: :delete
  has_many :walls_textures, dependent: :delete
  has_many :selected_facilities, dependent: :delete
  has_many :modeling_tasks, dependent: :delete
  has_many :sketches, dependent: :delete

  attr_accessible :name, :floor_area, :implementation_date, :views, :project_id, :blocked, :use_id, :status,
                  :furniture_plan_ids, :furniture_plan_comment, :sample_project_style_ids,
                  :ceiling_plan_ids, :ceiling_plan_comment,
                  :lighting_plan_ids, :lighting_plan_comment,
                  :coverings_plan_ids, :coverings_texture_ids, :coverings_plan_comment,
                  :walls_plan_ids, :walls_texture_ids, :walls_plan_comment, :sketch_ids,
                  :adjustments_count, :high_resolution_image, :initial_scene_with_settings, :drawings, :modeling_task_ids, :discount_id

  #validates_presence_of :name, :message => I18n.t('errors.messages.blank')
  #validates_length_of :name, :within => 1..255, :message => I18n.t('errors.messages.too_long.many')

  #validate :validate_sample_project_styles_count

  index({ name: -1 }, {  background: true })
  index({ created_at: -1 }, {  background: true })

  ### Validations

  def validate_sample_project_styles_count
    errors.add(:sample_project_style_ids, I18n.t('errors.messages.blank')) if self.sample_project_styles.size < 1
  end

  ### Validations

  def self.cache_key(query = {})
    Digest::MD5.hexdigest (where(query).last.nil? ? "#{Time.now.to_i}-#{scoped.count}" : "#{where(query).last.updated_at.to_i}-#{scoped.count}")
  end

  ### Scopes

  scope :top, where(:blocked => false).desc(:created_at)

  def self.recently_created(no_desc = nil)
    query = where(:blocked => false)
    no_desc.nil? ? query.desc(:created_at) : query
  end

  def self.ordered(field = :created_at, order = :desc)
    (order == :desc) ? self.recently_created(false).desc(field) : self.recently_created(false).asc(field)
  end

  def self.blocked(field = :created_at, order = :desc)
    if order == :desc
      self.where(:blocked => true).desc(field)
    else
      self.where(:blocked => true).asc(field)
    end
  end

  ### Scopes

  def generate_slug
    if !self.name.nil? && (self.name != '')
      Russian::transliterate(self.name).parameterize
    else
      self.id
    end
  end

  def get_name
    self.name
  end

  def add_view
    self.views += 1
    self.save
  end

  def validate_by_steps
    result =
    {
        "sample_project_styles" => (self.sample_project_styles.size > 0),
        "project" => (!self.project.nil?),
        "use" => (!self.use.nil?),
        "adjustments_count" => ((!self.adjustments_count.nil?) && (self.adjustments_count > 0)),
        "high_resolution_image" => ((!self.high_resolution_image.nil?) && self.high_resolution_image),
        "initial_scene_with_settings" => ((!self.initial_scene_with_settings.nil?) && self.initial_scene_with_settings),
        "drawings" => ((!self.drawings.nil?) && self.drawings),
        "floor_area" => ((!self.floor_area.nil?) && (self.floor_area != '')),
        "implementation_date" => ((!self.implementation_date.nil?) && (self.implementation_date != '')),
        "discount" => (!self.discount.nil?),
        "modeling_tasks" => (self.modeling_tasks.size > 0),
        "create-task" => ((!self.project.nil?) && (self.name != '') && (self.floor_area != '')),
        "furniture-plan" => (self.furniture_plans.size > 0),
        "ceiling-plan" => (self.ceiling_plans.size > 0),
        "lighting-plan" => (self.lighting_plans.size > 0),
        "coverings-plan" => ((self.coverings_plans.size > 0) || (self.coverings_textures.size > 0) || (self.coverings_plan_comment != '')),
        "walls-plan" => ((self.walls_plans.size > 0) || (self.walls_textures.size > 0) || (self.walls_plan_comment != '')),
        "select-content" => self.selected_facilities.size > 0
    }
    result["upload-plans"] = (result["furniture-plan"] && result["ceiling-plan"] && result["lighting-plan"] && result["coverings-plan"] && result["walls-plan"])
    result["all"] = (result["upload-plans"] && result["create-task"] && result["select-content"])
    result
  end

  ### Can?

  def can_read?(cur_user)
    true
  end

  def self.can_create?(cur_user)
    true
  end

  def can_update?(cur_user)
    true
  end

  def can_destroy?(cur_user)
    true
  end


  ### Can?

  def created_date
    created_at.strftime(Settings.date_format)
  end

  def showing_date
    created_at.strftime(Settings.date_format)
  end

  def block!
    self.blocked = true
    self.save
  end

  def unblock!
    self.blocked = false
    self.save
  end

  def author
    self.user
  end

  def ordered?
    self.status == 1
  end

  def order
    unless self.ordered?
      self.status = 1
      self.save
      Comment.create_order_comment(self)
    end
  end

  def create_update_comment(cur_user)
    comment = Comment.new
    comment.user_id = User.get_system_user.id unless User.get_system_user.nil?
    comment.visualization_id = self.id

    comment_body = Settings.visualization_comment_after_update
    comment_body = comment_body.gsub(/@project_name/, self.project.get_name)
    comment_body = comment_body.gsub(/@use_name/, self.use.get_name)
    comment_body = comment_body.gsub(/@implementation_date/, self.implementation_date.strftime(Settings.date_format))
    comment_body = comment_body.gsub(/@floor_area/, self.floor_area)
    comment_body = comment_body.gsub(/@amount/, self.get_order_amount.to_s)

    comment.body = comment_body

    ## Send email
    comment.save
  end

  ### Calculate

  def gn(number)
    number = number.gsub(/,/, '.') if number.class == String
    if (number.class == TrueClass) || (number.class == FalseClass)
      number = number ? 1 : 0
    end
    number.to_f
  end

  def get_room_space_price(attrs = {})
    real_floor_area = gn(attrs[:floor_area] || self.floor_area)
    factor = gn(real_floor_area) <= gn(Settings.room_space_less_then) ? gn(Settings.room_space_less_value) : gn(Settings.room_space_above_value)

    real_floor_area * factor
  end

  def get_discount_price(attrs = {})
    room_space_price = gn(attrs[:room_space_price] || self.get_room_space_price(attrs))

    room_space_price * self.get_discount(attrs)
  end

  def get_discount(attrs = {})
    real_floor_area = gn(attrs[:floor_area] || self.floor_area)

    real_floor_area < gn(Settings.discount_less_then) ? gn(Settings.discount_less_value) : gn(Settings.discount_above_value)
  end

  def get_urgency_price(attrs = {})
    room_space_price = gn(attrs[:room_space_price] || self.get_room_space_price(attrs))
    real_implementation_date = attrs[:implementation_date].nil? ? self.implementation_date : Date.parse(attrs[:implementation_date])

    return 0 if real_implementation_date.nil?

    days = real_implementation_date - Date.today
    urgency_factor = (days >= 1 && days <= 3) ? (gn(Settings.send("urgency_#{days.to_i}_day")) / 100) : 0

    room_space_price * urgency_factor
  end

  def get_coupon_discount_percent(attrs = {})
    value = 0
    value = attrs[:discount].nil? ? (self.discount.percent unless self.discount.nil?) : attrs[:discount]

    gn(value)
  end

  def get_furniture_plan_price(attrs = {})
    real_floor_area = gn(attrs[:floor_area] || self.floor_area)
    value = attrs[:furniture_plan_ids].nil? ? self.furniture_plan_ids.size : attrs[:furniture_plan_ids].size
    value = if value > 0 then 0 else 1 end

    gn(value) * gn(Settings.furniture_plan_cost) * real_floor_area
  end

  def get_ceiling_plan_price(attrs = {})
    real_floor_area = gn(attrs[:floor_area] || self.floor_area)
    value = attrs[:ceiling_plan_ids].nil? ? self.ceiling_plan_ids.size : attrs[:ceiling_plan_ids].size
    value = if value > 0 then 0 else 1 end

    gn(value) * gn(Settings.ceiling_plan_cost) * real_floor_area
  end

  def get_lighting_plan_price(attrs = {})
    real_floor_area = gn(attrs[:floor_area] || self.floor_area)
    value = attrs[:lighting_plan_ids].nil? ? self.lighting_plan_ids.size : attrs[:lighting_plan_ids].size
    value = if value > 0 then 0 else 1 end

    gn(value) * gn(Settings.lighting_plan_cost) * real_floor_area
  end

  def get_coverings_plan_price(attrs = {})
    real_floor_area = gn(attrs[:floor_area] || self.floor_area)
    value = attrs[:coverings_plan_ids].nil? ? self.coverings_plan_ids.size : attrs[:coverings_plan_ids].size
    value = if value > 0 then 0 else 1 end

    gn(value) * gn(Settings.coverings_plan_cost) * real_floor_area
  end

  def get_walls_plan_price(attrs = {})
    real_floor_area = gn(attrs[:floor_area] || self.floor_area)
    value = attrs[:walls_plan_ids].nil? ? self.walls_plan_ids.size : attrs[:walls_plan_ids].size
    value = if value > 0 then 0 else 1 end

    gn(value) * gn(Settings.walls_plan_cost) * real_floor_area
  end

  def get_selected_facilities_price(attrs = {})
    selected_facilities_count =  attrs[:selected_facilities_count].nil? ? self.selected_facility_ids.size : attrs[:selected_facilities_count]
    real_floor_area = gn(attrs[:floor_area] || self.floor_area)
    value = if selected_facilities_count > 0 then 0 else 1 end

    gn(value) * gn(Settings.selecting_facilities_cost) * real_floor_area
  end

  def get_adjusment_count_price(attrs = {})
    real_floor_area = gn(attrs[:floor_area] || self.floor_area)
    value = attrs[:adjustments_count].nil? ? self.adjustments_count : attrs[:adjustments_count]
    factor = if gn(Settings.adjustments_count_less_then) < gn(value) then gn(Settings.adjustments_count_less_value) else gn(Settings.adjustments_count_above_value) end

    gn(value) * gn(factor) * real_floor_area
  end

  def get_modeling_tasks_price(attrs = {})
    value = attrs[:modeling_tasks_count].nil? ? self.modeling_task_ids.size : attrs[:modeling_tasks_count]

    value * gn(Settings.modeling_tasks_cost)
  end

  def get_initial_scene_with_settings_price(attrs = {})
    value = attrs[:initial_scene_with_settings].nil? ? self.initial_scene_with_settings : attrs[:initial_scene_with_settings]
    real_floor_area = gn(attrs[:floor_area] || self.floor_area)

    gn(value) * gn(Settings.initial_scene_cost) * real_floor_area
  end

  def get_high_resolution_image_price(attrs = {})
    value = attrs[:high_resolution_image].nil? ? self.high_resolution_image : attrs[:high_resolution_image]
    real_floor_area = gn(attrs[:floor_area] || self.floor_area)

    gn(value) * gn(Settings.high_resolution_image_cost) * real_floor_area
  end

  def get_drawings_price(attrs = {})
    value = attrs[:drawings].nil? ? self.drawings : attrs[:drawings]
    real_floor_area = gn(attrs[:floor_area] || self.floor_area)

    gn(value) * gn(Settings.drawings_cost) * real_floor_area
  end

  def get_visualization_amount(attrs = {})
    self.get_room_space_price(attrs) - self.get_discount_price(attrs) + self.get_urgency_price(attrs)
  end

  def get_design_amount(attrs = {})
    self.get_furniture_plan_price(attrs) +
    self.get_ceiling_plan_price(attrs) +
    self.get_lighting_plan_price(attrs) +
    self.get_coverings_plan_price(attrs) +
    self.get_walls_plan_price(attrs) +
    self.get_selected_facilities_price(attrs) +
    self.get_adjusment_count_price(attrs)
  end

  def get_additional_options_price(attrs = {})
    self.get_modeling_tasks_price(attrs) +
    self.get_initial_scene_with_settings_price(attrs) +
    self.get_high_resolution_image_price(attrs) +
    self.get_drawings_price(attrs)
  end

  def get_order_amount(attrs = {})
    amount = self.get_visualization_amount(attrs) + self.get_design_amount(attrs) + self.get_additional_options_price(attrs)

    coupon_discount = self.get_coupon_discount_percent(attrs)
    amount = amount - (amount / 100 * coupon_discount).to_f if coupon_discount > 0

    amount.ceil
  end

  ### Calculate

  ### Caching

  after_create :flush_cache
  after_update :flush_cache
  after_destroy :flush_cache

  def flush_cache
    Cashier.expire self.class.to_s.underscore.pluralize

    unless self.project.nil?
      Rails.cache.delete ["Project", self.project.id]
      Rails.cache.delete ["Project", self.project.id.to_s]
      Rails.cache.delete ["Project", self.project.slug.to_s]
      Rails.cache.delete [self.project, 'visualizations_records']
      self.project.save
    end

    #Rails.cache.clear
  end

  def self.cached_find(id)
    Rails.cache.fetch([name, id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find(id) }
  end

  def self.cached_find_by_slug_or_id(slug_or_id)
    Rails.cache.fetch([name, slug_or_id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find_by_slug_or_id(slug_or_id) }
  end

  def method_missing(method_name, *args, &block)
    if method_name =~ /^cached_(.*)_count$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_count/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_count"], :tag => [pluralized_model]) { self.send(pluralized_model).size }

    elsif method_name =~ /^cached_(.*)_records$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_records$/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_records"], :tag => [pluralized_model]) { self.send(pluralized_model).to_a }

    elsif method_name =~ /^cached_(.*)_record$/
      model_name = method_name.to_s.gsub(/cached_(.*)_record$/, $1)
      Rails.cache.fetch([self, "#{model_name}_record"], :tag => [model_name.pluralize]) { self.send(model_name) }

    elsif method_name =~ /^cached_(.*)_ids$/
      model_name = method_name.to_s.gsub(/cached_(.*)_ids$/, $1)
      Rails.cache.fetch([self, "#{model_name}_ids"], :tag => [model_name.pluralize]) { self.send("#{model_name}_ids").to_a }

    elsif method_name =~ /^cached_(.*)_id$/
      model_name = method_name.to_s.gsub(/cached_(.*)_id$/, $1)
      Rails.cache.fetch([self, "#{model_name}_id"], :tag => [model_name.pluralize]) { self.send("#{model_name}_id") }

    elsif method_name =~ /^get_(.*)_section$/
      self.find(Settings.send("#{method_name.to_s.gsub(/get_/, '')}_section"))
    else
      super
    end
  end

  ### Caching

end
