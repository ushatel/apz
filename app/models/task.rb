class Task
  include Mongoid::Document
  include Mongoid::Timestamps
  #include Mongoid::Slugify
  include Mongoid::Versioning
  include Kaminari::MongoidExtension::Criteria
  include Kaminari::MongoidExtension::Document

  field :name, type: String
  field :description, type: String
  field :discount_code, type: String

  field :views, :type => Integer, :default => 0
  field :blocked, :type => Boolean, :default => false

  field :delivered, :type => Boolean, :default => false

  field :floor_area, type: Float
  field :implementation_date, type: Date, :default => (Date.today + 3.days)
  field :status, type: String, :default => 'checking'
  field :rectification_price, type: Money

  field :adjustments_count, type: Integer, :default => 0
  field :modelings_count, type: Integer, :default => 0
  field :high_value_image, type: Integer, :default => 0

  field :initial_scene_with_settings, :type => Boolean, :default => false
  field :drawing_documentation, :type => Boolean, :default => false
  field :furniture_plan, :type => Boolean, :default => false
  field :ceiling_plan, :type => Boolean, :default => false
  field :lighting_plan, :type => Boolean, :default => false
  field :coverings_plan, :type => Boolean, :default => false
  field :walls_plan, :type => Boolean, :default => false
  field :select_facilities, :type => Boolean, :default => false

  field :mark, type: Integer, :default => 0

  belongs_to :user, touch: true, index: true, inverse_of: :tasks
  has_many :selected_facilities
  has_many :selected_scenes
  has_many :file_attachments
  has_many :result_file_attachments
  has_many :discussions

  has_and_belongs_to_many :users_in_discussions, class_name: "User", index: true, inverse_of: :task_discussions
  has_and_belongs_to_many :users_chosen_tasks, class_name: "User", index: true, inverse_of: :selected_tasks

  attr_accessible :name, :blocked, :description, :discount_code, :file_attachment_ids, :delivered,
                  :adjustments_count, :modelings_count, :high_value_image, :mark, :result_file_attachment_ids,
                  :initial_scene_with_settings, :drawing_documentation, :furniture_plan, :ceiling_plan, :lighting_plan, :coverings_plan, :walls_plan, :select_facilities, :selected_scenes,
                  :selected_facility_ids, :selected_scene_ids, :floor_area, :implementation_date, :status, :rectification_price, :users_in_discussion_ids

  validates_presence_of :user, :message => I18n.t('errors.messages.blank')
  #validates_presence_of :name, :message => I18n.t('errors.messages.blank')
  #validates_presence_of :description, :message => I18n.t('errors.messages.blank')
  #validates_length_of :name, :within => 1..255, :message => I18n.t('errors.messages.too_long.many')
  #validate :file_attachments_count

  index({ name: -1 }, {  background: true })
  index({ created_at: -1 }, {  background: true })

  def self.cache_key(query = {})
    Digest::MD5.hexdigest (where(query).last.nil? ? "#{Time.now.to_i}-#{scoped.count}" : "#{where(query).last.updated_at.to_i}-#{scoped.count}")
  end

  ### Scopes

  scope :top, where(:blocked => false).desc(:created_at)

  def self.recently_created(no_desc = nil)
    query = where(:blocked => false, :delivered => true)
    no_desc.nil? ? query.desc(:created_at) : query
  end

  def self.ordered(field = :created_at, order = :desc)
    (order == :desc) ? self.recently_created(false).desc(field) : self.recently_created(false).asc(field)
  end

  def self.blocked(field = :created_at, order = :desc)
    if order == :desc
      self.where(:blocked => true).desc(field)
    else
      self.where(:blocked => true).asc(field)
    end
  end

  ### Scopes

  def file_attachments_count
    errors.add(:file_attachment_ids, I18n.t('errors.messages.blank')) if self.file_attachments.size < 1
  end

  def generate_slug
    Russian::transliterate(name).parameterize
  end

  def slug
    self.id
  end

  def get_name
    self.name == '' ? I18n.t('words.no_name') : self.name
  end

  def add_view
    self.views += 1
    self.save
  end

  def created_date
    created_at.strftime(Settings.date_format)
  end

  def showing_date
    created_at.strftime(Settings.date_format)
  end

  def block!
    self.blocked = true
    self.save
  end

  def unblock!
    self.blocked = false
    self.save
  end

  def author
    self.user
  end

  def get_file_attachment_ids
    self.file_attachments.where(:_type.ne => 'ResultFileAttachment').only(:_id).map(&:_id)
  end

  def get_file_attachments
    self.file_attachments.where(:_type.ne => 'ResultFileAttachment')
  end

  def set!(attrs = {})
    attrs = {delivered: true}.merge(attrs)
    self.update_attributes(attrs)
    UserMailer.set_task(self).deliver
  end

  def get_ordered_discussions
    self.discussions.desc(:created_at)
  end

  def get_hours_sum
    hours = 0
    minutes = 0
    self.discussions.each do |discussion|
      discussion.discussion_messages.each do |discussion_message|
        hours += discussion_message.hours.to_i
        minutes += discussion_message.minutes.to_i
      end
    end

    hours_in_minutes = (minutes / 60)
    hours_result = hours + hours_in_minutes
    minutes_result = minutes - (hours_in_minutes * 60)

    {hours: hours_result, minutes: minutes_result}
  end

  def get_users_in_messages
    user_ids = []
    self.discussions.each do |discussion|
      discussion.discussion_messages.each do |discussion_message|
        user_ids << discussion_message.user.id unless discussion_message.user.nil?
      end
    end
    User.where(:_id.in => user_ids.uniq) #.only(:_id, :name, :image)
  end

  def selected?(cur_user)
    cur_user.selected_tasks.include?(self) unless cur_user.nil?
  end

  def select!(cur_user)
    unless cur_user.nil?
      cur_user.selected_tasks << self unless self.selected?(cur_user)
      cur_user.save
    end
  end

  def unselect!(cur_user)
    unless cur_user.nil?
      cur_user.selected_tasks.delete(self)
    end
  end

  ### Caching

  after_create :flush_cache
  after_update :flush_cache
  after_destroy :flush_cache

  def flush_cache
    Cashier.expire self.class.to_s.underscore.pluralize
    Cashier.expire 'selected_facility'
    Cashier.expire 'selected_facilities'
    Cashier.expire 'selected_scene'
    Cashier.expire 'selected_scenes'
  end

  def self.cached_find(id)
    Rails.cache.fetch([name, id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find(id) }
  end

  def self.cached_find_by_slug_or_id(slug_or_id)
    Rails.cache.fetch([name, slug_or_id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find(slug_or_id) }
  end

  def method_missing(method_name, *args, &block)
    if method_name =~ /^cached_(.*)_count$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_count/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_count"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).size }

    elsif method_name =~ /^cached_(.*)_records$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_records$/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_records"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).to_a }

    elsif method_name =~ /^cached_(.*)_record$/
      model_name = method_name.to_s.gsub(/cached_(.*)_record$/, $1)
      Rails.cache.fetch([self, "#{model_name}_record"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(model_name) }

    elsif method_name =~ /^cached_(.*)_ids$/
      model_name = method_name.to_s.gsub(/cached_(.*)_ids$/, $1)
      Rails.cache.fetch([self, "#{model_name}_ids"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_ids").to_a }

    elsif method_name =~ /^cached_(.*)_id$/
      model_name = method_name.to_s.gsub(/cached_(.*)_id$/, $1)
      Rails.cache.fetch([self, "#{model_name}_id"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_id") }
    else
      super
    end
  end

  ### Caching

end
