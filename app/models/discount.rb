class Discount
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Slugify
  include Kaminari::MongoidExtension::Criteria
  include Kaminari::MongoidExtension::Document

  field :code, type: String
  field :percent, type: Integer

  index({ created_at: -1 }, {  background: true })

  belongs_to :visualization, touch: true, index: true

  attr_accessible :code, :percent

  validates_presence_of :code, :message => I18n.t('errors.messages.blank')
  validates_presence_of :percent, :message => I18n.t('errors.messages.blank')
  validates_uniqueness_of :code, :case_sensitive => false, :message => I18n.t('errors.messages.taken')
  validates_format_of :code,  :with =>  /^[a-zA-Z0-9_]+$/, :message => I18n.t('errors.messages.invalid')
  validates_format_of :percent,  :with =>  /^[^0].*$/, :message => I18n.t('errors.messages.invalid')
  validate :percent_value

  def percent_value
    if (self.percent < 0) || (self.percent > 100)
      errors.add(:percent, I18n.t('errors.messages.invalid'))
    end
  end

  def self.cache_key(query = {})
    Digest::MD5.hexdigest (where(query).last.nil? ? "#{Time.now.to_i}-#{scoped.count}" : "#{where(query).last.updated_at.to_i}-#{scoped.count}")
  end

  def self.recently_created(no_desc = nil)
    query = where(nil)
    no_desc.nil? ? query.desc(:created_at) : query
  end

  def self.ordered(field = :created_at, order = :desc)
    if order == :desc
      self.recently_created(false).desc(field)
    else
      self.recently_created(false).asc(field)
    end
  end

  def get_name
    self.code
  end

  def generate_slug
    code.parameterize
  end

  ### Caching

  after_create :flush_cache
  after_update :flush_cache
  after_destroy :flush_cache

  def flush_cache
    Cashier.expire self.class.to_s.underscore.pluralize
  end

  def self.cached_find(id)
    Rails.cache.fetch([name, id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find(id) }
  end

  def self.cached_find_by_slug_or_id(slug_or_id)
    Rails.cache.fetch([name, slug_or_id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find_by_slug_or_id(slug_or_id) }
  end

  def self.cached_find_by_code(code)
    Rails.cache.fetch([name, code], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { where(:code => code).first }
  end

  def method_missing(method_name, *args, &block)
    if method_name =~ /^cached_(.*)_count$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_count/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_count"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).size }

    elsif method_name =~ /^cached_(.*)_records$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_records$/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_records"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).to_a }

    elsif method_name =~ /^cached_(.*)_record$/
      model_name = method_name.to_s.gsub(/cached_(.*)_record$/, $1)
      Rails.cache.fetch([self, "#{model_name}_record"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(model_name) }

    elsif method_name =~ /^cached_(.*)_ids$/
      model_name = method_name.to_s.gsub(/cached_(.*)_ids$/, $1)
      Rails.cache.fetch([self, "#{model_name}_ids"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_ids").to_a }

    elsif method_name =~ /^cached_(.*)_id$/
      model_name = method_name.to_s.gsub(/cached_(.*)_id$/, $1)
      Rails.cache.fetch([self, "#{model_name}_id"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_id") }

    else
      super
    end
  end

  ### Caching

end
