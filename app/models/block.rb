class Block
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Slugify
  include Kaminari::MongoidExtension::Criteria
  include Kaminari::MongoidExtension::Document

  REGIONS = [:logo, :header, :left_sidebar, :right_sidebar, :top_footer, :left_footer, :right_footer, :bottom_header_left, :bottom_header_right]

  TYPES = [:html, :list, :login, :section]

  VIEW_MODES = [:on_these_pages, :exclude_these_pages]

  field :name, type: String
  field :title, type: String
  field :value, type: String
  field :type, type: String
  field :region, type: Symbol
  field :order, type: Integer, default: 0
  field :view_mode, type: Symbol
  field :view_rules, type: String
  field :blocked, :type => Boolean, :default => false

  index({ created_at: -1 }, {  background: true })

  attr_accessible :name, :slug, :title, :value, :region, :blocked, :type, :order, :view_mode, :view_rules

  validates_presence_of :name, :message => I18n.t('errors.messages.blank')
  validates_uniqueness_of :name, :case_sensitive => false, :message => I18n.t('errors.messages.taken')

  def self.cache_key(query = {})
    Digest::MD5.hexdigest (where(query).last.nil? ? "#{Time.now.to_i}-#{scoped.count}" : "#{where(query).last.updated_at.to_i}-#{scoped.count}")
  end

  def self.recently_created(no_desc = nil)
    query = where(:blocked => false)
    no_desc.nil? ? query.desc(:created_at) : query
  end

  def self.ordered(field = :created_at, order = :desc)
    if order == :desc
      self.recently_created(false).desc(field)
    else
      self.recently_created(false).asc(field)
    end
  end

  def self.blocked(field = :created_at, order = :desc)
    if order == :desc
      self.where(:blocked => true).desc(field)
    else
      self.where(:blocked => true).asc(field)
    end
  end

  def get_name
    self.name
  end

  def generate_slug
    name.parameterize
  end

  def self.method_missing(method, *args, &block)
    self.class.find(Settings.send("#{method.to_s.gsub(/^get_/, "")}_block_id"))
  end

  def self.get_types
    self.const_get(:TYPES).map { |type| [I18n.t("models.blocks.types.#{type}"), type] }
  end

  def self.get_types_in_json
    result = "["
    self.const_get(:TYPES).each_with_index do |type, index|
      result += "{val:'#{type}', label: '#{I18n.t("models.blocks.types.#{type}")}'}"
      result += "," unless ((index-1) == self.const_get(:TYPES).count)
    end
    result += "]"
    result
  end

  def self.get_regions
    self.const_get(:REGIONS).map { |region| [region.capitalize, region] }
  end

  def self.get_regions_in_json
    result = "["
    self.const_get(:REGIONS).each_with_index do |region, index|
      result += "{val:'#{region}', label: '#{region.capitalize}'}"
      result += "," if ((index+1) != self.const_get(:REGIONS).count)
    end
    result += "]"
    result
  end

  def self.get_view_modes
    self.const_get(:VIEW_MODES).map { |view_mode| [I18n.t("models.blocks.view_types.#{view_mode}"), view_mode] }
  end

  def self.get_view_modes_in_json
    result = "["
    self.const_get(:VIEW_MODES).each_with_index do |view_mode, index|
      result += "{val:'#{view_mode}', label: '#{I18n.t("models.blocks.view_types.#{view_mode}")}'}"
      result += "," unless ((index-1) == self.const_get(:VIEW_MODES).count)
    end
    result += "]"
    result
  end

  def self.get_blocks_in_region(region, blocked = nil)
    if blocked.nil?
      self.where(:region => region.to_sym, :blocked => false).asc(:order)
    elsif blocked == :all
      self.where(:region => region.to_sym).asc(:order)
    else
      self.where(:region => region.to_sym, :blocked => blocked).asc(:order)
    end

  end

  def view?(values = {})
    return true if (self.view_rules.nil? || (self.view_rules == ''))

    rules = self.view_rules.split("\r\n")
    result = self.view_mode == self.class.const_get(:VIEW_MODES).first ? true : false

    rules.each do |rule|

      if rule =~ /[a-zA-Z_-]+#[a-zA-Z_-]+$/ # Checking rule by controller and action
        unless values[:controller].nil? && values[:action].nil?
          if (rule.split("#").first == values[:controller]) && (rule.split("#").last == values[:action])
            return result
          end
        end
      elsif rule =~ /[a-zA-Z_-]+#[a-zA-Z_-]+=.+/ # Checking rule by controller, action and id
        if !values[:controller].nil? && !values[:action].nil? && !values[:id].nil?
          if (rule.split("#").first == values[:controller]) && (rule.split("#").last.split("=").first == values[:action]) && (rule.split("#").last.split("=").last == values[:id])
            return result
          end
        end
      else # Checking rule by url
        if rule == values[:url]
          return result
        end
      end

    end
    !result
  end

  ### Caching

  after_create :flush_cache
  after_update :flush_cache
  after_destroy :flush_cache

  def flush_cache
    Cashier.expire self.class.to_s.underscore.pluralize
  end

  def self.cached_find(id)
    Rails.cache.fetch([name, id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find(id) }
  end

  def self.cached_find_by_slug_or_id(slug_or_id)
    Rails.cache.fetch([name, slug_or_id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find_by_slug_or_id(slug_or_id) }
  end

  def method_missing(method_name, *args, &block)
    if method_name =~ /^cached_(.*)_count$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_count/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_count"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).size }

    elsif method_name =~ /^cached_(.*)_records$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_records$/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_records"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).to_a }

    elsif method_name =~ /^cached_(.*)_record$/
      model_name = method_name.to_s.gsub(/cached_(.*)_record$/, $1)
      Rails.cache.fetch([self, "#{model_name}_record"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(model_name) }

    elsif method_name =~ /^cached_(.*)_ids$/
      model_name = method_name.to_s.gsub(/cached_(.*)_ids$/, $1)
      Rails.cache.fetch([self, "#{model_name}_ids"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_ids").to_a }

    elsif method_name =~ /^cached_(.*)_id$/
      model_name = method_name.to_s.gsub(/cached_(.*)_id$/, $1)
      Rails.cache.fetch([self, "#{model_name}_id"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_id") }

    else
      super
    end
  end

  ### Caching

end
