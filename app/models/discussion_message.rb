class DiscussionMessage
  include Mongoid::Document
  include Mongoid::Timestamps
  include Kaminari::MongoidExtension::Criteria
  include Kaminari::MongoidExtension::Document
  include ViewsHelper
  include ActionView::Helpers::TextHelper
  include ActionView::Helpers::TagHelper

  field :body, :type => String
  field :hours, :type => Integer
  field :minutes, :type => Integer

  index({ created_at: -1 }, {  background: true })

  belongs_to :discussion, touch: true, index: true
  belongs_to :user, touch: true, index: true

  has_many :file_attachments

  attr_accessible :body, :hours, :minutes, :discussion_id, :user_id, :file_attachment_ids

  validates_presence_of :body, :message => I18n.t('errors.messages.blank')
  validates_presence_of :user, :message => I18n.t('errors.messages.blank')
  validates_presence_of :discussion, :message => I18n.t('errors.messages.blank')
  validates_format_of :hours,  :with =>  /^\d+$/i, :allow_blank => true, :message => I18n.t('errors.messages.invalid')
  validates_format_of :minutes,  :with =>  /^\d+$/i, :allow_blank => true, :message => I18n.t('errors.messages.invalid')
  after_save :add_user_to_task
  after_save :send_message

  def self.find_by_slug_or_id(slug_or_id)
    self.find(slug_or_id)
  end

  def self.find_by_slug(slug)
    self.find(slug)
  end

  def self.cache_key(query = {})
    Digest::MD5.hexdigest (where(query).last.nil? ? "#{Time.now.to_i}-#{scoped.count}" : "#{where(query).last.updated_at.to_i}-#{scoped.count}")
  end

  def self.recently_created(no_desc = nil)
    query = where(nil)
    no_desc.nil? ? query.desc(:created_at) : query
  end

  def self.ordered(field = :created_at, order = :asc)
    field = :topic if field == :name
    if order == :desc
      self.recently_created(false).desc(field)
    else
      self.recently_created(false).asc(field)
    end
  end

  def send_message

  end

  def add_user_to_task
    self.discussion.task.users_in_discussions << self.user unless self.discussion.task.users_in_discussions.include?(self.user)
  end

  def get_name
    self.id
  end

  def slug
    self.id
  end

  ### Caching

  after_create :flush_cache
  after_update :flush_cache
  after_destroy :flush_cache

  def flush_cache
    Cashier.expire self.class.to_s.underscore.pluralize
  end

  def self.cached_find(id)
    Rails.cache.fetch([name, id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find(id) }
  end

  def self.cached_find_by_slug_or_id(slug_or_id)
    Rails.cache.fetch([name, slug_or_id], :expires_in => 5.minutes, :tag => [self.to_s.underscore.pluralize]) { find_by_slug_or_id(slug_or_id) }
  end

  def method_missing(method_name, *args, &block)
    if method_name =~ /^cached_(.*)_count$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_count/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_count"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).size }

    elsif method_name =~ /^cached_(.*)_records$/
      pluralized_model = method_name.to_s.gsub(/cached_(.*)_records$/, $1)
      Rails.cache.fetch([self, "#{pluralized_model}_records"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(pluralized_model).to_a }

    elsif method_name =~ /^cached_(.*)_record$/
      model_name = method_name.to_s.gsub(/cached_(.*)_record$/, $1)
      Rails.cache.fetch([self, "#{model_name}_record"], :tag => [self.class.to_s.underscore.pluralize]) { self.send(model_name) }

    elsif method_name =~ /^cached_(.*)_ids$/
      model_name = method_name.to_s.gsub(/cached_(.*)_ids$/, $1)
      Rails.cache.fetch([self, "#{model_name}_ids"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_ids").to_a }

    elsif method_name =~ /^cached_(.*)_id$/
      model_name = method_name.to_s.gsub(/cached_(.*)_id$/, $1)
      Rails.cache.fetch([self, "#{model_name}_id"], :tag => [self.class.to_s.underscore.pluralize]) { self.send("#{model_name}_id") }

    else
      super
    end
  end

  ### Caching

end
