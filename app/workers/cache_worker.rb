class CacheWorker
  include Sidekiq::Worker

  def perform_all
    [Facility, Project, Visualization].each do |model|
      model.all.each do |record|
        serializer = record.active_model_serializer.new record
        # This wil cache the JSON and the hash it's generated from
        serializer.to_json
      end
    end
  end

  def perform(model_name, record_id)
    model = model_name.classify.constantize
    record = model.cached_find(record_id)
    serializer = record.active_model_serializer.new record
    # This wil cache the JSON and the hash it's generated from
    serializer.to_json
  end

end