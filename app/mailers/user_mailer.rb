class UserMailer < ActionMailer::Base
  default :from => Settings.site_email

  def new_feed_back(feed_back)
    @feed_back = feed_back

    topic = feed_back.topic == Settings.subscribe_form_topic ? feed_back.topic : Settings.feed_back_topic

    mail(:to => FeedBack.admin_email, :subject => topic)
  end

  def set_task(task)
    @task = task
    mail(:to => FeedBack.admin_email, :subject => Settings.task_topic)
  end

end
