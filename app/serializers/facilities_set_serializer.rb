class FacilitiesSetSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :blocked, :userSlug, :selected_facility_ids, :created, :updated

  #has_many :selected_facilities

  #def selected_facilities
  #  object.selected_facilities.desc(:created_at)
  #end

  def selected_facility_ids
    object.selected_facility_ids
  end

end
