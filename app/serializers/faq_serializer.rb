class FaqSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :answer, :type, :order, :user_id, :created, :updated

end
