class DiscountSerializer < ApplicationSerializer

  attributes :id, :slug, :name, :code, :percent, :created, :updated

  def name
    object.get_name
  end

end
