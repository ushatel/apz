class ReviewSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :body, :user_id, :created, :updated

end
