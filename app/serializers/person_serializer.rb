class PersonSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :email, :phone, :post, :type, :order, :description, :person_image_id, :created, :updated

  has_one :person_image

  def person_image
    object.person_image
  end

end
