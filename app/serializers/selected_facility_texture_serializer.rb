class SelectedFacilityTextureSerializer < TextureFileSerializer

  attributes :selected_facility_id

  def selected_facility_id
    object.cached_selected_facility_id
  end

end
