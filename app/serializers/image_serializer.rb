class ImageSerializer < ApplicationSerializer

  attributes :id, :slug, :name, :file, :created, :updated, :thumb_url, :big_thumb_url

  def slug
    object._id
  end

  def file
    result = {url: object.file.url}

    ImageUploader.versions.each do |key, value|
      result[key] = {url: object.file.send(key).url}
    end
    result
  end

  def thumb_url
    object.file_url(:thumb)
  end

  def big_thumb_url
    object.file_url(:big_thumb)
  end

end
