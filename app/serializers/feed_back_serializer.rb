class FeedBackSerializer < ApplicationSerializer

  attributes :id, :name, :email, :body, :created, :updated

end
