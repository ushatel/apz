class ScenesSetSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :blocked, :userSlug, :selected_scene_ids, :created, :updated

  has_many :selected_scenes

  def selected_scenes
    object.selected_scenes.desc(:created_at)
  end

end
