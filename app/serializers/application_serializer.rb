class ApplicationSerializer < ActiveModel::Serializer

  delegate :cache_key, :to => :object
  delegate :serializable_hash, :to => :object

  # Cache entire JSON string
  def to_json(*args)
    pluralized_model = self.class.to_s.underscore.pluralize.gsub(/^(.*)_serializers$/) { |m| $1 }.pluralize
    Rails.cache.fetch expand_cache_key(self.class.to_s.underscore, cache_key, 'to-json'), :tag => [pluralized_model] do
      super
    end
  end

  # Cache individual Hash objects before serialization
  # This also makes them available to associated serializers
  def serializable_hash
    pluralized_model = self.class.to_s.underscore.pluralize.gsub(/^(.*)_serializers$/) { |m| $1 }.pluralize
    Rails.cache.fetch expand_cache_key(self.class.to_s.underscore, cache_key, 'serializable-hash'), :tag => [pluralized_model] do
      super
    end
  end


  private

  def expand_cache_key(*args)
    ActiveSupport::Cache.expand_cache_key args
  end


  protected

  def name
    object.get_name
  end

  def created
    object.created_at.strftime(Settings.date_time_format)
  end

  def updated
    object.updated_at.strftime(Settings.date_time_format)
  end

  def userSlug
    object.cached_user_record.slug unless object.cached_user_record.nil?
  end

  def avatar
    object.get_avatar
  end

end
