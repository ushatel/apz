class UploadedFileSerializer < ApplicationSerializer

  attributes :id, :slug, :name, :created, :updated, :thumb_url

  def slug
    object._id
  end

  def thumb_url
    ActionController::Base.helpers.asset_path("3d-max.png")
  end

  def file
    {url: object.file.url}
  end

end
