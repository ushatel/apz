class DiscussionSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :task_id, :user_id, :discussion_message_ids, :amount, :prepayment, :last_message, :last_message_author, :last_message_author_avatar, :created, :updated

  has_one :user

  def taskSlug
    object.task.slug unless object.task.nil?
  end

  def amount
    object.amount.to_f.to_i unless object.amount.to_f.to_i == 0
  end

  def prepayment
    object.prepayment.to_f.to_i unless object.prepayment.to_f.to_i == 0
  end

  def last_message
    object.get_last_message
  end

  def last_message_author
    object.get_last_message.user unless object.get_last_message.nil?
  end

  def last_message_author_avatar
    object.get_last_message.user.image if !object.get_last_message.nil? && !object.get_last_message.user.nil?
  end

end
