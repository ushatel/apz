class NodeSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :title, :teaser, :body, :category_ids, :seo_keywords, :seo_description, :seo_present_in_xml_sitemap, :seo_robots, :blocked, :userSlug,
             :created, :updated

end
