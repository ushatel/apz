class UserSerializer < ApplicationSerializer

   attributes :id, :login, :slug, :name, :full_name, :surname, :email, :password, :password_confirmation, :remember_me,
              :last_request_at, :role_ids, :blocked, :manager_id, :city, :studio, :phone, :image_id,
              :created, :updated

   has_one :image

   def name
     object.name
   end

  def full_name
    object.get_name
  end

  def image
    object.image
  end

end
