class ModelingTaskSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :comment, :created, :updated, :userSlug, :visualization_id, :modeling_task_image_id, :modeling_task_texture_ids

  has_one :modeling_task_image

  has_many :modeling_task_textures

  def visualization_id
    object.cached_visualization_id
  end

  def modeling_task_image_id
    object.cached_modeling_task_image_id
  end

  def modeling_task_image
    object.cached_modeling_task_image_record
  end

  def modeling_task_texture_ids
    object.cached_modeling_task_texture_ids
  end

  def modeling_task_textures
    object.cached_modeling_task_textures_records
  end

end
