class DiscussionMessageSerializer < ApplicationSerializer
  include ActionView::Helpers::TextHelper
  include ActionView::Helpers::TagHelper

  attributes :id, :slug, :body, :hours, :minutes, :discussion_id, :user_id, :file_attachment_ids, :created, :updated

  has_one :user
  has_many :file_attachments

  def body
    simple_format(object.body)
  end

  def user
    object.user unless object.user.nil?
  end

end
