class LinkSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :href, :title, :class_string, :rel, :nofollow, :noindex, :order, :parent_id, :created, :updated, :menuSlug

  def menuSlug
    object.menu.slug
  end

end
