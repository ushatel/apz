class MenuSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :description, :created, :updated

end
