class CommentSerializer < ApplicationSerializer
  include ActionView::Helpers::TextHelper
  include ActionView::Helpers::TagHelper

  attributes :id, :body, :user_id, :visualization_id, :file_attachment_ids, :type, :created, :updated

  has_one :user
  has_many :file_attachments

  def user
    object.author
  end

  def body
    if object.user.system?
      object.body
    else
      simple_format(object.body)
    end

  end

  def type
    object.type
  end

  def file_attachments
    object.file_attachments
  end

end
