class SelectedFacilitySerializer < ApplicationSerializer

  attributes :id, :name, :slug, :created, :updated, :comment, :visualization_id, :facilities_set_id, :task_id, :facility_id, :selected_facility_texture_ids,
             :facility_name, :facility_group_name, :facility_avatar

  #has_one :facility
  has_many :selected_facility_textures

  def slug
    object._id
  end

  def facility_id
    object.facility_id
  end

  #def facility
  #  object.cached_facility_record
  #end

  def facility_name
    object.facility.name unless object.facility.nil?
  end

  def facility_group_name
    object.facility.group.name unless object.facility.nil?
  end

  def facility_avatar
    object.facility.get_avatar unless object.facility.nil?
  end

  def selected_facility_texture_ids
    object.selected_facility_texture_ids
  end

  def selected_facility_textures
    object.selected_facility_textures
  end

end
