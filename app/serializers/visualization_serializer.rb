class VisualizationSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :floor_area, :implementation_date, :project_id, :blocked, :sample_project_style_ids, :use_id,
             :furniture_plan_ids, :furniture_plan_comment,
             :ceiling_plan_ids, :ceiling_plan_comment,
             :lighting_plan_ids, :lighting_plan_comment,
             :coverings_plan_ids, :coverings_plan_comment,
             :walls_plan_ids, :walls_plan_comment, :modeling_task_ids, :selected_facility_ids,
             :userSlug, :validate, :status, :project_name,
             :adjustments_count, :high_resolution_image, :initial_scene_with_settings, :drawings, :discount_id,
             :created, :updated

  #has_one :project # not use
  has_one :use
  has_one :discount
  has_many :sample_project_styles
  has_many :furniture_plans
  has_many :ceiling_plans
  has_many :lighting_plans
  has_many :coverings_plans
  has_many :walls_plans
  #has_many :selected_facilities
  has_many :modeling_tasks

  def use_id
    #object.cached_use_id
    object.use_id
  end

  def use
    #object.cached_use_record
    object.use
  end

  def implementation_date
    object.implementation_date.strftime(Settings.date_format) unless object.implementation_date.nil?
  end

  def project_name
    #object.cached_project_record.get_name unless object.cached_project_record.nil?
    object.project.get_name unless object.project.nil?
  end

  def validate
    object.validate_by_steps
  end

  def sample_project_style_ids
    #object.cached_sample_project_style_id
    object.sample_project_style_ids
  end

  def sample_project_styles
    #object.cached_sample_project_style_record
    object.sample_project_styles
  end

  def project_id
    #object.cached_project_id
    object.project_id
  end

  def furniture_plan_ids
    #object.cached_furniture_plan_ids
    object.furniture_plan_ids
  end

  def furniture_plans
    #object.cached_furniture_plans_records
    object.furniture_plans
  end

  def ceiling_plan_ids
    #object.cached_ceiling_plan_ids
    object.ceiling_plan_ids
  end

  def ceiling_plans
    #object.cached_ceiling_plans_records
    object.ceiling_plans
  end

  def lighting_plan_ids
    #object.cached_lighting_plan_ids
    object.lighting_plan_ids
  end

  def lighting_plans
    #object.cached_lighting_plans_records
    object.lighting_plans
  end

  def coverings_plan_ids
    #object.cached_coverings_plan_ids
    object.coverings_plan_ids
  end

  def coverings_plans
    #object.cached_coverings_plans_records
    object.coverings_plans
  end

  def walls_plan_ids
    #object.cached_walls_plan_ids
    object.walls_plan_ids
  end

  def walls_plans
    #object.cached_walls_plans_records
    object.walls_plans
  end

  def selected_facility_ids
  #  #object.cached_selected_facility_ids
    object.selected_facility_ids
  end

  #def selected_facilities
  #  #object.cached_selected_facilities_records
  #  object.selected_facilities
  #end

  def modeling_task_ids
    #object.cached_modeling_task_ids
    object.modeling_task_ids
  end

  def modeling_tasks
    #object.cached_modeling_tasks_records
    object.modeling_tasks
  end

  def discount_id
    #object.cached_sample_project_style_id
    object.discount_id
  end

  def discount
    #object.cached_sample_project_style_record
    object.discount
  end

end
