class MessageSerializer < ApplicationSerializer

  attributes :id, :slug, :name, :topic, :body, :recipient_id, :has_read, :recipient_has_deleted, :sender_has_deleted, :created, :updated, :senderSlug, :recipientSlug

  has_one :sender
  has_one :recipient

  def slug
    object._id
  end

  def senderSlug
    object.sender.slug
  end

  def recipientSlug
    object.recipient.slug
  end

end
