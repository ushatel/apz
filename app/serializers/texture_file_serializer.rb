class TextureFileSerializer < ImageSerializer

  def file
    result = {url: object.file.url}
    TextureFileUploader.versions.each do |key, value|
      result[key] = {url: object.file.send(key).url}
    end
    result
  end

end
