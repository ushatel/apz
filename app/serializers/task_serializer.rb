class TaskSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :blocked, :userSlug, :description, :discount_code, :selected_facility_ids, :selected_scene_ids, :delivered, :created, :updated,
             :adjustments_count, :modelings_count, :high_value_image, :result_file_attachment_ids,
             :initial_scene_with_settings, :drawing_documentation, :furniture_plan, :ceiling_plan, :lighting_plan, :coverings_plan, :walls_plan, :select_facilities,
             :floor_area, :implementation_date, :status, :mark, :rectification_price, :hours_sum, :users_in_discussion_ids, :selected
              #:file_attachment_ids - do not use, because of cache conflicts

  has_many :file_attachments
  has_many :result_file_attachments
  # has_many :users_in_messages, :serializer => UserSerializer
  has_many :users_in_discussions
  #has_many :selected_facilities

  #def selected_facilities
  #  object.selected_facilities.desc(:created_at)
  #end

  def selected_facility_ids
    object.selected_facility_ids
  end

  def selected_scene_ids
    object.selected_scene_ids
  end

  def file_attachment_ids
    object.get_file_attachment_ids
  end

  def file_attachments
    object.get_file_attachments.desc(:created_at)
  end

  def result_file_attachment_ids
    object.result_file_attachment_ids
  end

  def result_file_attachments
    object.result_file_attachments
  end

  def implementation_date
    object.implementation_date.strftime(Settings.date_format) unless object.implementation_date.nil?
  end

  def rectification_price
    object.rectification_price.to_f
  end

  def hours_sum
    object.get_hours_sum
  end

  def users_in_discussions
    object.users_in_discussions
  end

  def selected
    object.selected?(current_user)
  end 

  # def users_in_messages
  #   object.get_users_in_messages
  # end

end
