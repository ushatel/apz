class PortfolioSerializer < ApplicationSerializer

  attributes :id, :slug, :name, :file, :category_ids, :created, :updated, :thumb_url

  def slug
    object._id
  end

  def file
    result = {url: object.file.url}

    PortfolioImageUploader.versions.each do |key, value|
      result[key] = {url: object.file.send(key).url}
    end
    result
  end

  def thumb_url
    object.file_url(:thumb)
  end

  def category_ids
    object.category_ids
  end

end
