class CategorySerializer < ApplicationSerializer

  attributes :id, :name, :name_with_count, :slug, :order, :parent_id, :section_id, :seo_keywords, :group_category_ids, :sectionSlug, :created, :updated

  #has_many :group_categories # Do not include - not using, only group_category_ids

  def name_with_count
    object.get_name_with_count
  end

  def sectionSlug
    object.cached_section_record.slug
  end

end
