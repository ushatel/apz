class ArticleSerializer < NodeSerializer

  attributes :static_image_id, :slideshow_image_ids

  has_one :static_image
  has_many :slideshow_images

  def static_image
    object.static_image
  end

  def slideshow_images
    object.slideshow_images
  end

end
