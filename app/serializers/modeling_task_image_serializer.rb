class ModelingTaskImageSerializer < ImageSerializer

  attributes :modeling_task_id

  def modeling_task_id
    object.cached_modeling_task_id
  end

end
