class SectionSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :created, :updated

end
