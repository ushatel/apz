class MaxFileSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :file, :md5hash, :created, :updated, :thumb_url, :big_thumb_url

  def slug
    object._id
  end

  def file
    {url: object.file.url}
  end

  def thumb_url
    ActionController::Base.helpers.asset_path("3d-max.png")
  end

  def big_thumb_url
    ActionController::Base.helpers.asset_path("big-3d-max.png")
  end

end
