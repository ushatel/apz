class SketchSerializer < ApplicationSerializer

  attributes :id, :slug, :name, :type, :file, :visualization_id, :created, :updated

  def slug
    object._id
  end

  def file
    result = {url: object.file.url}

    SketchUploader.versions.each do |key, value|
      result[key] = {url: object.file.send(key).url}
    end
    result
  end

end
