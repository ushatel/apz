class RoleSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :permission_ids, :user_ids, :created, :updated

end
