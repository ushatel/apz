class FacilitySerializer < ApplicationSerializer

  attributes :id, :name, :slug, :views, :blocked, :used_in_visualizations, :created, :updated, :userSlug,
             :image_ids, :max_file_ids, :material_ids,
             :avatar, :all_images,
             :manufacturer_id, :style_id, :group_id

  #has_many :images
  has_many :max_files
  has_many :materials

  has_one :manufacturer
  has_one :style
  has_one :group

  def image_ids
    #object.cached_image_ids
    object.image_ids
  end

  def max_file_ids
    #object.cached_max_file_ids
    object.max_file_ids
  end

  def all_images
    #object.cached_images_records
    object.images
  end

  def max_files
    #object.cached_max_files_records
    object.max_files
  end

  def materials
    #object.cached_materials_records
    object.materials
  end

  def material_ids
    #object.cached_material_ids
    object.material_ids
  end

  def manufacturer
    #object.cached_manufacturer_record
    object.manufacturer
  end

  def manufacturer_id
    #object.cached_manufacturer_id
    object.manufacturer_id
  end

  def style
    #object.cached_style_record
    object.style
  end

  def style_id
    #object.cached_style_id
    object.style_id
  end

  def group
    #object.cached_group_record
    object.group
  end

  def group_id
    #object.cached_group_id
    object.group_id
  end

end
