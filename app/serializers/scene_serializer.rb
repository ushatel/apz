class SceneSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :views, :blocked, :created, :updated, :userSlug,
             :image_ids, :max_file_ids, :material_ids, :project_name_id, :accent_color_ids, :main_color_ids,
             :avatar, :all_images,
             :gamma_id, :tone_id, :style_id, :room_id, :performer_id

  has_many :main_colors
  has_many :accent_colors
  has_many :materials
  #has_many :images all_images instead of it
  has_many :max_files

  has_one :project_name
  has_one :performer
  has_one :room
  has_one :style
  has_one :tone
  has_one :gamma

  def name
    object.get_name
  end

  def all_images
    #object.cached_images_records
    object.images
  end


end
