class BlockSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :title, :value, :region, :blocked, :type, :order, :view_mode, :view_rules, :created, :updated

end
