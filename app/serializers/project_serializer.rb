class ProjectSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :blocked, :manager_id, :userSlug, :visualization_ids, :created, :updated

  has_one :manager
  has_many :visualizations

  def manager_id
    object.manager
  end

  def manager
    object.manager
  end

  def visualizations
    object.visualizations.desc(:created_at)
  end

  def visualization_ids
    object.visualization_ids
  end

end
