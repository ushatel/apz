class SelectedSceneSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :created, :updated, :comment, :scenes_set_id, :task_id, :scene_id, :selected_scene_texture_ids,
             :scene_name, :scene_avatar, :scene_room_name, :scene_performer_name

  #has_one :facility
  has_many :selected_scene_textures

  def slug
    object._id
  end

  def facility_id
    object.facility_id
  end

  #def facility
  #  object.cached_facility_record
  #end

  def scene_name
    object.scene.get_name unless object.scene.nil?
  end

  def scene_avatar
    object.scene.get_avatar unless object.scene.nil?
  end

  def selected_scene_texture_ids
    object.selected_scene_texture_ids
  end

  def scene_room_name
    unless object.scene.nil?
      object.scene.room.get_name unless object.scene.room.nil?
    end
  end

  def scene_performer_name
    unless object.scene.nil?
      object.scene.performer.get_name unless object.scene.performer.nil?
    end
  end

  def selected_scene_textures
    object.selected_scene_textures
  end

end
