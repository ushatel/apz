class PersonImageSerializer < ApplicationSerializer

  attributes :id, :slug, :name, :file, :created, :updated, :thumb_url

  def slug
    object._id
  end

  def file
    result = {url: object.file.url}

    PersonImageUploader.versions.each do |key, value|
      result[key] = {url: object.file.send(key).url}
    end
    result
  end

  def thumb_url
    object.file_url(:thumb)
  end

end
