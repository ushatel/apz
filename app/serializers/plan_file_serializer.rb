class PlanFileSerializer < ApplicationSerializer

  attributes :id, :slug, :name, :file, :created, :updated, :thumb_url

  def slug
    object._id
  end

  def thumb_url
    %w"jpeg jpg png gif".include?(object.file.url.split(".").last) ? object.file_url(:mini_thumb_30x30) : ActionController::Base.helpers.asset_path("3d-max.png")
  end

  def file
    {url: object.file.url}
  end

end
