class PermissionSerializer < ApplicationSerializer

  attributes :id, :name, :slug, :action, :subject_class, :description, :role_ids, :created, :updated

  def get_name
    "#{object.action} - #{object.subject_class}"
  end

end
