class Support.SharedPaginatedCollection extends Backbone.Paginator.requestPager


  initialize: (params)->
    _.extend(@, params) unless _.isUndefined(params)
    params = {} if _.isUndefined(params)
    @totalRecords = @length
    @currentPage = params.page || 1
    @field = params.field || "created_at"
    @order = params.order || "desc"
    @getCount = 0
    super(params)

  paginator_ui: {
    firstPage: 1
    currentPage: 1
    perPage: 10
    totalPages: 10
  }

  server_api: {
    'page': () ->
      @currentPage
    'field': () ->
      if _.isUndefined(@sortField)
        @field
      else
        @sortField
    'order': () ->
      @order
    'per': () ->
      @perPage
    'filter_by': ->
      if _.isUndefined(@filterField)
        ""
      else
        @filterField
    'filter_value': ->
      if _.isUndefined(@filterValue)
        ""
      else
        @filterValue
    'disable_pagination': ->
      if _.isUndefined(@disablePagination)
        false
      else
        @disablePagination
    'filters': ->
      if _.isUndefined(@filters)
        JSON.stringify({})
      else
        JSON.stringify(@filters)

  }

  # Fix 'this' conflicts
  fetch: (options) ->
    options = (if options then _.clone(options) else {})
    options.parse = true  if options.parse is undefined
    collection = this
    success = options.success
    options.success = (resp, status, xhr) ->
      method = (if options.update then "update" else "reset")
      collection[method] resp, options
      success collection, resp, options  if success

    @sync "read", this, options

  parse: (response) ->
    items = response.items
    @totalPages = Math.ceil(response.meta.total / @perPage) unless _.isUndefined(response.meta)
    @getCount = response.meta.total unless _.isUndefined(response.meta)
    @period = response.meta.period unless (_.isUndefined(response.meta) && _.isUndefined(response.meta.period))
    @statuses = response.meta.statuses unless (_.isUndefined(response.meta) && _.isUndefined(response.meta.statuses))
    @totalRecords = response.meta.total_records unless (_.isUndefined(response.meta) && _.isUndefined(response.meta.total_records))
    items

  getReverseOrder: () ->
    if @order == "asc"
      "desc"
    else
      "asc"

  getPluralModelName: () ->
    @.constructor.name.replace(/PaginatedCollection/, "").toLowerCase()
