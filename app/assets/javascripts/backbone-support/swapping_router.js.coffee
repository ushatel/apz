Support.SwappingRouter = (options) ->
  Backbone.Router.apply this, [options]

_.extend Support.SwappingRouter::, Backbone.Router::,

  swap: (newView) ->
    @currentView.leave()  if @currentView and @currentView.leave
    @currentView = newView
    $(@el).empty().append @currentView.render().el

  addDefaultRoutes: (model, app_path = "", options = {}) ->
    app_path = "#{app_path}\\/" unless app_path == ""

    @route(new RegExp("^#{app_path}#{model}\\/([^\\/]+)"), "#{_.string.camelize(model)}Show") unless options.show == false
    @route(new RegExp("^#{app_path}#{model}\\/new$"), "#{_.string.camelize(model)}Add") unless options.add == false
    @route(new RegExp("^#{app_path}#{model}\\/([^\\/]+)\\/edit"), "#{_.string.camelize(model)}Edit") unless options.edit == false
    @route(new RegExp("^#{app_path}#{model}\\/([^\\/]+)\\/delete"), "#{_.string.camelize(model)}Delete") unless options.delete == false
    @route(new RegExp("^#{app_path}#{model}$"), "#{_.string.camelize(model)}Index") unless options.index == false
    @route(new RegExp("^#{app_path}#{model}\\/$"), "#{_.string.camelize(model)}Index") unless options.index == false
    @route(new RegExp("^#{app_path}#{model}\\/blocked$"), "#{_.string.camelize(model)}Blocked") if options.blockable == true
    @route(new RegExp("^#{app_path}#{model}\\/blocked\/$"), "#{_.string.camelize(model)}Blocked") if options.blockable == true
    @route(new RegExp("^#{app_path}#{model}\\/import"), "#{_.string.camelize(model)}Import") if options.importable == true
    @route(new RegExp("^#{app_path}#{model}\\/import/$"), "#{_.string.camelize(model)}Import") if options.importable == true
    @route(new RegExp("^#{app_path}#{model}\\/import-status\/$"), "#{_.string.camelize(model)}ImportStatus") if options.importable == true
    @route(new RegExp("^#{app_path}#{model}\\/import-status$"), "#{_.string.camelize(model)}ImportStatus") if options.importable == true
    @route(new RegExp("^#{app_path}#{model}\\/page\\/(\\d+)$"), "#{_.string.camelize(model)}Index") unless options.pagination == false
    @route(new RegExp("^#{app_path}#{model}\\/page\\/(\\d+)\\/([^\\/]+)\\/(asc|desc)$"), "#{_.string.camelize(model)}Index") unless options.sorting == false
    @route(new RegExp("^#{app_path}#{model}\\/([^\\/]+)\\/select-facilities$"), "#{_.string.camelize(model)}SelectFacilities") if options.selectFacilities == true
    @route(new RegExp("^#{app_path}#{model}\\/([^\\/]+)\\/select-facilities\/$"), "#{_.string.camelize(model)}SelectFacilities") if options.selectFacilities == true
    @route(new RegExp("^#{app_path}#{model}\\/([^\\/]+)\\/select-scenes$"), "#{_.string.camelize(model)}SelectScenes") if options.selectScenes == true
    @route(new RegExp("^#{app_path}#{model}\\/([^\\/]+)\\/select-scenes\/$"), "#{_.string.camelize(model)}SelectScenes") if options.selectScenes == true

  addChildRoutes: (parentModel, childModel, app_path = "", options = {}) ->
    app_path = "#{app_path}\\/" unless app_path == ""

    @route(new RegExp("^#{app_path}#{parentModel}\\/([^\\/]+)\\/#{childModel}\\/"), "#{_.string.camelize(childModel)}Index") unless options.index == false
    @route(new RegExp("^#{app_path}#{parentModel}\\/([^\\/]+)\\/#{childModel}"), "#{_.string.camelize(childModel)}Index") unless options.index == false
    @route(new RegExp("^#{app_path}#{parentModel}\\/([^\\/]+)\\/#{childModel}\\/([^\\/]+)"), "#{_.string.camelize(childModel)}Show") unless options.show == false
    @route(new RegExp("^#{app_path}#{parentModel}\\/([^\\/]+)\\/#{childModel}\\/new$"), "#{_.string.camelize(childModel)}Add") unless options.add == false
    @route(new RegExp("^#{app_path}#{parentModel}\\/([^\\/]+)\\/#{childModel}\\/structure"), "#{_.string.camelize(childModel)}Structure") unless options.structure == false
    @route(new RegExp("^#{app_path}#{parentModel}\\/([^\\/]+)\\/#{childModel}\\/([^\\/]+)\\/edit$"), "#{_.string.camelize(childModel)}Edit") unless options.edit == false
    @route(new RegExp("^#{app_path}#{parentModel}\\/([^\\/]+)\\/#{childModel}\\/([^\\/]+)\\/delete$"), "#{_.string.camelize(childModel)}Delete") unless options.delete == false
    @route(new RegExp("^#{app_path}#{parentModel}\\/([^\\/]+)\\/#{childModel}\\/page\\/(\\d+)$"), "#{_.string.camelize(childModel)}Index") unless options.pagination == false
    @route(new RegExp("^#{app_path}#{parentModel}\\/([^\\/]+)\\/#{childModel}\\/page\\/(\\d+)\\/([^\\/]+)\\/(asc|desc)$"), "#{_.string.camelize(childModel)}Index") unless options.sorting == false


  addNestedRoutes: (parentModel, childModel, nestedModel, app_path = "", options = {}) ->
    app_path = "#{app_path}\\/" unless app_path == ""
    @route(new RegExp("^#{app_path}#{parentModel}\\/([^\\/]+)\\/#{childModel}\\/([^\\/]+)\\/#{nestedModel}\\/"), "#{_.string.camelize(nestedModel)}Index") unless options.index == false
    @route(new RegExp("^#{app_path}#{parentModel}\\/([^\\/]+)\\/#{childModel}\\/([^\\/]+)\\/#{nestedModel}"), "#{_.string.camelize(nestedModel)}Index") unless options.index == false
    @route(new RegExp("^#{app_path}#{parentModel}\\/([^\\/]+)\\/#{childModel}\\/([^\\/]+)\\/#{nestedModel}\\/([^\\/]+)"), "#{_.string.camelize(nestedModel)}Show") unless options.show == false
    @route(new RegExp("^#{app_path}#{parentModel}\\/([^\\/]+)\\/#{childModel}\\/([^\\/]+)\\/#{nestedModel}\\/new$"), "#{_.string.camelize(nestedModel)}Add") unless options.add == false
    @route(new RegExp("^#{app_path}#{parentModel}\\/([^\\/]+)\\/#{childModel}\\/([^\\/]+)\\/#{nestedModel}\\/([^\\/]+)\\/edit$"), "#{_.string.camelize(nestedModel)}Edit") unless options.edit == false
    @route(new RegExp("^#{app_path}#{parentModel}\\/([^\\/]+)\\/#{childModel}\\/([^\\/]+)\\/#{nestedModel}\\/([^\\/]+)\\/delete$"), "#{_.string.camelize(nestedModel)}Delete") unless options.delete == false

  addFourNestedRoutes: (firstModel, secondModel, thirdModel, fourthModel, app_path = "", options = {}) ->
    app_path = "#{app_path}\\/" unless app_path == ""
    @route(new RegExp("^#{app_path}#{firstModel}\\/([^\\/]+)\\/#{secondModel}\\/([^\\/]+)\\/#{thirdModel}\\/([^\\/]+)\\/#{fourthModel}\\/"), "#{_.string.camelize(fourthModel)}Index") unless options.index == false
    @route(new RegExp("^#{app_path}#{firstModel}\\/([^\\/]+)\\/#{secondModel}\\/([^\\/]+)\\/#{thirdModel}\\/([^\\/]+)\\/#{fourthModel}"), "#{_.string.camelize(fourthModel)}Index") unless options.index == false
    @route(new RegExp("^#{app_path}#{firstModel}\\/([^\\/]+)\\/#{secondModel}\\/([^\\/]+)\\/#{thirdModel}\\/([^\\/]+)\\/#{fourthModel}\\/([^\\/]+)"), "#{_.string.camelize(fourthModel)}Show") unless options.show == false
    @route(new RegExp("^#{app_path}#{firstModel}\\/([^\\/]+)\\/#{secondModel}\\/([^\\/]+)\\/#{thirdModel}\\/([^\\/]+)\\/#{fourthModel}\\/new$"), "#{_.string.camelize(fourthModel)}Add") unless options.add == false
    @route(new RegExp("^#{app_path}#{firstModel}\\/([^\\/]+)\\/#{secondModel}\\/([^\\/]+)\\/#{thirdModel}\\/([^\\/]+)\\/#{fourthModel}\\/([^\\/]+)\\/edit$"), "#{_.string.camelize(fourthModel)}Edit") unless options.edit == false
    @route(new RegExp("^#{app_path}#{firstModel}\\/([^\\/]+)\\/#{secondModel}\\/([^\\/]+)\\/#{thirdModel}\\/([^\\/]+)\\/#{fourthModel}\\/([^\\/]+)\\/delete$"), "#{_.string.camelize(fourthModel)}Delete") unless options.delete == false

  add_routes: (models, app_path = "") ->
    _.each models, (model) =>
      if _.isArray(model)
        if _.isArray(model[1])
          @addDefaultRoutes(model[0], app_path)
          @addChildRoutes(model[0], model[1][0], app_path)
          @addChildRoutes(model[1][0], model[1][1], app_path)
          @addNestedRoutes(model[0], model[1][0], model[1][1], app_path)
        else
          @addDefaultRoutes(model[0], app_path)
          @addChildRoutes(model[0], model[1], app_path)
      else
        @addDefaultRoutes(model, app_path)

Support.SwappingRouter.extend = Backbone.Router.extend