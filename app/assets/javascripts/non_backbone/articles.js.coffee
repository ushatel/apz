jQuery ->

  $('article.post .body').hide()

  $('article.post footer a.more').on 'click', (e) ->
    e.preventDefault()
    $(this).hide()
    $('article.post .body').fadeIn(500)