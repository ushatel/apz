// page init
jQuery(function(){
  initCustomPlaceholder();
  initDatepicker();
  initTabs();
  jcf.customForms.replaceAll();
  initOpenClose();
  initLightbox();
  jQuery('input, textarea').placeholder();
  initCycleCarousel();
  initSlideShow();
  initCarousel();
  initAccordion();
  initInputs();
  initMasonry();
//  initCustomValue();
});

// portfolio page layout
function initMasonry() {
  $('.gallery-portfolio').masonry({
    itemSelector: 'li'
  });
}

// custom value init
function initCustomValue(){
  jQuery('.calculator-form').each(function(){
    var form = jQuery(this);
    var inputs = form.find('input:text');
    inputs.each(function(){
      var input = jQuery(this);
      var defVal = input.val();
      input.on('focus', function(){
        if(input.attr('value') === defVal) input.val('');
      });
      input.on('blur', function(){
        if(input.attr('value') === '') input.val(defVal);
      });
    })
  })
}

// custom placeholder inputs
function initCustomPlaceholder(){
  jQuery('.custom-placeholder').each(function(){
    var holder = jQuery(this),
      input = holder.find('textarea'),
      defaultPlaceholder = input.attr('placeholder'),
      fakePlaceholder = jQuery('<span class="input-placeholder-text"></span>');

    input.attr('placeholder', '');
    fakePlaceholder.prependTo(holder).text(defaultPlaceholder);
    fakePlaceholder.css({
      position: 'absolute',
      color: input.css('color')
    });

    function showPlaceholder(){
      if(input.attr('value') === '') {
        fakePlaceholder.show();
      } else {
        fakePlaceholder.hide();
      }
    }
    showPlaceholder();

    input.on('focus', function(){
      fakePlaceholder.hide();
    });
    input.on('blur', function(){
      showPlaceholder();
    });
    fakePlaceholder.on('click', function(){
      fakePlaceholder.hide();
      input.focus();
    });
  });
}

// clear inputs on focus
function initInputs() {
  PlaceholderInput.replaceByOptions({
    // filter options
    clearInputs: false,
    clearTextareas: false,
    clearPasswords: false,
    skipClass: 'default',

    // input options
    wrapWithElement: true,
    showUntilTyping: true,
    getParentByClass: false,
    placeholderAttr: 'placeholder'
  });
}

// accordion menu init
function initAccordion() {
  jQuery('ul.sketches').slideAccordion({
    opener: 'a.opener',
    slider: 'div.slide',
    animSpeed: 500
  });
}

// scroll gallery init
function initCarousel() {
  jQuery('div.carousel').scrollGallery({
    mask: 'div.mask',
    slider: 'div.slideset',
    slides: 'div.slide',
    btnPrev: 'a.btn-prev',
    btnNext: 'a.btn-next',
    pagerLinks: '.pagination li',
    autoRotation: false,
    switchTime: 3000,
    animSpeed: 500
  });
}

// fade gallery init
function initSlideShow() {
  jQuery('div.slideshow').fadeGallery({
    slides: 'div.slide',
    btnPrev: 'a.btn-prev',
    btnNext: 'a.btn-next',
    pagerLinks: '.pagination li',
    event: 'click',
    autoRotation: true,
    switchTime: 7000,
    animSpeed: 500
  });
  jQuery('div.reviews-gallery').fadeGallery({
    slides: 'div.slide',
    btnPrev: 'a.btn-prev',
    btnNext: 'a.btn-next',
    generatePagination: '.pagination',
    event: 'click',
    autoRotation: true,
    autoHeight: true,
    switchTime: 12000,
    animSpeed: 500
  });
}

// cycle scroll gallery init
function initCycleCarousel() {
  jQuery('div.cycle-gallery').scrollAbsoluteGallery({
    mask: 'div.mask',
    slider: 'div.slideset',
    slides: 'div.slide',
    btnPrev: 'a.btn-prev',
    btnNext: 'a.btn-next',
    generatePagination: '.pagination',
    stretchSlideToMask: true,
    pauseOnHover: true,
    autoRotation: true,
    switchTime: 7000,
    animSpeed: 500
  });
  jQuery('div.scheme-gallery').scrollAbsoluteGallery({
    mask: 'div.mask',
    slider: 'div.slideset',
    slides: 'div.slide',
    btnPrev: 'a.btn-prev',
    btnNext: 'a.btn-next',
    generatePagination: '.pagination',
    pauseOnHover: true,
    autoRotation: true,
    switchTime: 7000,
    animSpeed: 500
  });
}

// placeholder class
;(function(){
  var placeholderCollection = [];
  PlaceholderInput = function() {
    this.options = {
      element:null,
      showUntilTyping:false,
      wrapWithElement:false,
      getParentByClass:false,
      showPasswordBullets:false,
      placeholderAttr:'value',
      inputFocusClass:'focus',
      inputActiveClass:'text-active',
      parentFocusClass:'parent-focus',
      parentActiveClass:'parent-active',
      labelFocusClass:'label-focus',
      labelActiveClass:'label-active',
      fakeElementClass:'input-placeholder-text'
    };
    placeholderCollection.push(this);
    this.init.apply(this,arguments);
  };
  PlaceholderInput.refreshAllInputs = function(except) {
    for(var i = 0; i < placeholderCollection.length; i++) {
      if(except !== placeholderCollection[i]) {
        placeholderCollection[i].refreshState();
      }
    }
  };
  PlaceholderInput.replaceByOptions = function(opt) {
    var inputs = [].concat(
      convertToArray(document.getElementsByTagName('input')),
      convertToArray(document.getElementsByTagName('textarea'))
    );
    for(var i = 0; i < inputs.length; i++) {
      if(inputs[i].className.indexOf(opt.skipClass) < 0) {
        var inputType = getInputType(inputs[i]);
        var placeholderValue = inputs[i].getAttribute('placeholder');
        if(opt.focusOnly || (opt.clearInputs && (inputType === 'text' || inputType === 'email' || placeholderValue)) ||
          (opt.clearTextareas && inputType === 'textarea') ||
          (opt.clearPasswords && inputType === 'password')
          ) {
          new PlaceholderInput({
            element:inputs[i],
            focusOnly: opt.focusOnly,
            wrapWithElement:opt.wrapWithElement,
            showUntilTyping:opt.showUntilTyping,
            getParentByClass:opt.getParentByClass,
            showPasswordBullets:opt.showPasswordBullets,
            placeholderAttr: placeholderValue ? 'placeholder' : opt.placeholderAttr
          });
        }
      }
    }
  };
  PlaceholderInput.prototype = {
    init: function(opt) {
      this.setOptions(opt);
      if(this.element && this.element.PlaceholderInst) {
        this.element.PlaceholderInst.refreshClasses();
      } else {
        this.element.PlaceholderInst = this;
        if(this.elementType !== 'radio' || this.elementType !== 'checkbox' || this.elementType !== 'file') {
          this.initElements();
          this.attachEvents();
          this.refreshClasses();
        }
      }
    },
    setOptions: function(opt) {
      for(var p in opt) {
        if(opt.hasOwnProperty(p)) {
          this.options[p] = opt[p];
        }
      }
      if(this.options.element) {
        this.element = this.options.element;
        this.elementType = getInputType(this.element);
        if(this.options.focusOnly) {
          this.wrapWithElement = false;
        } else {
          if(this.elementType === 'password' && this.options.showPasswordBullets) {
            this.wrapWithElement = false;
          } else {
            this.wrapWithElement = this.elementType === 'password' || this.options.showUntilTyping ? true : this.options.wrapWithElement;
          }
        }
        this.setPlaceholderValue(this.options.placeholderAttr);
      }
    },
    setPlaceholderValue: function(attr) {
      this.origValue = (attr === 'value' ? this.element.defaultValue : (this.element.getAttribute(attr) || ''));
      if(this.options.placeholderAttr !== 'value') {
        this.element.removeAttribute(this.options.placeholderAttr);
      }
    },
    initElements: function() {
      // create fake element if needed
      if(this.wrapWithElement) {
        this.fakeElement = document.createElement('span');
        this.fakeElement.className = this.options.fakeElementClass;
        this.fakeElement.innerHTML += this.origValue;
        this.fakeElement.style.color = getStyle(this.element, 'color');
        this.fakeElement.style.position = 'absolute';
        this.element.parentNode.insertBefore(this.fakeElement, this.element);

        if(this.element.value === this.origValue || !this.element.value) {
          this.element.value = '';
          this.togglePlaceholderText(true);
        } else {
          this.togglePlaceholderText(false);
        }
      } else if(!this.element.value && this.origValue.length) {
        this.element.value = this.origValue;
      }
      // get input label
      if(this.element.id) {
        this.labels = document.getElementsByTagName('label');
        for(var i = 0; i < this.labels.length; i++) {
          if(this.labels[i].htmlFor === this.element.id) {
            this.labelFor = this.labels[i];
            break;
          }
        }
      }
      // get parent node (or parentNode by className)
      this.elementParent = this.element.parentNode;
      if(typeof this.options.getParentByClass === 'string') {
        var el = this.element;
        while(el.parentNode) {
          if(hasClass(el.parentNode, this.options.getParentByClass)) {
            this.elementParent = el.parentNode;
            break;
          } else {
            el = el.parentNode;
          }
        }
      }
    },
    attachEvents: function() {
      this.element.onfocus = bindScope(this.focusHandler, this);
      this.element.onblur = bindScope(this.blurHandler, this);
      if(this.options.showUntilTyping) {
        this.element.onkeydown = bindScope(this.typingHandler, this);
        this.element.onpaste = bindScope(this.typingHandler, this);
      }
      if(this.wrapWithElement) this.fakeElement.onclick = bindScope(this.focusSetter, this);
    },
    togglePlaceholderText: function(state) {
      if(!this.element.readOnly && !this.options.focusOnly) {
        if(this.wrapWithElement) {
          this.fakeElement.style.display = state ? '' : 'none';
        } else {
          this.element.value = state ? this.origValue : '';
        }
      }
    },
    focusSetter: function() {
      this.element.focus();
    },
    focusHandler: function() {
      clearInterval(this.checkerInterval);
      this.checkerInterval = setInterval(bindScope(this.intervalHandler,this), 1);
      this.focused = true;
      if(!this.element.value.length || this.element.value === this.origValue) {
        if(!this.options.showUntilTyping) {
          this.togglePlaceholderText(false);
        }
      }
      this.refreshClasses();
    },
    blurHandler: function() {
      clearInterval(this.checkerInterval);
      this.focused = false;
      if(!this.element.value.length || this.element.value === this.origValue) {
        this.togglePlaceholderText(true);
      }
      this.refreshClasses();
      PlaceholderInput.refreshAllInputs(this);
    },
    typingHandler: function() {
      setTimeout(bindScope(function(){
        if(this.element.value.length) {
          this.togglePlaceholderText(false);
          this.refreshClasses();
        }
      },this), 10);
    },
    intervalHandler: function() {
      if(typeof this.tmpValue === 'undefined') {
        this.tmpValue = this.element.value;
      }
      if(this.tmpValue != this.element.value) {
        PlaceholderInput.refreshAllInputs(this);
      }
    },
    refreshState: function() {
      if(this.wrapWithElement) {
        if(this.element.value.length && this.element.value !== this.origValue) {
          this.togglePlaceholderText(false);
        } else if(!this.element.value.length) {
          this.togglePlaceholderText(true);
        }
      }
      this.refreshClasses();
    },
    refreshClasses: function() {
      this.textActive = this.focused || (this.element.value.length && this.element.value !== this.origValue);
      this.setStateClass(this.element, this.options.inputFocusClass,this.focused);
      this.setStateClass(this.elementParent, this.options.parentFocusClass,this.focused);
      this.setStateClass(this.labelFor, this.options.labelFocusClass,this.focused);
      this.setStateClass(this.element, this.options.inputActiveClass, this.textActive);
      this.setStateClass(this.elementParent, this.options.parentActiveClass, this.textActive);
      this.setStateClass(this.labelFor, this.options.labelActiveClass, this.textActive);
    },
    setStateClass: function(el,cls,state) {
      if(!el) return; else if(state) addClass(el,cls); else removeClass(el,cls);
    }
  };

  // utility functions
  function convertToArray(collection) {
    var arr = [];
    for (var i = 0, ref = arr.length = collection.length; i < ref; i++) {
      arr[i] = collection[i];
    }
    return arr;
  }
  function getInputType(input) {
    return (input.type ? input.type : input.tagName).toLowerCase();
  }
  function hasClass(el,cls) {
    return el.className ? el.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)')) : false;
  }
  function addClass(el,cls) {
    if (!hasClass(el,cls)) el.className += " "+cls;
  }
  function removeClass(el,cls) {
    if (hasClass(el,cls)) {el.className=el.className.replace(new RegExp('(\\s|^)'+cls+'(\\s|$)'),' ');}
  }
  function bindScope(f, scope) {
    return function() {return f.apply(scope, arguments);};
  }
  function getStyle(el, prop) {
    if (document.defaultView && document.defaultView.getComputedStyle) {
      return document.defaultView.getComputedStyle(el, null)[prop];
    } else if (el.currentStyle) {
      return el.currentStyle[prop];
    } else {
      return el.style[prop];
    }
  }
}());

/*
 * jQuery Accordion plugin
 */
;(function($){
  $.fn.slideAccordion = function(opt){
    // default options
    var options = $.extend({
      addClassBeforeAnimation: false,
      activeClass:'active',
      opener:'.opener',
      slider:'.slide',
      animSpeed: 300,
      collapsible:true,
      event:'click'
    },opt);

    return this.each(function(){
      // options
      var accordion = $(this);
      var items = accordion.find(':has('+options.slider+')');

      items.each(function(){
        var item = $(this);
        var opener = item.find(options.opener);
        var slider = item.find(options.slider);
        opener.bind(options.event, function(e){
          if(!slider.is(':animated')) {
            if(item.hasClass(options.activeClass)) {
              if(options.collapsible) {
                slider.slideUp(options.animSpeed, function(){
                  hideSlide(slider);
                  item.removeClass(options.activeClass);
                });
              }
            } else {
              // show active
              var levelItems = item.siblings('.'+options.activeClass);
              var sliderElements = levelItems.find(options.slider);
              item.addClass(options.activeClass);
              showSlide(slider).hide().slideDown(options.animSpeed);

              // collapse others
              sliderElements.slideUp(options.animSpeed, function(){
                levelItems.removeClass(options.activeClass);
                hideSlide(sliderElements);
              });
            }
          }
          e.preventDefault();
        });
        if(item.hasClass(options.activeClass)) showSlide(slider); else hideSlide(slider);
      });
    });
  };

  // accordion slide visibility
  var showSlide = function(slide) {
    return slide.css({position:'', top: '', left: '', width: '' });
  };
  var hideSlide = function(slide) {
    return slide.show().css({position:'absolute', top: -9999, left: -9999, width: slide.width() });
  };
}(jQuery));

/*
 * jQuery Carousel plugin
 */
;(function($){
  function ScrollGallery(options) {
    this.options = $.extend({
      mask: 'div.mask',
      slider: '>*',
      slides: '>*',
      activeClass:'active',
      disabledClass:'disabled',
      btnPrev: 'a.btn-prev',
      btnNext: 'a.btn-next',
      generatePagination: false,
      pagerList: '<ul>',
      pagerListItem: '<li><a href="#"></a></li>',
      pagerListItemText: 'a',
      pagerLinks: '.pagination li',
      currentNumber: 'span.current-num',
      totalNumber: 'span.total-num',
      btnPlay: '.btn-play',
      btnPause: '.btn-pause',
      btnPlayPause: '.btn-play-pause',
      galleryReadyClass: 'gallery-js-ready',
      autorotationActiveClass: 'autorotation-active',
      autorotationDisabledClass: 'autorotation-disabled',
      stretchSlideToMask: false,
      circularRotation: true,
      disableWhileAnimating: false,
      autoRotation: false,
      pauseOnHover: isTouchDevice ? false : true,
      maskAutoSize: false,
      switchTime: 4000,
      animSpeed: 600,
      event:'click',
      swipeGap: false,
      swipeThreshold: 15,
      handleTouch: true,
      vertical: false,
      useTranslate3D: false,
      step: false
    }, options);
    this.init();
  }
  ScrollGallery.prototype = {
    init: function() {
      if(this.options.holder) {
        this.findElements();
        this.attachEvents();
        this.refreshPosition();
        this.refreshState(true);
        this.resumeRotation();
        this.makeCallback('onInit', this);
      }
    },
    findElements: function() {
      // define dimensions proporties
      this.fullSizeFunction = this.options.vertical ? 'outerHeight' : 'outerWidth';
      this.innerSizeFunction = this.options.vertical ? 'height' : 'width';
      this.slideSizeFunction = 'outerHeight';
      this.maskSizeProperty = 'height';
      this.animProperty = this.options.vertical ? 'marginTop' : 'marginLeft';
      this.swipeProperties = this.options.vertical ? ['up', 'down'] : ['left', 'right'];

      // control elements
      this.gallery = $(this.options.holder).addClass(this.options.galleryReadyClass);
      this.mask = this.gallery.find(this.options.mask);
      this.slider = this.mask.find(this.options.slider);
      this.slides = this.slider.find(this.options.slides);
      this.btnPrev = this.gallery.find(this.options.btnPrev);
      this.btnNext = this.gallery.find(this.options.btnNext);
      this.currentStep = 0; this.stepsCount = 0;

      // get start index
      if(this.options.step === false) {
        var activeSlide = this.slides.filter('.'+this.options.activeClass);
        if(activeSlide.length) {
          this.currentStep = this.slides.index(activeSlide);
        }
      }

      // calculate offsets
      this.calculateOffsets();

      // create gallery pagination
      if(typeof this.options.generatePagination === 'string') {
        this.pagerLinks = $();
        this.buildPagination();
      } else {
        this.pagerLinks = this.gallery.find(this.options.pagerLinks);
        this.attachPaginationEvents();
      }

      // autorotation control buttons
      this.btnPlay = this.gallery.find(this.options.btnPlay);
      this.btnPause = this.gallery.find(this.options.btnPause);
      this.btnPlayPause = this.gallery.find(this.options.btnPlayPause);

      // misc elements
      this.curNum = this.gallery.find(this.options.currentNumber);
      this.allNum = this.gallery.find(this.options.totalNumber);
    },
    attachEvents: function() {
      // bind handlers scope
      var self = this;
      this.bindHandlers(['onWindowResize']);
      $(window).bind('load resize orientationchange', this.onWindowResize);

      // previous and next button handlers
      if(this.btnPrev.length) {
        this.prevSlideHandler = function(e) {
          e.preventDefault();
          self.prevSlide();
        };
        this.btnPrev.bind(this.options.event, this.prevSlideHandler);
      }
      if(this.btnNext.length) {
        this.nextSlideHandler = function(e) {
          e.preventDefault();
          self.nextSlide();
        };
        this.btnNext.bind(this.options.event, this.nextSlideHandler);
      }

      // pause on hover handling
      if(this.options.pauseOnHover && !isTouchDevice) {
        this.hoverHandler = function() {
          if(self.options.autoRotation) {
            self.galleryHover = true;
            self.pauseRotation();
          }
        };
        this.leaveHandler = function() {
          if(self.options.autoRotation) {
            self.galleryHover = false;
            self.resumeRotation();
          }
        };
        this.gallery.bind({mouseenter: this.hoverHandler, mouseleave: this.leaveHandler});
      }

      // autorotation buttons handler
      if(this.btnPlay.length) {
        this.btnPlayHandler = function(e) {
          e.preventDefault();
          self.startRotation();
        };
        this.btnPlay.bind(this.options.event, this.btnPlayHandler);
      }
      if(this.btnPause.length) {
        this.btnPauseHandler = function(e) {
          e.preventDefault();
          self.stopRotation();
        };
        this.btnPause.bind(this.options.event, this.btnPauseHandler);
      }
      if(this.btnPlayPause.length) {
        this.btnPlayPauseHandler = function(e) {
          e.preventDefault();
          if(!self.gallery.hasClass(self.options.autorotationActiveClass)) {
            self.startRotation();
          } else {
            self.stopRotation();
          }
        };
        this.btnPlayPause.bind(this.options.event, this.btnPlayPauseHandler);
      }

      // swipe event handling
      if(isTouchDevice) {
        // enable hardware acceleration
        if(this.options.useTranslate3D) {
          this.slider.css({'-webkit-transform': 'translate3d(0px, 0px, 0px)'});
        }

        // swipe gestures
        if(this.options.handleTouch && jQuery.fn.hammer) {
          this.mask.hammer({
            drag_block_horizontal: this.options.vertical ? false : true,
            drag_block_vertical: this.options.vertical ? true : false,
            drag_min_distance: 1
          }).on(this.options.vertical ? 'touch release dragup dragdown swipeup swipedown' : 'touch release dragleft dragright swipeleft swiperight', function(ev){
              switch(ev.type) {
                case 'touch':
                  if(!self.galleryAnimating){
                    self.originalOffset = parseInt(self.slider.stop(true, false).css(self.animProperty), 10);
                  }
                  break;
                case (self.options.vertical? 'dragup' : 'dragright'):
                case (self.options.vertical? 'dragdown' : 'dragleft'):
                  if(!self.galleryAnimating){
                    if(ev.gesture.direction === self.swipeProperties[0] || ev.gesture.direction === self.swipeProperties[1]){
                      var tmpOffset = self.originalOffset + ev.gesture[self.options.vertical ? 'deltaY' : 'deltaX'];
                      tmpOffset = Math.max(Math.min(0, tmpOffset), self.maxOffset)
                      self.tmpProps = {};
                      self.tmpProps[self.animProperty] = tmpOffset;
                      self.slider.css(self.tmpProps);
                      ev.gesture.preventDefault();
                    };
                  };
                  break;
                case (self.options.vertical ? 'swipeup' : 'swipeleft'):
                  if(!self.galleryAnimating){
                    if(ev.gesture.direction === self.swipeProperties[0]) self.nextSlide();
                  }
                  ev.gesture.stopDetect();
                  break;
                case (self.options.vertical ? 'swipedown' : 'swiperight'):
                  if(!self.galleryAnimating){
                    if(ev.gesture.direction === self.swipeProperties[1]) self.prevSlide();
                  }
                  ev.gesture.stopDetect();
                  break;

                case 'release':
                  if(!self.galleryAnimating){
                    if(Math.abs(ev.gesture[self.options.vertical ? 'deltaY' : 'deltaX']) > self.options.swipeThreshold) {
                      if(self.options.vertical){
                        if(ev.gesture.direction == 'down') self.prevSlide(); else if(ev.gesture.direction == 'up') self.nextSlide();
                      }
                      else {
                        if(ev.gesture.direction == 'right') self.prevSlide(); else if(ev.gesture.direction == 'left') self.nextSlide();
                      }
                    }
                    else {
                      self.switchSlide();
                    }
                  }
                  break;
              }
            });
        }
      }
    },
    onWindowResize: function() {
      if(!this.galleryAnimating) {
        this.calculateOffsets();
        this.refreshPosition();
        this.buildPagination();
        this.refreshState();
        this.resizeQueue = false;
      } else {
        this.resizeQueue = true;
      }
    },
    refreshPosition: function() {
      this.currentStep = Math.min(this.currentStep, this.stepsCount - 1);
      this.tmpProps = {};
      this.tmpProps[this.animProperty] = this.getStepOffset();
      this.slider.stop().css(this.tmpProps);
    },
    calculateOffsets: function() {
      var self = this, tmpOffset, tmpStep;
      if(this.options.stretchSlideToMask) {
        var tmpObj = {};
        tmpObj[this.innerSizeFunction] = this.mask[this.innerSizeFunction]();
        this.slides.css(tmpObj);
      }

      this.maskSize = this.mask[this.innerSizeFunction]();
      this.sumSize = this.getSumSize();
      this.maxOffset = this.maskSize - this.sumSize;

      // vertical gallery with single size step custom behavior
      if(this.options.vertical && this.options.maskAutoSize) {
        this.options.step = 1;
        this.stepsCount = this.slides.length;
        this.stepOffsets = [0];
        tmpOffset = 0;
        for(var i = 0; i < this.slides.length; i++) {
          tmpOffset -= $(this.slides[i])[this.fullSizeFunction](true);
          this.stepOffsets.push(tmpOffset);
        }
        this.maxOffset = tmpOffset;
        return;
      }

      // scroll by slide size
      if(typeof this.options.step === 'number' && this.options.step > 0) {
        this.slideDimensions = [];
        this.slides.each($.proxy(function(ind, obj){
          self.slideDimensions.push( $(obj)[self.fullSizeFunction](true) );
        },this));

        // calculate steps count
        this.stepOffsets = [0];
        this.stepsCount = 1;
        tmpOffset = tmpStep = 0;
        while(tmpOffset > this.maxOffset) {
          tmpOffset -= this.getSlideSize(tmpStep, tmpStep + this.options.step);
          tmpStep += this.options.step;
          this.stepOffsets.push(Math.max(tmpOffset, this.maxOffset));
          this.stepsCount++;
        }
      }
      // scroll by mask size
      else {
        // define step size
        this.stepSize = this.maskSize;

        // calculate steps count
        this.stepsCount = 1;
        tmpOffset = 0;
        while(tmpOffset > this.maxOffset) {
          tmpOffset -= this.stepSize;
          this.stepsCount++;
        }
      }
    },
    getSumSize: function() {
      var sum = 0;
      this.slides.each($.proxy(function(ind, obj){
        sum += $(obj)[this.fullSizeFunction](true);
      },this));
      this.slider.css(this.innerSizeFunction, sum);
      return sum;
    },
    getStepOffset: function(step) {
      step = step || this.currentStep;
      if(typeof this.options.step === 'number') {
        return this.stepOffsets[this.currentStep];
      } else {
        return Math.max(-this.currentStep * this.stepSize, this.maxOffset);
      }
    },
    getSlideSize: function(i1, i2) {
      var sum = 0;
      for(var i = i1; i < Math.min(i2, this.slideDimensions.length); i++) {
        sum += this.slideDimensions[i];
      }
      return sum;
    },
    buildPagination: function() {
      if(typeof this.options.generatePagination === 'string') {
        if(!this.pagerHolder) {
          this.pagerHolder = this.gallery.find(this.options.generatePagination);
        }
        if(this.pagerHolder.length && this.oldStepsCount != this.stepsCount) {
          this.oldStepsCount = this.stepsCount;
          this.pagerHolder.empty();
          this.pagerList = $(this.options.pagerList).appendTo(this.pagerHolder);
          for(var i = 0; i < this.stepsCount; i++) {
            $(this.options.pagerListItem).appendTo(this.pagerList).find(this.options.pagerListItemText).text(i+1);
          }
          this.pagerLinks = this.pagerList.children();
          this.attachPaginationEvents();
        }
      }
    },
    attachPaginationEvents: function() {
      var self = this;
      this.pagerLinksHandler = function(e) {
        e.preventDefault();
        self.numSlide(self.pagerLinks.index(e.currentTarget));
      };
      this.pagerLinks.bind(this.options.event, this.pagerLinksHandler);
    },
    prevSlide: function() {
      if(!(this.options.disableWhileAnimating && this.galleryAnimating)) {
        if(this.currentStep > 0) {
          this.currentStep--;
          this.switchSlide();
        } else if(this.options.circularRotation) {
          this.currentStep = this.stepsCount - 1;
          this.switchSlide();
        }
      }
    },
    nextSlide: function(fromAutoRotation) {
      if(!(this.options.disableWhileAnimating && this.galleryAnimating)) {
        if(this.currentStep < this.stepsCount - 1) {
          this.currentStep++;
          this.switchSlide();
        } else if(this.options.circularRotation || fromAutoRotation === true) {
          this.currentStep = 0;
          this.switchSlide();
        }
      }
    },
    numSlide: function(c) {
      if(this.currentStep != c) {
        this.currentStep = c;
        this.switchSlide();
      }
    },
    switchSlide: function() {
      var self = this;
      this.galleryAnimating = true;
      this.tmpProps = {};
      this.tmpProps[this.animProperty] = this.getStepOffset();
      this.slider.stop().animate(this.tmpProps, {duration: this.options.animSpeed, complete: function(){
        // animation complete
        self.galleryAnimating = false;
        if(self.resizeQueue) {
          self.onWindowResize();
        }

        // onchange callback
        self.makeCallback('onChange', self);
        self.autoRotate();
      }});
      this.refreshState();

      // onchange callback
      this.makeCallback('onBeforeChange', this);
    },
    refreshState: function(initial) {
      if(this.options.step === 1 || this.stepsCount === this.slides.length) {
        this.slides.removeClass(this.options.activeClass).eq(this.currentStep).addClass(this.options.activeClass);
      }
      this.pagerLinks.removeClass(this.options.activeClass).eq(this.currentStep).addClass(this.options.activeClass);
      this.curNum.html(this.currentStep+1);
      this.allNum.html(this.stepsCount);

      // initial refresh
      if(this.options.maskAutoSize && typeof this.options.step === 'number') {
        this.tmpProps = {};
        this.tmpProps[this.maskSizeProperty] = this.slides.eq(Math.min(this.currentStep,this.slides.length-1))[this.slideSizeFunction](true);
        this.mask.stop()[initial ? 'css' : 'animate'](this.tmpProps);
      }

      // disabled state
      if(!this.options.circularRotation) {
        this.btnPrev.add(this.btnNext).removeClass(this.options.disabledClass);
        if(this.currentStep === 0) this.btnPrev.addClass(this.options.disabledClass);
        if(this.currentStep === this.stepsCount - 1) this.btnNext.addClass(this.options.disabledClass);
      }
    },
    startRotation: function() {
      this.options.autoRotation = true;
      this.galleryHover = false;
      this.autoRotationStopped = false;
      this.resumeRotation();
    },
    stopRotation: function() {
      this.galleryHover = true;
      this.autoRotationStopped = true;
      this.pauseRotation();
    },
    pauseRotation: function() {
      this.gallery.addClass(this.options.autorotationDisabledClass);
      this.gallery.removeClass(this.options.autorotationActiveClass);
      clearTimeout(this.timer);
    },
    resumeRotation: function() {
      if(!this.autoRotationStopped) {
        this.gallery.addClass(this.options.autorotationActiveClass);
        this.gallery.removeClass(this.options.autorotationDisabledClass);
        this.autoRotate();
      }
    },
    autoRotate: function() {
      var self = this;
      clearTimeout(this.timer);
      if(this.options.autoRotation && !this.galleryHover && !this.autoRotationStopped) {
        this.timer = setTimeout(function(){
          self.nextSlide(true);
        }, this.options.switchTime);
      } else {
        this.pauseRotation();
      }
    },
    bindHandlers: function(handlersList) {
      var self = this;
      $.each(handlersList, function(index, handler) {
        var origHandler = self[handler];
        self[handler] = function() {
          return origHandler.apply(self, arguments);
        };
      });
    },
    makeCallback: function(name) {
      if(typeof this.options[name] === 'function') {
        var args = Array.prototype.slice.call(arguments);
        args.shift();
        this.options[name].apply(this, args);
      }
    },
    destroy: function() {
      // destroy handler
      $(window).unbind('load resize orientationchange', this.onWindowResize);
      this.btnPrev.unbind(this.options.event, this.prevSlideHandler);
      this.btnNext.unbind(this.options.event, this.nextSlideHandler);
      this.pagerLinks.unbind(this.options.event, this.pagerLinksHandler);
      this.gallery.unbind({mouseenter: this.hoverHandler, mouseleave: this.leaveHandler});

      // autorotation buttons handlers
      this.stopRotation();
      this.btnPlay.unbind(this.options.event, this.btnPlayHandler);
      this.btnPause.unbind(this.options.event, this.btnPauseHandler);
      this.btnPlayPause.unbind(this.options.event, this.btnPlayPauseHandler);

      // destroy swipe handler
      if(this.options.handleTouch && $.fn.hammer) {
        this.mask.hammer().off('touch release dragup dragdown dragleft dragright swipeup swipedown swipeleft swiperight');
      }

      // remove inline styles, classes and pagination
      var unneededClasses = [this.options.galleryReadyClass, this.options.autorotationActiveClass, this.options.autorotationDisabledClass];
      this.gallery.removeClass(unneededClasses.join(' '));
      this.slider.add(this.slides).removeAttr('style');
      if(typeof this.options.generatePagination === 'string') {
        this.pagerHolder.empty();
      }
    }
  };

  // detect device type
  var isTouchDevice = /MSIE 10.*Touch/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;

  // jquery plugin
  $.fn.scrollGallery = function(opt){
    return this.each(function(){
      $(this).data('ScrollGallery', new ScrollGallery($.extend(opt,{holder:this})));
    });
  };
}(jQuery));
/*
 * jQuery Cycle Carousel plugin
 */
;(function($){
  function ScrollAbsoluteGallery(options) {
    this.options = $.extend({
      activeClass: 'active',
      mask: 'div.slides-mask',
      slider: '>ul',
      slides: '>li',
      btnPrev: '.btn-prev',
      btnNext: '.btn-next',
      pagerLinks: 'ul.pager > li',
      generatePagination: false,
      pagerList: '<ul>',
      pagerListItem: '<li><a href="#"></a></li>',
      pagerListItemText: 'a',
      galleryReadyClass: 'gallery-js-ready',
      currentNumber: 'span.current-num',
      totalNumber: 'span.total-num',
      maskAutoSize: false,
      autoRotation: false,
      pauseOnHover: false,
      stretchSlideToMask: false,
      switchTime: 3000,
      animSpeed: 500,
      handleTouch: true,
      swipeThreshold: 15,
      vertical: false
    }, options);
    this.init();
  }
  ScrollAbsoluteGallery.prototype = {
    init: function() {
      if(this.options.holder) {
        this.findElements();
        this.attachEvents();
      }
    },
    findElements: function() {
      // find structure elements
      this.holder = $(this.options.holder).addClass(this.options.galleryReadyClass);
      this.mask = this.holder.find(this.options.mask);
      this.slider = this.mask.find(this.options.slider);
      this.slides = this.slider.find(this.options.slides);
      this.btnPrev = this.holder.find(this.options.btnPrev);
      this.btnNext = this.holder.find(this.options.btnNext);

      // slide count display
      this.currentNumber = this.holder.find(this.options.currentNumber);
      this.totalNumber = this.holder.find(this.options.totalNumber);

      // create gallery pagination
      if(typeof this.options.generatePagination === 'string') {
        this.pagerLinks = this.buildPagination();
      } else {
        this.pagerLinks = this.holder.find(this.options.pagerLinks);
      }

      // define index variables
      this.sizeProperty = this.options.vertical ? 'height' : 'width';
      this.positionProperty = this.options.vertical ? 'top' : 'left';
      this.animProperty = this.options.vertical ? 'marginTop' : 'marginLeft';

      this.slideSize = this.slides[this.sizeProperty]();
      this.currentIndex = 0;
      this.prevIndex = 0;

      // reposition elements
      this.options.maskAutoSize = this.options.vertical ? false : this.options.maskAutoSize;
      if(this.options.vertical) {
        this.mask.css({
          height: this.slides.innerHeight()
        });
      }
      if(this.options.maskAutoSize){
        this.mask.css({
          height: this.slider.height()
        });
      }
      this.slider.css({
        position: 'relative',
        height: this.options.vertical ? this.slideSize * this.slides.length : '100%'
      });
      this.slides.css({
        position: 'absolute'
      }).css(this.positionProperty, -9999).eq(this.currentIndex).css(this.positionProperty, 0);
      this.refreshState();
    },
    buildPagination: function() {
      var pagerLinks = $();
      if(!this.pagerHolder) {
        this.pagerHolder = this.holder.find(this.options.generatePagination);
      }
      if(this.pagerHolder.length) {
        this.pagerHolder.empty();
        this.pagerList = $(this.options.pagerList).appendTo(this.pagerHolder);
        for(var i = 0; i < this.slides.length; i++) {
          $(this.options.pagerListItem).appendTo(this.pagerList).find(this.options.pagerListItemText).text(i+1);
        }
        pagerLinks = this.pagerList.children();
      }
      return pagerLinks;
    },
    attachEvents: function() {
      // attach handlers
      var self = this;
      if(this.btnPrev.length) {
        this.btnPrevHandler = function(e) {
          e.preventDefault();
          self.prevSlide();
        };
        this.btnPrev.click(this.btnPrevHandler);
      }
      if(this.btnNext.length) {
        this.btnNextHandler = function(e) {
          e.preventDefault();
          self.nextSlide();
        };
        this.btnNext.click(this.btnNextHandler);
      }
      if(this.pagerLinks.length) {
        this.pagerLinksHandler = function(e) {
          e.preventDefault();
          self.numSlide(self.pagerLinks.index(e.currentTarget));
        };
        this.pagerLinks.click(this.pagerLinksHandler);
      }

      // handle autorotation pause on hover
      if(this.options.pauseOnHover) {
        this.hoverHandler = function() {
          clearTimeout(self.timer);
        };
        this.leaveHandler = function() {
          self.autoRotate();
        };
        this.holder.bind({mouseenter: this.hoverHandler, mouseleave: this.leaveHandler});
      }

      // handle holder and slides dimensions
      this.resizeHandler = function() {
        if(!self.animating) {
          if(self.options.stretchSlideToMask) {
            self.resizeSlides();
          }
          self.resizeHolder();
          self.setSlidesPosition(self.currentIndex);
        }
      };
      $(window).bind('load resize orientationchange', this.resizeHandler);
      if(self.options.stretchSlideToMask) {
        self.resizeSlides();
      }

      // handle swipe on mobile devices
      if(this.options.handleTouch && jQuery.fn.hammer && this.slides.length > 1 && isTouchDevice) {
        this.mask.hammer({
          drag_block_horizontal: self.options.vertical ? false : true,
          drag_block_vertical: self.options.vertical ? true : false,
          drag_min_distance: 1
        }).on('touch release ' + (this.options.vertical ? 'swipeup swipedown dragup dragdown' : 'swipeleft swiperight dragleft dragright'), function(ev){
            switch(ev.type) {
              case (self.options.vertical ? 'dragup' : 'dragright'):
              case (self.options.vertical ? 'dragdown' : 'dragleft'):
                if(!self.animating){
                  self.swipeOffset = -self.slideSize + ev.gesture[self.options.vertical ? 'deltaY' : 'deltaX'];
                  self.slider.css(self.animProperty, self.swipeOffset);
                  clearTimeout(self.timer);
                }
                ev.gesture.preventDefault();
                break;
              case (self.options.vertical ? 'swipeup' : 'swipeleft'):
                if(!self.animating){
                  self.nextSlide();
                  self.swipeOffset = 0;
                }
                ev.gesture.stopDetect();
                break;
              case (self.options.vertical ? 'swipedown' : 'swiperight'):
                if(!self.animating){
                  self.prevSlide();
                  self.swipeOffset = 0;
                }
                ev.gesture.stopDetect();
                break;
              case 'release':
                if(Math.abs(ev.gesture[self.options.vertical ? 'deltaY' : 'deltaX']) > self.options.swipeThreshold) {
                  if(self.options.vertical){
                    if(ev.gesture.direction == 'down') {
                      self.prevSlide();
                    } else if(ev.gesture.direction == 'up') {
                      self.nextSlide();
                    }
                  }
                  else {
                    if(ev.gesture.direction == 'right') {
                      self.prevSlide();
                    } else if(ev.gesture.direction == 'left') {
                      self.nextSlide();
                    }
                  }
                }
                else {
                  var tmpObj = {};
                  tmpObj[self.animProperty] = -self.slideSize;
                  self.slider.animate(tmpObj, {duration: self.options.animSpeed});
                }
                self.swipeOffset = 0;
                break;
            }
          });
      }

      // start autorotation
      this.autoRotate();
      this.resizeHolder();
      this.setSlidesPosition(this.currentIndex);
    },
    resizeSlides: function() {
      this.slideSize = this.mask[this.options.vertical ? 'height' : 'width']();
      this.slides.css(this.sizeProperty, this.slideSize);
    },
    resizeHolder: function() {
      if(this.options.maskAutoSize) {
        this.mask.css({
          height: this.slides.eq(this.currentIndex).outerHeight(true)
        });
      }
    },
    prevSlide: function() {
      if(!this.animating && this.slides.length > 1) {
        this.direction = -1;
        this.prevIndex = this.currentIndex;
        if(this.currentIndex > 0) this.currentIndex--;
        else this.currentIndex = this.slides.length - 1;
        this.switchSlide();
      }
    },
    nextSlide: function(fromAutoRotation) {
      if(!this.animating && this.slides.length > 1) {
        this.direction = 1;
        this.prevIndex = this.currentIndex;
        if(this.currentIndex < this.slides.length - 1) this.currentIndex++;
        else this.currentIndex = 0;
        this.switchSlide();
      }
    },
    numSlide: function(c) {
      if(!this.animating && this.currentIndex !== c && this.slides.length > 1) {
        this.direction = c > this.currentIndex ? 1 : -1;
        this.prevIndex = this.currentIndex;
        this.currentIndex = c;
        this.switchSlide();
      }
    },
    preparePosition: function() {
      // prepare slides position before animation
      this.setSlidesPosition(this.prevIndex, this.direction < 0 ? this.currentIndex : null, this.direction > 0 ? this.currentIndex : null, this.direction);
    },
    setSlidesPosition: function(index, slideLeft, slideRight, direction) {
      // reposition holder and nearest slides
      if(this.slides.length > 1) {
        var prevIndex = (typeof slideLeft === 'number' ? slideLeft : index > 0 ? index - 1 : this.slides.length - 1);
        var nextIndex = (typeof slideRight === 'number' ? slideRight : index < this.slides.length - 1 ? index + 1 : 0);

        this.slider.css(this.animProperty, this.swipeOffset ? this.swipeOffset : -this.slideSize);
        this.slides.css(this.positionProperty, -9999).eq(index).css(this.positionProperty, this.slideSize);
        if(prevIndex === nextIndex && typeof direction === 'number') {
          var calcOffset = direction > 0 ? this.slideSize*2 : 0;
          this.slides.eq(nextIndex).css(this.positionProperty, calcOffset);
        } else {
          this.slides.eq(prevIndex).css(this.positionProperty, 0);
          this.slides.eq(nextIndex).css(this.positionProperty, this.slideSize*2);
        }
      }
    },
    switchSlide: function() {
      // prepare positions and calculate offset
      var self = this;
      var oldSlide = this.slides.eq(this.prevIndex);
      var newSlide = this.slides.eq(this.currentIndex);
      this.animating = true;

      // resize mask to fit slide
      if(this.options.maskAutoSize) {
        this.mask.animate({
          height: newSlide.outerHeight(true)
        }, {
          duration: this.options.animSpeed
        });
      }

      // start animation
      var animProps = {};
      animProps[this.animProperty] = this.direction > 0 ? -this.slideSize*2 : 0;
      this.preparePosition();
      this.slider.animate(animProps,{duration:this.options.animSpeed, complete:function() {
        self.setSlidesPosition(self.currentIndex);

        // start autorotation
        self.animating = false;
        self.autoRotate();
      }});

      // refresh classes
      this.refreshState();
    },
    refreshState: function(initial) {
      // slide change function
      this.slides.removeClass(this.options.activeClass).eq(this.currentIndex).addClass(this.options.activeClass);
      this.pagerLinks.removeClass(this.options.activeClass).eq(this.currentIndex).addClass(this.options.activeClass);

      // display current slide number
      this.currentNumber.html(this.currentIndex + 1);
      this.totalNumber.html(this.slides.length);
    },
    autoRotate: function() {
      var self = this;
      clearTimeout(this.timer);
      if(this.options.autoRotation) {
        this.timer = setTimeout(function() {
          self.nextSlide();
        }, this.options.switchTime);
      }
    },
    destroy: function() {
      // destroy handler
      this.btnPrev.unbind('click', this.btnPrevHandler);
      this.btnNext.unbind('click', this.btnNextHandler);
      this.pagerLinks.unbind('click', this.pagerLinksHandler);
      this.holder.unbind({mouseenter: this.hoverHandler, mouseleave: this.leaveHandler});
      $(window).unbind('load resize orientationchange', this.resizeHandler);
      clearTimeout(this.timer);

      // destroy swipe handler
      if(this.options.handleTouch && $.fn.hammer) {
        this.mask.hammer().off('touch release swipeleft swiperight swipeup swipedown dragup dragdown dragleft dragright');
      }

      // remove inline styles, classes and pagination
      this.holder.removeClass(this.options.galleryReadyClass);
      this.slider.add(this.slides).removeAttr('style');
      if(typeof this.options.generatePagination === 'string') {
        this.pagerHolder.empty();
      }
    }
  };

  // detect device type
  var isTouchDevice = /MSIE 10.*Touch/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;

  // jquery plugin
  $.fn.scrollAbsoluteGallery = function(opt){
    return this.each(function(){
      $(this).data('ScrollAbsoluteGallery', new ScrollAbsoluteGallery($.extend(opt,{holder:this})));
    });
  };
}(jQuery));

/*
 * jQuery SlideShow plugin
 */
;(function($){
  function FadeGallery(options) {
    this.options = $.extend({
      slides: 'ul.slideset > li',
      activeClass:'active',
      disabledClass:'disabled',
      btnPrev: 'a.btn-prev',
      btnNext: 'a.btn-next',
      generatePagination: false,
      pagerList: '<ul>',
      pagerListItem: '<li><a href="#"></a></li>',
      pagerListItemText: 'a',
      pagerLinks: '.pagination li',
      currentNumber: 'span.current-num',
      totalNumber: 'span.total-num',
      btnPlay: '.btn-play',
      btnPause: '.btn-pause',
      btnPlayPause: '.btn-play-pause',
      galleryReadyClass: 'gallery-js-ready',
      autorotationActiveClass: 'autorotation-active',
      autorotationDisabledClass: 'autorotation-disabled',
      autorotationStopAfterClick: false,
      circularRotation: true,
      switchSimultaneously: true,
      disableWhileAnimating: false,
      disableFadeIE: false,
      autoRotation: false,
      pauseOnHover: true,
      autoHeight: false,
      useSwipe: false,
      swipeThreshold: 15,
      switchTime: 4000,
      animSpeed: 600,
      event:'click'
    }, options);
    this.init();
  }
  FadeGallery.prototype = {
    init: function() {
      if(this.options.holder) {
        this.findElements();
        this.attachEvents();
        this.refreshState(true);
        this.autoRotate();
        this.makeCallback('onInit', this);
      }
    },
    findElements: function() {
      // control elements
      this.gallery = $(this.options.holder).addClass(this.options.galleryReadyClass);
      this.slides = this.gallery.find(this.options.slides);
      this.slidesHolder = this.slides.eq(0).parent();
      this.stepsCount = this.slides.length;
      this.btnPrev = this.gallery.find(this.options.btnPrev);
      this.btnNext = this.gallery.find(this.options.btnNext);
      this.currentIndex = 0;

      // disable fade effect in old IE
      if(this.options.disableFadeIE && !$.support.opacity) {
        this.options.animSpeed = 0;
      }

      // create gallery pagination
      if(typeof this.options.generatePagination === 'string') {
        this.pagerHolder = this.gallery.find(this.options.generatePagination).empty();
        this.pagerList = $(this.options.pagerList).appendTo(this.pagerHolder);
        for(var i = 0; i < this.stepsCount; i++) {
          $(this.options.pagerListItem).appendTo(this.pagerList).find(this.options.pagerListItemText).text(i+1);
        }
        this.pagerLinks = this.pagerList.children();
      } else {
        this.pagerLinks = this.gallery.find(this.options.pagerLinks);
      }

      // get start index
      var activeSlide = this.slides.filter('.'+this.options.activeClass);
      if(activeSlide.length) {
        this.currentIndex = this.slides.index(activeSlide);
      }
      this.prevIndex = this.currentIndex;

      // autorotation control buttons
      this.btnPlay = this.gallery.find(this.options.btnPlay);
      this.btnPause = this.gallery.find(this.options.btnPause);
      this.btnPlayPause = this.gallery.find(this.options.btnPlayPause);

      // misc elements
      this.curNum = this.gallery.find(this.options.currentNumber);
      this.allNum = this.gallery.find(this.options.totalNumber);

      // handle flexible layout
      this.slides.css({display:'block',opacity:0}).eq(this.currentIndex).css({
        opacity:''
      });
    },
    attachEvents: function() {
      var self = this;

      // flexible layout handler
      this.resizeHandler = function() {
        self.onWindowResize();
      };
      $(window).bind('load resize orientationchange', this.resizeHandler);

      if(this.btnPrev.length) {
        this.btnPrevHandler = function(e){
          e.preventDefault();
          self.prevSlide();
          if(self.options.autorotationStopAfterClick) {
            self.stopRotation();
          }
        };
        this.btnPrev.bind(this.options.event, this.btnPrevHandler);
      }
      if(this.btnNext.length) {
        this.btnNextHandler = function(e) {
          e.preventDefault();
          self.nextSlide();
          if(self.options.autorotationStopAfterClick) {
            self.stopRotation();
          }
        };
        this.btnNext.bind(this.options.event, this.btnNextHandler);
      }
      if(this.pagerLinks.length) {
        this.pagerLinksHandler = function(e) {
          e.preventDefault();
          self.numSlide(self.pagerLinks.index(e.currentTarget));
          if(self.options.autorotationStopAfterClick) {
            self.stopRotation();
          }
        };
        this.pagerLinks.bind(self.options.event, this.pagerLinksHandler);
      }

      // autorotation buttons handler
      if(this.btnPlay.length) {
        this.btnPlayHandler = function(e) {
          e.preventDefault();
          self.startRotation();
        };
        this.btnPlay.bind(this.options.event, this.btnPlayHandler);
      }
      if(this.btnPause.length) {
        this.btnPauseHandler = function(e) {
          e.preventDefault();
          self.stopRotation();
        };
        this.btnPause.bind(this.options.event, this.btnPauseHandler);
      }
      if(this.btnPlayPause.length) {
        this.btnPlayPauseHandler = function(e){
          e.preventDefault();
          if(!self.gallery.hasClass(self.options.autorotationActiveClass)) {
            self.startRotation();
          } else {
            self.stopRotation();
          }
        };
        this.btnPlayPause.bind(this.options.event, this.btnPlayPauseHandler);
      }

      // swipe gestures handler
      if(this.options.useSwipe && $.fn.hammer && isTouchDevice) {
        this.gallery.hammer({
          drag_block_horizontal: true,
          drag_min_distance: 1
        }).on('release dragleft dragright swipeleft swiperight', function(ev){
            switch(ev.type) {
              case 'dragright':
              case 'dragleft':
                ev.gesture.preventDefault();
                break;
              case 'swipeleft':
                self.nextSlide();
                ev.gesture.stopDetect();
                break;
              case 'swiperight':
                self.prevSlide();
                ev.gesture.stopDetect();
                break;
              case 'release':
                if(Math.abs(ev.gesture[self.options.vertical ? 'deltaY' : 'deltaX']) > self.options.swipeThreshold) {
                  if(ev.gesture.direction == 'right') self.prevSlide(); else if(ev.gesture.direction == 'left') self.nextSlide();
                }
                break;
            }
          });
      }

      // pause on hover handling
      if(this.options.pauseOnHover) {
        this.hoverHandler = function() {
          if(self.options.autoRotation) {
            self.galleryHover = true;
            self.pauseRotation();
          }
        };
        this.leaveHandler = function() {
          if(self.options.autoRotation) {
            self.galleryHover = false;
            self.resumeRotation();
          }
        };
        this.gallery.bind({mouseenter: this.hoverHandler, mouseleave: this.leaveHandler});
      }
    },
    onWindowResize: function(){
      if(this.options.autoHeight) {
        this.slidesHolder.css({height: this.slides.eq(this.currentIndex).outerHeight(true) });
      }
    },
    prevSlide: function() {
      if(!(this.options.disableWhileAnimating && this.galleryAnimating)) {
        this.prevIndex = this.currentIndex;
        if(this.currentIndex > 0) {
          this.currentIndex--;
          this.switchSlide();
        } else if(this.options.circularRotation) {
          this.currentIndex = this.stepsCount - 1;
          this.switchSlide();
        }
      }
    },
    nextSlide: function(fromAutoRotation) {
      if(!(this.options.disableWhileAnimating && this.galleryAnimating)) {
        this.prevIndex = this.currentIndex;
        if(this.currentIndex < this.stepsCount - 1) {
          this.currentIndex++;
          this.switchSlide();
        } else if(this.options.circularRotation || fromAutoRotation === true) {
          this.currentIndex = 0;
          this.switchSlide();
        }
      }
    },
    numSlide: function(c) {
      if(this.currentIndex != c) {
        this.prevIndex = this.currentIndex;
        this.currentIndex = c;
        this.switchSlide();
      }
    },
    switchSlide: function() {
      var self = this;
      if(this.slides.length > 1) {
        this.galleryAnimating = true;
        if(!this.options.animSpeed) {
          this.slides.eq(this.prevIndex).css({opacity:0});
        } else {
          this.slides.eq(this.prevIndex).stop().animate({opacity:0},{duration: this.options.animSpeed});
        }

        this.switchNext = function() {
          if(!self.options.animSpeed) {
            self.slides.eq(self.currentIndex).css({opacity:''});
          } else {
            self.slides.eq(self.currentIndex).stop().animate({opacity:1},{duration: self.options.animSpeed});
          }
          setTimeout(function() {
            self.slides.eq(self.currentIndex).css({opacity:''});
            self.galleryAnimating = false;
            self.autoRotate();

            // onchange callback
            self.makeCallback('onChange', self);
          }, self.options.animSpeed);
        };

        if(this.options.switchSimultaneously) {
          self.switchNext();
        } else {
          clearTimeout(this.switchTimer);
          this.switchTimer = setTimeout(function(){
            self.switchNext();
          }, this.options.animSpeed);
        }
        this.refreshState();

        // onchange callback
        this.makeCallback('onBeforeChange', this);
      }
    },
    refreshState: function(initial) {
      this.slides.removeClass(this.options.activeClass).eq(this.currentIndex).addClass(this.options.activeClass);
      this.pagerLinks.removeClass(this.options.activeClass).eq(this.currentIndex).addClass(this.options.activeClass);
      this.curNum.html(this.currentIndex+1);
      this.allNum.html(this.stepsCount);

      // initial refresh
      if(this.options.autoHeight) {
        if(initial) {
          this.slidesHolder.css({height: this.slides.eq(this.currentIndex).outerHeight(true) });
        } else {
          this.slidesHolder.stop().animate({height: this.slides.eq(this.currentIndex).outerHeight(true)}, {duration: this.options.animSpeed});
        }
      }

      // disabled state
      if(!this.options.circularRotation) {
        this.btnPrev.add(this.btnNext).removeClass(this.options.disabledClass);
        if(this.currentIndex === 0) this.btnPrev.addClass(this.options.disabledClass);
        if(this.currentIndex === this.stepsCount - 1) this.btnNext.addClass(this.options.disabledClass);
      }
    },
    startRotation: function() {
      this.options.autoRotation = true;
      this.galleryHover = false;
      this.autoRotationStopped = false;
      this.resumeRotation();
    },
    stopRotation: function() {
      this.galleryHover = true;
      this.autoRotationStopped = true;
      this.pauseRotation();
    },
    pauseRotation: function() {
      this.gallery.addClass(this.options.autorotationDisabledClass);
      this.gallery.removeClass(this.options.autorotationActiveClass);
      clearTimeout(this.timer);
    },
    resumeRotation: function() {
      if(!this.autoRotationStopped) {
        this.gallery.addClass(this.options.autorotationActiveClass);
        this.gallery.removeClass(this.options.autorotationDisabledClass);
        this.autoRotate();
      }
    },
    autoRotate: function() {
      var self = this;
      clearTimeout(this.timer);
      if(this.options.autoRotation && !this.galleryHover && !this.autoRotationStopped) {
        this.gallery.addClass(this.options.autorotationActiveClass);
        this.timer = setTimeout(function(){
          self.nextSlide(true);
        }, this.options.switchTime);
      } else {
        this.pauseRotation();
      }
    },
    makeCallback: function(name) {
      if(typeof this.options[name] === 'function') {
        var args = Array.prototype.slice.call(arguments);
        args.shift();
        this.options[name].apply(this, args);
      }
    },
    destroy: function() {
      // navigation buttons handler
      this.btnPrev.unbind(this.options.event, this.btnPrevHandler);
      this.btnNext.unbind(this.options.event, this.btnNextHandler);
      this.pagerLinks.unbind(this.options.event, this.pagerLinksHandler);
      $(window).unbind('load resize orientationchange', this.resizeHandler);

      // remove autorotation handlers
      this.stopRotation();
      this.btnPlay.unbind(this.options.event, this.btnPlayHandler);
      this.btnPause.unbind(this.options.event, this.btnPauseHandler);
      this.btnPlayPause.unbind(this.options.event, this.btnPlayPauseHandler);
      this.gallery.bind({mouseenter: this.hoverHandler, mouseleave: this.leaveHandler});

      // remove swipe handler if used
      if(this.options.useSwipe && $.fn.hammer) {
        this.gallery.hammer().off('release dragleft dragright swipeleft swiperight');
      }
      if(typeof this.options.generatePagination === 'string') {
        this.pagerHolder.empty();
      }

      // remove unneeded classes and styles
      var unneededClasses = [this.options.galleryReadyClass, this.options.autorotationActiveClass, this.options.autorotationDisabledClass];
      this.gallery.removeClass(unneededClasses.join(' '));
      this.slidesHolder.add(this.slides).removeAttr('style');
    }
  };

  // detect device type
  var isTouchDevice = /MSIE 10.*Touch/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;

  // jquery plugin
  $.fn.fadeGallery = function(opt){
    return this.each(function(){
      $(this).data('FadeGallery', new FadeGallery($.extend(opt,{holder:this})));
    });
  };
}(jQuery));

/*! Hammer.JS - v1.0.5 - 2013-04-07
 * http://eightmedia.github.com/hammer.js
 *
 * Copyright (c) 2013 Jorik Tangelder <j.tangelder@gmail.com>;
 * Licensed under the MIT license */
;(function(t,e){"use strict";function n(){if(!i.READY){i.event.determineEventTypes();for(var t in i.gestures)i.gestures.hasOwnProperty(t)&&i.detection.register(i.gestures[t]);i.event.onTouch(i.DOCUMENT,i.EVENT_MOVE,i.detection.detect),i.event.onTouch(i.DOCUMENT,i.EVENT_END,i.detection.detect),i.READY=!0}}var i=function(t,e){return new i.Instance(t,e||{})};i.defaults={stop_browser_behavior:{userSelect:"none",touchAction:"none",touchCallout:"none",contentZooming:"none",userDrag:"none",tapHighlightColor:"rgba(0,0,0,0)"}},i.HAS_POINTEREVENTS=navigator.pointerEnabled||navigator.msPointerEnabled,i.HAS_TOUCHEVENTS="ontouchstart"in t,i.MOBILE_REGEX=/mobile|tablet|ip(ad|hone|od)|android/i,i.NO_MOUSEEVENTS=i.HAS_TOUCHEVENTS&&navigator.userAgent.match(i.MOBILE_REGEX),i.EVENT_TYPES={},i.DIRECTION_DOWN="down",i.DIRECTION_LEFT="left",i.DIRECTION_UP="up",i.DIRECTION_RIGHT="right",i.POINTER_MOUSE="mouse",i.POINTER_TOUCH="touch",i.POINTER_PEN="pen",i.EVENT_START="start",i.EVENT_MOVE="move",i.EVENT_END="end",i.DOCUMENT=document,i.plugins={},i.READY=!1,i.Instance=function(t,e){var r=this;return n(),this.element=t,this.enabled=!0,this.options=i.utils.extend(i.utils.extend({},i.defaults),e||{}),this.options.stop_browser_behavior&&i.utils.stopDefaultBrowserBehavior(this.element,this.options.stop_browser_behavior),i.event.onTouch(t,i.EVENT_START,function(t){r.enabled&&i.detection.startDetect(r,t)}),this},i.Instance.prototype={on:function(t,e){for(var n=t.split(" "),i=0;n.length>i;i++)this.element.addEventListener(n[i],e,!1);return this},off:function(t,e){for(var n=t.split(" "),i=0;n.length>i;i++)this.element.removeEventListener(n[i],e,!1);return this},trigger:function(t,e){var n=i.DOCUMENT.createEvent("Event");n.initEvent(t,!0,!0),n.gesture=e;var r=this.element;return i.utils.hasParent(e.target,r)&&(r=e.target),r.dispatchEvent(n),this},enable:function(t){return this.enabled=t,this}};var r=null,o=!1,s=!1;i.event={bindDom:function(t,e,n){for(var i=e.split(" "),r=0;i.length>r;r++)t.addEventListener(i[r],n,!1)},onTouch:function(t,e,n){var a=this;this.bindDom(t,i.EVENT_TYPES[e],function(c){var u=c.type.toLowerCase();if(!u.match(/mouse/)||!s){(u.match(/touch/)||u.match(/pointerdown/)||u.match(/mouse/)&&1===c.which)&&(o=!0),u.match(/touch|pointer/)&&(s=!0);var h=0;o&&(i.HAS_POINTEREVENTS&&e!=i.EVENT_END?h=i.PointerEvent.updatePointer(e,c):u.match(/touch/)?h=c.touches.length:s||(h=u.match(/up/)?0:1),h>0&&e==i.EVENT_END?e=i.EVENT_MOVE:h||(e=i.EVENT_END),h||null===r?r=c:c=r,n.call(i.detection,a.collectEventData(t,e,c)),i.HAS_POINTEREVENTS&&e==i.EVENT_END&&(h=i.PointerEvent.updatePointer(e,c))),h||(r=null,o=!1,s=!1,i.PointerEvent.reset())}})},determineEventTypes:function(){var t;t=i.HAS_POINTEREVENTS?i.PointerEvent.getEvents():i.NO_MOUSEEVENTS?["touchstart","touchmove","touchend touchcancel"]:["touchstart mousedown","touchmove mousemove","touchend touchcancel mouseup"],i.EVENT_TYPES[i.EVENT_START]=t[0],i.EVENT_TYPES[i.EVENT_MOVE]=t[1],i.EVENT_TYPES[i.EVENT_END]=t[2]},getTouchList:function(t){return i.HAS_POINTEREVENTS?i.PointerEvent.getTouchList():t.touches?t.touches:[{identifier:1,pageX:t.pageX,pageY:t.pageY,target:t.target}]},collectEventData:function(t,e,n){var r=this.getTouchList(n,e),o=i.POINTER_TOUCH;return(n.type.match(/mouse/)||i.PointerEvent.matchType(i.POINTER_MOUSE,n))&&(o=i.POINTER_MOUSE),{center:i.utils.getCenter(r),timeStamp:(new Date).getTime(),target:n.target,touches:r,eventType:e,pointerType:o,srcEvent:n,preventDefault:function(){this.srcEvent.preventManipulation&&this.srcEvent.preventManipulation(),this.srcEvent.preventDefault&&this.srcEvent.preventDefault()},stopPropagation:function(){this.srcEvent.stopPropagation()},stopDetect:function(){return i.detection.stopDetect()}}}},i.PointerEvent={pointers:{},getTouchList:function(){var t=this,e=[];return Object.keys(t.pointers).sort().forEach(function(n){e.push(t.pointers[n])}),e},updatePointer:function(t,e){return t==i.EVENT_END?this.pointers={}:(e.identifier=e.pointerId,this.pointers[e.pointerId]=e),Object.keys(this.pointers).length},matchType:function(t,e){if(!e.pointerType)return!1;var n={};return n[i.POINTER_MOUSE]=e.pointerType==e.MSPOINTER_TYPE_MOUSE||e.pointerType==i.POINTER_MOUSE,n[i.POINTER_TOUCH]=e.pointerType==e.MSPOINTER_TYPE_TOUCH||e.pointerType==i.POINTER_TOUCH,n[i.POINTER_PEN]=e.pointerType==e.MSPOINTER_TYPE_PEN||e.pointerType==i.POINTER_PEN,n[t]},getEvents:function(){return["pointerdown MSPointerDown","pointermove MSPointerMove","pointerup pointercancel MSPointerUp MSPointerCancel"]},reset:function(){this.pointers={}}},i.utils={extend:function(t,n,i){for(var r in n)t[r]!==e&&i||(t[r]=n[r]);return t},hasParent:function(t,e){for(;t;){if(t==e)return!0;t=t.parentNode}return!1},getCenter:function(t){for(var e=[],n=[],i=0,r=t.length;r>i;i++)e.push(t[i].pageX),n.push(t[i].pageY);return{pageX:(Math.min.apply(Math,e)+Math.max.apply(Math,e))/2,pageY:(Math.min.apply(Math,n)+Math.max.apply(Math,n))/2}},getVelocity:function(t,e,n){return{x:Math.abs(e/t)||0,y:Math.abs(n/t)||0}},getAngle:function(t,e){var n=e.pageY-t.pageY,i=e.pageX-t.pageX;return 180*Math.atan2(n,i)/Math.PI},getDirection:function(t,e){var n=Math.abs(t.pageX-e.pageX),r=Math.abs(t.pageY-e.pageY);return n>=r?t.pageX-e.pageX>0?i.DIRECTION_LEFT:i.DIRECTION_RIGHT:t.pageY-e.pageY>0?i.DIRECTION_UP:i.DIRECTION_DOWN},getDistance:function(t,e){var n=e.pageX-t.pageX,i=e.pageY-t.pageY;return Math.sqrt(n*n+i*i)},getScale:function(t,e){return t.length>=2&&e.length>=2?this.getDistance(e[0],e[1])/this.getDistance(t[0],t[1]):1},getRotation:function(t,e){return t.length>=2&&e.length>=2?this.getAngle(e[1],e[0])-this.getAngle(t[1],t[0]):0},isVertical:function(t){return t==i.DIRECTION_UP||t==i.DIRECTION_DOWN},stopDefaultBrowserBehavior:function(t,e){var n,i=["webkit","khtml","moz","ms","o",""];if(e&&t.style){for(var r=0;i.length>r;r++)for(var o in e)e.hasOwnProperty(o)&&(n=o,i[r]&&(n=i[r]+n.substring(0,1).toUpperCase()+n.substring(1)),t.style[n]=e[o]);"none"==e.userSelect&&(t.onselectstart=function(){return!1})}}},i.detection={gestures:[],current:null,previous:null,stopped:!1,startDetect:function(t,e){this.current||(this.stopped=!1,this.current={inst:t,startEvent:i.utils.extend({},e),lastEvent:!1,name:""},this.detect(e))},detect:function(t){if(this.current&&!this.stopped){t=this.extendEventData(t);for(var e=this.current.inst.options,n=0,r=this.gestures.length;r>n;n++){var o=this.gestures[n];if(!this.stopped&&e[o.name]!==!1&&o.handler.call(o,t,this.current.inst)===!1){this.stopDetect();break}}return this.current&&(this.current.lastEvent=t),t.eventType==i.EVENT_END&&!t.touches.length-1&&this.stopDetect(),t}},stopDetect:function(){this.previous=i.utils.extend({},this.current),this.current=null,this.stopped=!0},extendEventData:function(t){var e=this.current.startEvent;if(e&&(t.touches.length!=e.touches.length||t.touches===e.touches)){e.touches=[];for(var n=0,r=t.touches.length;r>n;n++)e.touches.push(i.utils.extend({},t.touches[n]))}var o=t.timeStamp-e.timeStamp,s=t.center.pageX-e.center.pageX,a=t.center.pageY-e.center.pageY,c=i.utils.getVelocity(o,s,a);return i.utils.extend(t,{deltaTime:o,deltaX:s,deltaY:a,velocityX:c.x,velocityY:c.y,distance:i.utils.getDistance(e.center,t.center),angle:i.utils.getAngle(e.center,t.center),direction:i.utils.getDirection(e.center,t.center),scale:i.utils.getScale(e.touches,t.touches),rotation:i.utils.getRotation(e.touches,t.touches),startEvent:e}),t},register:function(t){var n=t.defaults||{};return n[t.name]===e&&(n[t.name]=!0),i.utils.extend(i.defaults,n,!0),t.index=t.index||1e3,this.gestures.push(t),this.gestures.sort(function(t,e){return t.index<e.index?-1:t.index>e.index?1:0}),this.gestures}},i.gestures=i.gestures||{},i.gestures.Hold={name:"hold",index:10,defaults:{hold_timeout:500,hold_threshold:1},timer:null,handler:function(t,e){switch(t.eventType){case i.EVENT_START:clearTimeout(this.timer),i.detection.current.name=this.name,this.timer=setTimeout(function(){"hold"==i.detection.current.name&&e.trigger("hold",t)},e.options.hold_timeout);break;case i.EVENT_MOVE:t.distance>e.options.hold_threshold&&clearTimeout(this.timer);break;case i.EVENT_END:clearTimeout(this.timer)}}},i.gestures.Tap={name:"tap",index:100,defaults:{tap_max_touchtime:250,tap_max_distance:10,tap_always:!0,doubletap_distance:20,doubletap_interval:300},handler:function(t,e){if(t.eventType==i.EVENT_END){var n=i.detection.previous,r=!1;if(t.deltaTime>e.options.tap_max_touchtime||t.distance>e.options.tap_max_distance)return;n&&"tap"==n.name&&t.timeStamp-n.lastEvent.timeStamp<e.options.doubletap_interval&&t.distance<e.options.doubletap_distance&&(e.trigger("doubletap",t),r=!0),(!r||e.options.tap_always)&&(i.detection.current.name="tap",e.trigger(i.detection.current.name,t))}}},i.gestures.Swipe={name:"swipe",index:40,defaults:{swipe_max_touches:1,swipe_velocity:.7},handler:function(t,e){if(t.eventType==i.EVENT_END){if(e.options.swipe_max_touches>0&&t.touches.length>e.options.swipe_max_touches)return;(t.velocityX>e.options.swipe_velocity||t.velocityY>e.options.swipe_velocity)&&(e.trigger(this.name,t),e.trigger(this.name+t.direction,t))}}},i.gestures.Drag={name:"drag",index:50,defaults:{drag_min_distance:10,drag_max_touches:1,drag_block_horizontal:!1,drag_block_vertical:!1,drag_lock_to_axis:!1,drag_lock_min_distance:25},triggered:!1,handler:function(t,n){if(i.detection.current.name!=this.name&&this.triggered)return n.trigger(this.name+"end",t),this.triggered=!1,e;if(!(n.options.drag_max_touches>0&&t.touches.length>n.options.drag_max_touches))switch(t.eventType){case i.EVENT_START:this.triggered=!1;break;case i.EVENT_MOVE:if(t.distance<n.options.drag_min_distance&&i.detection.current.name!=this.name)return;i.detection.current.name=this.name,(i.detection.current.lastEvent.drag_locked_to_axis||n.options.drag_lock_to_axis&&n.options.drag_lock_min_distance<=t.distance)&&(t.drag_locked_to_axis=!0);var r=i.detection.current.lastEvent.direction;t.drag_locked_to_axis&&r!==t.direction&&(t.direction=i.utils.isVertical(r)?0>t.deltaY?i.DIRECTION_UP:i.DIRECTION_DOWN:0>t.deltaX?i.DIRECTION_LEFT:i.DIRECTION_RIGHT),this.triggered||(n.trigger(this.name+"start",t),this.triggered=!0),n.trigger(this.name,t),n.trigger(this.name+t.direction,t),(n.options.drag_block_vertical&&i.utils.isVertical(t.direction)||n.options.drag_block_horizontal&&!i.utils.isVertical(t.direction))&&t.preventDefault();break;case i.EVENT_END:this.triggered&&n.trigger(this.name+"end",t),this.triggered=!1}}},i.gestures.Transform={name:"transform",index:45,defaults:{transform_min_scale:.01,transform_min_rotation:1,transform_always_block:!1},triggered:!1,handler:function(t,n){if(i.detection.current.name!=this.name&&this.triggered)return n.trigger(this.name+"end",t),this.triggered=!1,e;if(!(2>t.touches.length))switch(n.options.transform_always_block&&t.preventDefault(),t.eventType){case i.EVENT_START:this.triggered=!1;break;case i.EVENT_MOVE:var r=Math.abs(1-t.scale),o=Math.abs(t.rotation);if(n.options.transform_min_scale>r&&n.options.transform_min_rotation>o)return;i.detection.current.name=this.name,this.triggered||(n.trigger(this.name+"start",t),this.triggered=!0),n.trigger(this.name,t),o>n.options.transform_min_rotation&&n.trigger("rotate",t),r>n.options.transform_min_scale&&(n.trigger("pinch",t),n.trigger("pinch"+(1>t.scale?"in":"out"),t));break;case i.EVENT_END:this.triggered&&n.trigger(this.name+"end",t),this.triggered=!1}}},i.gestures.Touch={name:"touch",index:-1/0,defaults:{prevent_default:!1,prevent_mouseevents:!1},handler:function(t,n){return n.options.prevent_mouseevents&&t.pointerType==i.POINTER_MOUSE?(t.stopDetect(),e):(n.options.prevent_default&&t.preventDefault(),t.eventType==i.EVENT_START&&n.trigger(this.name,t),e)}},i.gestures.Release={name:"release",index:1/0,handler:function(t,e){t.eventType==i.EVENT_END&&e.trigger(this.name,t)}},"object"==typeof module&&"object"==typeof module.exports?module.exports=i:(t.Hammer=i,"function"==typeof t.define&&t.define.amd&&t.define("hammer",[],function(){return i}))})(this),function(t,e){"use strict";t!==e&&(Hammer.event.bindDom=function(n,i,r){t(n).on(i,function(t){var n=t.originalEvent||t;n.pageX===e&&(n.pageX=t.pageX,n.pageY=t.pageY),n.target||(n.target=t.target),n.which===e&&(n.which=n.button),n.preventDefault||(n.preventDefault=t.preventDefault),n.stopPropagation||(n.stopPropagation=t.stopPropagation),r.call(this,n)})},Hammer.Instance.prototype.on=function(e,n){return t(this.element).on(e,n)},Hammer.Instance.prototype.off=function(e,n){return t(this.element).off(e,n)},Hammer.Instance.prototype.trigger=function(e,n){var i=t(this.element);return i.has(n.target).length&&(i=t(n.target)),i.trigger({type:e,gesture:n})},t.fn.hammer=function(e){return this.each(function(){var n=t(this),i=n.data("hammer");i?i&&e&&Hammer.utils.extend(i.options,e):n.data("hammer",new Hammer(this,e||{}))})})}(window.jQuery||window.Zepto);
// datepicker init 
function initDatepicker(){
  jQuery('.datepicker-holder').each(function(){
    var holder = jQuery(this),
      input = holder.find('input:text'),
      currentDate = new Date();

    var recalculateDay = parseInt(currentDate.getMonth()) < 9 ? 0+String(parseInt(currentDate.getMonth()+1)) : currentDate.getMonth(),
      inputCurrentDate = recalculateDay +'.' + currentDate.getDate( )+'.' + currentDate.getFullYear();

    input.attr('value', inputCurrentDate)
    input.datepicker({
      showOn: 'both',
      dateFormat: 'mm.dd.yy',
      firstDay: 1,
      dayNamesMin: ["", "", "", "", "", "", "" ],
      gotoCurrent: true,
      beforeShowDay: function(date) {
        return [true, date < currentDate ? 'past' : ''];
      }
    });
  });
}

// content tabs init
function initTabs() {
  jQuery('ul.tabset').contentTabs({
    tabLinks: 'a'
  });
  jQuery('.menu-2').contentTabs({
    tabLinks: 'a'
  });
}

// open-close init
function initOpenClose() {
  jQuery('li.open-close').openClose({
    hideOnClickOutside: true,
    activeClass: 'active',
    opener: '.opener',
    slider: '.slide',
    animSpeed: 400,
    effect: 'slide'
  });
  jQuery('div.open-close').openClose({
    hideOnClickOutside: true,
    activeClass: 'active',
    opener: '.opener',
    slider: '.slide',
    animSpeed: 400,
    effect: 'slide'
  });
}

// fancybox modal popup init
function initLightbox() {
  jQuery('a.lightbox, a[data-rel*="lightbox"]').each(function(){
    var link = jQuery(this);
    link.attr('rel', link.attr('data-rel')).fancybox({
      padding: 0,
      margin: 0,
      cyclic: false,
      autoScale: true,
      overlayShow: true,
      overlayOpacity: 0.70,
      overlayColor: '#656565',
      titlePosition: 'inside',
      onComplete: function(box) {
        if(link.attr('href').indexOf('#') === 0) {
          jQuery('#fancybox-content').find('a.close').unbind('click.fb').bind('click.fb', function(e){
            jQuery.fancybox.close();
            e.preventDefault();
          });
        }
      }
    });
  });
}

/* Fancybox overlay fix */
jQuery(function(){
  // detect device type
  var isTouchDevice = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;
  var isWinPhoneDevice = navigator.msPointerEnabled && /MSIE 10.*Touch/.test(navigator.userAgent);

  if(!isTouchDevice && !isWinPhoneDevice) {
    // create <style> rules
    var head = document.getElementsByTagName('head')[0],
      style = document.createElement('style'),
      rules = document.createTextNode('#fancybox-overlay'+'{'+
        'position:fixed;'+
        'top:0;'+
        'left:0;'+
        '}');

    // append style element
    style.type = 'text/css';
    if(style.styleSheet) {
      style.styleSheet.cssText = rules.nodeValue;
    } else {
      style.appendChild(rules);
    }
    head.appendChild(style);
  }
});

/*
 * jQuery Tabs plugin
 */
;(function($){
  $.fn.contentTabs = function(o){
    // default options
    var options = $.extend({
      activeClass:'active',
      addToParent:false,
      autoHeight:false,
      autoRotate:false,
      checkHash:false,
      animSpeed:400,
      switchTime:3000,
      effect: 'none', // "fade", "slide"
      tabLinks:'a',
      attrib:'href',
      event:'click'
    },o);

    return this.each(function(){
      var tabset = $(this), tabs = $();
      var tabLinks = tabset.find(options.tabLinks);
      var tabLinksParents = tabLinks.parent();
      var prevActiveLink = tabLinks.eq(0), currentTab, animating;
      var tabHolder;

      // handle location hash
      if(options.checkHash && tabLinks.filter('[' + options.attrib + '="' + location.hash + '"]').length) {
        (options.addToParent ? tabLinksParents : tabLinks).removeClass(options.activeClass);
        setTimeout(function() {
          window.scrollTo(0,0);
        },1);
      }

      // init tabLinks
      tabLinks.each(function(){
        var link = $(this);
        var href = link.attr(options.attrib);
        var parent = link.parent();
        href = href.substr(href.lastIndexOf('#'));

        // get elements
        var tab = $(href);
        tabs = tabs.add(tab);
        link.data('cparent', parent);
        link.data('ctab', tab);

        // find tab holder
        if(!tabHolder && tab.length) {
          tabHolder = tab.parent();
        }

        // show only active tab
        var classOwner = options.addToParent ? parent : link;
        if(classOwner.hasClass(options.activeClass) || (options.checkHash && location.hash === href)) {
          classOwner.addClass(options.activeClass);
          prevActiveLink = link; currentTab = tab;
          tab.removeClass(tabHiddenClass).width('');
          contentTabsEffect[options.effect].show({tab:tab, fast:true});
        } else {
          var tabWidth = tab.width();
          if(tabWidth) {
            tab.width(tabWidth);
          }
          tab.addClass(tabHiddenClass);
        }

        // event handler
        link.bind(options.event, function(e){
          if(link != prevActiveLink && !animating) {
            switchTab(prevActiveLink, link);
            prevActiveLink = link;
          }
        });
        if(options.attrib === 'href') {
          link.bind('click', function(e){
            e.preventDefault();
          });
        }
      });

      // tab switch function
      function switchTab(oldLink, newLink) {
        animating = true;
        var oldTab = oldLink.data('ctab');
        var newTab = newLink.data('ctab');
        prevActiveLink = newLink;
        currentTab = newTab;

        // refresh pagination links
        (options.addToParent ? tabLinksParents : tabLinks).removeClass(options.activeClass);
        (options.addToParent ? newLink.data('cparent') : newLink).addClass(options.activeClass);

        // hide old tab
        resizeHolder(oldTab, true);
        contentTabsEffect[options.effect].hide({
          speed: options.animSpeed,
          tab:oldTab,
          complete: function() {
            // show current tab
            resizeHolder(newTab.removeClass(tabHiddenClass).width(''));
            contentTabsEffect[options.effect].show({
              speed: options.animSpeed,
              tab:newTab,
              complete: function() {
                if(!oldTab.is(newTab)) {
                  oldTab.width(oldTab.width()).addClass(tabHiddenClass);
                }
                animating = false;
                resizeHolder(newTab, false);
                autoRotate();
              }
            });
          }
        });
      }

      // holder auto height
      function resizeHolder(block, state) {
        var curBlock = block && block.length ? block : currentTab;
        if(options.autoHeight && curBlock) {
          tabHolder.stop();
          if(state === false) {
            tabHolder.css({height:''});
          } else {
            var origStyles = curBlock.attr('style');
            curBlock.show().css({width:curBlock.width()});
            var tabHeight = curBlock.outerHeight(true);
            if(!origStyles) curBlock.removeAttr('style'); else curBlock.attr('style', origStyles);
            if(state === true) {
              tabHolder.css({height: tabHeight});
            } else {
              tabHolder.animate({height: tabHeight}, {duration: options.animSpeed});
            }
          }
        }
      }
      if(options.autoHeight) {
        $(window).bind('resize orientationchange', function(){
          tabs.not(currentTab).removeClass(tabHiddenClass).show().each(function(){
            var tab = jQuery(this), tabWidth = tab.css({width:''}).width();
            if(tabWidth) {
              tab.width(tabWidth);
            }
          }).hide().addClass(tabHiddenClass);

          resizeHolder(currentTab, false);
        });
      }

      // autorotation handling
      var rotationTimer;
      function nextTab() {
        var activeItem = (options.addToParent ? tabLinksParents : tabLinks).filter('.' + options.activeClass);
        var activeIndex = (options.addToParent ? tabLinksParents : tabLinks).index(activeItem);
        var newLink = tabLinks.eq(activeIndex < tabLinks.length - 1 ? activeIndex + 1 : 0);
        prevActiveLink = tabLinks.eq(activeIndex);
        switchTab(prevActiveLink, newLink);
      }
      function autoRotate() {
        if(options.autoRotate && tabLinks.length > 1) {
          clearTimeout(rotationTimer);
          rotationTimer = setTimeout(function() {
            if(!animating) {
              nextTab();
            } else {
              autoRotate();
            }
          }, options.switchTime);
        }
      }
      autoRotate();
    });
  };

  // add stylesheet for tabs on DOMReady
  var tabHiddenClass = 'js-tab-hidden';
  $(function() {
    var tabStyleSheet = $('<style type="text/css">')[0];
    var tabStyleRule = '.'+tabHiddenClass;
    tabStyleRule += '{position:absolute !important;left:-9999px !important;top:-9999px !important;display:block !important}';
    if (tabStyleSheet.styleSheet) {
      tabStyleSheet.styleSheet.cssText = tabStyleRule;
    } else {
      tabStyleSheet.appendChild(document.createTextNode(tabStyleRule));
    }
    $('head').append(tabStyleSheet);
  });

  // tab switch effects
  var contentTabsEffect = {
    none: {
      show: function(o) {
        o.tab.css({display:'block'});
        if(o.complete) o.complete();
      },
      hide: function(o) {
        o.tab.css({display:'none'});
        if(o.complete) o.complete();
      }
    },
    fade: {
      show: function(o) {
        if(o.fast) o.speed = 1;
        o.tab.fadeIn(o.speed);
        if(o.complete) setTimeout(o.complete, o.speed);
      },
      hide: function(o) {
        if(o.fast) o.speed = 1;
        o.tab.fadeOut(o.speed);
        if(o.complete) setTimeout(o.complete, o.speed);
      }
    },
    slide: {
      show: function(o) {
        var tabHeight = o.tab.show().css({width:o.tab.width()}).outerHeight(true);
        var tmpWrap = $('<div class="effect-div">').insertBefore(o.tab).append(o.tab);
        tmpWrap.css({width:'100%', overflow:'hidden', position:'relative'}); o.tab.css({marginTop:-tabHeight,display:'block'});
        if(o.fast) o.speed = 1;
        o.tab.animate({marginTop: 0}, {duration: o.speed, complete: function(){
          o.tab.css({marginTop: '', width: ''}).insertBefore(tmpWrap);
          tmpWrap.remove();
          if(o.complete) o.complete();
        }});
      },
      hide: function(o) {
        var tabHeight = o.tab.show().css({width:o.tab.width()}).outerHeight(true);
        var tmpWrap = $('<div class="effect-div">').insertBefore(o.tab).append(o.tab);
        tmpWrap.css({width:'100%', overflow:'hidden', position:'relative'});

        if(o.fast) o.speed = 1;
        o.tab.animate({marginTop: -tabHeight}, {duration: o.speed, complete: function(){
          o.tab.css({display:'none', marginTop:'', width:''}).insertBefore(tmpWrap);
          tmpWrap.remove();
          if(o.complete) o.complete();
        }});
      }
    }
  };
}(jQuery));

/*
 * jQuery Open/Close plugin
 */
;(function($) {
  function OpenClose(options) {
    this.options = $.extend({
      addClassBeforeAnimation: true,
      hideOnClickOutside: false,
      activeClass:'active',
      opener:'.opener',
      slider:'.slide',
      animSpeed: 400,
      effect:'fade',
      event:'click'
    }, options);
    this.init();
  }
  OpenClose.prototype = {
    init: function() {
      if(this.options.holder) {
        this.findElements();
        this.attachEvents();
        this.makeCallback('onInit');
      }
    },
    findElements: function() {
      this.holder = $(this.options.holder);
      this.opener = this.holder.find(this.options.opener);
      this.slider = this.holder.find(this.options.slider);

      if (!this.holder.hasClass(this.options.activeClass)) {
        this.slider.addClass(slideHiddenClass);
      }
    },
    attachEvents: function() {
      // add handler
      var self = this;
      this.eventHandler = function(e) {
        e.preventDefault();
        if (self.slider.hasClass(slideHiddenClass)) {
          self.showSlide();
        } else {
          self.hideSlide();
        }
      };
      self.opener.bind(self.options.event, this.eventHandler);

      // hover mode handler
      if(self.options.event === 'over') {
        self.opener.bind('mouseenter', function() {
          self.holder.removeClass(self.options.activeClass);
          self.opener.trigger(self.options.event);
        });
        self.holder.bind('mouseleave', function() {
          self.holder.addClass(self.options.activeClass);
          self.opener.trigger(self.options.event);
        });
      }

      // outside click handler
      self.outsideClickHandler = function(e) {
        if(self.options.hideOnClickOutside) {
          var target = $(e.target);
          if (!target.is(self.holder) && !target.closest(self.holder).length) {
            self.hideSlide();
          }
        }
      };
    },
    showSlide: function() {
      var self = this;
      if (self.options.addClassBeforeAnimation) {
        self.holder.addClass(self.options.activeClass);
      }
      self.slider.removeClass(slideHiddenClass);
      $(document).bind('click touchstart', self.outsideClickHandler);

      self.makeCallback('animStart', true);
      toggleEffects[self.options.effect].show({
        box: self.slider,
        speed: self.options.animSpeed,
        complete: function() {
          if (!self.options.addClassBeforeAnimation) {
            self.holder.addClass(self.options.activeClass);
          }
          self.makeCallback('animEnd', true);
        }
      });
    },
    hideSlide: function() {
      var self = this;
      if (self.options.addClassBeforeAnimation) {
        self.holder.removeClass(self.options.activeClass);
      }
      $(document).unbind('click touchstart', self.outsideClickHandler);

      self.makeCallback('animStart', false);
      toggleEffects[self.options.effect].hide({
        box: self.slider,
        speed: self.options.animSpeed,
        complete: function() {
          if (!self.options.addClassBeforeAnimation) {
            self.holder.removeClass(self.options.activeClass);
          }
          self.slider.addClass(slideHiddenClass);
          self.makeCallback('animEnd', false);
        }
      });
    },
    destroy: function() {
      this.slider.removeClass(slideHiddenClass).css({display:''});
      this.opener.unbind(this.options.event, this.eventHandler);
      this.holder.removeClass(this.options.activeClass).removeData('OpenClose');
      $(document).unbind('click touchstart', this.outsideClickHandler);
    },
    makeCallback: function(name) {
      if(typeof this.options[name] === 'function') {
        var args = Array.prototype.slice.call(arguments);
        args.shift();
        this.options[name].apply(this, args);
      }
    }
  };

  // add stylesheet for slide on DOMReady
  var slideHiddenClass = 'js-slide-hidden';
  $(function() {
    var tabStyleSheet = $('<style type="text/css">')[0];
    var tabStyleRule = '.' + slideHiddenClass;
    tabStyleRule += '{position:absolute !important;left:-9999px !important;top:-9999px !important;display:block !important}';
    if (tabStyleSheet.styleSheet) {
      tabStyleSheet.styleSheet.cssText = tabStyleRule;
    } else {
      tabStyleSheet.appendChild(document.createTextNode(tabStyleRule));
    }
    $('head').append(tabStyleSheet);
  });

  // animation effects
  var toggleEffects = {
    slide: {
      show: function(o) {
        o.box.stop(true).hide().slideDown(o.speed, o.complete);
      },
      hide: function(o) {
        o.box.stop(true).slideUp(o.speed, o.complete);
      }
    },
    fade: {
      show: function(o) {
        o.box.stop(true).hide().fadeIn(o.speed, o.complete);
      },
      hide: function(o) {
        o.box.stop(true).fadeOut(o.speed, o.complete);
      }
    },
    none: {
      show: function(o) {
        o.box.hide().show(0, o.complete);
      },
      hide: function(o) {
        o.box.hide(0, o.complete);
      }
    }
  };

  // jQuery plugin interface
  $.fn.openClose = function(opt) {
    return this.each(function() {
      jQuery(this).data('OpenClose', new OpenClose($.extend(opt, {holder: this})));
    });
  };
}(jQuery));

/*! http://mths.be/placeholder v2.0.6 by @mathias */
;(function(window, document, $) {

  var isInputSupported = 'placeholder' in document.createElement('input'),
    isTextareaSupported = 'placeholder' in document.createElement('textarea'),
    prototype = $.fn,
    valHooks = $.valHooks,
    hooks,
    placeholder;
  if(navigator.userAgent.indexOf('Opera/') != -1) {
    isInputSupported = isTextareaSupported = false;
  }
  if (isInputSupported && isTextareaSupported) {

    placeholder = prototype.placeholder = function() {
      return this;
    };

    placeholder.input = placeholder.textarea = true;

  } else {

    placeholder = prototype.placeholder = function() {
      var $this = this;
      $this
        .filter((isInputSupported ? 'textarea' : ':input') + '[placeholder]')
        .not('.placeholder')
        .bind({
          'focus.placeholder': clearPlaceholder,
          'blur.placeholder': setPlaceholder
        })
        .data('placeholder-enabled', true)
        .trigger('blur.placeholder');
      return $this;
    };

    placeholder.input = isInputSupported;
    placeholder.textarea = isTextareaSupported;

    hooks = {
      'get': function(element) {
        var $element = $(element);
        return $element.data('placeholder-enabled') && $element.hasClass('placeholder') ? '' : element.value;
      },
      'set': function(element, value) {
        var $element = $(element);
        if (!$element.data('placeholder-enabled')) {
          return element.value = value;
        }
        if (value == '') {
          element.value = value;
          // Issue #56: Setting the placeholder causes problems if the element continues to have focus.
          if (element != document.activeElement) {
            // We canâ€™t use `triggerHandler` here because of dummy text/password inputs :(
            setPlaceholder.call(element);
          }
        } else if ($element.hasClass('placeholder')) {
          clearPlaceholder.call(element, true, value) || (element.value = value);
        } else {
          element.value = value;
        }
        // `set` can not return `undefined`; see http://jsapi.info/jquery/1.7.1/val#L2363
        return $element;
      }
    };

    isInputSupported || (valHooks.input = hooks);
    isTextareaSupported || (valHooks.textarea = hooks);

    $(function() {
      // Look for forms
      $(document).delegate('form', 'submit.placeholder', function() {
        // Clear the placeholder values so they donâ€™t get submitted
        var $inputs = $('.placeholder', this).each(clearPlaceholder);
        setTimeout(function() {
          $inputs.each(setPlaceholder);
        }, 10);
      });
    });

    // Clear placeholder values upon page reload
    $(window).bind('beforeunload.placeholder', function() {
      $('.placeholder').each(function() {
        this.value = '';
      });
    });

  }

  function args(elem) {
    // Return an object of element attributes
    var newAttrs = {},
      rinlinejQuery = /^jQuery\d+$/;
    $.each(elem.attributes, function(i, attr) {
      if (attr.specified && !rinlinejQuery.test(attr.name)) {
        newAttrs[attr.name] = attr.value;
      }
    });
    return newAttrs;
  }

  function clearPlaceholder(event, value) {
    var input = this,
      $input = $(input),
      hadFocus;
    if (input.value == $input.attr('placeholder') && $input.hasClass('placeholder')) {
      hadFocus = input == document.activeElement;
      if ($input.data('placeholder-password')) {
        $input = $input.hide().next().show().attr('id', $input.removeAttr('id').data('placeholder-id'));
        // If `clearPlaceholder` was called from `$.valHooks.input.set`
        if (event === true) {
          return $input[0].value = value;
        }
        $input.focus();
      } else {
        input.value = '';
        $input.removeClass('placeholder');
      }
      hadFocus && input.select();
    }
  }

  function setPlaceholder() {
    var $replacement,
      input = this,
      $input = $(input),
      $origInput = $input,
      id = this.id;
    if (input.value == '') {
      if (input.type == 'password') {
        if (!$input.data('placeholder-textinput')) {
          try {
            $replacement = $input.clone().attr({ 'type': 'text' });
          } catch(e) {
            $replacement = $('<input>').attr($.extend(args(this), { 'type': 'text' }));
          }
          $replacement
            .removeAttr('name')
            .data({
              'placeholder-password': true,
              'placeholder-id': id
            })
            .bind('focus.placeholder', clearPlaceholder);
          $input
            .data({
              'placeholder-textinput': $replacement,
              'placeholder-id': id
            })
            .before($replacement);
        }
        $input = $input.removeAttr('id').hide().prev().attr('id', id).show();
        // Note: `$input[0] != input` now!
      }
      $input.addClass('placeholder');
      $input[0].value = $input.attr('placeholder');
    } else {
      $input.removeClass('placeholder');
    }
  }

}(this, document, jQuery));

/*
 * JavaScript Custom Forms Module
 */
jcf = {
  // global options
  modules: {},
  plugins: {},
  baseOptions: {
    unselectableClass:'jcf-unselectable',
    labelActiveClass:'jcf-label-active',
    labelDisabledClass:'jcf-label-disabled',
    classPrefix: 'jcf-class-',
    hiddenClass:'jcf-hidden',
    focusClass:'jcf-focus',
    wrapperTag: 'div'
  },
  // replacer function
  customForms: {
    setOptions: function(obj) {
      for(var p in obj) {
        if(obj.hasOwnProperty(p) && typeof obj[p] === 'object') {
          jcf.lib.extend(jcf.modules[p].prototype.defaultOptions, obj[p]);
        }
      }
    },
    replaceAll: function(context) {
      for(var k in jcf.modules) {
        var els = jcf.lib.queryBySelector(jcf.modules[k].prototype.selector, context);
        for(var i = 0; i<els.length; i++) {
          if(els[i].jcf) {
            // refresh form element state
            els[i].jcf.refreshState();
          } else {
            // replace form element
            if(!jcf.lib.hasClass(els[i], 'default') && jcf.modules[k].prototype.checkElement(els[i])) {
              new jcf.modules[k]({
                replaces:els[i]
              });
            }
          }
        }
      }
    },
    refreshAll: function(context) {
      for(var k in jcf.modules) {
        var els = jcf.lib.queryBySelector(jcf.modules[k].prototype.selector, context);
        for(var i = 0; i<els.length; i++) {
          if(els[i].jcf) {
            // refresh form element state
            els[i].jcf.refreshState();
          }
        }
      }
    },
    refreshElement: function(obj) {
      if(obj && obj.jcf) {
        obj.jcf.refreshState();
      }
    },
    destroyAll: function() {
      for(var k in jcf.modules) {
        var els = jcf.lib.queryBySelector(jcf.modules[k].prototype.selector);
        for(var i = 0; i<els.length; i++) {
          if(els[i].jcf) {
            els[i].jcf.destroy();
          }
        }
      }
    }
  },
  // detect device type
  isTouchDevice: ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch,
  isWinPhoneDevice: navigator.msPointerEnabled && /MSIE 10.*Touch/.test(navigator.userAgent),
  // define base module
  setBaseModule: function(obj) {
    jcf.customControl = function(opt){
      this.options = jcf.lib.extend({}, jcf.baseOptions, this.defaultOptions, opt);
      this.init();
    };
    for(var p in obj) {
      jcf.customControl.prototype[p] = obj[p];
    }
  },
  // add module to jcf.modules
  addModule: function(obj) {
    if(obj.name){
      // create new module proto class
      jcf.modules[obj.name] = function(){
        jcf.modules[obj.name].superclass.constructor.apply(this, arguments);
      }
      jcf.lib.inherit(jcf.modules[obj.name], jcf.customControl);
      for(var p in obj) {
        jcf.modules[obj.name].prototype[p] = obj[p]
      }
      // on create module
      jcf.modules[obj.name].prototype.onCreateModule();
      // make callback for exciting modules
      for(var mod in jcf.modules) {
        if(jcf.modules[mod] != jcf.modules[obj.name]) {
          jcf.modules[mod].prototype.onModuleAdded(jcf.modules[obj.name]);
        }
      }
    }
  },
  // add plugin to jcf.plugins
  addPlugin: function(obj) {
    if(obj && obj.name) {
      jcf.plugins[obj.name] = function() {
        this.init.apply(this, arguments);
      }
      for(var p in obj) {
        jcf.plugins[obj.name].prototype[p] = obj[p];
      }
    }
  },
  // miscellaneous init
  init: function(){
    if(navigator.msPointerEnabled) {
      this.eventPress = 'MSPointerDown';
      this.eventMove = 'MSPointerMove';
      this.eventRelease = 'MSPointerUp';
    } else {
      this.eventPress = this.isTouchDevice ? 'touchstart' : 'mousedown';
      this.eventMove = this.isTouchDevice ? 'touchmove' : 'mousemove';
      this.eventRelease = this.isTouchDevice ? 'touchend' : 'mouseup';
    }

    setTimeout(function(){
      jcf.lib.domReady(function(){
        jcf.initStyles();
      });
    },1);
    return this;
  },
  initStyles: function() {
    // create <style> element and rules
    var head = document.getElementsByTagName('head')[0],
      style = document.createElement('style'),
      rules = document.createTextNode('.'+jcf.baseOptions.unselectableClass+'{'+
        '-moz-user-select:none;'+
        '-webkit-tap-highlight-color:rgba(255,255,255,0);'+
        '-webkit-user-select:none;'+
        'user-select:none;'+
        '}');

    // append style element
    style.type = 'text/css';
    if(style.styleSheet) {
      style.styleSheet.cssText = rules.nodeValue;
    } else {
      style.appendChild(rules);
    }
    head.appendChild(style);
  }
}.init();

/*
 * Custom Form Control prototype
 */
jcf.setBaseModule({
  init: function(){
    if(this.options.replaces) {
      this.realElement = this.options.replaces;
      this.realElement.jcf = this;
      this.replaceObject();
    }
  },
  defaultOptions: {
    // default module options (will be merged with base options)
  },
  checkElement: function(el){
    return true; // additional check for correct form element
  },
  replaceObject: function(){
    this.createWrapper();
    this.attachEvents();
    this.fixStyles();
    this.setupWrapper();
  },
  createWrapper: function(){
    this.fakeElement = jcf.lib.createElement(this.options.wrapperTag);
    this.labelFor = jcf.lib.getLabelFor(this.realElement);
    jcf.lib.disableTextSelection(this.fakeElement);
    jcf.lib.addClass(this.fakeElement, jcf.lib.getAllClasses(this.realElement.className, this.options.classPrefix));
    jcf.lib.addClass(this.realElement, jcf.baseOptions.hiddenClass);
  },
  attachEvents: function(){
    jcf.lib.event.add(this.realElement, 'focus', this.onFocusHandler, this);
    jcf.lib.event.add(this.realElement, 'blur', this.onBlurHandler, this);
    jcf.lib.event.add(this.fakeElement, 'click', this.onFakeClick, this);
    jcf.lib.event.add(this.fakeElement, jcf.eventPress, this.onFakePressed, this);
    jcf.lib.event.add(this.fakeElement, jcf.eventRelease, this.onFakeReleased, this);

    if(this.labelFor) {
      this.labelFor.jcf = this;
      jcf.lib.event.add(this.labelFor, 'click', this.onFakeClick, this);
      jcf.lib.event.add(this.labelFor, jcf.eventPress, this.onFakePressed, this);
      jcf.lib.event.add(this.labelFor, jcf.eventRelease, this.onFakeReleased, this);
    }
  },
  fixStyles: function() {
    // hide mobile webkit tap effect
    if(jcf.isTouchDevice) {
      var tapStyle = 'rgba(255,255,255,0)';
      this.realElement.style.webkitTapHighlightColor = tapStyle;
      this.fakeElement.style.webkitTapHighlightColor = tapStyle;
      if(this.labelFor) {
        this.labelFor.style.webkitTapHighlightColor = tapStyle;
      }
    }
  },
  setupWrapper: function(){
    // implement in subclass
  },
  refreshState: function(){
    // implement in subclass
  },
  destroy: function() {
    if(this.fakeElement && this.fakeElement.parentNode) {
      this.fakeElement.parentNode.removeChild(this.fakeElement);
    }
    jcf.lib.removeClass(this.realElement, jcf.baseOptions.hiddenClass);
    this.realElement.jcf = null;
  },
  onFocus: function(){
    // emulated focus event
    jcf.lib.addClass(this.fakeElement,this.options.focusClass);
  },
  onBlur: function(cb){
    // emulated blur event
    jcf.lib.removeClass(this.fakeElement,this.options.focusClass);
  },
  onFocusHandler: function() {
    // handle focus loses
    if(this.focused) return;
    this.focused = true;

    // handle touch devices also
    if(jcf.isTouchDevice) {
      if(jcf.focusedInstance && jcf.focusedInstance.realElement != this.realElement) {
        jcf.focusedInstance.onBlur();
        jcf.focusedInstance.realElement.blur();
      }
      jcf.focusedInstance = this;
    }
    this.onFocus.apply(this, arguments);
  },
  onBlurHandler: function() {
    // handle focus loses
    if(!this.pressedFlag) {
      this.focused = false;
      this.onBlur.apply(this, arguments);
    }
  },
  onFakeClick: function(){
    if(jcf.isTouchDevice) {
      this.onFocus();
    } else if(!this.realElement.disabled) {
      this.realElement.focus();
    }
  },
  onFakePressed: function(e){
    this.pressedFlag = true;
  },
  onFakeReleased: function(){
    this.pressedFlag = false;
  },
  onCreateModule: function(){
    // implement in subclass
  },
  onModuleAdded: function(module) {
    // implement in subclass
  },
  onControlReady: function() {
    // implement in subclass
  }
});

/*
 * JCF Utility Library
 */
jcf.lib = {
  bind: function(func, scope){
    return function() {
      return func.apply(scope, arguments);
    };
  },
  browser: (function() {
    var ua = navigator.userAgent.toLowerCase(), res = {},
      match = /(webkit)[ \/]([\w.]+)/.exec(ua) || /(opera)(?:.*version)?[ \/]([\w.]+)/.exec(ua) ||
        /(msie) ([\w.]+)/.exec(ua) || ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+))?/.exec(ua) || [];
    res[match[1]] = true;
    res.version = match[2] || "0";
    res.safariMac = ua.indexOf('mac') != -1 && ua.indexOf('safari') != -1;
    return res;
  })(),
  getOffset: function (obj) {
    if (obj.getBoundingClientRect && !jcf.isWinPhoneDevice) {
      var scrollLeft = window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft;
      var scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop;
      var clientLeft = document.documentElement.clientLeft || document.body.clientLeft || 0;
      var clientTop = document.documentElement.clientTop || document.body.clientTop || 0;
      return {
        top:Math.round(obj.getBoundingClientRect().top + scrollTop - clientTop),
        left:Math.round(obj.getBoundingClientRect().left + scrollLeft - clientLeft)
      };
    } else {
      var posLeft = 0, posTop = 0;
      while (obj.offsetParent) {posLeft += obj.offsetLeft; posTop += obj.offsetTop; obj = obj.offsetParent;}
      return {top:posTop,left:posLeft};
    }
  },
  getScrollTop: function() {
    return window.pageYOffset || document.documentElement.scrollTop;
  },
  getScrollLeft: function() {
    return window.pageXOffset || document.documentElement.scrollLeft;
  },
  getWindowWidth: function(){
    return document.compatMode=='CSS1Compat' ? document.documentElement.clientWidth : document.body.clientWidth;
  },
  getWindowHeight: function(){
    return document.compatMode=='CSS1Compat' ? document.documentElement.clientHeight : document.body.clientHeight;
  },
  getStyle: function(el, prop) {
    if (document.defaultView && document.defaultView.getComputedStyle) {
      return document.defaultView.getComputedStyle(el, null)[prop];
    } else if (el.currentStyle) {
      return el.currentStyle[prop];
    } else {
      return el.style[prop];
    }
  },
  getParent: function(obj, selector) {
    while(obj.parentNode && obj.parentNode != document.body) {
      if(obj.parentNode.tagName.toLowerCase() == selector.toLowerCase()) {
        return obj.parentNode;
      }
      obj = obj.parentNode;
    }
    return false;
  },
  isParent: function(child, parent) {
    while(child.parentNode) {
      if(child.parentNode === parent) {
        return true;
      }
      child = child.parentNode;
    }
    return false;
  },
  getLabelFor: function(object) {
    var parentLabel = jcf.lib.getParent(object,'label');
    if(parentLabel) {
      return parentLabel;
    } else if(object.id) {
      return jcf.lib.queryBySelector('label[for="' + object.id + '"]')[0];
    }
  },
  disableTextSelection: function(el){
    if (typeof el.onselectstart !== 'undefined') {
      el.onselectstart = function() {return false;};
    } else if(window.opera) {
      el.setAttribute('unselectable', 'on');
    } else {
      jcf.lib.addClass(el, jcf.baseOptions.unselectableClass);
    }
  },
  enableTextSelection: function(el) {
    if (typeof el.onselectstart !== 'undefined') {
      el.onselectstart = null;
    } else if(window.opera) {
      el.removeAttribute('unselectable');
    } else {
      jcf.lib.removeClass(el, jcf.baseOptions.unselectableClass);
    }
  },
  queryBySelector: function(selector, scope){
    if(typeof scope === 'string') {
      var result = [];
      var holders = this.getElementsBySelector(scope);
      for (var i = 0, contextNodes; i < holders.length; i++) {
        contextNodes = Array.prototype.slice.call(this.getElementsBySelector(selector, holders[i]));
        result = result.concat(contextNodes);
      }
      return result;
    } else {
      return this.getElementsBySelector(selector, scope);
    }
  },
  prevSibling: function(node) {
    while(node = node.previousSibling) if(node.nodeType == 1) break;
    return node;
  },
  nextSibling: function(node) {
    while(node = node.nextSibling) if(node.nodeType == 1) break;
    return node;
  },
  fireEvent: function(element,event) {
    if(element.dispatchEvent){
      var evt = document.createEvent('HTMLEvents');
      evt.initEvent(event, true, true );
      return !element.dispatchEvent(evt);
    }else if(document.createEventObject){
      var evt = document.createEventObject();
      return element.fireEvent('on'+event,evt);
    }
  },
  isParent: function(p, c) {
    while(c.parentNode) {
      if(p == c) {
        return true;
      }
      c = c.parentNode;
    }
    return false;
  },
  inherit: function(Child, Parent) {
    var F = function() { }
    F.prototype = Parent.prototype
    Child.prototype = new F()
    Child.prototype.constructor = Child
    Child.superclass = Parent.prototype
  },
  extend: function(obj) {
    for(var i = 1; i < arguments.length; i++) {
      for(var p in arguments[i]) {
        if(arguments[i].hasOwnProperty(p)) {
          obj[p] = arguments[i][p];
        }
      }
    }
    return obj;
  },
  hasClass: function (obj,cname) {
    return (obj.className ? obj.className.match(new RegExp('(\\s|^)'+cname+'(\\s|$)')) : false);
  },
  addClass: function (obj,cname) {
    if (!this.hasClass(obj,cname)) obj.className += (!obj.className.length || obj.className.charAt(obj.className.length - 1) === ' ' ? '' : ' ') + cname;
  },
  removeClass: function (obj,cname) {
    if (this.hasClass(obj,cname)) obj.className=obj.className.replace(new RegExp('(\\s|^)'+cname+'(\\s|$)'),' ').replace(/\s+$/, '');
  },
  toggleClass: function(obj, cname, condition) {
    if(condition) this.addClass(obj, cname); else this.removeClass(obj, cname);
  },
  createElement: function(tagName, options) {
    var el = document.createElement(tagName);
    for(var p in options) {
      if(options.hasOwnProperty(p)) {
        switch (p) {
          case 'class': el.className = options[p]; break;
          case 'html': el.innerHTML = options[p]; break;
          case 'style': this.setStyles(el, options[p]); break;
          default: el.setAttribute(p, options[p]);
        }
      }
    }
    return el;
  },
  setStyles: function(el, styles) {
    for(var p in styles) {
      if(styles.hasOwnProperty(p)) {
        switch (p) {
          case 'float': el.style.cssFloat = styles[p]; break;
          case 'opacity': el.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity='+styles[p]*100+')'; el.style.opacity = styles[p]; break;
          default: el.style[p] = (typeof styles[p] === 'undefined' ? 0 : styles[p]) + (typeof styles[p] === 'number' ? 'px' : '');
        }
      }
    }
    return el;
  },
  getInnerWidth: function(el) {
    return el.offsetWidth - (parseInt(this.getStyle(el,'paddingLeft')) || 0) - (parseInt(this.getStyle(el,'paddingRight')) || 0);
  },
  getInnerHeight: function(el) {
    return el.offsetHeight - (parseInt(this.getStyle(el,'paddingTop')) || 0) - (parseInt(this.getStyle(el,'paddingBottom')) || 0);
  },
  getAllClasses: function(cname, prefix, skip) {
    if(!skip) skip = '';
    if(!prefix) prefix = '';
    return cname ? cname.replace(new RegExp('(\\s|^)'+skip+'(\\s|$)'),' ').replace(/[\s]*([\S]+)+[\s]*/gi,prefix+"$1 ") : '';
  },
  getElementsBySelector: function(selector, scope) {
    if(typeof document.querySelectorAll === 'function') {
      return (scope || document).querySelectorAll(selector);
    }
    var selectors = selector.split(',');
    var resultList = [];
    for(var s = 0; s < selectors.length; s++) {
      var currentContext = [scope || document];
      var tokens = selectors[s].replace(/^\s+/,'').replace(/\s+$/,'').split(' ');
      for (var i = 0; i < tokens.length; i++) {
        token = tokens[i].replace(/^\s+/,'').replace(/\s+$/,'');
        if (token.indexOf('#') > -1) {
          var bits = token.split('#'), tagName = bits[0], id = bits[1];
          var element = document.getElementById(id);
          if (tagName && element.nodeName.toLowerCase() != tagName) {
            return [];
          }
          currentContext = [element];
          continue;
        }
        if (token.indexOf('.') > -1) {
          var bits = token.split('.'), tagName = bits[0] || '*', className = bits[1], found = [], foundCount = 0;
          for (var h = 0; h < currentContext.length; h++) {
            var elements;
            if (tagName == '*') {
              elements = currentContext[h].getElementsByTagName('*');
            } else {
              elements = currentContext[h].getElementsByTagName(tagName);
            }
            for (var j = 0; j < elements.length; j++) {
              found[foundCount++] = elements[j];
            }
          }
          currentContext = [];
          var currentContextIndex = 0;
          for (var k = 0; k < found.length; k++) {
            if (found[k].className && found[k].className.match(new RegExp('(\\s|^)'+className+'(\\s|$)'))) {
              currentContext[currentContextIndex++] = found[k];
            }
          }
          continue;
        }
        if (token.match(/^(\w*)\[(\w+)([=~\|\^\$\*]?)=?"?([^\]"]*)"?\]$/)) {
          var tagName = RegExp.$1 || '*', attrName = RegExp.$2, attrOperator = RegExp.$3, attrValue = RegExp.$4;
          if(attrName.toLowerCase() == 'for' && this.browser.msie && this.browser.version < 8) {
            attrName = 'htmlFor';
          }
          var found = [], foundCount = 0;
          for (var h = 0; h < currentContext.length; h++) {
            var elements;
            if (tagName == '*') {
              elements = currentContext[h].getElementsByTagName('*');
            } else {
              elements = currentContext[h].getElementsByTagName(tagName);
            }
            for (var j = 0; elements[j]; j++) {
              found[foundCount++] = elements[j];
            }
          }
          currentContext = [];
          var currentContextIndex = 0, checkFunction;
          switch (attrOperator) {
            case '=': checkFunction = function(e) { return (e.getAttribute(attrName) == attrValue) }; break;
            case '~': checkFunction = function(e) { return (e.getAttribute(attrName).match(new RegExp('(\\s|^)'+attrValue+'(\\s|$)'))) }; break;
            case '|': checkFunction = function(e) { return (e.getAttribute(attrName).match(new RegExp('^'+attrValue+'-?'))) }; break;
            case '^': checkFunction = function(e) { return (e.getAttribute(attrName).indexOf(attrValue) == 0) }; break;
            case '$': checkFunction = function(e) { return (e.getAttribute(attrName).lastIndexOf(attrValue) == e.getAttribute(attrName).length - attrValue.length) }; break;
            case '*': checkFunction = function(e) { return (e.getAttribute(attrName).indexOf(attrValue) > -1) }; break;
            default : checkFunction = function(e) { return e.getAttribute(attrName) };
          }
          currentContext = [];
          var currentContextIndex = 0;
          for (var k = 0; k < found.length; k++) {
            if (checkFunction(found[k])) {
              currentContext[currentContextIndex++] = found[k];
            }
          }
          continue;
        }
        tagName = token;
        var found = [], foundCount = 0;
        for (var h = 0; h < currentContext.length; h++) {
          var elements = currentContext[h].getElementsByTagName(tagName);
          for (var j = 0; j < elements.length; j++) {
            found[foundCount++] = elements[j];
          }
        }
        currentContext = found;
      }
      resultList = [].concat(resultList,currentContext);
    }
    return resultList;
  },
  scrollSize: (function(){
    var content, hold, sizeBefore, sizeAfter;
    function buildSizer(){
      if(hold) removeSizer();
      content = document.createElement('div');
      hold = document.createElement('div');
      hold.style.cssText = 'position:absolute;overflow:hidden;width:100px;height:100px';
      hold.appendChild(content);
      document.body.appendChild(hold);
    }
    function removeSizer(){
      document.body.removeChild(hold);
      hold = null;
    }
    function calcSize(vertical) {
      buildSizer();
      content.style.cssText = 'height:'+(vertical ? '100%' : '200px');
      sizeBefore = (vertical ? content.offsetHeight : content.offsetWidth);
      hold.style.overflow = 'scroll'; content.innerHTML = 1;
      sizeAfter = (vertical ? content.offsetHeight : content.offsetWidth);
      if(vertical && hold.clientHeight) sizeAfter = hold.clientHeight;
      removeSizer();
      return sizeBefore - sizeAfter;
    }
    return {
      getWidth:function(){
        return calcSize(false);
      },
      getHeight:function(){
        return calcSize(true)
      }
    }
  }()),
  domReady: function (handler){
    var called = false
    function ready() {
      if (called) return;
      called = true;
      handler();
    }
    if (document.addEventListener) {
      document.addEventListener("DOMContentLoaded", ready, false);
    } else if (document.attachEvent) {
      if (document.documentElement.doScroll && window == window.top) {
        function tryScroll(){
          if (called) return
          if (!document.body) return
          try {
            document.documentElement.doScroll("left")
            ready()
          } catch(e) {
            setTimeout(tryScroll, 0)
          }
        }
        tryScroll()
      }
      document.attachEvent("onreadystatechange", function(){
        if (document.readyState === "complete") {
          ready()
        }
      })
    }
    if (window.addEventListener) window.addEventListener('load', ready, false)
    else if (window.attachEvent) window.attachEvent('onload', ready)
  },
  event: (function(){
    var guid = 0;
    function fixEvent(e) {
      e = e || window.event;
      if (e.isFixed) {
        return e;
      }
      e.isFixed = true;
      e.preventDefault = e.preventDefault || function(){this.returnValue = false}
      e.stopPropagation = e.stopPropagaton || function(){this.cancelBubble = true}
      if (!e.target) {
        e.target = e.srcElement
      }
      if (!e.relatedTarget && e.fromElement) {
        e.relatedTarget = e.fromElement == e.target ? e.toElement : e.fromElement;
      }
      if (e.pageX == null && e.clientX != null) {
        var html = document.documentElement, body = document.body;
        e.pageX = e.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0);
        e.pageY = e.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0);
      }
      if (!e.which && e.button) {
        e.which = e.button & 1 ? 1 : (e.button & 2 ? 3 : (e.button & 4 ? 2 : 0));
      }
      if(e.type === "DOMMouseScroll" || e.type === 'mousewheel') {
        e.mWheelDelta = 0;
        if (e.wheelDelta) {
          e.mWheelDelta = e.wheelDelta/120;
        } else if (e.detail) {
          e.mWheelDelta = -e.detail/3;
        }
      }
      return e;
    }
    function commonHandle(event, customScope) {
      event = fixEvent(event);
      var handlers = this.events[event.type];
      for (var g in handlers) {
        var handler = handlers[g];
        var ret = handler.call(customScope || this, event);
        if (ret === false) {
          event.preventDefault()
          event.stopPropagation()
        }
      }
    }
    var publicAPI = {
      add: function(elem, type, handler, forcedScope) {
        if (elem.setInterval && (elem != window && !elem.frameElement)) {
          elem = window;
        }
        if (!handler.guid) {
          handler.guid = ++guid;
        }
        if (!elem.events) {
          elem.events = {};
          elem.handle = function(event) {
            return commonHandle.call(elem, event);
          }
        }
        if (!elem.events[type]) {
          elem.events[type] = {};
          if (elem.addEventListener) elem.addEventListener(type, elem.handle, false);
          else if (elem.attachEvent) elem.attachEvent("on" + type, elem.handle);
          if(type === 'mousewheel') {
            publicAPI.add(elem, 'DOMMouseScroll', handler, forcedScope);
          }
        }
        var fakeHandler = jcf.lib.bind(handler, forcedScope);
        fakeHandler.guid = handler.guid;
        elem.events[type][handler.guid] = forcedScope ? fakeHandler : handler;
      },
      remove: function(elem, type, handler) {
        var handlers = elem.events && elem.events[type];
        if (!handlers) return;
        delete handlers[handler.guid];
        for(var any in handlers) return;
        if (elem.removeEventListener) elem.removeEventListener(type, elem.handle, false);
        else if (elem.detachEvent) elem.detachEvent("on" + type, elem.handle);
        delete elem.events[type];
        for (var any in elem.events) return;
        try {
          delete elem.handle;
          delete elem.events;
        } catch(e) {
          if(elem.removeAttribute) {
            elem.removeAttribute("handle");
            elem.removeAttribute("events");
          }
        }
        if(type === 'mousewheel') {
          publicAPI.remove(elem, 'DOMMouseScroll', handler);
        }
      }
    }
    return publicAPI;
  }())
}

// custom select module
jcf.addModule({
  name:'select',
  selector:'select',
  defaultOptions: {
    useNativeDropOnMobileDevices: true,
    hideDropOnScroll: false,
    showNativeDrop: false,
    handleDropPosition: false,
    selectDropPosition: 'bottom', // or 'top'
    wrapperClass:'select-area',
    focusClass:'select-focus',
    dropActiveClass:'select-active',
    selectedClass:'item-selected',
    currentSelectedClass:'current-selected',
    disabledClass:'select-disabled',
    valueSelector:'span.center',
    optGroupClass:'optgroup',
    openerSelector:'a.select-opener',
    selectStructure:'<span class="left"></span><span class="center"></span><a class="select-opener"></a>',
    wrapperTag: 'span',
    classPrefix:'select-',
    dropMaxHeight: 500,
    dropFlippedClass: 'select-options-flipped',
    dropHiddenClass:'options-hidden',
    dropScrollableClass:'options-overflow',
    dropClass:'select-options',
    dropClassPrefix:'drop-',
    dropStructure:'<div class="drop-holder"><div class="drop-list"></div></div>',
    dropSelector:'div.drop-list'
  },
  checkElement: function(el){
    return (!el.size && !el.multiple);
  },
  setupWrapper: function(){
    jcf.lib.addClass(this.fakeElement, this.options.wrapperClass);
    this.realElement.parentNode.insertBefore(this.fakeElement, this.realElement);
    this.fakeElement.innerHTML = this.options.selectStructure;
    this.fakeElement.style.width = (this.realElement.offsetWidth > 0 ? this.realElement.offsetWidth + 'px' : 'auto');

    // show native drop if specified in options
    if(this.options.useNativeDropOnMobileDevices && (jcf.isTouchDevice || jcf.isWinPhoneDevice)) {
      this.options.showNativeDrop = true;
    }
    if(this.options.showNativeDrop) {
      this.fakeElement.appendChild(this.realElement);
      jcf.lib.removeClass(this.realElement, this.options.hiddenClass);
      jcf.lib.setStyles(this.realElement, {
        top:0,
        left:0,
        margin:0,
        padding:0,
        opacity:0,
        border:'none',
        position:'absolute',
        width: jcf.lib.getInnerWidth(this.fakeElement) - 1,
        height: jcf.lib.getInnerHeight(this.fakeElement) - 1
      });
      jcf.lib.event.add(this.realElement, jcf.eventPress, function(){
        this.realElement.title = '';
      }, this)
    }

    // create select body
    this.opener = jcf.lib.queryBySelector(this.options.openerSelector, this.fakeElement)[0];
    this.valueText = jcf.lib.queryBySelector(this.options.valueSelector, this.fakeElement)[0];
    jcf.lib.disableTextSelection(this.valueText);
    this.opener.jcf = this;

    if(!this.options.showNativeDrop) {
      this.createDropdown();
      this.refreshState();
      this.onControlReady(this);
      this.hideDropdown(true);
    } else {
      this.refreshState();
    }
    this.addEvents();
  },
  addEvents: function(){
    if(this.options.showNativeDrop) {
      jcf.lib.event.add(this.realElement, 'click', this.onChange, this);
    } else {
      jcf.lib.event.add(this.fakeElement, 'click', this.toggleDropdown, this);
    }
    jcf.lib.event.add(this.realElement, 'change', this.onChange, this);
  },
  onFakeClick: function() {
    // do nothing (drop toggles by toggleDropdown method)
  },
  onFocus: function(){
    jcf.modules[this.name].superclass.onFocus.apply(this, arguments);
    if(!this.options.showNativeDrop) {
      // Mac Safari Fix
      if(jcf.lib.browser.safariMac) {
        this.realElement.setAttribute('size','2');
      }
      jcf.lib.event.add(this.realElement, 'keydown', this.onKeyDown, this);
      if(jcf.activeControl && jcf.activeControl != this) {
        jcf.activeControl.hideDropdown();
        jcf.activeControl = this;
      }
    }
  },
  onBlur: function(){
    if(!this.options.showNativeDrop) {
      // Mac Safari Fix
      if(jcf.lib.browser.safariMac) {
        this.realElement.removeAttribute('size');
      }
      if(!this.isActiveDrop() || !this.isOverDrop()) {
        jcf.modules[this.name].superclass.onBlur.apply(this);
        if(jcf.activeControl === this) jcf.activeControl = null;
        if(!jcf.isTouchDevice) {
          this.hideDropdown();
        }
      }
      jcf.lib.event.remove(this.realElement, 'keydown', this.onKeyDown);
    } else {
      jcf.modules[this.name].superclass.onBlur.apply(this);
    }
  },
  onChange: function() {
    this.refreshState();
  },
  onKeyDown: function(e){
    this.dropOpened = true;
    jcf.tmpFlag = true;
    setTimeout(function(){jcf.tmpFlag = false},100);
    var context = this;
    context.keyboardFix = true;
    setTimeout(function(){
      context.refreshState();
    },10);
    if(e.keyCode == 13) {
      context.toggleDropdown.apply(context);
      return false;
    }
  },
  onResizeWindow: function(e){
    if(this.isActiveDrop()) {
      this.hideDropdown();
    }
  },
  onScrollWindow: function(e){
    if(this.options.hideDropOnScroll) {
      this.hideDropdown();
    } else if(this.isActiveDrop()) {
      this.positionDropdown();
    }
  },
  onOptionClick: function(e){
    var opener = e.target && e.target.tagName && e.target.tagName.toLowerCase() == 'li' ? e.target : jcf.lib.getParent(e.target, 'li');
    if(opener) {
      this.dropOpened = true;
      this.realElement.selectedIndex = parseInt(opener.getAttribute('rel'));
      if(jcf.isTouchDevice) {
        this.onFocus();
      } else {
        this.realElement.focus();
      }
      this.refreshState();
      this.hideDropdown();
      jcf.lib.fireEvent(this.realElement, 'change');
    }
    return false;
  },
  onClickOutside: function(e){
    if(jcf.tmpFlag) {
      jcf.tmpFlag = false;
      return;
    }
    if(!jcf.lib.isParent(this.fakeElement, e.target) && !jcf.lib.isParent(this.selectDrop, e.target)) {
      this.hideDropdown();
    }
  },
  onDropHover: function(e){
    if(!this.keyboardFix) {
      this.hoverFlag = true;
      var opener = e.target && e.target.tagName && e.target.tagName.toLowerCase() == 'li' ? e.target : jcf.lib.getParent(e.target, 'li');
      if(opener) {
        this.realElement.selectedIndex = parseInt(opener.getAttribute('rel'));
        this.refreshSelectedClass(parseInt(opener.getAttribute('rel')));
      }
    } else {
      this.keyboardFix = false;
    }
  },
  onDropLeave: function(){
    this.hoverFlag = false;
  },
  isActiveDrop: function(){
    return !jcf.lib.hasClass(this.selectDrop, this.options.dropHiddenClass);
  },
  isOverDrop: function(){
    return this.hoverFlag;
  },
  createDropdown: function(){
    // remove old dropdown if exists
    if(this.selectDrop) {
      this.selectDrop.parentNode.removeChild(this.selectDrop);
    }

    // create dropdown holder
    this.selectDrop = document.createElement('div');
    this.selectDrop.className = this.options.dropClass;
    this.selectDrop.innerHTML = this.options.dropStructure;
    jcf.lib.setStyles(this.selectDrop, {position:'absolute'});
    this.selectList = jcf.lib.queryBySelector(this.options.dropSelector,this.selectDrop)[0];
    jcf.lib.addClass(this.selectDrop, this.options.dropHiddenClass);
    document.body.appendChild(this.selectDrop);
    this.selectDrop.jcf = this;
    jcf.lib.event.add(this.selectDrop, 'click', this.onOptionClick, this);
    jcf.lib.event.add(this.selectDrop, 'mouseover', this.onDropHover, this);
    jcf.lib.event.add(this.selectDrop, 'mouseout', this.onDropLeave, this);
    this.buildDropdown();
  },
  buildDropdown: function() {
    // build select options / optgroups
    this.buildDropdownOptions();

    // position and resize dropdown
    this.positionDropdown();

    // cut dropdown if height exceedes
    this.buildDropdownScroll();
  },
  buildDropdownOptions: function() {
    this.resStructure = '';
    this.optNum = 0;
    for(var i = 0; i < this.realElement.children.length; i++) {
      this.resStructure += this.buildElement(this.realElement.children[i], i) +'\n';
    }
    this.selectList.innerHTML = this.resStructure;
  },
  buildDropdownScroll: function() {
    jcf.lib.addClass(this.selectDrop, jcf.lib.getAllClasses(this.realElement.className, this.options.dropClassPrefix, jcf.baseOptions.hiddenClass));
    if(this.options.dropMaxHeight) {
      if(this.selectDrop.offsetHeight > this.options.dropMaxHeight) {
        this.selectList.style.height = this.options.dropMaxHeight+'px';
        this.selectList.style.overflow = 'auto';
        this.selectList.style.overflowX = 'hidden';
        jcf.lib.addClass(this.selectDrop, this.options.dropScrollableClass);
      }
    }
  },
  parseOptionTitle: function(optTitle) {
    return (typeof optTitle === 'string' && /\.(jpg|gif|png|bmp|jpeg)(.*)?$/i.test(optTitle)) ? optTitle : '';
  },
  buildElement: function(obj, index){
    // build option
    var res = '', optImage;
    if(obj.tagName.toLowerCase() == 'option') {
      if(!jcf.lib.prevSibling(obj) || jcf.lib.prevSibling(obj).tagName.toLowerCase() != 'option') {
        res += '<ul>';
      }

      optImage = this.parseOptionTitle(obj.title);
      res += '<li rel="'+(this.optNum++)+'" class="'+(obj.className? obj.className + ' ' : '')+(index % 2 ? 'option-even ' : '')+'jcfcalc"><a href="#">'+(optImage ? '<img src="'+optImage+'" alt="" />' : '')+'<span>' + obj.innerHTML + '</span></a></li>';
      if(!jcf.lib.nextSibling(obj) || jcf.lib.nextSibling(obj).tagName.toLowerCase() != 'option') {
        res += '</ul>';
      }
      return res;
    }
    // build option group with options
    else if(obj.tagName.toLowerCase() == 'optgroup' && obj.label) {
      res += '<div class="'+this.options.optGroupClass+'">';
      res += '<strong class="jcfcalc"><em>'+(obj.label)+'</em></strong>';
      for(var i = 0; i < obj.children.length; i++) {
        res += this.buildElement(obj.children[i], i);
      }
      res += '</div>';
      return res;
    }
  },
  positionDropdown: function(){
    var ofs = jcf.lib.getOffset(this.fakeElement), selectAreaHeight = this.fakeElement.offsetHeight, selectDropHeight = this.selectDrop.offsetHeight;
    var fitInTop = ofs.top - selectDropHeight >= jcf.lib.getScrollTop() && jcf.lib.getScrollTop() + jcf.lib.getWindowHeight() < ofs.top + selectAreaHeight + selectDropHeight;


    if((this.options.handleDropPosition && fitInTop) || this.options.selectDropPosition === 'top') {
      this.selectDrop.style.top = (ofs.top - selectDropHeight)+'px';
      jcf.lib.addClass(this.selectDrop, this.options.dropFlippedClass);
      jcf.lib.addClass(this.fakeElement, this.options.dropFlippedClass);
    } else {
      this.selectDrop.style.top = (ofs.top + selectAreaHeight)+'px';
      jcf.lib.removeClass(this.selectDrop, this.options.dropFlippedClass);
      jcf.lib.removeClass(this.fakeElement, this.options.dropFlippedClass);
    }
    this.selectDrop.style.left = ofs.left+'px';
    this.selectDrop.style.width = this.fakeElement.offsetWidth+'px';
  },
  showDropdown: function(){
    document.body.appendChild(this.selectDrop);
    jcf.lib.removeClass(this.selectDrop, this.options.dropHiddenClass);
    jcf.lib.addClass(this.fakeElement,this.options.dropActiveClass);
    this.positionDropdown();

    // highlight current active item
    var activeItem = this.getFakeActiveOption();
    this.removeClassFromItems(this.options.currentSelectedClass);
    jcf.lib.addClass(activeItem, this.options.currentSelectedClass);

    // show current dropdown
    jcf.lib.event.add(window, 'resize', this.onResizeWindow, this);
    jcf.lib.event.add(window, 'scroll', this.onScrollWindow, this);
    jcf.lib.event.add(document, jcf.eventPress, this.onClickOutside, this);
    this.positionDropdown();
  },
  hideDropdown: function(partial){
    if(this.selectDrop.parentNode) {
      if(this.selectDrop.offsetWidth) {
        this.selectDrop.parentNode.removeChild(this.selectDrop);
      }
      if(partial) {
        return;
      }
    }
    if(typeof this.origSelectedIndex === 'number') {
      this.realElement.selectedIndex = this.origSelectedIndex;
    }
    jcf.lib.removeClass(this.fakeElement,this.options.dropActiveClass);
    jcf.lib.addClass(this.selectDrop, this.options.dropHiddenClass);
    jcf.lib.event.remove(window, 'resize', this.onResizeWindow);
    jcf.lib.event.remove(window, 'scroll', this.onScrollWindow);
    jcf.lib.event.remove(document.documentElement, jcf.eventPress, this.onClickOutside);
    if(jcf.isTouchDevice) {
      this.onBlur();
    }
  },
  toggleDropdown: function(){
    if(!this.realElement.disabled) {
      if(jcf.isTouchDevice) {
        this.onFocus();
      } else {
        this.realElement.focus();
      }
      if(this.isActiveDrop()) {
        this.hideDropdown();
      } else {
        this.showDropdown();
      }
      this.refreshState();
    }
  },
  scrollToItem: function(){
    if(this.isActiveDrop()) {
      var dropHeight = this.selectList.offsetHeight;
      var offsetTop = this.calcOptionOffset(this.getFakeActiveOption());
      var sTop = this.selectList.scrollTop;
      var oHeight = this.getFakeActiveOption().offsetHeight;
      //offsetTop+=sTop;

      if(offsetTop >= sTop + dropHeight) {
        this.selectList.scrollTop = offsetTop - dropHeight + oHeight;
      } else if(offsetTop < sTop) {
        this.selectList.scrollTop = offsetTop;
      }
    }
  },
  getFakeActiveOption: function(c) {
    return jcf.lib.queryBySelector('li[rel="'+(typeof c === 'number' ? c : this.realElement.selectedIndex) +'"]',this.selectList)[0];
  },
  calcOptionOffset: function(fake) {
    var h = 0;
    var els = jcf.lib.queryBySelector('.jcfcalc',this.selectList);
    for(var i = 0; i < els.length; i++) {
      if(els[i] == fake) break;
      h+=els[i].offsetHeight;
    }
    return h;
  },
  childrenHasItem: function(hold,item) {
    var items = hold.getElementsByTagName('*');
    for(i = 0; i < items.length; i++) {
      if(items[i] == item) return true;
    }
    return false;
  },
  removeClassFromItems: function(className){
    var children = jcf.lib.queryBySelector('li',this.selectList);
    for(var i = children.length - 1; i >= 0; i--) {
      jcf.lib.removeClass(children[i], className);
    }
  },
  setSelectedClass: function(c){
    jcf.lib.addClass(this.getFakeActiveOption(c), this.options.selectedClass);
  },
  refreshSelectedClass: function(c){
    if(!this.options.showNativeDrop) {
      this.removeClassFromItems(this.options.selectedClass);
      this.setSelectedClass(c);
    }
    if(this.realElement.disabled) {
      jcf.lib.addClass(this.fakeElement, this.options.disabledClass);
      if(this.labelFor) {
        jcf.lib.addClass(this.labelFor, this.options.labelDisabledClass);
      }
    } else {
      jcf.lib.removeClass(this.fakeElement, this.options.disabledClass);
      if(this.labelFor) {
        jcf.lib.removeClass(this.labelFor, this.options.labelDisabledClass);
      }
    }
  },
  refreshSelectedText: function() {
    if(!this.dropOpened && this.realElement.title) {
      this.valueText.innerHTML = this.realElement.title;
    } else {
      if(this.realElement.options[this.realElement.selectedIndex].title) {
        var optImage = this.parseOptionTitle(this.realElement.options[this.realElement.selectedIndex].title);
        this.valueText.innerHTML = (optImage ? '<img src="'+optImage+'" alt="" />' : '') + this.realElement.options[this.realElement.selectedIndex].innerHTML;
      } else {
        this.valueText.innerHTML = this.realElement.options[this.realElement.selectedIndex].innerHTML;
      }
    }
  },
  refreshState: function(){
    this.origSelectedIndex = this.realElement.selectedIndex;
    this.refreshSelectedClass();
    this.refreshSelectedText();
    if(!this.options.showNativeDrop) {
      this.positionDropdown();
      if(this.selectDrop.offsetWidth) {
        this.scrollToItem();
      }
    }
  }
});

// custom radio module
jcf.addModule({
  name:'radio',
  selector: 'input[type="radio"]',
  defaultOptions: {
    wrapperClass:'rad-area',
    focusClass:'rad-focus',
    checkedClass:'rad-checked',
    uncheckedClass:'rad-unchecked',
    disabledClass:'rad-disabled',
    radStructure:'<span></span>'
  },
  getRadioGroup: function(item){
    var name = item.getAttribute('name');
    if(name) {
      return jcf.lib.queryBySelector('input[name="'+name+'"]', jcf.lib.getParent('form'));
    } else {
      return [item];
    }
  },
  setupWrapper: function(){
    jcf.lib.addClass(this.fakeElement, this.options.wrapperClass);
    this.fakeElement.innerHTML = this.options.radStructure;
    this.realElement.parentNode.insertBefore(this.fakeElement, this.realElement);
    this.refreshState();
    this.addEvents();
  },
  addEvents: function(){
    jcf.lib.event.add(this.fakeElement, 'click', this.toggleRadio, this);
    if(this.labelFor) {
      jcf.lib.event.add(this.labelFor, 'click', this.toggleRadio, this);
    }
  },
  onFocus: function(e) {
    jcf.modules[this.name].superclass.onFocus.apply(this, arguments);
    setTimeout(jcf.lib.bind(function(){
      this.refreshState();
    },this),10);
  },
  toggleRadio: function(){
    if(!this.realElement.disabled && !this.realElement.checked) {
      this.realElement.checked = true;
      jcf.lib.fireEvent(this.realElement, 'change');
    }
    this.refreshState();
  },
  refreshState: function(){
    var els = this.getRadioGroup(this.realElement);
    for(var i = 0; i < els.length; i++) {
      var curEl = els[i].jcf;
      if(curEl) {
        if(curEl.realElement.checked) {
          jcf.lib.addClass(curEl.fakeElement, curEl.options.checkedClass);
          jcf.lib.removeClass(curEl.fakeElement, curEl.options.uncheckedClass);
          if(curEl.labelFor) {
            jcf.lib.addClass(curEl.labelFor, curEl.options.labelActiveClass);
          }
        } else {
          jcf.lib.removeClass(curEl.fakeElement, curEl.options.checkedClass);
          jcf.lib.addClass(curEl.fakeElement, curEl.options.uncheckedClass);
          if(curEl.labelFor) {
            jcf.lib.removeClass(curEl.labelFor, curEl.options.labelActiveClass);
          }
        }
        if(curEl.realElement.disabled) {
          jcf.lib.addClass(curEl.fakeElement, curEl.options.disabledClass);
          if(curEl.labelFor) {
            jcf.lib.addClass(curEl.labelFor, curEl.options.labelDisabledClass);
          }
        } else {
          jcf.lib.removeClass(curEl.fakeElement, curEl.options.disabledClass);
          if(curEl.labelFor) {
            jcf.lib.removeClass(curEl.labelFor, curEl.options.labelDisabledClass);
          }
        }
      }
    }
  }
});

//// custom checkbox module
//jcf.addModule({
//  name:'checkbox',
//  selector:'input[type="checkbox"]',
//  defaultOptions: {
//    wrapperClass:'chk-area',
//    focusClass:'chk-focus',
//    checkedClass:'chk-checked',
//    labelActiveClass:'chk-label-active',
//    uncheckedClass:'chk-unchecked',
//    disabledClass:'chk-disabled',
//    chkStructure:'<span></span>'
//  },
//  setupWrapper: function(){
//    jcf.lib.addClass(this.fakeElement, this.options.wrapperClass);
//    this.fakeElement.innerHTML = this.options.chkStructure;
//    this.realElement.parentNode.insertBefore(this.fakeElement, this.realElement);
//    jcf.lib.event.add(this.realElement, 'click', this.onRealClick, this);
//    this.refreshState();
//  },
//  isLinkTarget: function(target, limitParent) {
//    while(target.parentNode || target === limitParent) {
//      if(target.tagName.toLowerCase() === 'a') {
//        return true;
//      }
//      target = target.parentNode;
//    }
//  },
//  onFakePressed: function() {
//    jcf.modules[this.name].superclass.onFakePressed.apply(this, arguments);
//    if(!this.realElement.disabled) {
//      this.realElement.focus();
//    }
//  },
//  onFakeClick: function(e) {
//    jcf.modules[this.name].superclass.onFakeClick.apply(this, arguments);
//    this.tmpTimer = setTimeout(jcf.lib.bind(function(){
//      this.toggle();
//    },this),10);
//    if(!this.isLinkTarget(e.target, this.labelFor)) {
//      return false;
//    }
//  },
//  onRealClick: function(e) {
//    setTimeout(jcf.lib.bind(function(){
//      this.refreshState();
//    },this),10);
//    e.stopPropagation();
//  },
//  toggle: function(e){
//    if(!this.realElement.disabled) {
//      if(this.realElement.checked) {
//        this.realElement.checked = false;
//      } else {
//        this.realElement.checked = true;
//      }
//    }
//    this.refreshState();
//    jcf.lib.fireEvent(this.realElement, 'change');
//    return false;
//  },
//  refreshState: function(){
//    if(this.realElement.checked) {
//      jcf.lib.addClass(this.fakeElement, this.options.checkedClass);
//      jcf.lib.removeClass(this.fakeElement, this.options.uncheckedClass);
//      if(this.labelFor) {
//        jcf.lib.addClass(this.labelFor, this.options.labelActiveClass);
//      }
//    } else {
//      jcf.lib.removeClass(this.fakeElement, this.options.checkedClass);
//      jcf.lib.addClass(this.fakeElement, this.options.uncheckedClass);
//      if(this.labelFor) {
//        jcf.lib.removeClass(this.labelFor, this.options.labelActiveClass);
//      }
//    }
//    if(this.realElement.disabled) {
//      jcf.lib.addClass(this.fakeElement, this.options.disabledClass);
//      if(this.labelFor) {
//        jcf.lib.addClass(this.labelFor, this.options.labelDisabledClass);
//      }
//    } else {
//      jcf.lib.removeClass(this.fakeElement, this.options.disabledClass);
//      if(this.labelFor) {
//        jcf.lib.removeClass(this.labelFor, this.options.labelDisabledClass);
//      }
//    }
//  }
//});


/*! jQuery UI - v1.10.3 - 2013-09-08
 * http://jqueryui.com
 * Includes: jquery.ui.core.js, jquery.ui.datepicker.js
 * Copyright 2013 jQuery Foundation and other contributors; Licensed MIT */

(function(t,e){function n(e,n){var r,s,o,a=e.nodeName.toLowerCase();return"area"===a?(r=e.parentNode,s=r.name,e.href&&s&&"map"===r.nodeName.toLowerCase()?(o=t("img[usemap=#"+s+"]")[0],!!o&&i(o)):!1):(/input|select|textarea|button|object/.test(a)?!e.disabled:"a"===a?e.href||n:n)&&i(e)}function i(e){return t.expr.filters.visible(e)&&!t(e).parents().addBack().filter(function(){return"hidden"===t.css(this,"visibility")}).length}var r=0,s=/^ui-id-\d+$/;t.ui=t.ui||{},t.extend(t.ui,{version:"1.10.3",keyCode:{BACKSPACE:8,COMMA:188,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,LEFT:37,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SPACE:32,TAB:9,UP:38}}),t.fn.extend({focus:function(e){return function(n,i){return"number"==typeof n?this.each(function(){var e=this;setTimeout(function(){t(e).focus(),i&&i.call(e)},n)}):e.apply(this,arguments)}}(t.fn.focus),scrollParent:function(){var e;return e=t.ui.ie&&/(static|relative)/.test(this.css("position"))||/absolute/.test(this.css("position"))?this.parents().filter(function(){return/(relative|absolute|fixed)/.test(t.css(this,"position"))&&/(auto|scroll)/.test(t.css(this,"overflow")+t.css(this,"overflow-y")+t.css(this,"overflow-x"))}).eq(0):this.parents().filter(function(){return/(auto|scroll)/.test(t.css(this,"overflow")+t.css(this,"overflow-y")+t.css(this,"overflow-x"))}).eq(0),/fixed/.test(this.css("position"))||!e.length?t(document):e},zIndex:function(n){if(n!==e)return this.css("zIndex",n);if(this.length)for(var i,r,s=t(this[0]);s.length&&s[0]!==document;){if(i=s.css("position"),("absolute"===i||"relative"===i||"fixed"===i)&&(r=parseInt(s.css("zIndex"),10),!isNaN(r)&&0!==r))return r;s=s.parent()}return 0},uniqueId:function(){return this.each(function(){this.id||(this.id="ui-id-"+ ++r)})},removeUniqueId:function(){return this.each(function(){s.test(this.id)&&t(this).removeAttr("id")})}}),t.extend(t.expr[":"],{data:t.expr.createPseudo?t.expr.createPseudo(function(e){return function(n){return!!t.data(n,e)}}):function(e,n,i){return!!t.data(e,i[3])},focusable:function(e){return n(e,!isNaN(t.attr(e,"tabindex")))},tabbable:function(e){var i=t.attr(e,"tabindex"),r=isNaN(i);return(r||i>=0)&&n(e,!r)}}),t("<a>").outerWidth(1).jquery||t.each(["Width","Height"],function(n,i){function r(e,n,i,r){return t.each(s,function(){n-=parseFloat(t.css(e,"padding"+this))||0,i&&(n-=parseFloat(t.css(e,"border"+this+"Width"))||0),r&&(n-=parseFloat(t.css(e,"margin"+this))||0)}),n}var s="Width"===i?["Left","Right"]:["Top","Bottom"],o=i.toLowerCase(),a={innerWidth:t.fn.innerWidth,innerHeight:t.fn.innerHeight,outerWidth:t.fn.outerWidth,outerHeight:t.fn.outerHeight};t.fn["inner"+i]=function(n){return n===e?a["inner"+i].call(this):this.each(function(){t(this).css(o,r(this,n)+"px")})},t.fn["outer"+i]=function(e,n){return"number"!=typeof e?a["outer"+i].call(this,e):this.each(function(){t(this).css(o,r(this,e,!0,n)+"px")})}}),t.fn.addBack||(t.fn.addBack=function(t){return this.add(null==t?this.prevObject:this.prevObject.filter(t))}),t("<a>").data("a-b","a").removeData("a-b").data("a-b")&&(t.fn.removeData=function(e){return function(n){return arguments.length?e.call(this,t.camelCase(n)):e.call(this)}}(t.fn.removeData)),t.ui.ie=!!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()),t.support.selectstart="onselectstart"in document.createElement("div"),t.fn.extend({disableSelection:function(){return this.bind((t.support.selectstart?"selectstart":"mousedown")+".ui-disableSelection",function(t){t.preventDefault()})},enableSelection:function(){return this.unbind(".ui-disableSelection")}}),t.extend(t.ui,{plugin:{add:function(e,n,i){var r,s=t.ui[e].prototype;for(r in i)s.plugins[r]=s.plugins[r]||[],s.plugins[r].push([n,i[r]])},call:function(t,e,n){var i,r=t.plugins[e];if(r&&t.element[0].parentNode&&11!==t.element[0].parentNode.nodeType)for(i=0;r.length>i;i++)t.options[r[i][0]]&&r[i][1].apply(t.element,n)}},hasScroll:function(e,n){if("hidden"===t(e).css("overflow"))return!1;var i=n&&"left"===n?"scrollLeft":"scrollTop",r=!1;return e[i]>0?!0:(e[i]=1,r=e[i]>0,e[i]=0,r)}})})(jQuery);(function(t,e){function i(){this._curInst=null,this._keyEvent=!1,this._disabledInputs=[],this._datepickerShowing=!1,this._inDialog=!1,this._mainDivId="ui-datepicker-div",this._inlineClass="ui-datepicker-inline",this._appendClass="ui-datepicker-append",this._triggerClass="ui-datepicker-trigger",this._dialogClass="ui-datepicker-dialog",this._disableClass="ui-datepicker-disabled",this._unselectableClass="ui-datepicker-unselectable",this._currentClass="ui-datepicker-current-day",this._dayOverClass="ui-datepicker-days-cell-over",this.regional=[],this.regional[""]={closeText:"Done",prevText:"Prev",nextText:"Next",currentText:"Today",monthNames:["January","February","March","April","May","June","July","August","September","October","November","December"],monthNamesShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],dayNames:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],dayNamesShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],dayNamesMin:["Su","Mo","Tu","We","Th","Fr","Sa"],weekHeader:"Wk",dateFormat:"mm/dd/yy",firstDay:0,isRTL:!1,showMonthAfterYear:!1,yearSuffix:""},this._defaults={showOn:"focus",showAnim:"fadeIn",showOptions:{},defaultDate:null,appendText:"",buttonText:"...",buttonImage:"",buttonImageOnly:!1,hideIfNoPrevNext:!1,navigationAsDateFormat:!1,gotoCurrent:!1,changeMonth:!1,changeYear:!1,yearRange:"c-10:c+10",showOtherMonths:!1,selectOtherMonths:!1,showWeek:!1,calculateWeek:this.iso8601Week,shortYearCutoff:"+10",minDate:null,maxDate:null,duration:"fast",beforeShowDay:null,beforeShow:null,onSelect:null,onChangeMonthYear:null,onClose:null,numberOfMonths:1,showCurrentAtPos:0,stepMonths:1,stepBigMonths:12,altField:"",altFormat:"",constrainInput:!0,showButtonPanel:!1,autoSize:!1,disabled:!1},t.extend(this._defaults,this.regional[""]),this.dpDiv=s(t("<div id='"+this._mainDivId+"' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"))}function s(e){var i="button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";return e.delegate(i,"mouseout",function(){t(this).removeClass("ui-state-hover"),-1!==this.className.indexOf("ui-datepicker-prev")&&t(this).removeClass("ui-datepicker-prev-hover"),-1!==this.className.indexOf("ui-datepicker-next")&&t(this).removeClass("ui-datepicker-next-hover")}).delegate(i,"mouseover",function(){t.datepicker._isDisabledDatepicker(a.inline?e.parent()[0]:a.input[0])||(t(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"),t(this).addClass("ui-state-hover"),-1!==this.className.indexOf("ui-datepicker-prev")&&t(this).addClass("ui-datepicker-prev-hover"),-1!==this.className.indexOf("ui-datepicker-next")&&t(this).addClass("ui-datepicker-next-hover"))})}function n(e,i){t.extend(e,i);for(var s in i)null==i[s]&&(e[s]=i[s]);return e}t.extend(t.ui,{datepicker:{version:"1.10.3"}});var a,r="datepicker";t.extend(i.prototype,{markerClassName:"hasDatepicker",maxRows:4,_widgetDatepicker:function(){return this.dpDiv},setDefaults:function(t){return n(this._defaults,t||{}),this},_attachDatepicker:function(e,i){var s,n,a;s=e.nodeName.toLowerCase(),n="div"===s||"span"===s,e.id||(this.uuid+=1,e.id="dp"+this.uuid),a=this._newInst(t(e),n),a.settings=t.extend({},i||{}),"input"===s?this._connectDatepicker(e,a):n&&this._inlineDatepicker(e,a)},_newInst:function(e,i){var n=e[0].id.replace(/([^A-Za-z0-9_\-])/g,"\\\\$1");return{id:n,input:e,selectedDay:0,selectedMonth:0,selectedYear:0,drawMonth:0,drawYear:0,inline:i,dpDiv:i?s(t("<div class='"+this._inlineClass+" ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")):this.dpDiv}},_connectDatepicker:function(e,i){var s=t(e);i.append=t([]),i.trigger=t([]),s.hasClass(this.markerClassName)||(this._attachments(s,i),s.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp),this._autoSize(i),t.data(e,r,i),i.settings.disabled&&this._disableDatepicker(e))},_attachments:function(e,i){var s,n,a,r=this._get(i,"appendText"),o=this._get(i,"isRTL");i.append&&i.append.remove(),r&&(i.append=t("<span class='"+this._appendClass+"'>"+r+"</span>"),e[o?"before":"after"](i.append)),e.unbind("focus",this._showDatepicker),i.trigger&&i.trigger.remove(),s=this._get(i,"showOn"),("focus"===s||"both"===s)&&e.focus(this._showDatepicker),("button"===s||"both"===s)&&(n=this._get(i,"buttonText"),a=this._get(i,"buttonImage"),i.trigger=t(this._get(i,"buttonImageOnly")?t("<img/>").addClass(this._triggerClass).attr({src:a,alt:n,title:n}):t("<button type='button'></button>").addClass(this._triggerClass).html(a?t("<img/>").attr({src:a,alt:n,title:n}):n)),e[o?"before":"after"](i.trigger),i.trigger.click(function(){return t.datepicker._datepickerShowing&&t.datepicker._lastInput===e[0]?t.datepicker._hideDatepicker():t.datepicker._datepickerShowing&&t.datepicker._lastInput!==e[0]?(t.datepicker._hideDatepicker(),t.datepicker._showDatepicker(e[0])):t.datepicker._showDatepicker(e[0]),!1}))},_autoSize:function(t){if(this._get(t,"autoSize")&&!t.inline){var e,i,s,n,a=new Date(2009,11,20),r=this._get(t,"dateFormat");r.match(/[DM]/)&&(e=function(t){for(i=0,s=0,n=0;t.length>n;n++)t[n].length>i&&(i=t[n].length,s=n);return s},a.setMonth(e(this._get(t,r.match(/MM/)?"monthNames":"monthNamesShort"))),a.setDate(e(this._get(t,r.match(/DD/)?"dayNames":"dayNamesShort"))+20-a.getDay())),t.input.attr("size",this._formatDate(t,a).length)}},_inlineDatepicker:function(e,i){var s=t(e);s.hasClass(this.markerClassName)||(s.addClass(this.markerClassName).append(i.dpDiv),t.data(e,r,i),this._setDate(i,this._getDefaultDate(i),!0),this._updateDatepicker(i),this._updateAlternate(i),i.settings.disabled&&this._disableDatepicker(e),i.dpDiv.css("display","block"))},_dialogDatepicker:function(e,i,s,a,o){var h,l,c,u,d,p=this._dialogInst;return p||(this.uuid+=1,h="dp"+this.uuid,this._dialogInput=t("<input type='text' id='"+h+"' style='position: absolute; top: -100px; width: 0px;'/>"),this._dialogInput.keydown(this._doKeyDown),t("body").append(this._dialogInput),p=this._dialogInst=this._newInst(this._dialogInput,!1),p.settings={},t.data(this._dialogInput[0],r,p)),n(p.settings,a||{}),i=i&&i.constructor===Date?this._formatDate(p,i):i,this._dialogInput.val(i),this._pos=o?o.length?o:[o.pageX,o.pageY]:null,this._pos||(l=document.documentElement.clientWidth,c=document.documentElement.clientHeight,u=document.documentElement.scrollLeft||document.body.scrollLeft,d=document.documentElement.scrollTop||document.body.scrollTop,this._pos=[l/2-100+u,c/2-150+d]),this._dialogInput.css("left",this._pos[0]+20+"px").css("top",this._pos[1]+"px"),p.settings.onSelect=s,this._inDialog=!0,this.dpDiv.addClass(this._dialogClass),this._showDatepicker(this._dialogInput[0]),t.blockUI&&t.blockUI(this.dpDiv),t.data(this._dialogInput[0],r,p),this},_destroyDatepicker:function(e){var i,s=t(e),n=t.data(e,r);s.hasClass(this.markerClassName)&&(i=e.nodeName.toLowerCase(),t.removeData(e,r),"input"===i?(n.append.remove(),n.trigger.remove(),s.removeClass(this.markerClassName).unbind("focus",this._showDatepicker).unbind("keydown",this._doKeyDown).unbind("keypress",this._doKeyPress).unbind("keyup",this._doKeyUp)):("div"===i||"span"===i)&&s.removeClass(this.markerClassName).empty())},_enableDatepicker:function(e){var i,s,n=t(e),a=t.data(e,r);n.hasClass(this.markerClassName)&&(i=e.nodeName.toLowerCase(),"input"===i?(e.disabled=!1,a.trigger.filter("button").each(function(){this.disabled=!1}).end().filter("img").css({opacity:"1.0",cursor:""})):("div"===i||"span"===i)&&(s=n.children("."+this._inlineClass),s.children().removeClass("ui-state-disabled"),s.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",!1)),this._disabledInputs=t.map(this._disabledInputs,function(t){return t===e?null:t}))},_disableDatepicker:function(e){var i,s,n=t(e),a=t.data(e,r);n.hasClass(this.markerClassName)&&(i=e.nodeName.toLowerCase(),"input"===i?(e.disabled=!0,a.trigger.filter("button").each(function(){this.disabled=!0}).end().filter("img").css({opacity:"0.5",cursor:"default"})):("div"===i||"span"===i)&&(s=n.children("."+this._inlineClass),s.children().addClass("ui-state-disabled"),s.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",!0)),this._disabledInputs=t.map(this._disabledInputs,function(t){return t===e?null:t}),this._disabledInputs[this._disabledInputs.length]=e)},_isDisabledDatepicker:function(t){if(!t)return!1;for(var e=0;this._disabledInputs.length>e;e++)if(this._disabledInputs[e]===t)return!0;return!1},_getInst:function(e){try{return t.data(e,r)}catch(i){throw"Missing instance data for this datepicker"}},_optionDatepicker:function(i,s,a){var r,o,h,l,c=this._getInst(i);return 2===arguments.length&&"string"==typeof s?"defaults"===s?t.extend({},t.datepicker._defaults):c?"all"===s?t.extend({},c.settings):this._get(c,s):null:(r=s||{},"string"==typeof s&&(r={},r[s]=a),c&&(this._curInst===c&&this._hideDatepicker(),o=this._getDateDatepicker(i,!0),h=this._getMinMaxDate(c,"min"),l=this._getMinMaxDate(c,"max"),n(c.settings,r),null!==h&&r.dateFormat!==e&&r.minDate===e&&(c.settings.minDate=this._formatDate(c,h)),null!==l&&r.dateFormat!==e&&r.maxDate===e&&(c.settings.maxDate=this._formatDate(c,l)),"disabled"in r&&(r.disabled?this._disableDatepicker(i):this._enableDatepicker(i)),this._attachments(t(i),c),this._autoSize(c),this._setDate(c,o),this._updateAlternate(c),this._updateDatepicker(c)),e)},_changeDatepicker:function(t,e,i){this._optionDatepicker(t,e,i)},_refreshDatepicker:function(t){var e=this._getInst(t);e&&this._updateDatepicker(e)},_setDateDatepicker:function(t,e){var i=this._getInst(t);i&&(this._setDate(i,e),this._updateDatepicker(i),this._updateAlternate(i))},_getDateDatepicker:function(t,e){var i=this._getInst(t);return i&&!i.inline&&this._setDateFromField(i,e),i?this._getDate(i):null},_doKeyDown:function(e){var i,s,n,a=t.datepicker._getInst(e.target),r=!0,o=a.dpDiv.is(".ui-datepicker-rtl");if(a._keyEvent=!0,t.datepicker._datepickerShowing)switch(e.keyCode){case 9:t.datepicker._hideDatepicker(),r=!1;break;case 13:return n=t("td."+t.datepicker._dayOverClass+":not(."+t.datepicker._currentClass+")",a.dpDiv),n[0]&&t.datepicker._selectDay(e.target,a.selectedMonth,a.selectedYear,n[0]),i=t.datepicker._get(a,"onSelect"),i?(s=t.datepicker._formatDate(a),i.apply(a.input?a.input[0]:null,[s,a])):t.datepicker._hideDatepicker(),!1;case 27:t.datepicker._hideDatepicker();break;case 33:t.datepicker._adjustDate(e.target,e.ctrlKey?-t.datepicker._get(a,"stepBigMonths"):-t.datepicker._get(a,"stepMonths"),"M");break;case 34:t.datepicker._adjustDate(e.target,e.ctrlKey?+t.datepicker._get(a,"stepBigMonths"):+t.datepicker._get(a,"stepMonths"),"M");break;case 35:(e.ctrlKey||e.metaKey)&&t.datepicker._clearDate(e.target),r=e.ctrlKey||e.metaKey;break;case 36:(e.ctrlKey||e.metaKey)&&t.datepicker._gotoToday(e.target),r=e.ctrlKey||e.metaKey;break;case 37:(e.ctrlKey||e.metaKey)&&t.datepicker._adjustDate(e.target,o?1:-1,"D"),r=e.ctrlKey||e.metaKey,e.originalEvent.altKey&&t.datepicker._adjustDate(e.target,e.ctrlKey?-t.datepicker._get(a,"stepBigMonths"):-t.datepicker._get(a,"stepMonths"),"M");break;case 38:(e.ctrlKey||e.metaKey)&&t.datepicker._adjustDate(e.target,-7,"D"),r=e.ctrlKey||e.metaKey;break;case 39:(e.ctrlKey||e.metaKey)&&t.datepicker._adjustDate(e.target,o?-1:1,"D"),r=e.ctrlKey||e.metaKey,e.originalEvent.altKey&&t.datepicker._adjustDate(e.target,e.ctrlKey?+t.datepicker._get(a,"stepBigMonths"):+t.datepicker._get(a,"stepMonths"),"M");break;case 40:(e.ctrlKey||e.metaKey)&&t.datepicker._adjustDate(e.target,7,"D"),r=e.ctrlKey||e.metaKey;break;default:r=!1}else 36===e.keyCode&&e.ctrlKey?t.datepicker._showDatepicker(this):r=!1;r&&(e.preventDefault(),e.stopPropagation())},_doKeyPress:function(i){var s,n,a=t.datepicker._getInst(i.target);return t.datepicker._get(a,"constrainInput")?(s=t.datepicker._possibleChars(t.datepicker._get(a,"dateFormat")),n=String.fromCharCode(null==i.charCode?i.keyCode:i.charCode),i.ctrlKey||i.metaKey||" ">n||!s||s.indexOf(n)>-1):e},_doKeyUp:function(e){var i,s=t.datepicker._getInst(e.target);if(s.input.val()!==s.lastVal)try{i=t.datepicker.parseDate(t.datepicker._get(s,"dateFormat"),s.input?s.input.val():null,t.datepicker._getFormatConfig(s)),i&&(t.datepicker._setDateFromField(s),t.datepicker._updateAlternate(s),t.datepicker._updateDatepicker(s))}catch(n){}return!0},_showDatepicker:function(e){if(e=e.target||e,"input"!==e.nodeName.toLowerCase()&&(e=t("input",e.parentNode)[0]),!t.datepicker._isDisabledDatepicker(e)&&t.datepicker._lastInput!==e){var i,s,a,r,o,h,l;i=t.datepicker._getInst(e),t.datepicker._curInst&&t.datepicker._curInst!==i&&(t.datepicker._curInst.dpDiv.stop(!0,!0),i&&t.datepicker._datepickerShowing&&t.datepicker._hideDatepicker(t.datepicker._curInst.input[0])),s=t.datepicker._get(i,"beforeShow"),a=s?s.apply(e,[e,i]):{},a!==!1&&(n(i.settings,a),i.lastVal=null,t.datepicker._lastInput=e,t.datepicker._setDateFromField(i),t.datepicker._inDialog&&(e.value=""),t.datepicker._pos||(t.datepicker._pos=t.datepicker._findPos(e),t.datepicker._pos[1]+=e.offsetHeight),r=!1,t(e).parents().each(function(){return r|="fixed"===t(this).css("position"),!r}),o={left:t.datepicker._pos[0],top:t.datepicker._pos[1]},t.datepicker._pos=null,i.dpDiv.empty(),i.dpDiv.css({position:"absolute",display:"block",top:"-1000px"}),t.datepicker._updateDatepicker(i),o=t.datepicker._checkOffset(i,o,r),i.dpDiv.css({position:t.datepicker._inDialog&&t.blockUI?"static":r?"fixed":"absolute",display:"none",left:o.left+"px",top:o.top+"px"}),i.inline||(h=t.datepicker._get(i,"showAnim"),l=t.datepicker._get(i,"duration"),i.dpDiv.zIndex(t(e).zIndex()+1),t.datepicker._datepickerShowing=!0,t.effects&&t.effects.effect[h]?i.dpDiv.show(h,t.datepicker._get(i,"showOptions"),l):i.dpDiv[h||"show"](h?l:null),t.datepicker._shouldFocusInput(i)&&i.input.focus(),t.datepicker._curInst=i))}},_updateDatepicker:function(e){this.maxRows=4,a=e,e.dpDiv.empty().append(this._generateHTML(e)),this._attachHandlers(e),e.dpDiv.find("."+this._dayOverClass+" a").mouseover();var i,s=this._getNumberOfMonths(e),n=s[1],r=17;e.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width(""),n>1&&e.dpDiv.addClass("ui-datepicker-multi-"+n).css("width",r*n+"em"),e.dpDiv[(1!==s[0]||1!==s[1]?"add":"remove")+"Class"]("ui-datepicker-multi"),e.dpDiv[(this._get(e,"isRTL")?"add":"remove")+"Class"]("ui-datepicker-rtl"),e===t.datepicker._curInst&&t.datepicker._datepickerShowing&&t.datepicker._shouldFocusInput(e)&&e.input.focus(),e.yearshtml&&(i=e.yearshtml,setTimeout(function(){i===e.yearshtml&&e.yearshtml&&e.dpDiv.find("select.ui-datepicker-year:first").replaceWith(e.yearshtml),i=e.yearshtml=null},0))},_shouldFocusInput:function(t){return t.input&&t.input.is(":visible")&&!t.input.is(":disabled")&&!t.input.is(":focus")},_checkOffset:function(e,i,s){var n=e.dpDiv.outerWidth(),a=e.dpDiv.outerHeight(),r=e.input?e.input.outerWidth():0,o=e.input?e.input.outerHeight():0,h=document.documentElement.clientWidth+(s?0:t(document).scrollLeft()),l=document.documentElement.clientHeight+(s?0:t(document).scrollTop());return i.left-=this._get(e,"isRTL")?n-r:0,i.left-=s&&i.left===e.input.offset().left?t(document).scrollLeft():0,i.top-=s&&i.top===e.input.offset().top+o?t(document).scrollTop():0,i.left-=Math.min(i.left,i.left+n>h&&h>n?Math.abs(i.left+n-h):0),i.top-=Math.min(i.top,i.top+a>l&&l>a?Math.abs(a+o):0),i},_findPos:function(e){for(var i,s=this._getInst(e),n=this._get(s,"isRTL");e&&("hidden"===e.type||1!==e.nodeType||t.expr.filters.hidden(e));)e=e[n?"previousSibling":"nextSibling"];return i=t(e).offset(),[i.left,i.top]},_hideDatepicker:function(e){var i,s,n,a,o=this._curInst;!o||e&&o!==t.data(e,r)||this._datepickerShowing&&(i=this._get(o,"showAnim"),s=this._get(o,"duration"),n=function(){t.datepicker._tidyDialog(o)},t.effects&&(t.effects.effect[i]||t.effects[i])?o.dpDiv.hide(i,t.datepicker._get(o,"showOptions"),s,n):o.dpDiv["slideDown"===i?"slideUp":"fadeIn"===i?"fadeOut":"hide"](i?s:null,n),i||n(),this._datepickerShowing=!1,a=this._get(o,"onClose"),a&&a.apply(o.input?o.input[0]:null,[o.input?o.input.val():"",o]),this._lastInput=null,this._inDialog&&(this._dialogInput.css({position:"absolute",left:"0",top:"-100px"}),t.blockUI&&(t.unblockUI(),t("body").append(this.dpDiv))),this._inDialog=!1)},_tidyDialog:function(t){t.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar")},_checkExternalClick:function(e){if(t.datepicker._curInst){var i=t(e.target),s=t.datepicker._getInst(i[0]);(i[0].id!==t.datepicker._mainDivId&&0===i.parents("#"+t.datepicker._mainDivId).length&&!i.hasClass(t.datepicker.markerClassName)&&!i.closest("."+t.datepicker._triggerClass).length&&t.datepicker._datepickerShowing&&(!t.datepicker._inDialog||!t.blockUI)||i.hasClass(t.datepicker.markerClassName)&&t.datepicker._curInst!==s)&&t.datepicker._hideDatepicker()}},_adjustDate:function(e,i,s){var n=t(e),a=this._getInst(n[0]);this._isDisabledDatepicker(n[0])||(this._adjustInstDate(a,i+("M"===s?this._get(a,"showCurrentAtPos"):0),s),this._updateDatepicker(a))},_gotoToday:function(e){var i,s=t(e),n=this._getInst(s[0]);this._get(n,"gotoCurrent")&&n.currentDay?(n.selectedDay=n.currentDay,n.drawMonth=n.selectedMonth=n.currentMonth,n.drawYear=n.selectedYear=n.currentYear):(i=new Date,n.selectedDay=i.getDate(),n.drawMonth=n.selectedMonth=i.getMonth(),n.drawYear=n.selectedYear=i.getFullYear()),this._notifyChange(n),this._adjustDate(s)},_selectMonthYear:function(e,i,s){var n=t(e),a=this._getInst(n[0]);a["selected"+("M"===s?"Month":"Year")]=a["draw"+("M"===s?"Month":"Year")]=parseInt(i.options[i.selectedIndex].value,10),this._notifyChange(a),this._adjustDate(n)},_selectDay:function(e,i,s,n){var a,r=t(e);t(n).hasClass(this._unselectableClass)||this._isDisabledDatepicker(r[0])||(a=this._getInst(r[0]),a.selectedDay=a.currentDay=t("a",n).html(),a.selectedMonth=a.currentMonth=i,a.selectedYear=a.currentYear=s,this._selectDate(e,this._formatDate(a,a.currentDay,a.currentMonth,a.currentYear)))},_clearDate:function(e){var i=t(e);this._selectDate(i,"")},_selectDate:function(e,i){var s,n=t(e),a=this._getInst(n[0]);i=null!=i?i:this._formatDate(a),a.input&&a.input.val(i),this._updateAlternate(a),s=this._get(a,"onSelect"),s?s.apply(a.input?a.input[0]:null,[i,a]):a.input&&a.input.trigger("change"),a.inline?this._updateDatepicker(a):(this._hideDatepicker(),this._lastInput=a.input[0],"object"!=typeof a.input[0]&&a.input.focus(),this._lastInput=null)},_updateAlternate:function(e){var i,s,n,a=this._get(e,"altField");a&&(i=this._get(e,"altFormat")||this._get(e,"dateFormat"),s=this._getDate(e),n=this.formatDate(i,s,this._getFormatConfig(e)),t(a).each(function(){t(this).val(n)}))},noWeekends:function(t){var e=t.getDay();return[e>0&&6>e,""]},iso8601Week:function(t){var e,i=new Date(t.getTime());return i.setDate(i.getDate()+4-(i.getDay()||7)),e=i.getTime(),i.setMonth(0),i.setDate(1),Math.floor(Math.round((e-i)/864e5)/7)+1},parseDate:function(i,s,n){if(null==i||null==s)throw"Invalid arguments";if(s="object"==typeof s?""+s:s+"",""===s)return null;var a,r,o,h,l=0,c=(n?n.shortYearCutoff:null)||this._defaults.shortYearCutoff,u="string"!=typeof c?c:(new Date).getFullYear()%100+parseInt(c,10),d=(n?n.dayNamesShort:null)||this._defaults.dayNamesShort,p=(n?n.dayNames:null)||this._defaults.dayNames,f=(n?n.monthNamesShort:null)||this._defaults.monthNamesShort,g=(n?n.monthNames:null)||this._defaults.monthNames,m=-1,v=-1,_=-1,b=-1,y=!1,k=function(t){var e=i.length>a+1&&i.charAt(a+1)===t;return e&&a++,e},D=function(t){var e=k(t),i="@"===t?14:"!"===t?20:"y"===t&&e?4:"o"===t?3:2,n=RegExp("^\\d{1,"+i+"}"),a=s.substring(l).match(n);if(!a)throw"Missing number at position "+l;return l+=a[0].length,parseInt(a[0],10)},w=function(i,n,a){var r=-1,o=t.map(k(i)?a:n,function(t,e){return[[e,t]]}).sort(function(t,e){return-(t[1].length-e[1].length)});if(t.each(o,function(t,i){var n=i[1];return s.substr(l,n.length).toLowerCase()===n.toLowerCase()?(r=i[0],l+=n.length,!1):e}),-1!==r)return r+1;throw"Unknown name at position "+l},C=function(){if(s.charAt(l)!==i.charAt(a))throw"Unexpected literal at position "+l;l++};for(a=0;i.length>a;a++)if(y)"'"!==i.charAt(a)||k("'")?C():y=!1;else switch(i.charAt(a)){case"d":_=D("d");break;case"D":w("D",d,p);break;case"o":b=D("o");break;case"m":v=D("m");break;case"M":v=w("M",f,g);break;case"y":m=D("y");break;case"@":h=new Date(D("@")),m=h.getFullYear(),v=h.getMonth()+1,_=h.getDate();break;case"!":h=new Date((D("!")-this._ticksTo1970)/1e4),m=h.getFullYear(),v=h.getMonth()+1,_=h.getDate();break;case"'":k("'")?C():y=!0;break;default:C()}if(s.length>l&&(o=s.substr(l),!/^\s+/.test(o)))throw"Extra/unparsed characters found in date: "+o;if(-1===m?m=(new Date).getFullYear():100>m&&(m+=(new Date).getFullYear()-(new Date).getFullYear()%100+(u>=m?0:-100)),b>-1)for(v=1,_=b;;){if(r=this._getDaysInMonth(m,v-1),r>=_)break;v++,_-=r}if(h=this._daylightSavingAdjust(new Date(m,v-1,_)),h.getFullYear()!==m||h.getMonth()+1!==v||h.getDate()!==_)throw"Invalid date";return h},ATOM:"yy-mm-dd",COOKIE:"D, dd M yy",ISO_8601:"yy-mm-dd",RFC_822:"D, d M y",RFC_850:"DD, dd-M-y",RFC_1036:"D, d M y",RFC_1123:"D, d M yy",RFC_2822:"D, d M yy",RSS:"D, d M y",TICKS:"!",TIMESTAMP:"@",W3C:"yy-mm-dd",_ticksTo1970:1e7*60*60*24*(718685+Math.floor(492.5)-Math.floor(19.7)+Math.floor(4.925)),formatDate:function(t,e,i){if(!e)return"";var s,n=(i?i.dayNamesShort:null)||this._defaults.dayNamesShort,a=(i?i.dayNames:null)||this._defaults.dayNames,r=(i?i.monthNamesShort:null)||this._defaults.monthNamesShort,o=(i?i.monthNames:null)||this._defaults.monthNames,h=function(e){var i=t.length>s+1&&t.charAt(s+1)===e;return i&&s++,i},l=function(t,e,i){var s=""+e;if(h(t))for(;i>s.length;)s="0"+s;return s},c=function(t,e,i,s){return h(t)?s[e]:i[e]},u="",d=!1;if(e)for(s=0;t.length>s;s++)if(d)"'"!==t.charAt(s)||h("'")?u+=t.charAt(s):d=!1;else switch(t.charAt(s)){case"d":u+=l("d",e.getDate(),2);break;case"D":u+=c("D",e.getDay(),n,a);break;case"o":u+=l("o",Math.round((new Date(e.getFullYear(),e.getMonth(),e.getDate()).getTime()-new Date(e.getFullYear(),0,0).getTime())/864e5),3);break;case"m":u+=l("m",e.getMonth()+1,2);break;case"M":u+=c("M",e.getMonth(),r,o);break;case"y":u+=h("y")?e.getFullYear():(10>e.getYear()%100?"0":"")+e.getYear()%100;break;case"@":u+=e.getTime();break;case"!":u+=1e4*e.getTime()+this._ticksTo1970;break;case"'":h("'")?u+="'":d=!0;break;default:u+=t.charAt(s)}return u},_possibleChars:function(t){var e,i="",s=!1,n=function(i){var s=t.length>e+1&&t.charAt(e+1)===i;return s&&e++,s};for(e=0;t.length>e;e++)if(s)"'"!==t.charAt(e)||n("'")?i+=t.charAt(e):s=!1;else switch(t.charAt(e)){case"d":case"m":case"y":case"@":i+="0123456789";break;case"D":case"M":return null;case"'":n("'")?i+="'":s=!0;break;default:i+=t.charAt(e)}return i},_get:function(t,i){return t.settings[i]!==e?t.settings[i]:this._defaults[i]},_setDateFromField:function(t,e){if(t.input.val()!==t.lastVal){var i=this._get(t,"dateFormat"),s=t.lastVal=t.input?t.input.val():null,n=this._getDefaultDate(t),a=n,r=this._getFormatConfig(t);try{a=this.parseDate(i,s,r)||n}catch(o){s=e?"":s}t.selectedDay=a.getDate(),t.drawMonth=t.selectedMonth=a.getMonth(),t.drawYear=t.selectedYear=a.getFullYear(),t.currentDay=s?a.getDate():0,t.currentMonth=s?a.getMonth():0,t.currentYear=s?a.getFullYear():0,this._adjustInstDate(t)}},_getDefaultDate:function(t){return this._restrictMinMax(t,this._determineDate(t,this._get(t,"defaultDate"),new Date))},_determineDate:function(e,i,s){var n=function(t){var e=new Date;return e.setDate(e.getDate()+t),e},a=function(i){try{return t.datepicker.parseDate(t.datepicker._get(e,"dateFormat"),i,t.datepicker._getFormatConfig(e))}catch(s){}for(var n=(i.toLowerCase().match(/^c/)?t.datepicker._getDate(e):null)||new Date,a=n.getFullYear(),r=n.getMonth(),o=n.getDate(),h=/([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g,l=h.exec(i);l;){switch(l[2]||"d"){case"d":case"D":o+=parseInt(l[1],10);break;case"w":case"W":o+=7*parseInt(l[1],10);break;case"m":case"M":r+=parseInt(l[1],10),o=Math.min(o,t.datepicker._getDaysInMonth(a,r));break;case"y":case"Y":a+=parseInt(l[1],10),o=Math.min(o,t.datepicker._getDaysInMonth(a,r))}l=h.exec(i)}return new Date(a,r,o)},r=null==i||""===i?s:"string"==typeof i?a(i):"number"==typeof i?isNaN(i)?s:n(i):new Date(i.getTime());return r=r&&"Invalid Date"==""+r?s:r,r&&(r.setHours(0),r.setMinutes(0),r.setSeconds(0),r.setMilliseconds(0)),this._daylightSavingAdjust(r)},_daylightSavingAdjust:function(t){return t?(t.setHours(t.getHours()>12?t.getHours()+2:0),t):null},_setDate:function(t,e,i){var s=!e,n=t.selectedMonth,a=t.selectedYear,r=this._restrictMinMax(t,this._determineDate(t,e,new Date));t.selectedDay=t.currentDay=r.getDate(),t.drawMonth=t.selectedMonth=t.currentMonth=r.getMonth(),t.drawYear=t.selectedYear=t.currentYear=r.getFullYear(),n===t.selectedMonth&&a===t.selectedYear||i||this._notifyChange(t),this._adjustInstDate(t),t.input&&t.input.val(s?"":this._formatDate(t))},_getDate:function(t){var e=!t.currentYear||t.input&&""===t.input.val()?null:this._daylightSavingAdjust(new Date(t.currentYear,t.currentMonth,t.currentDay));return e},_attachHandlers:function(e){var i=this._get(e,"stepMonths"),s="#"+e.id.replace(/\\\\/g,"\\");e.dpDiv.find("[data-handler]").map(function(){var e={prev:function(){t.datepicker._adjustDate(s,-i,"M")},next:function(){t.datepicker._adjustDate(s,+i,"M")},hide:function(){t.datepicker._hideDatepicker()},today:function(){t.datepicker._gotoToday(s)},selectDay:function(){return t.datepicker._selectDay(s,+this.getAttribute("data-month"),+this.getAttribute("data-year"),this),!1},selectMonth:function(){return t.datepicker._selectMonthYear(s,this,"M"),!1},selectYear:function(){return t.datepicker._selectMonthYear(s,this,"Y"),!1}};t(this).bind(this.getAttribute("data-event"),e[this.getAttribute("data-handler")])})},_generateHTML:function(t){var e,i,s,n,a,r,o,h,l,c,u,d,p,f,g,m,v,_,b,y,k,D,w,C,x,P,I,M,T,S,N,A,E,H,O,F,Y,W,L,j=new Date,R=this._daylightSavingAdjust(new Date(j.getFullYear(),j.getMonth(),j.getDate())),z=this._get(t,"isRTL"),K=this._get(t,"showButtonPanel"),U=this._get(t,"hideIfNoPrevNext"),B=this._get(t,"navigationAsDateFormat"),X=this._getNumberOfMonths(t),Q=this._get(t,"showCurrentAtPos"),q=this._get(t,"stepMonths"),V=1!==X[0]||1!==X[1],G=this._daylightSavingAdjust(t.currentDay?new Date(t.currentYear,t.currentMonth,t.currentDay):new Date(9999,9,9)),$=this._getMinMaxDate(t,"min"),J=this._getMinMaxDate(t,"max"),Z=t.drawMonth-Q,te=t.drawYear;if(0>Z&&(Z+=12,te--),J)for(e=this._daylightSavingAdjust(new Date(J.getFullYear(),J.getMonth()-X[0]*X[1]+1,J.getDate())),e=$&&$>e?$:e;this._daylightSavingAdjust(new Date(te,Z,1))>e;)Z--,0>Z&&(Z=11,te--);for(t.drawMonth=Z,t.drawYear=te,i=this._get(t,"prevText"),i=B?this.formatDate(i,this._daylightSavingAdjust(new Date(te,Z-q,1)),this._getFormatConfig(t)):i,s=this._canAdjustMonth(t,-1,te,Z)?"<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click' title='"+i+"'><span class='ui-icon ui-icon-circle-triangle-"+(z?"e":"w")+"'>"+i+"</span></a>":U?"":"<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='"+i+"'><span class='ui-icon ui-icon-circle-triangle-"+(z?"e":"w")+"'>"+i+"</span></a>",n=this._get(t,"nextText"),n=B?this.formatDate(n,this._daylightSavingAdjust(new Date(te,Z+q,1)),this._getFormatConfig(t)):n,a=this._canAdjustMonth(t,1,te,Z)?"<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click' title='"+n+"'><span class='ui-icon ui-icon-circle-triangle-"+(z?"w":"e")+"'>"+n+"</span></a>":U?"":"<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='"+n+"'><span class='ui-icon ui-icon-circle-triangle-"+(z?"w":"e")+"'>"+n+"</span></a>",r=this._get(t,"currentText"),o=this._get(t,"gotoCurrent")&&t.currentDay?G:R,r=B?this.formatDate(r,o,this._getFormatConfig(t)):r,h=t.inline?"":"<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>"+this._get(t,"closeText")+"</button>",l=K?"<div class='ui-datepicker-buttonpane ui-widget-content'>"+(z?h:"")+(this._isInRange(t,o)?"<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'>"+r+"</button>":"")+(z?"":h)+"</div>":"",c=parseInt(this._get(t,"firstDay"),10),c=isNaN(c)?0:c,u=this._get(t,"showWeek"),d=this._get(t,"dayNames"),p=this._get(t,"dayNamesMin"),f=this._get(t,"monthNames"),g=this._get(t,"monthNamesShort"),m=this._get(t,"beforeShowDay"),v=this._get(t,"showOtherMonths"),_=this._get(t,"selectOtherMonths"),b=this._getDefaultDate(t),y="",D=0;X[0]>D;D++){for(w="",this.maxRows=4,C=0;X[1]>C;C++){if(x=this._daylightSavingAdjust(new Date(te,Z,t.selectedDay)),P=" ui-corner-all",I="",V){if(I+="<div class='ui-datepicker-group",X[1]>1)switch(C){case 0:I+=" ui-datepicker-group-first",P=" ui-corner-"+(z?"right":"left");break;case X[1]-1:I+=" ui-datepicker-group-last",P=" ui-corner-"+(z?"left":"right");break;default:I+=" ui-datepicker-group-middle",P=""}I+="'>"}for(I+="<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix"+P+"'>"+(/all|left/.test(P)&&0===D?z?a:s:"")+(/all|right/.test(P)&&0===D?z?s:a:"")+this._generateMonthYearHeader(t,Z,te,$,J,D>0||C>0,f,g)+"</div><table class='ui-datepicker-calendar'><thead>"+"<tr>",M=u?"<th class='ui-datepicker-week-col'>"+this._get(t,"weekHeader")+"</th>":"",k=0;7>k;k++)T=(k+c)%7,M+="<th"+((k+c+6)%7>=5?" class='ui-datepicker-week-end'":"")+">"+"<span title='"+d[T]+"'>"+p[T]+"</span></th>";for(I+=M+"</tr></thead><tbody>",S=this._getDaysInMonth(te,Z),te===t.selectedYear&&Z===t.selectedMonth&&(t.selectedDay=Math.min(t.selectedDay,S)),N=(this._getFirstDayOfMonth(te,Z)-c+7)%7,A=Math.ceil((N+S)/7),E=V?this.maxRows>A?this.maxRows:A:A,this.maxRows=E,H=this._daylightSavingAdjust(new Date(te,Z,1-N)),O=0;E>O;O++){for(I+="<tr>",F=u?"<td class='ui-datepicker-week-col'>"+this._get(t,"calculateWeek")(H)+"</td>":"",k=0;7>k;k++)Y=m?m.apply(t.input?t.input[0]:null,[H]):[!0,""],W=H.getMonth()!==Z,L=W&&!_||!Y[0]||$&&$>H||J&&H>J,F+="<td class='"+((k+c+6)%7>=5?" ui-datepicker-week-end":"")+(W?" ui-datepicker-other-month":"")+(H.getTime()===x.getTime()&&Z===t.selectedMonth&&t._keyEvent||b.getTime()===H.getTime()&&b.getTime()===x.getTime()?" "+this._dayOverClass:"")+(L?" "+this._unselectableClass+" ui-state-disabled":"")+(W&&!v?"":" "+Y[1]+(H.getTime()===G.getTime()?" "+this._currentClass:"")+(H.getTime()===R.getTime()?" ui-datepicker-today":""))+"'"+(W&&!v||!Y[2]?"":" title='"+Y[2].replace(/'/g,"&#39;")+"'")+(L?"":" data-handler='selectDay' data-event='click' data-month='"+H.getMonth()+"' data-year='"+H.getFullYear()+"'")+">"+(W&&!v?"&#xa0;":L?"<span class='ui-state-default'>"+H.getDate()+"</span>":"<a class='ui-state-default"+(H.getTime()===R.getTime()?" ui-state-highlight":"")+(H.getTime()===G.getTime()?" ui-state-active":"")+(W?" ui-priority-secondary":"")+"' href='#'>"+H.getDate()+"</a>")+"</td>",H.setDate(H.getDate()+1),H=this._daylightSavingAdjust(H);I+=F+"</tr>"}Z++,Z>11&&(Z=0,te++),I+="</tbody></table>"+(V?"</div>"+(X[0]>0&&C===X[1]-1?"<div class='ui-datepicker-row-break'></div>":""):""),w+=I}y+=w}return y+=l,t._keyEvent=!1,y},_generateMonthYearHeader:function(t,e,i,s,n,a,r,o){var h,l,c,u,d,p,f,g,m=this._get(t,"changeMonth"),v=this._get(t,"changeYear"),_=this._get(t,"showMonthAfterYear"),b="<div class='ui-datepicker-title'>",y="";if(a||!m)y+="<span class='ui-datepicker-month'>"+r[e]+"</span>";else{for(h=s&&s.getFullYear()===i,l=n&&n.getFullYear()===i,y+="<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>",c=0;12>c;c++)(!h||c>=s.getMonth())&&(!l||n.getMonth()>=c)&&(y+="<option value='"+c+"'"+(c===e?" selected='selected'":"")+">"+o[c]+"</option>");y+="</select>"}if(_||(b+=y+(!a&&m&&v?"":"&#xa0;")),!t.yearshtml)if(t.yearshtml="",a||!v)b+="<span class='ui-datepicker-year'>"+i+"</span>";else{for(u=this._get(t,"yearRange").split(":"),d=(new Date).getFullYear(),p=function(t){var e=t.match(/c[+\-].*/)?i+parseInt(t.substring(1),10):t.match(/[+\-].*/)?d+parseInt(t,10):parseInt(t,10);
  return isNaN(e)?d:e},f=p(u[0]),g=Math.max(f,p(u[1]||"")),f=s?Math.max(f,s.getFullYear()):f,g=n?Math.min(g,n.getFullYear()):g,t.yearshtml+="<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>";g>=f;f++)t.yearshtml+="<option value='"+f+"'"+(f===i?" selected='selected'":"")+">"+f+"</option>";t.yearshtml+="</select>",b+=t.yearshtml,t.yearshtml=null}return b+=this._get(t,"yearSuffix"),_&&(b+=(!a&&m&&v?"":"&#xa0;")+y),b+="</div>"},_adjustInstDate:function(t,e,i){var s=t.drawYear+("Y"===i?e:0),n=t.drawMonth+("M"===i?e:0),a=Math.min(t.selectedDay,this._getDaysInMonth(s,n))+("D"===i?e:0),r=this._restrictMinMax(t,this._daylightSavingAdjust(new Date(s,n,a)));t.selectedDay=r.getDate(),t.drawMonth=t.selectedMonth=r.getMonth(),t.drawYear=t.selectedYear=r.getFullYear(),("M"===i||"Y"===i)&&this._notifyChange(t)},_restrictMinMax:function(t,e){var i=this._getMinMaxDate(t,"min"),s=this._getMinMaxDate(t,"max"),n=i&&i>e?i:e;return s&&n>s?s:n},_notifyChange:function(t){var e=this._get(t,"onChangeMonthYear");e&&e.apply(t.input?t.input[0]:null,[t.selectedYear,t.selectedMonth+1,t])},_getNumberOfMonths:function(t){var e=this._get(t,"numberOfMonths");return null==e?[1,1]:"number"==typeof e?[1,e]:e},_getMinMaxDate:function(t,e){return this._determineDate(t,this._get(t,e+"Date"),null)},_getDaysInMonth:function(t,e){return 32-this._daylightSavingAdjust(new Date(t,e,32)).getDate()},_getFirstDayOfMonth:function(t,e){return new Date(t,e,1).getDay()},_canAdjustMonth:function(t,e,i,s){var n=this._getNumberOfMonths(t),a=this._daylightSavingAdjust(new Date(i,s+(0>e?e:n[0]*n[1]),1));return 0>e&&a.setDate(this._getDaysInMonth(a.getFullYear(),a.getMonth())),this._isInRange(t,a)},_isInRange:function(t,e){var i,s,n=this._getMinMaxDate(t,"min"),a=this._getMinMaxDate(t,"max"),r=null,o=null,h=this._get(t,"yearRange");return h&&(i=h.split(":"),s=(new Date).getFullYear(),r=parseInt(i[0],10),o=parseInt(i[1],10),i[0].match(/[+\-].*/)&&(r+=s),i[1].match(/[+\-].*/)&&(o+=s)),(!n||e.getTime()>=n.getTime())&&(!a||e.getTime()<=a.getTime())&&(!r||e.getFullYear()>=r)&&(!o||o>=e.getFullYear())},_getFormatConfig:function(t){var e=this._get(t,"shortYearCutoff");return e="string"!=typeof e?e:(new Date).getFullYear()%100+parseInt(e,10),{shortYearCutoff:e,dayNamesShort:this._get(t,"dayNamesShort"),dayNames:this._get(t,"dayNames"),monthNamesShort:this._get(t,"monthNamesShort"),monthNames:this._get(t,"monthNames")}},_formatDate:function(t,e,i,s){e||(t.currentDay=t.selectedDay,t.currentMonth=t.selectedMonth,t.currentYear=t.selectedYear);var n=e?"object"==typeof e?e:this._daylightSavingAdjust(new Date(s,i,e)):this._daylightSavingAdjust(new Date(t.currentYear,t.currentMonth,t.currentDay));return this.formatDate(this._get(t,"dateFormat"),n,this._getFormatConfig(t))}}),t.fn.datepicker=function(e){if(!this.length)return this;t.datepicker.initialized||(t(document).mousedown(t.datepicker._checkExternalClick),t.datepicker.initialized=!0),0===t("#"+t.datepicker._mainDivId).length&&t("body").append(t.datepicker.dpDiv);var i=Array.prototype.slice.call(arguments,1);return"string"!=typeof e||"isDisabled"!==e&&"getDate"!==e&&"widget"!==e?"option"===e&&2===arguments.length&&"string"==typeof arguments[1]?t.datepicker["_"+e+"Datepicker"].apply(t.datepicker,[this[0]].concat(i)):this.each(function(){"string"==typeof e?t.datepicker["_"+e+"Datepicker"].apply(t.datepicker,[this].concat(i)):t.datepicker._attachDatepicker(this,e)}):t.datepicker["_"+e+"Datepicker"].apply(t.datepicker,[this[0]].concat(i))},t.datepicker=new i,t.datepicker.initialized=!1,t.datepicker.uuid=(new Date).getTime(),t.datepicker.version="1.10.3"})(jQuery);

/*!
 * Masonry PACKAGED v3.1.2
 * Cascading grid layout library
 * http://masonry.desandro.com
 * MIT License
 * by David DeSandro
 */

(function(t){"use strict";function e(){}function i(t){function i(e){e.prototype.option||(e.prototype.option=function(e){t.isPlainObject(e)&&(this.options=t.extend(!0,this.options,e))})}function o(e,i){t.fn[e]=function(o){if("string"==typeof o){for(var s=n.call(arguments,1),a=0,h=this.length;h>a;a++){var p=this[a],u=t.data(p,e);if(u)if(t.isFunction(u[o])&&"_"!==o.charAt(0)){var f=u[o].apply(u,s);if(void 0!==f)return f}else r("no such method '"+o+"' for "+e+" instance");else r("cannot call methods on "+e+" prior to initialization; "+"attempted to call '"+o+"'")}return this}return this.each(function(){var n=t.data(this,e);n?(n.option(o),n._init()):(n=new i(this,o),t.data(this,e,n))})}}if(t){var r="undefined"==typeof console?e:function(t){console.error(t)};t.bridget=function(t,e){i(e),o(t,e)}}}var n=Array.prototype.slice;"function"==typeof define&&define.amd?define(["jquery"],i):i(t.jQuery)})(window),function(t){var e=document.documentElement,i=function(){};e.addEventListener?i=function(t,e,i){t.addEventListener(e,i,!1)}:e.attachEvent&&(i=function(e,i,n){e[i+n]=n.handleEvent?function(){var e=t.event;e.target=e.target||e.srcElement,n.handleEvent.call(n,e)}:function(){var i=t.event;i.target=i.target||i.srcElement,n.call(e,i)},e.attachEvent("on"+i,e[i+n])});var n=function(){};e.removeEventListener?n=function(t,e,i){t.removeEventListener(e,i,!1)}:e.detachEvent&&(n=function(t,e,i){t.detachEvent("on"+e,t[e+i]);try{delete t[e+i]}catch(n){t[e+i]=void 0}});var o={bind:i,unbind:n};"function"==typeof define&&define.amd?define("eventie/eventie",o):t.eventie=o}(this),function(t){function e(t){"function"==typeof t&&(e.isReady?t():r.push(t))}function i(t){var i="readystatechange"===t.type&&"complete"!==o.readyState;if(!e.isReady&&!i){e.isReady=!0;for(var n=0,s=r.length;s>n;n++){var a=r[n];a()}}}function n(n){return n.bind(o,"DOMContentLoaded",i),n.bind(o,"readystatechange",i),n.bind(t,"load",i),e}var o=t.document,r=[];e.isReady=!1,"function"==typeof define&&define.amd?(e.isReady="function"==typeof requirejs,define("doc-ready/doc-ready",["eventie/eventie"],n)):t.docReady=n(t.eventie)}(this),function(){function t(){}function e(t,e){for(var i=t.length;i--;)if(t[i].listener===e)return i;return-1}function i(t){return function(){return this[t].apply(this,arguments)}}var n=t.prototype,o=this,r=o.EventEmitter;n.getListeners=function(t){var e,i,n=this._getEvents();if("object"==typeof t){e={};for(i in n)n.hasOwnProperty(i)&&t.test(i)&&(e[i]=n[i])}else e=n[t]||(n[t]=[]);return e},n.flattenListeners=function(t){var e,i=[];for(e=0;t.length>e;e+=1)i.push(t[e].listener);return i},n.getListenersAsObject=function(t){var e,i=this.getListeners(t);return i instanceof Array&&(e={},e[t]=i),e||i},n.addListener=function(t,i){var n,o=this.getListenersAsObject(t),r="object"==typeof i;for(n in o)o.hasOwnProperty(n)&&-1===e(o[n],i)&&o[n].push(r?i:{listener:i,once:!1});return this},n.on=i("addListener"),n.addOnceListener=function(t,e){return this.addListener(t,{listener:e,once:!0})},n.once=i("addOnceListener"),n.defineEvent=function(t){return this.getListeners(t),this},n.defineEvents=function(t){for(var e=0;t.length>e;e+=1)this.defineEvent(t[e]);return this},n.removeListener=function(t,i){var n,o,r=this.getListenersAsObject(t);for(o in r)r.hasOwnProperty(o)&&(n=e(r[o],i),-1!==n&&r[o].splice(n,1));return this},n.off=i("removeListener"),n.addListeners=function(t,e){return this.manipulateListeners(!1,t,e)},n.removeListeners=function(t,e){return this.manipulateListeners(!0,t,e)},n.manipulateListeners=function(t,e,i){var n,o,r=t?this.removeListener:this.addListener,s=t?this.removeListeners:this.addListeners;if("object"!=typeof e||e instanceof RegExp)for(n=i.length;n--;)r.call(this,e,i[n]);else for(n in e)e.hasOwnProperty(n)&&(o=e[n])&&("function"==typeof o?r.call(this,n,o):s.call(this,n,o));return this},n.removeEvent=function(t){var e,i=typeof t,n=this._getEvents();if("string"===i)delete n[t];else if("object"===i)for(e in n)n.hasOwnProperty(e)&&t.test(e)&&delete n[e];else delete this._events;return this},n.removeAllListeners=i("removeEvent"),n.emitEvent=function(t,e){var i,n,o,r,s=this.getListenersAsObject(t);for(o in s)if(s.hasOwnProperty(o))for(n=s[o].length;n--;)i=s[o][n],i.once===!0&&this.removeListener(t,i.listener),r=i.listener.apply(this,e||[]),r===this._getOnceReturnValue()&&this.removeListener(t,i.listener);return this},n.trigger=i("emitEvent"),n.emit=function(t){var e=Array.prototype.slice.call(arguments,1);return this.emitEvent(t,e)},n.setOnceReturnValue=function(t){return this._onceReturnValue=t,this},n._getOnceReturnValue=function(){return this.hasOwnProperty("_onceReturnValue")?this._onceReturnValue:!0},n._getEvents=function(){return this._events||(this._events={})},t.noConflict=function(){return o.EventEmitter=r,t},"function"==typeof define&&define.amd?define("eventEmitter/EventEmitter",[],function(){return t}):"object"==typeof module&&module.exports?module.exports=t:this.EventEmitter=t}.call(this),function(t){function e(t){if(t){if("string"==typeof n[t])return t;t=t.charAt(0).toUpperCase()+t.slice(1);for(var e,o=0,r=i.length;r>o;o++)if(e=i[o]+t,"string"==typeof n[e])return e}}var i="Webkit Moz ms Ms O".split(" "),n=document.documentElement.style;"function"==typeof define&&define.amd?define("get-style-property/get-style-property",[],function(){return e}):"object"==typeof exports?module.exports=e:t.getStyleProperty=e}(window),function(t){function e(t){var e=parseFloat(t),i=-1===t.indexOf("%")&&!isNaN(e);return i&&e}function i(){for(var t={width:0,height:0,innerWidth:0,innerHeight:0,outerWidth:0,outerHeight:0},e=0,i=a.length;i>e;e++){var n=a[e];t[n]=0}return t}function n(t){function n(t){if("string"==typeof t&&(t=document.querySelector(t)),t&&"object"==typeof t&&t.nodeType){var n=s(t);if("none"===n.display)return i();var r={};r.width=t.offsetWidth,r.height=t.offsetHeight;for(var u=r.isBorderBox=!(!p||!n[p]||"border-box"!==n[p]),f=0,l=a.length;l>f;f++){var d=a[f],c=n[d];c=o(t,c);var m=parseFloat(c);r[d]=isNaN(m)?0:m}var y=r.paddingLeft+r.paddingRight,g=r.paddingTop+r.paddingBottom,v=r.marginLeft+r.marginRight,_=r.marginTop+r.marginBottom,E=r.borderLeftWidth+r.borderRightWidth,b=r.borderTopWidth+r.borderBottomWidth,L=u&&h,z=e(n.width);z!==!1&&(r.width=z+(L?0:y+E));var S=e(n.height);return S!==!1&&(r.height=S+(L?0:g+b)),r.innerWidth=r.width-(y+E),r.innerHeight=r.height-(g+b),r.outerWidth=r.width+v,r.outerHeight=r.height+_,r}}function o(t,e){if(r||-1===e.indexOf("%"))return e;var i=t.style,n=i.left,o=t.runtimeStyle,s=o&&o.left;return s&&(o.left=t.currentStyle.left),i.left=e,e=i.pixelLeft,i.left=n,s&&(o.left=s),e}var h,p=t("boxSizing");return function(){if(p){var t=document.createElement("div");t.style.width="200px",t.style.padding="1px 2px 3px 4px",t.style.borderStyle="solid",t.style.borderWidth="1px 2px 3px 4px",t.style[p]="border-box";var i=document.body||document.documentElement;i.appendChild(t);var n=s(t);h=200===e(n.width),i.removeChild(t)}}(),n}var o=document.defaultView,r=o&&o.getComputedStyle,s=r?function(t){return o.getComputedStyle(t,null)}:function(t){return t.currentStyle},a=["paddingLeft","paddingRight","paddingTop","paddingBottom","marginLeft","marginRight","marginTop","marginBottom","borderLeftWidth","borderRightWidth","borderTopWidth","borderBottomWidth"];"function"==typeof define&&define.amd?define("get-size/get-size",["get-style-property/get-style-property"],n):"object"==typeof exports?module.exports=n(require("get-style-property")):t.getSize=n(t.getStyleProperty)}(window),function(t,e){function i(t,e){return t[a](e)}function n(t){if(!t.parentNode){var e=document.createDocumentFragment();e.appendChild(t)}}function o(t,e){n(t);for(var i=t.parentNode.querySelectorAll(e),o=0,r=i.length;r>o;o++)if(i[o]===t)return!0;return!1}function r(t,e){return n(t),i(t,e)}var s,a=function(){if(e.matchesSelector)return"matchesSelector";for(var t=["webkit","moz","ms","o"],i=0,n=t.length;n>i;i++){var o=t[i],r=o+"MatchesSelector";if(e[r])return r}}();if(a){var h=document.createElement("div"),p=i(h,"div");s=p?i:r}else s=o;"function"==typeof define&&define.amd?define("matches-selector/matches-selector",[],function(){return s}):window.matchesSelector=s}(this,Element.prototype),function(t){function e(t,e){for(var i in e)t[i]=e[i];return t}function i(t){for(var e in t)return!1;return e=null,!0}function n(t){return t.replace(/([A-Z])/g,function(t){return"-"+t.toLowerCase()})}function o(t,o,r){function a(t,e){t&&(this.element=t,this.layout=e,this.position={x:0,y:0},this._create())}var h=r("transition"),p=r("transform"),u=h&&p,f=!!r("perspective"),l={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"otransitionend",transition:"transitionend"}[h],d=["transform","transition","transitionDuration","transitionProperty"],c=function(){for(var t={},e=0,i=d.length;i>e;e++){var n=d[e],o=r(n);o&&o!==n&&(t[n]=o)}return t}();e(a.prototype,t.prototype),a.prototype._create=function(){this._transition={ingProperties:{},clean:{},onEnd:{}},this.css({position:"absolute"})},a.prototype.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},a.prototype.getSize=function(){this.size=o(this.element)},a.prototype.css=function(t){var e=this.element.style;for(var i in t){var n=c[i]||i;e[n]=t[i]}},a.prototype.getPosition=function(){var t=s(this.element),e=this.layout.options,i=e.isOriginLeft,n=e.isOriginTop,o=parseInt(t[i?"left":"right"],10),r=parseInt(t[n?"top":"bottom"],10);o=isNaN(o)?0:o,r=isNaN(r)?0:r;var a=this.layout.size;o-=i?a.paddingLeft:a.paddingRight,r-=n?a.paddingTop:a.paddingBottom,this.position.x=o,this.position.y=r},a.prototype.layoutPosition=function(){var t=this.layout.size,e=this.layout.options,i={};e.isOriginLeft?(i.left=this.position.x+t.paddingLeft+"px",i.right=""):(i.right=this.position.x+t.paddingRight+"px",i.left=""),e.isOriginTop?(i.top=this.position.y+t.paddingTop+"px",i.bottom=""):(i.bottom=this.position.y+t.paddingBottom+"px",i.top=""),this.css(i),this.emitEvent("layout",[this])};var m=f?function(t,e){return"translate3d("+t+"px, "+e+"px, 0)"}:function(t,e){return"translate("+t+"px, "+e+"px)"};a.prototype._transitionTo=function(t,e){this.getPosition();var i=this.position.x,n=this.position.y,o=parseInt(t,10),r=parseInt(e,10),s=o===this.position.x&&r===this.position.y;if(this.setPosition(t,e),s&&!this.isTransitioning)return this.layoutPosition(),void 0;var a=t-i,h=e-n,p={},u=this.layout.options;a=u.isOriginLeft?a:-a,h=u.isOriginTop?h:-h,p.transform=m(a,h),this.transition({to:p,onTransitionEnd:{transform:this.layoutPosition},isCleaning:!0})},a.prototype.goTo=function(t,e){this.setPosition(t,e),this.layoutPosition()},a.prototype.moveTo=u?a.prototype._transitionTo:a.prototype.goTo,a.prototype.setPosition=function(t,e){this.position.x=parseInt(t,10),this.position.y=parseInt(e,10)},a.prototype._nonTransition=function(t){this.css(t.to),t.isCleaning&&this._removeStyles(t.to);for(var e in t.onTransitionEnd)t.onTransitionEnd[e].call(this)},a.prototype._transition=function(t){if(!parseFloat(this.layout.options.transitionDuration))return this._nonTransition(t),void 0;var e=this._transition;for(var i in t.onTransitionEnd)e.onEnd[i]=t.onTransitionEnd[i];for(i in t.to)e.ingProperties[i]=!0,t.isCleaning&&(e.clean[i]=!0);if(t.from){this.css(t.from);var n=this.element.offsetHeight;n=null}this.enableTransition(t.to),this.css(t.to),this.isTransitioning=!0};var y=p&&n(p)+",opacity";a.prototype.enableTransition=function(){this.isTransitioning||(this.css({transitionProperty:y,transitionDuration:this.layout.options.transitionDuration}),this.element.addEventListener(l,this,!1))},a.prototype.transition=a.prototype[h?"_transition":"_nonTransition"],a.prototype.onwebkitTransitionEnd=function(t){this.ontransitionend(t)},a.prototype.onotransitionend=function(t){this.ontransitionend(t)};var g={"-webkit-transform":"transform","-moz-transform":"transform","-o-transform":"transform"};a.prototype.ontransitionend=function(t){if(t.target===this.element){var e=this._transition,n=g[t.propertyName]||t.propertyName;if(delete e.ingProperties[n],i(e.ingProperties)&&this.disableTransition(),n in e.clean&&(this.element.style[t.propertyName]="",delete e.clean[n]),n in e.onEnd){var o=e.onEnd[n];o.call(this),delete e.onEnd[n]}this.emitEvent("transitionEnd",[this])}},a.prototype.disableTransition=function(){this.removeTransitionStyles(),this.element.removeEventListener(l,this,!1),this.isTransitioning=!1},a.prototype._removeStyles=function(t){var e={};for(var i in t)e[i]="";this.css(e)};var v={transitionProperty:"",transitionDuration:""};return a.prototype.removeTransitionStyles=function(){this.css(v)},a.prototype.removeElem=function(){this.element.parentNode.removeChild(this.element),this.emitEvent("remove",[this])},a.prototype.remove=function(){if(!h||!parseFloat(this.layout.options.transitionDuration))return this.removeElem(),void 0;var t=this;this.on("transitionEnd",function(){return t.removeElem(),!0}),this.hide()},a.prototype.reveal=function(){delete this.isHidden,this.css({display:""});var t=this.layout.options;this.transition({from:t.hiddenStyle,to:t.visibleStyle,isCleaning:!0})},a.prototype.hide=function(){this.isHidden=!0,this.css({display:""});var t=this.layout.options;this.transition({from:t.visibleStyle,to:t.hiddenStyle,isCleaning:!0,onTransitionEnd:{opacity:function(){this.isHidden&&this.css({display:"none"})}}})},a.prototype.destroy=function(){this.css({position:"",left:"",right:"",top:"",bottom:"",transition:"",transform:""})},a}var r=document.defaultView,s=r&&r.getComputedStyle?function(t){return r.getComputedStyle(t,null)}:function(t){return t.currentStyle};"function"==typeof define&&define.amd?define("outlayer/item",["eventEmitter/EventEmitter","get-size/get-size","get-style-property/get-style-property"],o):(t.Outlayer={},t.Outlayer.Item=o(t.EventEmitter,t.getSize,t.getStyleProperty))}(window),function(t){function e(t,e){for(var i in e)t[i]=e[i];return t}function i(t){return"[object Array]"===f.call(t)}function n(t){var e=[];if(i(t))e=t;else if(t&&"number"==typeof t.length)for(var n=0,o=t.length;o>n;n++)e.push(t[n]);else e.push(t);return e}function o(t,e){var i=d(e,t);-1!==i&&e.splice(i,1)}function r(t){return t.replace(/(.)([A-Z])/g,function(t,e,i){return e+"-"+i}).toLowerCase()}function s(i,s,f,d,c,m){function y(t,i){if("string"==typeof t&&(t=a.querySelector(t)),!t||!l(t))return h&&h.error("Bad "+this.settings.namespace+" element: "+t),void 0;this.element=t,this.options=e({},this.options),this.option(i);var n=++v;this.element.outlayerGUID=n,_[n]=this,this._create(),this.options.isInitLayout&&this.layout()}function g(t,i){t.prototype[i]=e({},y.prototype[i])}var v=0,_={};return y.prototype.settings={namespace:"outlayer",item:m},y.prototype.options={containerStyle:{position:"relative"},isInitLayout:!0,isOriginLeft:!0,isOriginTop:!0,isResizeBound:!0,transitionDuration:"0.4s",hiddenStyle:{opacity:0,transform:"scale(0.001)"},visibleStyle:{opacity:1,transform:"scale(1)"}},e(y.prototype,f.prototype),y.prototype.option=function(t){e(this.options,t)},y.prototype._create=function(){this.reloadItems(),this.stamps=[],this.stamp(this.options.stamp),e(this.element.style,this.options.containerStyle),this.options.isResizeBound&&this.bindResize()},y.prototype.reloadItems=function(){this.items=this._itemize(this.element.children)},y.prototype._itemize=function(t){for(var e=this._filterFindItemElements(t),i=this.settings.item,n=[],o=0,r=e.length;r>o;o++){var s=e[o],a=new i(s,this);n.push(a)}return n},y.prototype._filterFindItemElements=function(t){t=n(t);for(var e=this.options.itemSelector,i=[],o=0,r=t.length;r>o;o++){var s=t[o];if(l(s))if(e){c(s,e)&&i.push(s);for(var a=s.querySelectorAll(e),h=0,p=a.length;p>h;h++)i.push(a[h])}else i.push(s)}return i},y.prototype.getItemElements=function(){for(var t=[],e=0,i=this.items.length;i>e;e++)t.push(this.items[e].element);return t},y.prototype.layout=function(){this._resetLayout(),this._manageStamps();var t=void 0!==this.options.isLayoutInstant?this.options.isLayoutInstant:!this._isLayoutInited;this.layoutItems(this.items,t),this._isLayoutInited=!0},y.prototype._init=y.prototype.layout,y.prototype._resetLayout=function(){this.getSize()},y.prototype.getSize=function(){this.size=d(this.element)},y.prototype._getMeasurement=function(t,e){var i,n=this.options[t];n?("string"==typeof n?i=this.element.querySelector(n):l(n)&&(i=n),this[t]=i?d(i)[e]:n):this[t]=0},y.prototype.layoutItems=function(t,e){t=this._getItemsForLayout(t),this._layoutItems(t,e),this._postLayout()},y.prototype._getItemsForLayout=function(t){for(var e=[],i=0,n=t.length;n>i;i++){var o=t[i];o.isIgnored||e.push(o)}return e},y.prototype._layoutItems=function(t,e){if(!t||!t.length)return this.emitEvent("layoutComplete",[this,t]),void 0;this._itemsOn(t,"layout",function(){this.emitEvent("layoutComplete",[this,t])});for(var i=[],n=0,o=t.length;o>n;n++){var r=t[n],s=this._getItemLayoutPosition(r);s.item=r,s.isInstant=e,i.push(s)}this._processLayoutQueue(i)},y.prototype._getItemLayoutPosition=function(){return{x:0,y:0}},y.prototype._processLayoutQueue=function(t){for(var e=0,i=t.length;i>e;e++){var n=t[e];this._positionItem(n.item,n.x,n.y,n.isInstant)}},y.prototype._positionItem=function(t,e,i,n){n?t.goTo(e,i):t.moveTo(e,i)},y.prototype._postLayout=function(){var t=this._getContainerSize();t&&(this._setContainerMeasure(t.width,!0),this._setContainerMeasure(t.height,!1))},y.prototype._getContainerSize=u,y.prototype._setContainerMeasure=function(t,e){if(void 0!==t){var i=this.size;i.isBorderBox&&(t+=e?i.paddingLeft+i.paddingRight+i.borderLeftWidth+i.borderRightWidth:i.paddingBottom+i.paddingTop+i.borderTopWidth+i.borderBottomWidth),t=Math.max(t,0),this.element.style[e?"width":"height"]=t+"px"}},y.prototype._itemsOn=function(t,e,i){function n(){return o++,o===r&&i.call(s),!0}for(var o=0,r=t.length,s=this,a=0,h=t.length;h>a;a++){var p=t[a];p.on(e,n)}},y.prototype.ignore=function(t){var e=this.getItem(t);e&&(e.isIgnored=!0)},y.prototype.unignore=function(t){var e=this.getItem(t);e&&delete e.isIgnored},y.prototype.stamp=function(t){if(t=this._find(t)){this.stamps=this.stamps.concat(t);for(var e=0,i=t.length;i>e;e++){var n=t[e];this.ignore(n)}}},y.prototype.unstamp=function(t){if(t=this._find(t))for(var e=0,i=t.length;i>e;e++){var n=t[e];o(n,this.stamps),this.unignore(n)}},y.prototype._find=function(t){return t?("string"==typeof t&&(t=this.element.querySelectorAll(t)),t=n(t)):void 0},y.prototype._manageStamps=function(){if(this.stamps&&this.stamps.length){this._getBoundingRect();for(var t=0,e=this.stamps.length;e>t;t++){var i=this.stamps[t];this._manageStamp(i)}}},y.prototype._getBoundingRect=function(){var t=this.element.getBoundingClientRect(),e=this.size;this._boundingRect={left:t.left+e.paddingLeft+e.borderLeftWidth,top:t.top+e.paddingTop+e.borderTopWidth,right:t.right-(e.paddingRight+e.borderRightWidth),bottom:t.bottom-(e.paddingBottom+e.borderBottomWidth)}},y.prototype._manageStamp=u,y.prototype._getElementOffset=function(t){var e=t.getBoundingClientRect(),i=this._boundingRect,n=d(t),o={left:e.left-i.left-n.marginLeft,top:e.top-i.top-n.marginTop,right:i.right-e.right-n.marginRight,bottom:i.bottom-e.bottom-n.marginBottom};return o},y.prototype.handleEvent=function(t){var e="on"+t.type;this[e]&&this[e](t)},y.prototype.bindResize=function(){this.isResizeBound||(i.bind(t,"resize",this),this.isResizeBound=!0)},y.prototype.unbindResize=function(){i.unbind(t,"resize",this),this.isResizeBound=!1},y.prototype.onresize=function(){function t(){e.resize(),delete e.resizeTimeout}this.resizeTimeout&&clearTimeout(this.resizeTimeout);var e=this;this.resizeTimeout=setTimeout(t,100)},y.prototype.resize=function(){var t=d(this.element),e=this.size&&t;e&&t.innerWidth===this.size.innerWidth||this.layout()},y.prototype.addItems=function(t){var e=this._itemize(t);return e.length&&(this.items=this.items.concat(e)),e},y.prototype.appended=function(t){var e=this.addItems(t);e.length&&(this.layoutItems(e,!0),this.reveal(e))},y.prototype.prepended=function(t){var e=this._itemize(t);if(e.length){var i=this.items.slice(0);this.items=e.concat(i),this._resetLayout(),this._manageStamps(),this.layoutItems(e,!0),this.reveal(e),this.layoutItems(i)}},y.prototype.reveal=function(t){if(t&&t.length)for(var e=0,i=t.length;i>e;e++){var n=t[e];n.reveal()}},y.prototype.hide=function(t){if(t&&t.length)for(var e=0,i=t.length;i>e;e++){var n=t[e];n.hide()}},y.prototype.getItem=function(t){for(var e=0,i=this.items.length;i>e;e++){var n=this.items[e];if(n.element===t)return n}},y.prototype.getItems=function(t){if(t&&t.length){for(var e=[],i=0,n=t.length;n>i;i++){var o=t[i],r=this.getItem(o);r&&e.push(r)}return e}},y.prototype.remove=function(t){t=n(t);var e=this.getItems(t);if(e&&e.length){this._itemsOn(e,"remove",function(){this.emitEvent("removeComplete",[this,e])});for(var i=0,r=e.length;r>i;i++){var s=e[i];s.remove(),o(s,this.items)}}},y.prototype.destroy=function(){var t=this.element.style;t.height="",t.position="",t.width="";for(var e=0,i=this.items.length;i>e;e++){var n=this.items[e];n.destroy()}this.unbindResize(),delete this.element.outlayerGUID,p&&p.removeData(this.element,this.settings.namespace)},y.data=function(t){var e=t&&t.outlayerGUID;return e&&_[e]},y.create=function(t,i){function n(){y.apply(this,arguments)}return e(n.prototype,y.prototype),g(n,"options"),g(n,"settings"),e(n.prototype.options,i),n.prototype.settings.namespace=t,n.data=y.data,n.Item=function(){m.apply(this,arguments)},n.Item.prototype=new m,n.prototype.settings.item=n.Item,s(function(){for(var e=r(t),i=a.querySelectorAll(".js-"+e),o="data-"+e+"-options",s=0,u=i.length;u>s;s++){var f,l=i[s],d=l.getAttribute(o);try{f=d&&JSON.parse(d)}catch(c){h&&h.error("Error parsing "+o+" on "+l.nodeName.toLowerCase()+(l.id?"#"+l.id:"")+": "+c);continue}var m=new n(l,f);p&&p.data(l,t,m)}}),p&&p.bridget&&p.bridget(t,n),n},y.Item=m,y}var a=t.document,h=t.console,p=t.jQuery,u=function(){},f=Object.prototype.toString,l="object"==typeof HTMLElement?function(t){return t instanceof HTMLElement}:function(t){return t&&"object"==typeof t&&1===t.nodeType&&"string"==typeof t.nodeName},d=Array.prototype.indexOf?function(t,e){return t.indexOf(e)}:function(t,e){for(var i=0,n=t.length;n>i;i++)if(t[i]===e)return i;return-1};"function"==typeof define&&define.amd?define("outlayer/outlayer",["eventie/eventie","doc-ready/doc-ready","eventEmitter/EventEmitter","get-size/get-size","matches-selector/matches-selector","./item"],s):t.Outlayer=s(t.eventie,t.docReady,t.EventEmitter,t.getSize,t.matchesSelector,t.Outlayer.Item)}(window),function(t){function e(t,e){var n=t.create("masonry");return n.prototype._resetLayout=function(){this.getSize(),this._getMeasurement("columnWidth","outerWidth"),this._getMeasurement("gutter","outerWidth"),this.measureColumns();var t=this.cols;for(this.colYs=[];t--;)this.colYs.push(0);this.maxY=0},n.prototype.measureColumns=function(){if(this.getContainerWidth(),!this.columnWidth){var t=this.items[0],i=t&&t.element;this.columnWidth=i&&e(i).outerWidth||this.containerWidth}this.columnWidth+=this.gutter,this.cols=Math.floor((this.containerWidth+this.gutter)/this.columnWidth),this.cols=Math.max(this.cols,1)},n.prototype.getContainerWidth=function(){var t=this.options.isFitWidth?this.element.parentNode:this.element,i=e(t);this.containerWidth=i&&i.innerWidth},n.prototype._getItemLayoutPosition=function(t){t.getSize();var e=Math.ceil(t.size.outerWidth/this.columnWidth);e=Math.min(e,this.cols);for(var n=this._getColGroup(e),o=Math.min.apply(Math,n),r=i(n,o),s={x:this.columnWidth*r,y:o},a=o+t.size.outerHeight,h=this.cols+1-n.length,p=0;h>p;p++)this.colYs[r+p]=a;return s},n.prototype._getColGroup=function(t){if(2>t)return this.colYs;for(var e=[],i=this.cols+1-t,n=0;i>n;n++){var o=this.colYs.slice(n,n+t);e[n]=Math.max.apply(Math,o)}return e},n.prototype._manageStamp=function(t){var i=e(t),n=this._getElementOffset(t),o=this.options.isOriginLeft?n.left:n.right,r=o+i.outerWidth,s=Math.floor(o/this.columnWidth);s=Math.max(0,s);var a=Math.floor(r/this.columnWidth);a=Math.min(this.cols-1,a);for(var h=(this.options.isOriginTop?n.top:n.bottom)+i.outerHeight,p=s;a>=p;p++)this.colYs[p]=Math.max(h,this.colYs[p])},n.prototype._getContainerSize=function(){this.maxY=Math.max.apply(Math,this.colYs);var t={height:this.maxY};return this.options.isFitWidth&&(t.width=this._getContainerFitWidth()),t},n.prototype._getContainerFitWidth=function(){for(var t=0,e=this.cols;--e&&0===this.colYs[e];)t++;return(this.cols-t)*this.columnWidth-this.gutter},n.prototype.resize=function(){var t=this.containerWidth;this.getContainerWidth(),t!==this.containerWidth&&this.layout()},n}var i=Array.prototype.indexOf?function(t,e){return t.indexOf(e)}:function(t,e){for(var i=0,n=t.length;n>i;i++){var o=t[i];if(o===e)return i}return-1};"function"==typeof define&&define.amd?define("masonry/masonry",["outlayer/outlayer","get-size/get-size"],e):t.Masonry=e(t.Outlayer,t.getSize)}(window);

/*
 * FancyBox - jQuery Plugin
 * Simple and fancy lightbox alternative
 *
 * Examples and documentation at: http://fancybox.net
 * 
 * Copyright (c) 2008 - 2010 Janis Skarnelis
 * That said, it is hardly a one-person project. Many people have submitted bugs, code, and offered their advice freely. Their support is greatly appreciated.
 *
 * Version: 1.3.4 (11/11/2010)
 * Requires: jQuery v1.3+
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */
;(function(B){var L,T,Q,M,d,m,J,A,O,z,C=0,H={},j=[],e=0,G={},y=[],f=null,o=new Image(),i=/\.(jpg|gif|png|bmp|jpeg)(.*)?$/i,k=/[^\.]\.(swf)\s*$/i,p,N=1,h=0,t="",b,c,P=false,s=B.extend(B("<div/>")[0],{prop:0}),S=/MSIE 6/.test(navigator.userAgent)&&B.browser.version<7&&!window.XMLHttpRequest,r=function(){T.hide();o.onerror=o.onload=null;if(f){f.abort()}L.empty()},x=function(){if(false===H.onError(j,C,H)){T.hide();P=false;return}H.titleShow=false;H.width="auto";H.height="auto";L.html('<p id="fancybox-error">The requested content cannot be loaded.<br />Please try again later.</p>');n()},w=function(){var Z=j[C],W,Y,ab,aa,V,X;r();H=B.extend({},B.fn.fancybox.defaults,(typeof B(Z).data("fancybox")=="undefined"?H:B(Z).data("fancybox")));X=H.onStart(j,C,H);if(X===false){P=false;return}else{if(typeof X=="object"){H=B.extend(H,X)}}ab=H.title||(Z.nodeName?B(Z).attr("title"):Z.title)||"";if(Z.nodeName&&!H.orig){H.orig=B(Z).children("img:first").length?B(Z).children("img:first"):B(Z)}if(ab===""&&H.orig&&H.titleFromAlt){ab=H.orig.attr("alt")}W=H.href||(Z.nodeName?B(Z).attr("href"):Z.href)||null;if((/^(?:javascript)/i).test(W)||W=="#"){W=null}if(H.type){Y=H.type;if(!W){W=H.content}}else{if(H.content){Y="html"}else{if(W){if(W.match(i)){Y="image"}else{if(W.match(k)){Y="swf"}else{if(B(Z).hasClass("iframe")){Y="iframe"}else{if(W.indexOf("#")===0){Y="inline"}else{Y="ajax"}}}}}}}if(!Y){x();return}if(Y=="inline"){Z=W.substr(W.indexOf("#"));Y=B(Z).length>0?"inline":"ajax"}H.type=Y;H.href=W;H.title=ab;if(H.autoDimensions){if(H.type=="html"||H.type=="inline"||H.type=="ajax"){H.width="auto";H.height="auto"}else{H.autoDimensions=false}}if(H.modal){H.overlayShow=true;H.hideOnOverlayClick=false;H.hideOnContentClick=false;H.enableEscapeButton=false;H.showCloseButton=false}H.padding=parseInt(H.padding,10);H.margin=parseInt(H.margin,10);L.css("padding",(H.padding+H.margin));B(".fancybox-inline-tmp").unbind("fancybox-cancel").bind("fancybox-change",function(){B(this).replaceWith(m.children())});switch(Y){case"html":L.html(H.content);n();break;case"inline":if(B(Z).parent().is("#fancybox-content")===true){P=false;return}B('<div class="fancybox-inline-tmp" />').hide().insertBefore(B(Z)).bind("fancybox-cleanup",function(){B(this).replaceWith(m.children())}).bind("fancybox-cancel",function(){B(this).replaceWith(L.children())});B(Z).appendTo(L);n();break;case"image":P=false;B.fancybox.showActivity();o=new Image();o.onerror=function(){x()};o.onload=function(){P=true;o.onerror=o.onload=null;F()};o.src=W;break;case"swf":H.scrolling="no";aa='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="'+H.width+'" height="'+H.height+'"><param name="movie" value="'+W+'"></param>';V="";B.each(H.swf,function(ac,ad){aa+='<param name="'+ac+'" value="'+ad+'"></param>';V+=" "+ac+'="'+ad+'"'});aa+='<embed src="'+W+'" type="application/x-shockwave-flash" width="'+H.width+'" height="'+H.height+'"'+V+"></embed></object>";L.html(aa);n();break;case"ajax":P=false;B.fancybox.showActivity();H.ajax.win=H.ajax.success;f=B.ajax(B.extend({},H.ajax,{url:W,data:H.ajax.data||{},dataType:"text",error:function(ac,ae,ad){if(ac.status>0){x()}},success:function(ad,af,ac){var ae=typeof ac=="object"?ac:f;if(ae.status==200||ae.status===0){if(typeof H.ajax.win=="function"){X=H.ajax.win(W,ad,af,ac);if(X===false){T.hide();return}else{if(typeof X=="string"||typeof X=="object"){ad=X}}}L.html(ad);n()}}}));break;case"iframe":E();break}},n=function(){var V=H.width,W=H.height;if(V.toString().indexOf("%")>-1){V=parseInt((B(window).width()-(H.margin*2))*parseFloat(V)/100,10)+"px"}else{V=V=="auto"?"auto":V+"px"}if(W.toString().indexOf("%")>-1){W=parseInt((B(window).height()-(H.margin*2))*parseFloat(W)/100,10)+"px"}else{W=W=="auto"?"auto":W+"px"}L.wrapInner('<div style="width:'+V+";height:"+W+";overflow: "+(H.scrolling=="auto"?"auto":(H.scrolling=="yes"?"scroll":"hidden"))+';position:relative;"></div>');H.width=L.width();H.height=L.height();E()},F=function(){H.width=o.width;H.height=o.height;B("<img />").attr({id:"fancybox-img",src:o.src,alt:H.title}).appendTo(L);E()},E=function(){var W,V;T.hide();if(M.is(":visible")&&false===G.onCleanup(y,e,G)){B('.fancybox-inline-tmp').trigger('fancybox-cancel');P=false;return}P=true;B(m.add(Q)).unbind();B(window).unbind("resize.fb scroll.fb");B(document).unbind("keydown.fb");if(M.is(":visible")&&G.titlePosition!=="outside"){M.css("height",M.height())}y=j;e=C;G=H;if(G.overlayShow){Q.css({"background-color":G.overlayColor,opacity:G.overlayOpacity,cursor:G.hideOnOverlayClick?"pointer":"auto",height:B(document).height()});if(!Q.is(":visible")){if(S){B("select:not(#fancybox-tmp select)").filter(function(){return this.style.visibility!=="hidden"}).css({visibility:"hidden"}).one("fancybox-cleanup",function(){this.style.visibility="inherit"})}Q.show()}}else{Q.hide()}c=R();l();if(M.is(":visible")){B(J.add(O).add(z)).hide();W=M.position(),b={top:W.top,left:W.left,width:M.width(),height:M.height()};V=(b.width==c.width&&b.height==c.height);m.fadeTo(G.changeFade,0.3,function(){var X=function(){m.html(L.contents()).fadeTo(G.changeFade,1,v)};B('.fancybox-inline-tmp').trigger('fancybox-change');m.empty().removeAttr("filter").css({"border-width":G.padding,width:c.width-G.padding*2,height:H.autoDimensions?"auto":c.height-h-G.padding*2});if(V){X()}else{s.prop=0;B(s).animate({prop:1},{duration:G.changeSpeed,easing:G.easingChange,step:U,complete:X})}});return}M.removeAttr("style");m.css("border-width",G.padding);if(G.transitionIn=="elastic"){b=I();m.html(L.contents());M.show();if(G.opacity){c.opacity=0}s.prop=0;B(s).animate({prop:1},{duration:G.speedIn,easing:G.easingIn,step:U,complete:v});return}if(G.titlePosition=="inside"&&h>0){A.show()}m.css({width:c.width-G.padding*2,height:H.autoDimensions?"auto":c.height-h-G.padding*2}).html(L.contents());M.css(c).fadeIn(G.transitionIn=="none"?0:G.speedIn,v)},D=function(V){if(V&&V.length){if(G.titlePosition=="float"){return'<table id="fancybox-title-float-wrap" cellpadding="0" cellspacing="0"><tr><td id="fancybox-title-float-left"></td><td id="fancybox-title-float-main">'+V+'</td><td id="fancybox-title-float-right"></td></tr></table>'}return'<div id="fancybox-title-'+G.titlePosition+'">'+V+"</div>"}return false},l=function(){t=G.title||"";h=0;A.empty().removeAttr("style").removeClass();if(G.titleShow===false){A.hide();return}t=B.isFunction(G.titleFormat)?G.titleFormat(t,y,e,G):D(t);if(!t||t===""){A.hide();return}A.addClass("fancybox-title-"+G.titlePosition).html(t).appendTo("body").show();switch(G.titlePosition){case"inside":A.css({width:c.width-(G.padding*2),marginLeft:G.padding,marginRight:G.padding});h=A.outerHeight(true);A.appendTo(d);c.height+=h;break;case"over":A.css({marginLeft:G.padding,width:c.width-(G.padding*2),bottom:G.padding}).appendTo(d);break;case"float":A.css("left",parseInt((A.width()-c.width-40)/2,10)*-1).appendTo(M);break;default:A.css({width:c.width-(G.padding*2),paddingLeft:G.padding,paddingRight:G.padding}).appendTo(M);break}A.hide()},g=function(){if(G.enableEscapeButton||G.enableKeyboardNav){B(document).bind("keydown.fb",function(V){if(V.keyCode==27&&G.enableEscapeButton){V.preventDefault();B.fancybox.close()}else{if((V.keyCode==37||V.keyCode==39)&&G.enableKeyboardNav&&V.target.tagName!=="INPUT"&&V.target.tagName!=="TEXTAREA"&&V.target.tagName!=="SELECT"){V.preventDefault();B.fancybox[V.keyCode==37?"prev":"next"]()}}})}if(!G.showNavArrows){O.hide();z.hide();return}if((G.cyclic&&y.length>1)||e!==0){O.show()}if((G.cyclic&&y.length>1)||e!=(y.length-1)){z.show()}},v=function(){if(!B.support.opacity){m.get(0).style.removeAttribute("filter");M.get(0).style.removeAttribute("filter")}if(H.autoDimensions){m.css("height","auto")}M.css("height","auto");if(t&&t.length){A.show()}if(G.showCloseButton){J.show()}g();if(G.hideOnContentClick){m.bind("click",B.fancybox.close)}if(G.hideOnOverlayClick){Q.bind("click",B.fancybox.close)}B(window).bind("resize.fb",B.fancybox.resize);if(G.centerOnScroll){B(window).bind("scroll.fb",B.fancybox.center)}if(G.type=="iframe"){B('<iframe id="fancybox-frame" name="fancybox-frame'+new Date().getTime()+'" frameborder="0" hspace="0" '+(window.attachEvent?'allowtransparency="true""':"")+' scrolling="'+H.scrolling+'" src="'+G.href+'"></iframe>').appendTo(m)}M.show();P=false;B.fancybox.center();G.onComplete(y,e,G);K()},K=function(){var V,W;if((y.length-1)>e){V=y[e+1].href;if(typeof V!=="undefined"&&V.match(i)){W=new Image();W.src=V}}if(e>0){V=y[e-1].href;if(typeof V!=="undefined"&&V.match(i)){W=new Image();W.src=V}}},U=function(W){var V={width:parseInt(b.width+(c.width-b.width)*W,10),height:parseInt(b.height+(c.height-b.height)*W,10),top:parseInt(b.top+(c.top-b.top)*W,10),left:parseInt(b.left+(c.left-b.left)*W,10)};if(typeof c.opacity!=="undefined"){V.opacity=W<0.5?0.5:W}M.css(V);m.css({width:V.width-G.padding*2,height:V.height-(h*W)-G.padding*2})},u=function(){return[B(window).width()-(G.margin*2),B(window).height()-(G.margin*2),B(document).scrollLeft()+G.margin,B(document).scrollTop()+G.margin]},R=function(){var V=u(),Z={},W=G.autoScale,X=G.padding*2,Y;if(G.width.toString().indexOf("%")>-1){Z.width=parseInt((V[0]*parseFloat(G.width))/100,10)}else{Z.width=G.width+X}if(G.height.toString().indexOf("%")>-1){Z.height=parseInt((V[1]*parseFloat(G.height))/100,10)}else{Z.height=G.height+X}if(W&&(Z.width>V[0]||Z.height>V[1])){if(H.type=="image"||H.type=="swf"){Y=(G.width)/(G.height);if((Z.width)>V[0]){Z.width=V[0];Z.height=parseInt(((Z.width-X)/Y)+X,10)}if((Z.height)>V[1]){Z.height=V[1];Z.width=parseInt(((Z.height-X)*Y)+X,10)}}else{Z.width=Math.min(Z.width,V[0]);Z.height=Math.min(Z.height,V[1])}}Z.top=parseInt(Math.max(V[3]-20,V[3]+((V[1]-Z.height-40)*0.5)),10);Z.left=parseInt(Math.max(V[2]-20,V[2]+((V[0]-Z.width-40)*0.5)),10);return Z},q=function(V){var W=V.offset();W.top+=parseInt(V.css("paddingTop"),10)||0;W.left+=parseInt(V.css("paddingLeft"),10)||0;W.top+=parseInt(V.css("border-top-width"),10)||0;W.left+=parseInt(V.css("border-left-width"),10)||0;W.width=V.width();W.height=V.height();return W},I=function(){var Y=H.orig?B(H.orig):false,X={},W,V;if(Y&&Y.length){W=q(Y);X={width:W.width+(G.padding*2),height:W.height+(G.padding*2),top:W.top-G.padding-20,left:W.left-G.padding-20}}else{V=u();X={width:G.padding*2,height:G.padding*2,top:parseInt(V[3]+V[1]*0.5,10),left:parseInt(V[2]+V[0]*0.5,10)}}return X},a=function(){if(!T.is(":visible")){clearInterval(p);return}B("div",T).css("top",(N*-40)+"px");N=(N+1)%12};B.fn.fancybox=function(V){if(!B(this).length){return this}B(this).data("fancybox",B.extend({},V,(B.metadata?B(this).metadata():{}))).unbind("click.fb").bind("click.fb",function(X){X.preventDefault();if(P){return}P=true;B(this).blur();j=[];C=0;var W=B(this).attr("rel")||"";if(!W||W==""||W==="nofollow"){j.push(this)}else{j=B('a[rel="'+W+'"], area[rel="'+W+'"]');C=j.index(this)}w();return});return this};B.fancybox=function(Y){var X;if(P){return}P=true;X=typeof arguments[1]!=="undefined"?arguments[1]:{};j=[];C=parseInt(X.index,10)||0;if(B.isArray(Y)){for(var W=0,V=Y.length;W<V;W++){if(typeof Y[W]=="object"){B(Y[W]).data("fancybox",B.extend({},X,Y[W]))}else{Y[W]=B({}).data("fancybox",B.extend({content:Y[W]},X))}}j=jQuery.merge(j,Y)}else{if(typeof Y=="object"){B(Y).data("fancybox",B.extend({},X,Y))}else{Y=B({}).data("fancybox",B.extend({content:Y},X))}j.push(Y)}if(C>j.length||C<0){C=0}w()};B.fancybox.showActivity=function(){clearInterval(p);T.show();p=setInterval(a,66)};B.fancybox.hideActivity=function(){T.hide()};B.fancybox.next=function(){return B.fancybox.pos(e+1)};B.fancybox.prev=function(){return B.fancybox.pos(e-1)};B.fancybox.pos=function(V){if(P){return}V=parseInt(V);j=y;if(V>-1&&V<y.length){C=V;w()}else{if(G.cyclic&&y.length>1){C=V>=y.length?0:y.length-1;w()}}return};B.fancybox.cancel=function(){if(P){return}P=true;B('.fancybox-inline-tmp').trigger('fancybox-cancel');r();H.onCancel(j,C,H);P=false};B.fancybox.close=function(){if(P||M.is(":hidden")){return}P=true;if(G&&false===G.onCleanup(y,e,G)){P=false;return}r();B(J.add(O).add(z)).hide();B(m.add(Q)).unbind();B(window).unbind("resize.fb scroll.fb");B(document).unbind("keydown.fb");if(G.type==="iframe"){m.find("iframe").attr("src",S&&/^https/i.test(window.location.href||"")?"javascript:void(false)":"about:blank")}if(G.titlePosition!=="inside"){A.empty()}M.stop();function V(){Q.fadeOut("fast");A.empty().hide();M.hide();B('.fancybox-inline-tmp').trigger('fancybox-cleanup');m.empty();G.onClosed(y,e,G);y=H=[];e=C=0;G=H={};P=false}if(G.transitionOut=="elastic"){b=I();var W=M.position();c={top:W.top,left:W.left,width:M.width(),height:M.height()};if(G.opacity){c.opacity=1}A.empty().hide();s.prop=1;B(s).animate({prop:0},{duration:G.speedOut,easing:G.easingOut,step:U,complete:V})}else{M.fadeOut(G.transitionOut=="none"?0:G.speedOut,V)}};B.fancybox.resize=function(){if(Q.is(":visible")){Q.css("height",B(document).height())}B.fancybox.center(true)};B.fancybox.center=function(){var V,W;if(P){return}W=arguments[0]===true?1:0;V=u();if(!W&&(M.width()>V[0]||M.height()>V[1])){return}M.stop().animate({top:parseInt(Math.max(V[3]-20,V[3]+((V[1]-m.height()-40)*0.5)-G.padding)),left:parseInt(Math.max(V[2]-20,V[2]+((V[0]-m.width()-40)*0.5)-G.padding))},typeof arguments[0]=="number"?arguments[0]:200)};B.fancybox.init=function(){if(B("#fancybox-wrap").length){return}B("body").append(L=B('<div id="fancybox-tmp"></div>'),T=B('<div id="fancybox-loading"><div></div></div>'),Q=B('<div id="fancybox-overlay"></div>'),M=B('<div id="fancybox-wrap"></div>'));d=B('<div id="fancybox-outer"></div>').append('<div class="fancybox-bg" id="fancybox-bg-n"></div><div class="fancybox-bg" id="fancybox-bg-ne"></div><div class="fancybox-bg" id="fancybox-bg-e"></div><div class="fancybox-bg" id="fancybox-bg-se"></div><div class="fancybox-bg" id="fancybox-bg-s"></div><div class="fancybox-bg" id="fancybox-bg-sw"></div><div class="fancybox-bg" id="fancybox-bg-w"></div><div class="fancybox-bg" id="fancybox-bg-nw"></div>').appendTo(M);d.append(m=B('<div id="fancybox-content"></div>'),J=B('<a id="fancybox-close"></a>'),A=B('<div id="fancybox-title"></div>'),O=B('<a href="javascript:;" id="fancybox-left"><span class="fancy-ico" id="fancybox-left-ico"></span></a>'),z=B('<a href="javascript:;" id="fancybox-right"><span class="fancy-ico" id="fancybox-right-ico"></span></a>'));J.click(B.fancybox.close);T.click(B.fancybox.cancel);O.click(function(V){V.preventDefault();B.fancybox.prev()});z.click(function(V){V.preventDefault();B.fancybox.next()});if(B.fn.mousewheel){M.bind("mousewheel.fb",function(V,W){if(P){V.preventDefault()}else{if(B(V.target).get(0).clientHeight==0||B(V.target).get(0).scrollHeight===B(V.target).get(0).clientHeight){V.preventDefault();B.fancybox[W>0?"prev":"next"]()}}})}if(!B.support.opacity){M.addClass("fancybox-ie")}if(S){T.addClass("fancybox-ie6");M.addClass("fancybox-ie6");B('<iframe id="fancybox-hide-sel-frame" src="'+(/^https/i.test(window.location.href||"")?"javascript:void(false)":"about:blank")+'" scrolling="no" border="0" frameborder="0" tabindex="-1"></iframe>').prependTo(d)}};B.fn.fancybox.defaults={padding:10,margin:40,opacity:false,modal:false,cyclic:false,scrolling:"auto",width:560,height:340,autoScale:true,autoDimensions:true,centerOnScroll:false,ajax:{},swf:{wmode:"transparent"},hideOnOverlayClick:true,hideOnContentClick:false,overlayShow:true,overlayOpacity:0.7,overlayColor:"#777",titleShow:true,titlePosition:"float",titleFormat:null,titleFromAlt:false,transitionIn:"fade",transitionOut:"fade",speedIn:300,speedOut:300,changeSpeed:300,changeFade:"fast",easingIn:"swing",easingOut:"swing",showCloseButton:true,showNavArrows:true,enableEscapeButton:true,enableKeyboardNav:true,onStart:function(){},onCancel:function(){},onComplete:function(){},onCleanup:function(){},onClosed:function(){},onError:function(){}};B(document).ready(function(){B.fancybox.init()})})(jQuery);