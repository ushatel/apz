jQuery ->

  $('#popup4 a.close').on 'click', (e) ->
    e.preventDefault()
    $.fancybox.close()

  $('a.feed-back').fancybox({
    padding: 0
    margin: 0
    cyclic: false
    autoScale: true
    overlayShow: true
  })

  $('#popup4 input.close').on 'click', (e) ->
    if $('#feed_back_email').val() == ''
      e.preventDefault()
      $('#feed_back_email').parent().parent().parent().css('border', '1px solid #FF0000')
