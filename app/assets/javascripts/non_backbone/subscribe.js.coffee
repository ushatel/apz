jQuery ->

  sendEmail = (attrs) ->
    if /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(attrs.email)

      $.ajax({
        url: '/feed-back/subscribe'
        type: 'post'
        data: attrs.data
        beforeSend: attrs.beforeSend
        success: attrs.success
      })

    else

      $.fancybox({
        padding: 0
        margin: 0
        content: $('<div>', {id: 'flash-message-wrapper', html: 'Введите правильный E-mail адрес'})
        cyclic: false
        autoScale: true
        overlayShow: true
        overlayOpacity: 0.65
        overlayColor: '#000000'
        titlePosition: 'inside'
      })

  $('#mc-embedded-subscribe').on 'click', (e) ->
    e.preventDefault()

    sendEmail
      email: $('#mce-EMAIL').val()
      data: $("#mc-embedded-subscribe-form").serialize()
      beforeSend: () =>
        $(this).val("Отправляем....")
        $(this).prop('disabled', 'disabled')

      success: (data) ->

        $("#subscribe-form").html('')
        $.when($('.task-box').animate({width: '760px'})).then () ->
          $("#subscribe-form").text('Спасибо, заявка принята. Мы свяжемся с вами в ближайшее время.')

  $("#calc-subscribe").on 'click', (e) ->
    e.preventDefault()

    sendEmail
      email: $('#mce-EMAIL').val()
      data: $("#mc-embedded-subscribe-form").serialize()
      beforeSend: () =>
        $(this).val("Отправляем....")
        $(this).prop('disabled', 'disabled')

      success: (data) ->

        $("#calc-subscribe-form").html('')
        $("#calc-subscribe-form").text('Спасибо, заявка принята. Мы свяжемся с вами в ближайшее время.')
