jQuery ->

  class Calculator

    @getNum: (num) ->
      res = num
      res = num.replace(",", ".") if _.isString(num)
      if _.isBoolean(num)
        res = if num then 1 else 0
      res = num.length if _.isArray(num)
      res = 0 if num == ''

      if (_.isUndefined(res) || _.isNull(res) || _.isNaN(res)) then 0 else parseFloat(res)

    @getAllValues: () ->
      result = {}
      result['room_space'] = Calculator.getNum($("#room-space").val())
      result['discount_code'] = Calculator.getNum($("#discount-code").val())
      result['adjustments_count'] = Calculator.getNum($("#adjustments-count").val())
      result['modelings_count'] = Calculator.getNum($("#modelings-count").val())
      result['high_value_image'] = Calculator.getNum($("#high-value-image").val())
      result['initial_scene_with_settings'] = Calculator.getNum(!$("#initial-scene-with-settings").prop('checked'))
      result['drawings'] = Calculator.getNum(!$("#drawings").prop('checked'))
      result['furniture_plan'] = Calculator.getNum(!$("#furniture-plan").prop('checked'))
      result['ceiling_plan'] = Calculator.getNum(!$("#ceiling-plan").prop('checked'))
      result['lighting_plan'] = Calculator.getNum(!$("#lighting-plan").prop('checked'))
      result['coverings_plan'] = Calculator.getNum(!$("#coverings-plan").prop('checked'))
      result['walls_plan'] = Calculator.getNum(!$("#walls-plan").prop('checked'))
      result['select_facilities'] = Calculator.getNum(!$("#select-facilities").prop('checked'))

      result

    @checkOrUnCkeck: (element) ->
      value = Calculator.getAllValues()[$(element).prop('id').replace('#', '').replace(/-/g, '_').replace('_more', '').replace('_less', '')]

      if parseInt(value) > 0
        Calculator.check(element)
      else
        Calculator.unCheck(element)

    @check: (element) ->
      if $(element).prop('id') == 'discount-code'
        unless $(element).parent().find('.chk-area').hasClass('chk-checked')
          $(element).parent().find('.chk-area').removeClass('chk-unchecked').addClass('chk-checked')
        $(element).parent().addClass('selected') unless $(element).parent().hasClass('selected')

      else if ($(element).prop('id') == 'adjustments-count') || ($(element).prop('id') == 'modelings-count') || ($(element).prop('id') == 'high-value-image')
        unless $(element).parent().parent().parent().find('.chk-area').hasClass('chk-checked')
          $(element).parent().parent().parent().find('.chk-area').removeClass('chk-unchecked').addClass('chk-checked')
        $(element).parent().parent().parent().addClass('selected') unless $(element).parent().parent().hasClass('selected')

      else
        unless $(element).parent().find('.chk-area').hasClass('chk-checked')
          $(element).parent().find('.chk-area').removeClass('chk-unchecked').addClass('chk-checked')
        $(element).parent().parent().addClass('selected') unless $(element).parent().parent().hasClass('selected')


    @unCheck: (element) ->
      if $(element).prop('id') == 'discount-code'
        unless $(element).parent().find('.chk-area').hasClass('chk-unchecked')
          $(element).parent().find('.chk-area').removeClass('chk-checked').addClass('chk-unchecked')
        $(element).parent().removeClass('selected')

      else if ($(element).prop('id') == 'adjustments-count') || ($(element).prop('id') == 'modelings-count') || ($(element).prop('id') == 'high-value-image')
        unless $(element).parent().parent().parent().find('.chk-area').hasClass('chk-unchecked')
          $(element).parent().parent().parent().find('.chk-area').removeClass('chk-checked').addClass('chk-unchecked')
        $(element).parent().parent().parent().removeClass('selected')

      else
        unless $(element).parent().find('.chk-area').hasClass('chk-unchecked')
          $(element).parent().find('.chk-area').removeClass('chk-checked').addClass('chk-unchecked')
        $(element).parent().parent().removeClass('selected')

    @getRoomSpacePrice: (params = {}) ->
      gn = Calculator.getNum
      floorArea = if _.isUndefined(params['room_space']) then Calculator.getAllValues()['room_space'] else gn(params['room_space'])
      factor = if gn(floorArea) <= gn(gon.roomSpaceLessThen) then gn(gon.roomSpaceLessValue) else gn(gon.roomSpaceAboveValue)

      (gn(floorArea) * gn(factor)).toFixed(0)

    @getDiscountPrice: (params = {}) ->
#      roomSpacePrice = Calculator.getRoomSpacePrice(params)
      if Calculator.getAllValues()['discount_code'] == 777 then 70 else 0
#      gn = Calculator.getNum
#
#
#      (gn(roomSpacePrice) * gn(@getDiscount(params))).toFixed(0)

    @getDiscount: (params = {}) ->
      gn = Calculator.getNum
      floorArea = if _.isUndefined(params['room_space']) then Calculator.getAllValues()['room_space'] else gn(params['room_space'])

      if gn(floorArea) < gn(gon.discountLessThen)
        gn(gon.discountLessValue)
      else
        gn(gon.discountAboveValue)

    @getUrgencyPrice: (params = {}) ->
      0

    @getCouponDiscountPercent: (params = {}) =>
      0
#      gn = Calculator.getNum
#      value = 0
#      if _.isUndefined(params['discount'])
#
#        value = gn(@get("discount").percent) if !_.isUndefined(@get("discount")) && !_.isNull(@get("discount"))
#      else
#        value = gn(params['discount'])
#
#      gn(value)
    @getAdjusmentCountPrice: (params = {}) =>
      gn = Calculator.getNum

      floorArea = if _.isUndefined(params['room_space']) then Calculator.getAllValues()['room_space'] else gn(params['room_space'])
      value = if _.isUndefined(params['adjustments_count']) then Calculator.getAllValues()['adjustments_count'] else gn(params['adjustments_count'])
      factor = if gn(gon.adjustmentsCountLessThen) < gn(value) then gn(gon.adjustmentsCountLessValue) else gn(gon.adjustmentsCountAboveValue)

      (gn(value) * gn(factor) * gn(floorArea)).toFixed(0)

    @getModelingTasksPrice: (params = {}) =>
      gn = Calculator.getNum

      value = if _.isUndefined(params['modelings_count']) then Calculator.getAllValues()['modelings_count'] else gn(params['modelings_count'])

      (value * gn(gon.modelingTasksCost)).toFixed(0)

    @getFurniturePlanPrice: (params = {}) ->
      gn = Calculator.getNum
      floorArea = if _.isUndefined(params['room_space']) then Calculator.getAllValues()['room_space'] else gn(params['room_space'])
      value = if _.isUndefined(params['furniture_plan']) then Calculator.getAllValues()['furniture_plan'] else gn(params['furniture_plan'])
      #value = if value > 0 then 0 else 1

      (gn(value) * gn(gon.furniturePlanCost) * gn(floorArea)).toFixed(0)

    @getCeilingPlanPrice: (params = {}) ->
      gn = Calculator.getNum
      floorArea = if _.isUndefined(params['room_space']) then Calculator.getAllValues()['room_space'] else gn(params['room_space'])
      value = if _.isUndefined(params['ceiling_plan']) then Calculator.getAllValues()['ceiling_plan'] else gn(params['ceiling_plan'])
      #value = if value > 0 then 1 else -1

      (gn(value) * gn(gon.ceilingPlanCost) * gn(floorArea)).toFixed(0)

    @getLightingPlanPrice: (params = {}) ->
      gn = Calculator.getNum
      floorArea = if _.isUndefined(params['room_space']) then Calculator.getAllValues()['room_space'] else gn(params['room_space'])
      value = if _.isUndefined(params['lighting_plan']) then Calculator.getAllValues()['lighting_plan'] else gn(params['lighting_plan'])
      #value = if value > 0 then 0 else 1

      (gn(value) * gn(gon.lightingPlanCost) * gn(floorArea)).toFixed(0)

    @getCoveringsPlanPrice: (params = {}) ->
      gn = Calculator.getNum
      floorArea = if _.isUndefined(params['room_space']) then Calculator.getAllValues()['room_space'] else gn(params['room_space'])
      value = if _.isUndefined(params['coverings_plan']) then Calculator.getAllValues()['coverings_plan'] else gn(params['coverings_plan'])
      #value = if value > 0 then 0 else 1

      (gn(value) * gn(gon.coveringsPlanCost) * gn(floorArea)).toFixed(0)

    @getWallsPlanPrice: (params = {}) ->
      gn = Calculator.getNum
      floorArea = if _.isUndefined(params['room_space']) then Calculator.getAllValues()['room_space'] else gn(params['room_space'])
      value = if _.isUndefined(params['walls_plan']) then Calculator.getAllValues()['walls_plan'] else gn(params['walls_plan'])
      #value = if value > 0 then 0 else 1

      (gn(value) * gn(gon.wallsPlanCost) * gn(floorArea)).toFixed(0)

    @getSelectedFacilitiesPrice: (params = {}) ->
      gn = Calculator.getNum
      floorArea = if _.isUndefined(params['room_space']) then Calculator.getAllValues()['room_space'] else gn(params['room_space'])
      value = if _.isUndefined(params['select_facilities']) then Calculator.getAllValues()['select_facilities'] else gn(params['select_facilities'])
      #value = if value > 0 then 0 else 1

      (gn(value) * gn(gon.selectingFacilitiesCost) * gn(floorArea)).toFixed(0)

    @getHighResolutionImagePrice: (params = {}) =>
#      gn = Calculator.getNum
#      floorArea = if _.isUndefined(params['room_space']) then Calculator.getAllValues()['room_space'] else gn(params['room_space'])
#      value = if _.isUndefined(params['high_value_image']) then Calculator.getAllValues()['high_value_image'] else gn(params['high_value_image'])
#
#      (gn(value) * gn(gon.highResolutionImageCost) * gn(floorArea)).toFixed(0)

      gn = Calculator.getNum

      value = if _.isUndefined(params['high_value_image']) then Calculator.getAllValues()['high_value_image'] else gn(params['high_value_image'])

      (value * gn(gon.highValueImageCost)).toFixed(0)

    @getInitialSceneWithSettingsPrice: (params = {}) =>

      gn = Calculator.getNum
      floorArea = if _.isUndefined(params['room_space']) then Calculator.getAllValues()['room_space'] else gn(params['room_space'])

      amount = Calculator.getVisualizationAmount(params) +
      Calculator.getDesignAmount(params) +
      parseFloat(Calculator.getModelingTasksPrice(params)) +
      parseFloat(Calculator.getHighResolutionImagePrice(params)) +
      parseFloat(Calculator.getDrawingsPrice(params))

      value = if _.isUndefined(params['initial_scene_with_settings']) then Calculator.getAllValues()['initial_scene_with_settings'] else gn(params['initial_scene_with_settings'])

      value = if Calculator.getAllValues()['initial_scene_with_settings'] then amount else Calculator.getAllValues()['initial_scene_with_settings']

      return 0 if value == 0
      result = gn(value) * gn(gon.initialSceneCost) #* gn(floorArea)

      if parseInt(result) < 25 then 25 else result.toFixed(0)

    @getDrawingsPrice: (params = {}) =>
      gn = Calculator.getNum
      floorArea = if _.isUndefined(params['room_space']) then Calculator.getAllValues()['room_space'] else gn(params['room_space'])
      value = if _.isUndefined(params['drawings']) then Calculator.getAllValues()['drawings'] else gn(params['drawings'])
      #value = if value > 0 then 0 else 1

      (value * gn(gon.drawingsCost) * gn(floorArea)).toFixed(0)

    @getVisualizationAmount: (params = {}) ->
      parseFloat(Calculator.getRoomSpacePrice(params)) -
      parseFloat(Calculator.getDiscountPrice(params)) +
      parseFloat(Calculator.getUrgencyPrice(params))

    @getDesignAmount: (params = {}) ->
      parseFloat(Calculator.getFurniturePlanPrice(params)) +
      parseFloat(Calculator.getCeilingPlanPrice(params)) +
      parseFloat(Calculator.getLightingPlanPrice(params)) +
      parseFloat(Calculator.getCoveringsPlanPrice(params)) +
      parseFloat(Calculator.getWallsPlanPrice(params)) +
      parseFloat(Calculator.getSelectedFacilitiesPrice(params)) +
      parseFloat(Calculator.getAdjusmentCountPrice(params))

    @getAdditionalOptionsPrice: (params = {}) ->
      parseFloat(Calculator.getModelingTasksPrice(params)) +
      parseFloat(Calculator.getInitialSceneWithSettingsPrice(params)) +
      parseFloat(Calculator.getHighResolutionImagePrice(params)) +
      parseFloat(Calculator.getDrawingsPrice(params))

    @getOrderAmount: (params = {}) ->
      Calculator.reloadValues()
      amount = Calculator.getVisualizationAmount(params) + Calculator.getDesignAmount(params) + Calculator.getAdditionalOptionsPrice(params)

      amount = 5 if (amount > 0) && (amount < 5)

      $('.summ strong').text("#{amount.toFixed(0)}$")

    @reloadValues: () ->
      _.each ['#discount-code', "#adjustments-count", "#modelings-count", '#high-value-image', '#initial-scene-with-settings', '#drawings', '#furniture-plan', '#ceiling-plan', '#lighting-plan', '#coverings-plan', '#walls-plan', '#select-facilities'], (id) ->
        Calculator.checkOrUnCkeck(id) unless id == "#discount-code"

        if $(id).prop('id') == 'discount-code'
          $(id).parent().find('.price').text('-' + Calculator.getDiscountPrice() + '$')

        else if $(id).prop('id') == 'adjustments-count'
          params = if parseInt(Calculator.getAdjusmentCountPrice()) == 0 then {adjustments_count: 1} else {}
          $(id).parent().parent().parent().find('.price').text('+' + Calculator.getAdjusmentCountPrice(params) + '$')

        else if $(id).prop('id') == 'modelings-count'
          params = if parseInt(Calculator.getModelingTasksPrice()) == 0 then {modelings_count: 1} else {}
          $(id).parent().parent().parent().find('.price').text('+' + Calculator.getModelingTasksPrice(params) + '$')

        else if $(id).prop('id') == 'high-value-image'
          params = if parseInt(Calculator.getHighResolutionImagePrice()) == 0 then {high_value_image: 1} else {}
          $(id).parent().parent().parent().find('.price').text('+' + Calculator.getHighResolutionImagePrice(params) + '$')

        else if $(id).prop('id') == 'initial-scene-with-settings'
          $(id).parent().parent().find('.price').text('+' + Calculator.getInitialSceneWithSettingsPrice({initial_scene_with_settings: 1}) + '$')

        else if $(id).prop('id') == 'drawings'
          $(id).parent().parent().find('.price').text('+' + Calculator.getDrawingsPrice({drawings: 1}) + '$')

        else if $(id).prop('id') == 'furniture-plan'
          $(id).parent().parent().find('.price').text('+' + Calculator.getFurniturePlanPrice(furniture_plan: 1) + '$')

        else if $(id).prop('id') == 'ceiling-plan'
          $(id).parent().parent().find('.price').text('+' + Calculator.getCeilingPlanPrice(ceiling_plan: 1) + '$')

        else if $(id).prop('id') == 'lighting-plan'
          $(id).parent().parent().find('.price').text('+' + Calculator.getLightingPlanPrice(lighting_plan: 1) + '$')

        else if $(id).prop('id') == 'coverings-plan'
          $(id).parent().parent().find('.price').text('+' + Calculator.getCoveringsPlanPrice(coverings_plan: 1) + '$')

        else if $(id).prop('id') == 'walls-plan'
          $(id).parent().parent().find('.price').text('+' + Calculator.getWallsPlanPrice(walls_plan: 1) + '$')

        else if $(id).prop('id') == 'select-facilities'
          $(id).parent().parent().find('.price').text('+' + Calculator.getSelectedFacilitiesPrice(select_facilities: 1) + '$')

  $('.calculator-form, #room-space').on 'keyup keypress', (e) ->
    code = e.keyCode || e.which

    if code  == 13
      e.preventDefault()
      return false

  $('#room-space, #discount-code').on 'change keyup', ->


    $("#room-space").val(9999) if Calculator.getNum($("#room-space").val()) > 9999
    $("#room-space").val(Math.abs(Calculator.getNum($("#room-space").val()))) if Calculator.getNum($("#room-space").val()) < 0

    Calculator.getOrderAmount()

  _.each ['#discount-code'], (id) ->

    $(id).parent().find('.chk-area').on 'click', (e) ->

      if $(this).parent().parent().hasClass('selected')
        $(this).addClass('chk-unchecked')
        $(this).removeClass('chk-checked')
        $(this).parent().parent().removeClass('selected')

      else
        $(this).parent().parent().addClass('selected')
        $(this).addClass('chk-checked')
        $(this).removeClass('chk-unchecked')

  _.each ["#adjustments-count", "#modelings-count", "#high-value-image"], (id) ->

    $("#{id}-less").on 'click', (e) ->
      e.preventDefault()
      newValue = parseInt($(id).val()) - 1
      if newValue >= 0
        $(id).val(newValue)

        Calculator.getOrderAmount()

    $("#{id}-more").on 'click', (e) ->
      e.preventDefault()
      newValue = parseInt($(id).val()) + 1
      $(id).val(newValue)

      Calculator.getOrderAmount()

    $(id).parent().parent().parent().find('.chk-area').on 'click', (e) ->

      if $(this).parent().parent().hasClass('selected')
        $(this).addClass('chk-unchecked')
        $(this).removeClass('chk-checked')
        $(this).parent().parent().removeClass('selected')
        $(id).val(0)
      else
        $(this).parent().parent().addClass('selected')
        $(this).addClass('chk-checked')
        $(this).removeClass('chk-unchecked')
        $(id).val(1)

      Calculator.getOrderAmount()

  _.each ['#initial-scene-with-settings', '#drawings', '#furniture-plan', '#ceiling-plan', '#lighting-plan', '#coverings-plan', '#walls-plan', '#select-facilities'], (id) ->

    $(id).parent().children('.chk-area').on 'click', (e) ->

      if $(id).prop('checked')
        $(id).prop('checked', false)
      else
        $(id).prop('checked', true)

      Calculator.getOrderAmount()

  Calculator.unCheck($('#discount-code'))
  Calculator.getOrderAmount() unless $('section.calculator').length == 0

