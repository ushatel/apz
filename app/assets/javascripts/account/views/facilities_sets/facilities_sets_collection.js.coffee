class App.Views.FacilitiesSetsCollection extends App.Views.SharedCollection

  template: HoganTemplates['account/facilities_sets/collection']

  id: "facilities_sets"

  tagName: "div"

  className: ""

  getItemView: (item) ->
    new App.Views.FacilitiesSetCollectionItem({
      model: item
      collection: @collection
    })