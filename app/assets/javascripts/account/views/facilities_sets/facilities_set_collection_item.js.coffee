class App.Views.FacilitiesSetCollectionItem extends App.Views.SharedRecord

  tagName: "div"

  className: "open-close"

  template: JST['account/facilities_sets/collection_item']

  events: {
    "click a.facilities-set": "gotoSet"
  }

  initialize: (params) ->

    super(params)

  render: (params) ->
    super(params)

    @

  renderTemplate: () ->
    @$el.html(@template({
      record: @model
      collection: @collection
    }))

  gotoSet: (e) ->
    e.preventDefault()
    window.router.navigate("#{@model.getLink()}/edit", true)