class App.Views.AccountProfilePage extends App.View

  template: JST['account/account/profile_page']

  className: "user-profile"

  events:
    'click .js-close': 'cancelButtonClicked'

  initialize: (params) ->
    _.extend(@, params)

    @fetched = false

    @model = new App.Models.User(id: App.CurrentUser.getId())

    $.when(@model.fetch()).done =>
      @fetched = true

      @createControls()
      @render()

    super

  render: ->
    if @fetched
      @renderTemplate()

      @renderControls()

    else
      @showLoadingView()

    @

  renderTemplate: ->
    @$el.html(@template({
      record: @model
    }))

  createControls: ->
    @currentUserFormControl = new App.Views.CurrentUserFormControl(model: @model)

  renderControls: ->
    @$('#profile-page').append @currentUserFormControl.render().el

  cancelButtonClicked: (e) ->
    e.preventDefault()

    Backbone.history.history.back()