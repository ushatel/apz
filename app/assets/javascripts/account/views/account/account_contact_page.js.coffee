class App.Views.AccountContactPage extends App.View

  template: JST['account/account/contact_page']

  events:
    'click .js-close': 'cancelButtonClicked'

  className: "contact-page"

  initialize: (params) ->
    _.extend(@, params)

    @model = new App.Models.Feedback()

    @createControls()

    super

  render: ->
    @renderTemplate()

    @renderControls()

    @

  renderTemplate: ->
    @$el.html(@template({
      record: @model
    }))

  createControls: ->
    @feedbackFormControl = new App.Views.FeedbackFormControl(model: @model)

  renderControls: ->
    @$('#contact-page').append @feedbackFormControl.render().el

  cancelButtonClicked: (e) ->
    e.preventDefault()

    Backbone.history.history.back()