#class App.Views.ProjectCollectionItem extends App.Views.SharedRecord
#
#  tagName: "div"
#
#  className: "open-close"
#
#  template: JST['account/projects/collection_item']
#
#  events: {
#    "click .add-task": "addTaskClick"
#    "click .js-edit-link": "editVisualizationClick"
#    "click .js-view-link": "showVisualization"
#  }
#
#  initialize: (params) ->
#
#    @selectedTaskStatus = if _.isObject(@collection.filters) then parseInt(@collection.filters.visualizationsStatus) else -1
#    @selectedTaskStatus = -1 if _.isNaN(@selectedTaskStatus)
#
#    @selectedTaskPeriod = if _.isObject(@collection.filters) then parseInt(@collection.filters.visualizationsMonth) else -1
#    @selectedTaskPeriod = -1 if _.isNaN(@selectedTaskStatus)
#
#    super(params)
#
#  render: (params) ->
#    super(params)
#
#    @$el.openClose({
#      hideOnClickOutside: true
#      activeClass: 'active'
#      opener: '.opener'
#      slider: '.slide'
#      animSpeed: 400
#      effect: 'slide'
#    });
#
#    @
#
#  renderTemplate: () ->
#    @$el.html(@template({
#      record: @model
#      collection: @collection
#      selectedTaskStatus: @selectedTaskStatus
#      selectedTaskPeriod: @selectedTaskPeriod
#    }))
#
#  addTaskClick: (e) ->
#    e.preventDefault()
#    window.router.navigate("#{@model.getLink()}/visualizations/new", true)
#
#  showVisualization: (e) ->
#    e.preventDefault()
#    window.router.navigate(e.target.pathname, true)
#
#  editVisualizationClick: (e) ->
#    e.preventDefault()
#    window.router.navigate(e.target.pathname, true)