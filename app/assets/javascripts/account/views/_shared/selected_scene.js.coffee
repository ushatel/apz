class App.Views.SelectedScene extends App.Views.SharedRecord

  template: JST['account/visualizations/selected_scene']

  tagName: "li"

  events: {
    "click a.delete": "deleteSelectedScene"
    "click a.view": "showFullScreenPreview"
    "click a.lightbox": "showFullScreenPreview"
    "mouseenter a.lightbox": "showBigImage"
    "mouseleave a.lightbox": "hideBigImage"
  }

  renderTemplate: () ->
    @$el.addClass(@model.get("slug"))

    @$el.html(@template({
      record: @model
      collectionPath: @model.constructor.getCollectionPath(@model.getCollectionParam(), @model.getAdditionalCollectionParam())
      slug: @model.get(@model.idAttribute)
    }))

  deleteSelectedScene: (e) ->
    e.preventDefault()

    @model.destroy({
      success: () =>
        vent.trigger("removeScene", @model)
        @trigger("removeScene", @model)
    })

  showFullScreenPreview: (e) ->
    e.preventDefault()

    scene = {}
    scene.id = @model.get("scene_id")
    scene.name = @model.get("scene_name")
    scene.avatar = @model.get("scene_avatar")
    scene.group ?= {}
    scene.group.name ?= @model.get("scene_group_name")

    new App.Views.SceneFancyboxView({model: scene}).show()

  updateModel: () ->
    attributes = Backbone.Syphon.serialize(@)
    @model.save(attributes)

  showBigImage: () =>
    @$(".big-image-wrapper").css(top: -(@$(".big-image-wrapper").height()/2 + 3))
    @$(".big-image-wrapper").fadeIn(1)

  hideBigImage: () =>
    @$(".big-image-wrapper").fadeOut(1)