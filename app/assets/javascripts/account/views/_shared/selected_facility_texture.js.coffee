class App.Views.SelectedFacilityTexture extends App.View

  template: JST['account/visualizations/selected_facility_texture']

  tagName: "li"

  events: {
    "click a.delete": "deleteSelectedFacility"
    "click a.view": "showFullScreenPreview"
  }

  render: () ->
    @$el.html(@template({record: @model}))

    @

  deleteSelectedFacility: (e) =>
    e.preventDefault()
    @trigger("selectedFacilityTextureDestroyed", @model)
    @remove()

  showFullScreenPreview: (e) ->
    e.preventDefault()
    new App.Views.FancyboxView({model: @model}).show()
