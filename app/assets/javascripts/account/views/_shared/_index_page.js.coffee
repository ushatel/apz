class App.Views.SharedIndexPage extends App.Views.MainLayout

  template: HoganTemplates['account/shared/index_page']

  id: "index-page"

  events: {
    "click #add-item": "addItem"
  }

  initialize: (params) ->
    _.bindAll(@)
    @collection.on('reset', @render, @)
    @collection.on('change', @render, @)
    @collection.fetch({cache: true})

  render: ->
    @renderTemplate()

    @$("#records").html(@getItemsView().render().el)
    @$("#pagination").html(@getPaginationView().render().el)
    @$("#tabs").html(@getTabsView().render().el)

    @

  renderTemplate: () ->
    @$el.html(@template.render({
      collectionPath: @collection.getPath()
      pluralModelString: @options.pluralModelString
      addModelString: @options.addModelString
      count: @collection.getCount
    }, {}))
    super()

  getTabsView: () ->
    new App.View

  getPaginationView: () ->
    new App.Views.PaginatedView({collection: @collection})

  getItemsView: () ->
    new @collectionView({collection: @collection})

  addItem: (e)->
    e.preventDefault()
    window.router.navigate("#{@collection.getPath()}/new", true)
