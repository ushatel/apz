class App.Views.SharedRecord extends App.View

  template: HoganTemplates['account/shared/record']
  tagName: 'tr'

  events: ->
    'click .js-show-link' : "showRecord"
    'click .js-edit-link' : "editRecord"
    'click .js-delete-link' : "deleteRecord"

  initialize: (params) ->
    _.extend(@, params)

    @model.on("change", @render, @)

    super(params)

  render: () ->
    @renderTemplate()

    @

  renderTemplate: () ->
    @$el.addClass(@model.get("slug"))

    @$el.html(@template.render({
      record: @model.toJSON()
      collectionPath: @model.constructor.getCollectionPath(@model.getCollectionParam(), @model.getAdditionalCollectionParam())
      slug: @model.get(@model.idAttribute)
    }, {}))

  showRecord: (e) ->
    e.preventDefault()
    window.router.navigate(@getPath(e), true)

  editRecord: (e) ->
    e.preventDefault()
    window.router.navigate(@getPath(e), true)

  deleteRecord: (e) ->
    e.preventDefault()
    window.router.navigate(@getPath(e), true)

  getPath: (e) ->
    if _.isUndefined(e.target.pathname)
      e.target.parentNode.pathname
    else
      e.target.pathname




