class App.Views.FancyboxView extends App.View

  className: "lightbox"

  template: JST['account/shared/fancybox_view']

  events: {
    "click .close": "destroyFancybox"
  }

  initialize: (params) ->
    _.extend(@, params)

    @options = {}

    super(params)

  render: ->
    @$el.html(@template({
      record: @model
      options: @options
    }))

    @

  show: () ->
    $.fancybox({
      padding: 0
      margin: 0
      content: @render().el
      cyclic: false
      autoScale: true
      overlayShow: true
      overlayOpacity: 0.65
      overlayColor: '#000000'
      titlePosition: 'inside'
    })

  destroyFancybox: (e = null) ->
    e.preventDefault() unless e == null
    $.fancybox.close()

