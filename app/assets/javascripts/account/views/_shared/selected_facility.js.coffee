class App.Views.SelectedFacility extends App.Views.SharedRecord

  template: JST['account/visualizations/selected_facility']

  tagName: "li"

  events: {
    "click a.delete": "deleteSelectedFacility"
    "click a.view": "showFullScreenPreview"
    "click a.lightbox": "showFullScreenPreview"
    "mouseenter a.lightbox": "showBigImage"
    "mouseleave a.lightbox": "hideBigImage"
  }

  renderTemplate: () ->
    @$el.addClass(@model.get("slug"))

    @$el.html(@template({
      record: @model
      collectionPath: @model.constructor.getCollectionPath(@model.getCollectionParam(), @model.getAdditionalCollectionParam())
      slug: @model.get(@model.idAttribute)
    }))

  deleteSelectedFacility: (e) ->
    e.preventDefault()

    @model.destroy({
      success: () =>
        vent.trigger("removeFacility", @model)
        @trigger("removeFacility", @model)
    })

  showFullScreenPreview: (e) ->
    e.preventDefault()

    facility = {}
    facility.id = @model.get("facility_id")
    facility.name = @model.get("facility_name")
    facility.avatar = @model.get("facility_avatar")
    facility.group ?= {}
    facility.group.name ?= @model.get("facility_group_name")

    new App.Views.FacilityFancyboxView({model: facility}).show()

  updateModel: () ->
    attributes = Backbone.Syphon.serialize(@)
    @model.save(attributes)

  showBigImage: () =>
    @$(".big-image-wrapper").css(top: -(@$(".big-image-wrapper").height()/2 + 3))
    @$(".big-image-wrapper").fadeIn(1)

  hideBigImage: () =>
    @$(".big-image-wrapper").fadeOut(1)