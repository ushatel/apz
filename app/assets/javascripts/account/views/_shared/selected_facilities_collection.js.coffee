class App.Views.SelectedFacilitiesCollection extends App.Views.SharedCollection

  template: JST['account/visualizations/selected_facilities']

  tagName: "section"
  className: "selected-item"

  initialize: (params) ->
    _.extend(@, params)

    @settings ?= false

    @itemView = if @settings then App.Views.SelectedFacilityWithSettings else App.Views.SelectedFacility
    @collection.on('add', @render, @)
    @collection.on('destroy', @render, @)

    super(params)

  renderTemplate: () ->
    if (_.size(@collection) > 0) && !_.isUndefined(@collection.filterValue)
      @$el.addClass('selected-item') unless @$el.hasClass('selected-item')
      @$el.html(@template({collection: @collection}))
    else
      @$el.html('')
      @$el.removeClass('selected-item')

  appendItem: (item) =>
    view = @getItemView(item)
    view.on "removeFacility", (removedFacility) =>
      @trigger("removeFacility", {removedFacilityModel: removedFacility, selectedFacilitiesCollection: @collection})

    @$("ul.selected-facilities").append(view.render().el)