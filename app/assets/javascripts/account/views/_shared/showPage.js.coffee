class App.Views.SharedShowPage extends App.Views.MainLayout

  template: JST['account/shared/show_page']

  initialize: (params) ->
    _.bindAll(@)
    _.extend(@, params) unless _.isUndefined(params)
    @model.on("change", @render, @)
    @options = {}
    @options.afterFetch = () ->
      null
    @model.fetch(
      success: () =>
        @options.afterFetch()
    )

  getRecordTabsView: () ->
    new App.Views.RecordTabsView({model: @model})

  render: ->
    @renderTemplate()

    @$("#tabs").html(@getRecordTabsView().render().el)

    @

  renderTemplate: ->
    @$el.html(@template({
      record: @model
    }))
