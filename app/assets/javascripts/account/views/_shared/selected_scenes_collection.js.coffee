class App.Views.SelectedScenesCollection extends App.Views.SharedCollection

  template: JST['account/visualizations/selected_scenes']

  tagName: "section"
  className: "selected-item"

  initialize: (params) ->
    _.extend(@, params)

    @settings ?= false

    @itemView = if @settings then App.Views.SelectedSceneWithSettings else App.Views.SelectedScene
    @collection.on('add', @render, @)
    @collection.on('destroy', @render, @)

    super(params)

  renderTemplate: () ->
    if (_.size(@collection) > 0) && !_.isUndefined(@collection.filterValue)
      @$el.addClass('selected-item') unless @$el.hasClass('selected-item')
      @$el.html(@template({collection: @collection}))
    else
      @$el.html('')
      @$el.removeClass('selected-item')

  appendItem: (item) =>
    view = @getItemView(item)
    view.on "removeScene", (removedScene) =>
      @trigger("removeScene", {removedSceneModel: removedScene, selectedScenesCollection: @collection})

    @$("ul.selected-scenes").append(view.render().el)