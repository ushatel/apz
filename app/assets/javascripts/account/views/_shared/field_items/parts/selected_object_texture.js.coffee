class App.Views.SelectedObjectTexture extends App.View

  template: JST['account/shared/field_items/parts/selected_object_texture']

  tagName: "li"

  events: {
    "click a.delete": "deleteSelectedFacility"
    "click a.view": "showFullScreenPreview"
  }

  render: () ->
    @$el.html(@template({
      record: @model
    }))

    @

  deleteSelectedFacility: (e) =>
    e.preventDefault()
    @trigger("selectedObjectTextureDestroyed", @model)
    @remove()

  showFullScreenPreview: (e) ->
    e.preventDefault()
    new App.Views.FancyboxView({model: @model}).show()
