class App.Views.SelectedFacilityWithSettings extends App.Views.SelectedFacility

  template: JST['account/visualizations/selected_facility_with_settings']

  events: {
    "click a.delete": "deleteSelectedFacility"
    "click a.js-settings": "toggleSettings"
    "click .js-cancel-settings": "toggleSettings"
    "click .js-save-settings": "saveSettings"
    "click a.view": "showFullScreenPreview"
    "click a.lightbox": "showFullScreenPreview"
    "mouseenter a.lightbox": "showBigImage"
    "mouseleave a.lightbox": "hideBigImage"
  }

  initialize: ->

    @fancyboxView = App.Views.FacilityFancyboxView
    @texturesFileInputName = 'selected_facility_texture[file]'
    @texturesUploadUrl = new App.Models.SelectedFacilityTexture().url()
    @selectedObjectModel = App.Models.SelectedFacilityTexture
    @selectedObjectTexturesFieldName = 'selected_facility_texture_ids'

    @opened = false

    @$fileInput = $("<input>", {type: 'file', multiple: 'multiple', name: @texturesFileInputName})
    @$fileInput.attr('data-url', @texturesUploadUrl)

    vent.on "selectedObjectSettingsShow", (firedView) =>
      @hideSettingsBox() if @opened && (firedView != @)

    super

  render: () ->
    super

    @$(".fileinput-button").append @$fileInput

    @initFileUpload()

    @setFileIds([])

    _.each @model.get("#{@selectedObjectTexturesFieldName.replace(/_ids$/, '')}s"), @appendSeletedObjectTexture

    @

  renderTemplate: () ->
    @$el.addClass(@model.get("slug"))

    @$el.html(@template({
      record: @model
      collectionPath: @model.constructor.getCollectionPath(@model.getCollectionParam(), @model.getAdditionalCollectionParam())
      slug: @model.get(@model.idAttribute)
      selectedObjectTexturesFieldName: @selectedObjectTexturesFieldName
    }))

  deleteSelectedFacility: (e) ->
    e.preventDefault()

    vent.trigger("removeFacility", @model)
    @trigger("removeFacility", @model)
    @remove()

  toggleSettings: (e) ->
    e.preventDefault()

    if @opened
      @hideSettingsBox()
    else
      @showSettingsBox()

    vent.trigger "selectedObjectSettingsShow", @

  hideSettingsBox: ->
    @$("#selected-object-#{@model.get('id')}.selected-object-settings-wrapper").slideUp()
    @opened = false

  showSettingsBox: ->
    @$("#selected-object-#{@model.get('id')}.selected-object-settings-wrapper").slideDown()
    @opened = true

  saveSettings: (e) ->
    e.preventDefault()

    data = Backbone.Syphon.serialize(@)
    delete data[@texturesFileInputName.replace(/\[file\]/, '')]
    ids = JSON.parse data[@selectedObjectTexturesFieldName]
    data[@selectedObjectTexturesFieldName] = ids

    @model.save data,
      success: =>
        @hideSettingsBox()

  initFileUpload: () ->
    @$fileInput.fileupload

      dataType: "json"
      dropZone: @$el
#      formData: {"selected_facility_texture[selected_facility_id]": @model.get("id")}

      add: (e, data) ->
        data.submit()
#        types = <%= SelectedFacilityTexture.get_file_extensions_as_regexp %>
#        file = data.files[0]
#        if types.test(file.type) or types.test(file.name)
#          data.submit()
#        else
#          alert(I18n.t("errors.messsages.not_texture_format"))

      done: (e, data, result) =>
        @addFileId(data.result.id) unless _.isUndefined(data.result)
        #        @trigger("selectedFacilityTextureUploaded", data.result)
        @appendSeletedObjectTexture(data.result)

      error: (e, data, message) => console.log "Error", e, data, message
      fail: (e, data, message) => console.log "Fail", e, data, message

  appendSeletedObjectTexture: (selectedObjectTexture) =>
    @addFileId(selectedObjectTexture.id)

    selectedObjectTextureView = new App.Views.SelectedObjectTexture({
      model: new @selectedObjectModel(selectedObjectTexture)
    })

    selectedObjectTextureView.on "selectedObjectTextureDestroyed", (data) =>
      @removeFileId(data.get("id"))

    @$("ul.textures").append selectedObjectTextureView.render().el

  getFileIds: () ->
    JSON.parse @$("input[name=#{@selectedObjectTexturesFieldName}]").val()

  setFileIds: (ids) ->
    @$("input[name=#{@selectedObjectTexturesFieldName}]").val(JSON.stringify(ids))

  addFileId: (id) ->
    ids = @getFileIds()
    ids.push(id) unless _.contains(ids, id)
    @setFileIds(ids)

  removeFileId: (id) ->
    ids = @getFileIds()
    ids = _.without(ids, id)
    @setFileIds(ids)