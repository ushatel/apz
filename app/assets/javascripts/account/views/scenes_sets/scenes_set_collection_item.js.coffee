class App.Views.ScenesSetCollectionItem extends App.Views.SharedRecord

  tagName: "div"

  className: "open-close"

  template: JST['account/scenes_sets/collection_item']

  events: {
    "click a.scenes-set": "gotoSet"
  }

  initialize: (params) ->

    super(params)

  render: (params) ->
    super(params)

    @

  renderTemplate: () ->
    @$el.html(@template({
      record: @model
      collection: @collection
    }))

  gotoSet: (e) ->
    e.preventDefault()
    window.router.navigate("#{@model.getLink()}/edit", true)