class App.Views.ScenesSetsCollection extends App.Views.SharedCollection

  template: HoganTemplates['account/scenes_sets/collection']

  id: "scenes-sets"

  tagName: "div"

  className: ""

  getItemView: (item) ->
    new App.Views.FacilitiesSetCollectionItem({
      model: item
      collection: @collection
    })