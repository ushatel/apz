class App.Views.DiscussionsAddPage extends App.View

  template: JST['account/discussions/add_page']

  events: {
    'click .js-close': 'closeButtonClicked'
  }

  initialize: (params) ->
    _.extend(@, params)

    @taskModel = new App.Models.Task({id: @taskSlug})
    @model = new App.Models.Discussion({taskSlug: @taskModel.get('id')})

    @fetched = false

    $.when(@taskModel.fetch()).done () =>
      @fetched = true
      @render()

    super(params)

  render: () ->
    super

    if @fetched
      @$el.html(@template({
        record: @model
        taskRecord: @taskModel
      }))

      @$el.addClass('delivered') if @taskModel.isDelivered() && !@$el.hasClass('delivered')

      @createControls()
      @renderControls()

    else
      @showLoadingView()

    @

  createControls: ->
    @discussionFormControl = new App.Views.DiscussionFormControl({model: @model, taskModel: @taskModel})
    @discussionFormControl.on 'modelSaved', (discussion) =>
      window.router.navigate(discussion.getLink(), true)
      $("html, body").animate({ scrollTop: 0 }, "slow")

  renderControls: () ->
    @$('#discussion-form').append @discussionFormControl.render().el

  closeButtonClicked: (e) ->
    e.preventDefault()
    window.router.navigate(@taskModel.getLink(), true)
    $("html, body").animate({ scrollTop: 0 }, "slow")
