class App.Views.DiscussionShowPage extends App.View

  template: JST['account/discussions/show_page']

  events: {
    'click .js-close': 'closeButtonClicked'
  }

  initialize: (params) ->
    _.extend(@, params)

    @taskModel = new App.Models.Task({id: @taskSlug})
    @model = new App.Models.Discussion({taskSlug: @taskModel.get('id'), slug: @id})

    @fetched = false

    $.when(@taskModel.fetch(), @model.fetch()).done () =>
      @fetched = true
      @render()

    super(params)

  render: () ->
    super

    if @fetched
      @$el.html(@template({
        record: @model
        taskRecord: @taskModel
      }))

      @$el.addClass('delivered') if @taskModel.isDelivered() && !@$el.hasClass('delivered')

      @createControls()
      @renderControls()

    else
      @showLoadingView()

    @

  createControls: () ->

    @messagesControl = new App.Views.DiscussionMessagesControl(discussionModel: @model)

    @messageFormControl = new App.Views.MessageFormControl({discussionModel: @model, taskModel: @taskModel})
    @messageFormControl.on 'modelSaved', (discussionMessage) =>
      @messagesControl.trigger 'addDiscussionMessage', discussionMessage
#      window.router.navigate(discussion.getLink(), true)
#      $("html, body").animate({ scrollTop: 0 }, "slow")



  renderControls: () ->
    @$('#messages').append @messagesControl.render().el
    @$('#message-form').append @messageFormControl.render().el

  closeButtonClicked: (e) ->
    e.preventDefault()
    window.router.navigate(@taskModel.getLink(), true)
    $("html, body").animate({ scrollTop: 0 }, "slow")
