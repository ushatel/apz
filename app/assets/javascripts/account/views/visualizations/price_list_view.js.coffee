#class App.Views.PriceListView extends App.View
#
#  className: "price-list-values"
#
#  template: JST["account/visualizations/price_list_view"]
#
#  initialize: (params)->
#    _.bindAll(this)
#
#    @selectedFacilitiesCollection = new App.Collections.SelectedFacilitiesPaginatedCollection({visualizationSlug: @model.get('id')})
#    @sketchesCollection = new App.Collections.SketchesPaginatedCollection({visualizationSlug: @model.get('id')})
#
#    @createSelectedFacilitiesCollectionView()
#    @createSketchesCollectionView()
#
#    @params = {}
#
#    @on "changeModel", @changeModel
#    @on "selectFacility", @selectFacility
#    @on "removeFacility", @removeFacility
#    @on "changeFloorAreaValue", @changeFloorAreaValue
#    @on "changeImplementationDateValue", @changeImplementationDateValue
#    @on "changeCouponNumberValue", @changeCouponNumberValue
#    @on "changeAdjustmentCountValue", @changeAdjustmentCountValue
#    @on "changeModelingTasksValue", @changeModelingTasksValue
#    @on "changeHighResolutionImageValue", @changeHighResolutionImageValue
#    @on "changeInitialSceneWithSettingsValue", @changeInitialSceneWithSettingsValue
#    @on "changeDrawingsValue", @changeDrawingsValue
#
#    @countValues()
#
#  render: ->
#    @$el.html(@template({
#      record: @model
#      adjustmentsCount: @adjustmentsCount
#      adjustmentsCountPrice: @adjustmentsCountPrice
#      modelingTasksCount: @modelingTasksCount
#      modelingTasksPrice: @modelingTasksPrice
#      highResolutionImagePrice: @highResolutionImagePrice
#      initialSceneWithSettingsPrice: @initialSceneWithSettingsPrice
#      furniturePlanPrice: @furniturePlanPrice
#      ceilingPlanPrice: @ceilingPlanPrice
#      lightingPlanPrice: @lightingPlanPrice
#      coveringsPlanPrice: @coveringsPlanPrice
#      wallsPlanPrice: @wallsPlanPrice
#      selectedFacilitiesPrice: @selectedFacilitiesPrice
#      drawingsPrice: @drawingsPrice
#      orderAmount: @orderAmount
#    }))
#
#    @$("#selected-facilities-wrapper").html(@selectedFacilitiesCollectionView.render().el)
#
#    if (@model.step == 'work-on-task') && (gon.current_user_role == 'manager')
#      @$("#sketches-wrapper").html(@sketchesCollectionView.render().el)
#
#
#    setTimeout () ->
#      $('ul.sketches').slideAccordion({
#        opener: 'a.opener',
#        slider: 'div.slide',
#        animSpeed: 500
#      })
#    , 1000
#
#    @
#
#  createSketchesCollectionView: () ->
#    @sketchesCollectionView = new App.Views.SketchesCollection({collection: @sketchesCollection, visualization: @model})
#    @sketchesCollectionView
#
#  createSelectedFacilitiesCollectionView: () ->
#    @selectedFacilitiesCollectionView = new App.Views.SelectedFacilitiesCollection({collection: @selectedFacilitiesCollection})
#    @selectedFacilitiesCollectionView.on "removeFacility", @removeFacility
#    @selectedFacilitiesCollectionView
#
#  countValues: () ->
#
#    @adjustmentsCount = if _.isUndefined(@params['adjustmentsCount']) then @model.get("adjustments_count") else @params['adjustmentsCount']
#    @adjustmentsCountPrice = @model.getAdjusmentCountPrice(@params)
#    @modelingTasksCount = if _.isUndefined(@params['modelingTasksCount']) then (if _.isUndefined(@model.get("modeling_tasks")) then 0 else @model.get("modeling_tasks").length) else @params['modelingTasksCount']
#    @modelingTasksPrice = @model.getModelingTasksPrice(@params)
#    @initialSceneWithSettingsPrice = @model.getInitialSceneWithSettingsPrice(@params)
#    @highResolutionImagePrice = @model.getHighResolutionImagePrice(@params)
#
#    @furniturePlanPrice = @model.getFurniturePlanPrice(@params)
#    @ceilingPlanPrice = @model.getCeilingPlanPrice(@params)
#    @lightingPlanPrice = @model.getLightingPlanPrice(@params)
#    @coveringsPlanPrice = @model.getCoveringsPlanPrice(@params)
#    @wallsPlanPrice = @model.getWallsPlanPrice(@params)
#
#    @selectedFacilitiesPrice = @model.getSelectedFacilitiesPrice(@params)
#    @drawingsPrice = @model.getDrawingsPrice(@params)
#
#    @orderAmount = @model.getOrderAmount(@params)
#
#  changeModel: (newModel) =>
#    @model = newModel
#
#    @selectedFacilitiesCollection.setVisualizationSlug(@model.get('id'))
#    @selectedFacilitiesCollection.fetch()
#
#    @sketchesCollection.setVisualizationSlug(@model.get('id'))
#    @sketchesCollection.fetch()
#
#    @countValues()
#    @render()
#
#  changeFloorAreaValue: (newValue) =>
#    @params['floorArea'] = newValue
#    @countValues()
#    @render()
#
#  changeImplementationDateValue: (newValue) =>
#    @params['implementationDate'] = newValue
#    @countValues()
#    @render()
#
#  changeCouponNumberValue: (newValue) =>
#    @params['discount'] = newValue
#    @countValues()
#    @render()
#
#  changeAdjustmentCountValue: (newValue) =>
#    @params['adjustmentsCount'] = newValue
#    @countValues()
#    @render()
#
#  changeModelingTasksValue: (newValue) =>
#    @params['modelingTasksCount'] = newValue
#    @countValues()
#    @render()
#
#  changeHighResolutionImageValue: (newValue) =>
#    @params['highResolutionImage'] = newValue
#    @countValues()
#    @render()
#
#  changeInitialSceneWithSettingsValue: (newValue) =>
#    @params['initialSceneWithSettings'] = newValue
#    @countValues()
#    @render()
#
#  changeDrawingsValue: (newValue) =>
#    @params['drawings'] = newValue
#    @countValues()
#    @render()
#
#  removeFacility: (params) =>
#    @trigger("removeSelectedFacility", params)
#    unless _.isUndefined(params['selectedFacilitiesCollection'])
#
#      @params['selectedFacilitiesCount'] = params['selectedFacilitiesCollection'].length
#      @countValues()
#      @render()
#
#  selectFacility: (selectedFacility) =>
#
#    alreadySelected = false
#    @selectedFacilitiesCollection.each (eachSelectedFacility) =>
#      alreadySelected = true if eachSelectedFacility.get('facility_id') == selectedFacility.get('id')
#
#    unless alreadySelected
#      newSelectedFacilityModel = new App.Models.SelectedFacility()
#      newSelectedFacilityModel.save({
#        visualization_id: @model.get("id")
#        facility_id: selectedFacility.get("id")
#      }, {
#        wait: true
#        success: (model) =>
#          @selectedFacilitiesCollection.add(newSelectedFacilityModel)
#          @params['selectedFacilitiesCount'] = @selectedFacilitiesCollection.length
#          @countValues()
#          @render()
#
#        error: (model, response) =>
#
#      })