class App.Views.ColorsSelectedFiltersView extends App.Views.SelectedFiltersView

  appendCheckedValues: () ->
    _.each @checkedValues, (checkedValue) =>
      wrapper = $("<div>", {class: 'selected-color-wrapper'})

      selectedColor = $('<span>', {class: 'selected-color'})
      selectedColor.css('background', checkedValue.name)

      selectedColor.on "click", () =>
        @removeFilter(checkedValue)

      cross = $('<span>', {class: 'selected-color-cross'})

      wrapper.on "click", () =>
        @removeFilter(checkedValue)

      wrapper.append(selectedColor)
      wrapper.append(cross)



      @$el.append(wrapper)