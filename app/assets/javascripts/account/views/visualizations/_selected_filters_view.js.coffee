class App.Views.SelectedFiltersView extends App.View

  template: JST["account/visualizations/selected_filters_view"]

  initialize: (params)->
    _.extend(@, params)

    @checkedValues ||= []
    super(params)

  render: ->
    if @checkedValues.length > 0
      @$el.show()
      @$el.html(@template({label: @label}))
      @$el.prop("class", @modelName)

      @appendCheckedValues()
    else
      @$el.hide()
    @

  appendCheckedValues: () ->
    _.each @checkedValues, (checkedValue) =>
      link = $("<a>", {href: "#", text: checkedValue.name})

      link.on "click", () =>
        @removeFilter(checkedValue)

      @$el.append(link)

  setCheckedValues: (newCheckedValues) ->
    @checkedValues = newCheckedValues
    @render()

  removeFilter: (checkedValue) =>
    @checkedValues = _.without(@checkedValues, checkedValue)
    @trigger("removeFilter", checkedValue)
    @render()