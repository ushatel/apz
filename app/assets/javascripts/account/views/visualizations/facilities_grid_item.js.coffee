class App.Views.FacilitiesGridItem extends App.Views.SharedRecord

  template: HoganTemplates['account/visualizations/facilities_grid_item']

  tagName: "li"

  events: {
    "click .zoom": "showFullScreenPreview"
    "click .lightbox": "showFullScreenPreview"
    "click .cart": "selectFacility"
  }

  initialize: (params = {}) ->
    _.extend(@, params)
    super(params)

    vent.on "removeFacility", @removeFacility

  render: () ->
    super

    selected = _.find @selectedFacilitiesCollection.toArray(), (selectedFacility) => selectedFacility.get('facility_id') == @model.get('id') unless _.isUndefined(selectedFacility)
    @$(".cart").addClass("selected") unless _.isUndefined(selected)

    @

  showFullScreenPreview: (e) ->
    e.preventDefault()
    new App.Views.FacilityFancyboxView({model: @model}).show()

  selectFacility: (e) ->
    e.preventDefault()
    unless @$(".cart").hasClass('selected')
      @$(".cart").addClass("selected")
      @trigger("selectFacility", @model)

  removeFacility: (selectedFacility) =>
    @$(".cart").removeClass('selected') if selectedFacility.get('facility_id') == @model.get('id')