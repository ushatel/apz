class App.Views.FacilitiesGridCollection extends App.Views.SharedCollection

  template: HoganTemplates['account/visualizations/facilities_grid']

  tagName: "ul"
  className: "product-list"

  initialize: (params) ->
    _.bindAll(@)
    _.extend(@, params)
    @itemView = App.Views.FacilitiesGridItem

#    $(window).scroll () =>
#      if (@$("a.more-link").length > 0) && ($(window).scrollTop() >= $(document).height() - $(window).height() - 2000) && (/select-facilities$/.test(location.pathname))
#        @showMore()

    super(params)

  render: ->
    super

    @addMoreLink() if @collection.length >= 32

    @

  addMoreLink: () ->
    @moreLink = $("<a>", {href: "#", class: "more-link", text: I18n.t("words.load_more")})
    @moreLink.on "click", (e) =>
      e.preventDefault()
      @showMore()

    @$el.append(@moreLink)

  getItemView: (item) ->
    view = new @itemView({model: item, selectedFacilitiesCollection: @selectedFacilitiesCollection})
    view.on("selectFacility", @selectFacility, @)
    view.on("removeFacility", @removeFacility, @)
    view

  selectFacility: (selectedFacility) ->
    @trigger("selectFacility", selectedFacility)

  removeFacility: (removedFacility) ->
    @trigger("removeFacility", removedFacility)

  showMore: () ->
    @$("a.more-link").remove()
    @collection.currentPage = @collection.currentPage + 1
    @collection.fetch({
      update:true
      success: (models) =>
        models.each(@appendItem)
        @addMoreLink() if models.length > 1
    })