class App.Views.ScenesGridItem extends App.Views.SharedRecord

  template: HoganTemplates['account/visualizations/scenes_grid_item']

  tagName: "li"

  events: {
    "click .zoom": "showFullScreenPreview"
    "click .lightbox": "showFullScreenPreview"
    "click .cart": "selectScene"
  }

  initialize: (params = {}) ->
    _.extend(@, params)
    super(params)

    vent.on "removeScene", @removeScene

  render: () ->
    super

    selected = _.find @selectedScenesCollection.toArray(), (selectedScene) => selectedScene.get('scene_id') == @model.get('id') unless _.isUndefined(selectedScene)
    @$(".cart").addClass("selected") unless _.isUndefined(selected)

    @

  showFullScreenPreview: (e) ->
    e.preventDefault()
    new App.Views.SceneFancyboxView({model: @model}).show()

  selectScene: (e) ->
    e.preventDefault()
    unless @$(".cart").hasClass('selected')
      @$(".cart").addClass("selected")
      @trigger("selectScene", @model)

  removeScene: (selectedScene) =>
    @$(".cart").removeClass('selected') if selectedScene.get('scene_id') == @model.get('id')