#class App.Views.SpecificationView extends App.View
#
#  tagName: "section"
#  className: "specification"
#
#  template: JST["account/visualizations/specification_view"]
#
#  initialize: (params)->
#    _.bindAll(this)
#
#    @on "changeModel", @changeModel
#
#    @reloadValues()
#
#    @on "changeFloorAreaValue", (newFloorAreaValue) =>
#      @floorArea = newFloorAreaValue
#      @discount = @model.getDiscountInPercent({floorArea: @floorArea})
#      @render()
#
#    @on "changeImplementationDateValue", (newValue) =>
#      @implementationDate = newValue
#      @render()
#
#    @on "changeCouponNumberValue", (newValue) =>
#      @couponDiscount = newValue
#      @render()
#
#
#
#  render: ->
#    @$el.html(@template({
#      record: @model
#      floorArea: @floorArea
#      discount: @discount
#      implementationDate: @implementationDate
#      couponDiscount: @couponDiscount
#      taskStatus: @taskStatus
#    }))
#    @
#
#  reloadValues: () ->
#    @floorArea = @model.get("floor_area")
#    @discount = @model.getDiscountInPercent({floorArea: @floorArea})
#    @implementationDate = @model.get("implementation_date")
#    @couponDiscount = @model.get("discount").percent if !_.isUndefined(@model.get("discount")) && !_.isNull(@model.get("discount"))
#    @taskStatus = gon.taskStatuses[@model.get("status")]
#
#  changeModel: (newModel) =>
#    @model = newModel
#
#    @reloadValues()
#    @render()