class App.Views.ScenesGridCollection extends App.Views.SharedCollection

  template: HoganTemplates['account/visualizations/scenes_grid']

  tagName: "ul"
  className: "product-list"

  initialize: (params) ->
    _.bindAll(@)
    _.extend(@, params)
    @itemView = App.Views.ScenesGridItem

#    $(window).scroll () =>
#      if (@$("a.more-link").length > 0) && ($(window).scrollTop() >= $(document).height() - $(window).height() - 2000) && (/select-scenes$/.test(location.pathname))
#        @showMore()

    super(params)

  render: ->
    super

    @addMoreLink() if @collection.length >= 32

    @

  addMoreLink: () ->
    @moreLink = $("<a>", {href: "#", class: "more-link", text: I18n.t("words.load_more")})
    @moreLink.on "click", (e) =>
      e.preventDefault()
      @showMore()

    @$el.append(@moreLink)

  getItemView: (item) ->
    view = new @itemView({model: item, selectedScenesCollection: @selectedScenesCollection})
    view.on("selectScene", @selectScene, @)
    view.on("removeScene", @removeScene, @)
    view

  selectScene: (selectedScene) ->
    @trigger("selectScene", selectedScene)

  removeScene: (removedScene) ->
    @trigger("removeScene", removedScene)

  showMore: () ->
    @$("a.more-link").remove()
    @collection.currentPage = @collection.currentPage + 1
    @collection.fetch({
      update:true
      success: (models) =>
        models.each(@appendItem)
        @addMoreLink() if models.length > 1
    })