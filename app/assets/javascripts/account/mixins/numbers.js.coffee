class App.Mixins.Numbers

  @getNum: (num) ->
    res = num
    res = num.replace(",", ".") if _.isString(num)
    if _.isBoolean(num)
      res = if num then 1 else 0
    res = num.length if _.isArray(num)
    res = 0 if num == ''

    if (_.isUndefined(res) || _.isNull(res) || _.isNaN(res)) then 0 else parseFloat(res)