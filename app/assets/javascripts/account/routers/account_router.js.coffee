class App.Routers.Account extends Support.SwappingRouter

  routes: {

    'account': "accountIndex"
    'account/contact': "accountContact"
    'account/profile': "accountProfile"
#    'account/edit': "accountEdit"
#    'account/form': "accountForm"

    'account/projects/:project/visualizations/new': "visualizationsAdd"
  }



  initialize: ->
    @el = $("#main")

    @addDefaultRoutes("tasks", "account", {edit: false, delete: false, blockable: false, importable: false, selectFacilities: true, selectScenes: true})
    @addChildRoutes("tasks", "discussions", "account", {edit: false, delete: false})
    @addDefaultRoutes("facilities-sets", "account", {show: false, delete: false, blockable: false, importable: false})
    @addDefaultRoutes("scenes-sets", "account", {show: false, delete: false, blockable: false, importable: false})

#    'account/facilities-sets': "facilitiesSetsIndex"
#    'account/facilities-sets/new': "facilitiesSetsAdd"
#    'account/facilities-sets/:id/edit': "facilitiesSetsEdit"
#

#    @addChildRoutes("projects", "visualizations", "account", {index: false, show: false, edit: false, delete: false})

  before: (route, params) ->
    if (_.size(params) > 0) || (/tasks/.test(route)) || (/contact/.test(route)) || (/profile/.test(route))
      @removeModelClasses()

      if /tasks/.test(route)
        @addModelClasses('tasks')
        @addModelClasses('single-task') unless location.pathname == '/account/tasks'
      else if /facilities-sets/.test(route)
        @addModelClasses('facilities-sets')
      else if /scenes-sets/.test(route)
        @addModelClasses('scenes-sets')
      else if /contact/.test(route)
        @addModelClasses('contact-page')
      else if /profile/.test(route)
        @addModelClasses('profile-page')

      $("#header").html(new App.Views.HeaderView().render().el)

  addModelClasses: (className) ->
    $('body').addClass(className) unless $('body').hasClass(className)
    @el.addClass(className) unless @el.hasClass(className)

  removeModelClasses: ->
    _.each ['contact-page', 'profile-page', 'single-task', 'tasks', 'facilities-sets', 'scenes-sets'], (className) =>
      $('body').removeClass(className)
      @el.removeClass(className)

  # Account

  accountIndex: () ->
    if App.CurrentUser.isClient() || App.CurrentUser.isAdmin()
      window.router.navigate('account/tasks', true)

    else
      window.router.navigate('account/facilities-sets/new', true)

  accountContact: () ->
    view = new App.Views.AccountContactPage()
    this.swap(view)

  accountProfile: () ->
    view = new App.Views.AccountProfilePage()
    this.swap(view)
#
#  accountEdit: () ->
#    view = new App.Views.AccountEditPage()
#    this.swap(view)

#  # Projects
#
#  projectsIndex: (page, field, order) ->
#    if page == "1" && _.isUndefined(field) && _.isUndefined(order)
#      window.router.navigate(App.Models.Project.getCollectionPath(), true)
#      return
#    page = parseInt(page)
#    page = 1 if isNaN(page)
#    field = "created_at" if _.isUndefined(field)
#    order = "desc" if _.isUndefined(order)
#
#    view = new App.Views.ProjectsIndexPage({page: page, field: field, order: order})
#    this.swap(view)
#    $("#header").html(new App.Views.HeaderView().render().el)
#
#  projectsShow: (id) ->
#    view = new App.Views.ProjectShowPage({id: id})
#    this.swap(view)
#    $("#header").html(new App.Views.HeaderView().render().el)
#
#  projectsAdd: () ->
#    window.router.navigate("#{App.Models.Visualization.getCollectionPath()}/new", true)
#
#  projectsEdit: (id) ->
#    view = new App.Views.ProjectEditPage({id: id})
#    this.swap(view)
#
#  projectsDelete: (id) ->
#    view = new App.Views.ProjectDeletePage({id: id})
#    this.swap(view)

#  # Visualizations
#
#  visualizationsIndex: (page, field, order) ->
#    if page == '1' && _.isUndefined(field) && _.isUndefined(order)
#      window.router.navigate(App.Models.Visualization.getCollectionPath(), true)
#      return
#    page = parseInt(page)
#    page = 1 if isNaN(page)
#    field = 'created_at' if _.isUndefined(field)
#    order = 'desc' if _.isUndefined(order)
#
#    view = new App.Views.VisualizationsIndexPage({page: page, field: field, order: order})
#    this.swap(view)
#    $("#header").html(new App.Views.HeaderView().render().el)
#
#  visualizationsShow: (id) ->
#    view = new App.Views.VisualizationShowPage({id: id})
#    this.swap(view)
#    $("#header").html(new App.Views.HeaderView().render().el)
#
#  visualizationsAdd: (projectSlug) ->
#    view = new App.Views.VisualizationAddPage({projectSlug: projectSlug})
#    this.swap(view)
#    $("#header").html(new App.Views.HeaderView().render().el)
#
#  visualizationsEdit: (id) ->
#    view = new App.Views.VisualizationEditPage({id: id})
#    this.swap(view)
#    $("#header").html(new App.Views.HeaderView().render().el)
#
#  visualizationsDelete: (id) ->
#    view = new App.Views.VisualizationDeletePage({id: id})
#    this.swap(view)
#    $("#header").html(new App.Views.HeaderView().render().el)
#
#  visualizationsAddDrawings: (id) ->
#    view = new App.Views.VisualizationAddDrawingsPage({id: id})
#    this.swap(view)
#    $("#header").html(new App.Views.HeaderView().render().el)
#
#  visualizationsSelectFacilitiesPage: (id) ->
#    view = new App.Views.VisualizationSelectFacilitiesPage({id: id})
#    this.swap(view)
#    $("#header").html(new App.Views.HeaderView().render().el)
#
#  visualizationsWorkOnTaskPage: (id) ->
#    view = new App.Views.VisualizationWorkOnTaskPage({id: id})
#    this.swap(view)
#    $("#header").html(new App.Views.HeaderView().render().el)
#
#  visualizationsGetLinkPage: (id) ->
#    view = new App.Views.VisualizationGetLinkPage({id: id})
#    this.swap(view)
#    $("#header").html(new App.Views.HeaderView().render().el)
#
#  visualizationsZipPage: (id) ->
#    document.location.href = Routes.zip_api_visualization_path(id)

  # FacilitiesSets

  facilitiesSetsIndex: (page, field, order) ->
    if page == "1" && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.FacilitiesSet.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.FacilitiesSetsIndexPage({page: page, field: field, order: order})
    this.swap(view)

  facilitiesSetsAdd: () ->
    facilitiesSet = new App.Models.FacilitiesSet()
    facilitiesSet.save({}, {
      success: (data) ->
        window.router.navigate("#{facilitiesSet.getLink()}/edit", true)
    })

  facilitiesSetsEdit: (id) ->
    view = new App.Views.FacilitiesSetEditPage({id: id})
    this.swap(view)

  # ScenesSets

  scenesSetsIndex: (page, field, order) ->
    if page == "1" && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.ScenesSet.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.ScenesSetsIndexPage({page: page, field: field, order: order})
    this.swap(view)

  scenesSetsAdd: () ->
    scenesSet = new App.Models.ScenesSet()
    scenesSet.save({}, {
      success: (data) ->
        window.router.navigate("#{scenesSet.getLink()}/edit", true)
    })

  scenesSetsEdit: (id) ->
    view = new App.Views.ScenesSetEditPage({id: id})
    this.swap(view)

  # Tasks

  tasksIndex: (page, field, order) ->
    if page == "1" && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Task.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.TasksIndexPage({page: page, field: field, order: order})
    this.swap(view)

  tasksAdd: () ->
#    view = new App.Views.TaskAddPage()
#    this.swap(view)
#    $("#header").html(new App.Views.HeaderView().render().el)

    task = new App.Models.Task()
    task.save({}, {
      success: (data) ->
        window.router.navigate("#{task.getLink()}", true)
    })

  tasksShow: (id) ->
    view = new App.Views.TaskShowPage({id: id})
    this.swap(view)

  tasksSelectFacilities: (id) ->
    view = new App.Views.TaskSelectFacilitiesPage({id: id})
    this.swap(view)

  tasksSelectScenes: (id) ->
    view = new App.Views.TaskSelectScenesPage({id: id})
    this.swap(view)

  # Discussions

  discussionsIndex: (taskSlug) ->
    window.router.navigate("#{new App.Models.Task({slug: taskSlug}).getLink()}/discussions/new", true)

  discussionsAdd: (taskSlug) ->
    view = new App.Views.DiscussionsAddPage({taskSlug: taskSlug})
    this.swap(view)

  discussionsShow: (taskSlug, id) ->
    view = new App.Views.DiscussionShowPage({taskSlug: taskSlug, id: id})
    this.swap(view)
