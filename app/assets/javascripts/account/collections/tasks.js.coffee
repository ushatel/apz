class App.Collections.TasksPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Task

  paginator_core: {
    dataType: 'json'
    url: '/api/tasks'
  }

  getPath: () ->
    "/account/tasks"