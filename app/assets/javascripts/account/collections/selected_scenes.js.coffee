class App.Collections.SelectedScenesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.SelectedScene

  paginator_core: {
    dataType: 'json'
    url: '/api/selected_scenes'
  }

  getPath: () ->
    "/account/selected_scenes"

  initialize: (params) ->
    if params.scenesSetSlug
      @filterField = "scenes_set_slug"
      @filterValue = params.scenesSetSlug
    else if params.taskSlug
      @filterField = "task_slug"
      @filterValue = params.taskSlug

    super(params)

  setScenesSetSlug: (newScenesSetSlug) ->
    @filterValue = newScenesSetSlug

  setTaskSlug: (newTaskSlug) ->
    @filterValue = newTaskSlug

  empty: (params = {}) ->
    params = _.defaults(params, {
      success: () ->
    })
    data = {}
    data['filter_by'] = @filterField
    data['filter_value'] = @filterValue
    $.ajax({
      url: "#{new App.Models.SelectedScene().urlRoot}/empty"
      type: 'post'
      data: data
      success: params?.success
    })