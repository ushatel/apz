class App.Collections.DiscussionMessagesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.DiscussionMessage

  paginator_core: {
    dataType: 'json'
    url: Routes.api_discussion_messages_path()
  }