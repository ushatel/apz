class App.Collections.LinksPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Link

  paginator_core: {
    dataType: 'json'
    url: () ->
      "/api/menus/#{@menuSlug}/links"
  }

  getPath: () ->
    "/account/menus/#{@menuSlug}/links"
