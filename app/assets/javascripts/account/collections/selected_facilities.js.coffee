class App.Collections.SelectedFacilitiesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.SelectedFacility

  paginator_core: {
    dataType: 'json'
    url: '/api/selected_facilities'
  }

  getPath: () ->
    "/account/selected_facilities"

  initialize: (params) ->
    if params.visualizationSlug
      @filterField = "visualization_slug"
      @filterValue = params.visualizationSlug
    else if params.facilitiesSetSlug
      @filterField = "facilities_set_slug"
      @filterValue = params.facilitiesSetSlug
    else if params.taskSlug
      @filterField = "task_slug"
      @filterValue = params.taskSlug

    super(params)

  setVisualizationSlug: (newVisualizationSlug) ->
    @filterValue = newVisualizationSlug

  setFacilitiesSetSlug: (newFacilitiesSetSlug) ->
    @filterValue = newFacilitiesSetSlug

  setTaskSlug: (newTaskSlug) ->
    @filterValue = newTaskSlug

  empty: (params = {}) ->
    params = _.defaults(params, {
      success: () ->
    })
    data = {}
    data['filter_by'] = @filterField
    data['filter_value'] = @filterValue
    $.ajax({
      url: "#{new App.Models.SelectedFacility().urlRoot}/empty"
      type: 'post'
      data: data
      success: params?.success
    })