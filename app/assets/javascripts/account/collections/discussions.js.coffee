class App.Collections.DiscussionsPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Discussion

  paginator_core: {
    dataType: 'json'
    url: -> Routes.api_task_discussions_path(@taskSlug)
  }

  getPath: -> "/account/tasks/#{@taskSlug}/discussions"

