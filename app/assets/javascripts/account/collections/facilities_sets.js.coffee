class App.Collections.FacilitiesSetsPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.FacilitiesSet

  paginator_core: {
    dataType: 'json'
    url: '/api/facilities-sets'
  }

  getPath: () ->
    "/account/facilities-sets"