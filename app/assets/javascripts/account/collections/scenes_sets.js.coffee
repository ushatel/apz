class App.Collections.ScenesSetsPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.ScenesSet

  paginator_core: {
    dataType: 'json'
    url: '/api/scenes-sets'
  }

  getPath: () ->
    "/account/scenes-sets"