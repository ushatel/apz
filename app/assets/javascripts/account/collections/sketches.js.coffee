class App.Collections.SketchesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Sketch

  paginator_core: {
    dataType: 'json'
    url: '/api/sketches'
  }

  getPath: () ->
    "/account/sketches"

  initialize: (params) ->
    @filterField = "visualization_slug"
    @filterValue = params.visualizationSlug

    super(params)

  setVisualizationSlug: (newVisualizationSlug) ->
    @filterValue = newVisualizationSlug
