class App.Collections.CategoriesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Category

  paginator_core: {
    dataType: 'json'
    url: () ->
      "/api/sections/#{@sectionSlug}/categories"
  }

  getPath: () ->
    "/account/sections/#{@sectionSlug}/categories"
