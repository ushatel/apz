class App.Collections.ScenesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Facility

  paginator_core: {
    dataType: 'json'
    url: '/api/scenes'
  }

  getPath: () ->
    "/account/scenes"