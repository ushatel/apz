class App.Models.Status extends Backbone.Model

  initialize: ->
    super
    @name = @getLabel()

  get: (attribute) ->
    if attribute == 'name'
      @getLabel()
    else if attribute == 'slug'
      @get('id')
    else
      super

  getLabel: ->
    I18n.t("statuses.#{@get('id')}")

class App.Collections.StatusesCollection extends Backbone.Collection

  model: App.Models.Status

  initialize: () ->
    @add new App.Models.Status({id: 'definition'})
    @add new App.Models.Status({id: 'checking'})
    @add new App.Models.Status({id: 'implementation'})
    @add new App.Models.Status({id: 'performed'})

  fetch: ->
    ## abstract