class App.Collections.CommentsPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Comment

  paginator_core: {
    dataType: 'json'
    url: '/api/comments'
  }

  getPath: () ->
    "/account/comments"