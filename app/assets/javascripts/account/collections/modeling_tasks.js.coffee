class App.Collections.ModelingTasksPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.ModelingTask

  paginator_core: {
    dataType: 'json'
    url: '/api/modeling_tasks'
  }

  getPath: () ->
    "/account/modeling_tasks"

  initialize: (params) ->
    _.extend(@, params)

    @filterField = "visualization_id"
    @filterValue = params.visualization_id

    super(params)