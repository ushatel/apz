class App.Collections.VisualizationsPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Visualization

  paginator_core: {
    dataType: 'json'
    url: '/api/visualizations'
  }

  getPath: () ->
    "/account/visualizations"

  initialize: (params) ->
    @filterField = "user_slug"
    @filterValue = gon.current_user

    super(params)