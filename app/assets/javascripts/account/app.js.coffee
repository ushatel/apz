I18n.locale = "ru"

window.App =
  Models: {}
  Mixins: {}
  Collections: {}
  Views: {}
  Routers: {}
  Helpers: {}
  initialize: ->
    $("body").addClass(BrowserDetect.browser)

    if /^\/account.{0,}/.test(location.pathname)
      window.router = new App.Routers.Account()
      Backbone.history.start({pushState: true})


window.vent = _.extend({}, Backbone.Events);

$(document).ready ->

  $(document).ajaxStart ->
    $("#ajax-loader").animate({top: "0px"}, 10)

  $(document).ajaxComplete ->
    $("#ajax-loader").animate({top: "-55px"}, 10)

    #    _.each $("textarea.ckeditor-in-form"), (textarea) ->
    #      if textarea.id != "" && !_.has(CKEDITOR.instances, textarea.id)
    #        CKEDITOR.replace(textarea.id)

  App.CurrentUser = new App.Models.CurrentUser(gon.current_user_object)
  App.initialize()


