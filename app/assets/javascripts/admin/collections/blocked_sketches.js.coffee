class App.Collections.BlockedSketchesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Sketch

  paginator_core: {
    dataType: 'json'
    url: '/api/sketches/blocked'
  }

  getPath: () ->
    "/admin/sketches"