class App.Collections.UsersPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.User

  paginator_core: {
    dataType: 'json'
    url: '/api/users'
  }

  getPath: () ->
    "/admin/users"