class App.Collections.WallsTexturesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.WallsTexture

  paginator_core: {
    dataType: 'json'
    url: '/api/walls_textures'
  }

  getPath: () ->
    "/admin/walls_textures"