class App.Collections.BlocksPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Block

  paginator_core: {
    dataType: 'json'
    url: '/api/blocks'
  }

  getPath: () ->
    "/admin/blocks"