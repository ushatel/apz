class App.Collections.PagesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Page

  paginator_core: {
    dataType: 'json'
    url: '/api/pages'
  }

  getPath: () ->
    "/admin/pages"