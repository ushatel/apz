class App.Collections.BlockedSlidesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Slide

  paginator_core: {
    dataType: 'json'
    url: '/api/slides/blocked'
  }

  getPath: () ->
    "/admin/slides"