class App.Collections.RolesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Role

  paginator_core: {
    dataType: 'json'
    url: '/api/roles'
  }

  getPath: () ->
    "/admin/roles"