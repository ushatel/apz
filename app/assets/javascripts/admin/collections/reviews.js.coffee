class App.Collections.ReviewsPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Review

  paginator_core: {
    dataType: 'json'
    url: '/api/reviews'
  }

  getPath: () ->
    "/admin/reviews"