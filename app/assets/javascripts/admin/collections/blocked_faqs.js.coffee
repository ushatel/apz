class App.Collections.BlockedFaqsPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Faq

  paginator_core: {
    dataType: 'json'
    url: '/api/faqs/blocked'
  }

  getPath: () ->
    "/admin/faqs"