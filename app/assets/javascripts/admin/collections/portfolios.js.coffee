class App.Collections.PortfoliosPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Portfolio

  paginator_core: {
    dataType: 'json'
    url: '/api/portfolios'
  }

  getPath: () ->
    "/admin/portfolios"