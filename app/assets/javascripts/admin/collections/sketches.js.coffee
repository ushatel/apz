class App.Collections.SketchesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Sketch

  paginator_core: {
    dataType: 'json'
    url: '/api/sketches'
  }

  getPath: () ->
    "/admin/sketches"