class App.Collections.DiscountsPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Discount

  paginator_core: {
    dataType: 'json'
    url: '/api/discounts'
  }

  getPath: () ->
    "/admin/discounts"