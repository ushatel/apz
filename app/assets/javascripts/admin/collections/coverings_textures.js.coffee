class App.Collections.CoveringsTexturesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.CoveringsTexture

  paginator_core: {
    dataType: 'json'
    url: '/api/coverings_textures'
  }

  getPath: () ->
    "/admin/coverings_textures"