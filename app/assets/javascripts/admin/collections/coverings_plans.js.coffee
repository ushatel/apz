class App.Collections.CoveringsPlansPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.CoveringsPlan

  paginator_core: {
    dataType: 'json'
    url: '/api/coverings_plans'
  }

  getPath: () ->
    "/admin/coverings_plans"