class App.Collections.FacilitiesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Facility

  paginator_core: {
    dataType: 'json'
    url: '/api/facilities'
  }

  getPath: () ->
    "/admin/facilities"