class App.Collections.FaqsPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Faq

  paginator_core: {
    dataType: 'json'
    url: '/api/faqs'
  }

  getPath: () ->
    "/admin/faqs"