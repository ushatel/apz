class App.Collections.ScenesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Scene

  paginator_core: {
    dataType: 'json'
    url: '/api/scenes'
  }

  getPath: () ->
    "/admin/scenes"