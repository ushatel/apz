class App.Collections.ImagesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Image

  paginator_core: {
    dataType: 'json'
    url: '/api/images'
  }

  getPath: () ->
    "/admin/images"