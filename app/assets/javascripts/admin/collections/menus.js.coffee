class App.Collections.MenusPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Menu

  paginator_core: {
    dataType: 'json'
    url: '/api/menus'
  }

  getPath: () ->
    "/admin/menus"