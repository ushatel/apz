class App.Collections.ProjectsPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Project

  paginator_core: {
    dataType: 'json'
    url: '/api/projects'
  }

  getPath: () ->
    "/admin/projects"