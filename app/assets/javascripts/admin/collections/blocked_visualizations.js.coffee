class App.Collections.BlockedVisualizationsPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Visualization

  paginator_core: {
    dataType: 'json'
    url: '/api/visualizations/blocked'
  }

  getPath: () ->
    "/admin/visualizations"