class App.Collections.PermissionsPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Permission

  paginator_core: {
    dataType: 'json'
    url: '/api/permissions'
  }

  getPath: () ->
    "/admin/permissions"