class App.Collections.PeoplePaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Person

  paginator_core: {
    dataType: 'json'
    url: '/api/people'
  }

  getPath: () ->
    "/admin/people"