class App.Collections.BlockedArticlesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Article

  paginator_core: {
    dataType: 'json'
    url: '/api/articles/blocked'
  }

  getPath: () ->
    "/admin/articles"