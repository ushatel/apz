class App.Collections.CeilingPlansPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.CeilingPlan

  paginator_core: {
    dataType: 'json'
    url: '/api/ceiling_plans'
  }

  getPath: () ->
    "/admin/ceiling_plans"