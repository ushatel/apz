class App.Collections.BlockedFacilitiesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Facility

  paginator_core: {
    dataType: 'json'
    url: '/api/facilities/blocked'
  }

  getPath: () ->
    "/admin/facilities"