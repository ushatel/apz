class App.Collections.BlockedPagesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Page

  paginator_core: {
    dataType: 'json'
    url: '/api/pages/blocked'
  }

  getPath: () ->
    "/admin/pages"