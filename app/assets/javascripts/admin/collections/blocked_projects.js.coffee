class App.Collections.BlockedProjectsPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Project

  paginator_core: {
    dataType: 'json'
    url: '/api/projects/blocked'
  }

  getPath: () ->
    "/admin/projects"