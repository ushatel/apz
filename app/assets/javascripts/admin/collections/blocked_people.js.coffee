class App.Collections.BlockedPeoplePaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Person

  paginator_core: {
    dataType: 'json'
    url: '/api/people/blocked'
  }

  getPath: () ->
    "/admin/people"