class App.Collections.LightingPlansPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.LightingPlan

  paginator_core: {
    dataType: 'json'
    url: '/api/lighting_plans'
  }

  getPath: () ->
    "/admin/lighting_plans"