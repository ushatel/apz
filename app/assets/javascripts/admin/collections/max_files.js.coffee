class App.Collections.MaxFilesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.MaxFile

  paginator_core: {
    dataType: 'json'
    url: '/api/max_files'
  }

  getPath: () ->
    "/admin/max_files"