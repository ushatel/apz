class App.Collections.FurniturePlansPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.FurniturePlan

  paginator_core: {
    dataType: 'json'
    url: '/api/furniture_plans'
  }

  getPath: () ->
    "/admin/furniture_plans"