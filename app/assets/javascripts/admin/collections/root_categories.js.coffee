class App.Collections.RootCategoriesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Category

  paginator_core: {
    dataType: 'json'
    url: () ->
      "/api/sections/#{@sectionSlug}/root-categories"
  }

  getPath: () ->
    "/admin/sections/#{@sectionSlug}/root-categories"

  initialize: (params) ->
    @disablePagination = true
    super(params)
