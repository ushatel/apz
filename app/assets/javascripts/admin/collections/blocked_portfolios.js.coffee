class App.Collections.BlockedPortfoliosPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Portfolio

  paginator_core: {
    dataType: 'json'
    url: '/api/portfolios/blocked'
  }

  getPath: () ->
    "/admin/portfolios"