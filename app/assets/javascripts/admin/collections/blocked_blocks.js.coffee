class App.Collections.BlockedBlocksPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Block

  paginator_core: {
    dataType: 'json'
    url: '/api/blocks/blocked'
  }

  getPath: () ->
    "/admin/blocks"