class App.Collections.BlockedUsersPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.User

  paginator_core: {
    dataType: 'json'
    url: '/api/users/blocked'
  }

  getPath: () ->
    "/admin/users"