class App.Collections.SlidesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Slide

  paginator_core: {
    dataType: 'json'
    url: '/api/slides'
  }

  getPath: () ->
    "/admin/slides"