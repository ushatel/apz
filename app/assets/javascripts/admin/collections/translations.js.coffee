class App.Collections.TranslationsPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Translation

  paginator_core: {
    dataType: 'json'
    url: '/api/translations'
  }

  getPath: () ->
    "/admin/translations"

  setFilter: (field, value) ->
    @reset()
    @filterField = field
    @filterValue = value
    @fetch()
