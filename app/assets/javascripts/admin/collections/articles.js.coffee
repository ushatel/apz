class App.Collections.ArticlesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Article

  paginator_core: {
    dataType: 'json'
    url: '/api/articles'
  }

  getPath: () ->
    "/admin/articles"