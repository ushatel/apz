class App.Collections.VisualizationsPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Visualization

  paginator_core: {
    dataType: 'json'
    url: '/api/visualizations'
  }

  getPath: () ->
    "/admin/visualizations"