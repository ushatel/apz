class App.Collections.SettingsPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Setting

  paginator_core: {
    dataType: 'json'
    url: '/api/settings'
  }

  getPath: () ->
    "/admin/settings"