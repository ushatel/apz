class App.Collections.WallsPlansPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.WallsPlan

  paginator_core: {
    dataType: 'json'
    url: '/api/walls_plans'
  }

  getPath: () ->
    "/admin/walls_plans"