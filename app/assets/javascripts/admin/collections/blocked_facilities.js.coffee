class App.Collections.BlockedScenesPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Scene

  paginator_core: {
    dataType: 'json'
    url: '/api/scenes/blocked'
  }

  getPath: () ->
    "/admin/scenes"