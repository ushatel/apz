class App.Collections.SectionsPaginatedCollection extends Support.SharedPaginatedCollection

  model: App.Models.Section

  paginator_core: {
    dataType: 'json'
    url: '/api/sections'
  }

  getPath: () ->
    "/admin/sections"