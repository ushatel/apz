class App.Mixins.Blockable

  block_it: () ->

    @save {blocked: true}, {
      wait: true
      success: (model) =>

      error: (model, response) =>

    }

  unblock_it: () ->

    @save {blocked: false}, {
      wait: true
      success: (model) =>

      error: (model, response) =>

    }