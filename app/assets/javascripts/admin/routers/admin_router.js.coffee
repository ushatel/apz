class App.Routers.Admin extends Support.SwappingRouter

  routes: {

    'admin': 'index'

    'admin/translations': "translationsIndex"
    'admin/translations/dump': "translationsDump"
    'admin/translations/import': "translationsImport"

    'admin/settings': "settingsIndex"
    'admin/settings/structure': "settingsStructure"
    'admin/settings/system': "settingsSystem"
    'admin/settings/messages': "settingsMessages"
    'admin/settings/visualization-labels': "settingsVisualizationLabels"
    'admin/settings/seo': "settingsSeo"
    'admin/settings/calc': "settingsCalc"
    'admin/settings/other': "settingsOther"

  }

  initialize: ->
    @el = $("#maincontent")
    models = ["roles", "permissions", ["menus", "links"], ["sections", "categories"], "portfolios", "slides", "images", "reviews", "max_files", "furniture_plans", "ceiling_plans", "lighting_plans", "coverings_plans", "coverings_textures", "walls_plans", "walls_textures", "discounts"]

    @add_routes(models, "admin")
    @addDefaultRoutes("blocks", "admin", {blockable: true})
    @addDefaultRoutes("users", "admin", {blockable: true})
    @addDefaultRoutes("pages", "admin", {blockable: true})
    @addDefaultRoutes("people", "admin", {blockable: true})
    @addDefaultRoutes("faqs", "admin", {blockable: true})
    @addDefaultRoutes("articles", "admin", {blockable: true})
    @addDefaultRoutes("facilities", "admin", {blockable: true})
    @addDefaultRoutes("scenes", "admin", {blockable: true})
    @addDefaultRoutes("projects", "admin", {blockable: true})
    @addDefaultRoutes("visualizations", "admin", {blockable: true})

  index: ()->
    view = new App.Views.MainPage()
    this.swap(view)

  # Roles

  rolesIndex: (page, field, order)->
    if page == "1" && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Role.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.RolesIndexPage({page: page, field: field, order: order})
    this.swap(view)

  rolesShow: (id) ->
    view = new App.Views.RoleShowPage({id: id})
    this.swap(view)

  rolesAdd: () ->
    view = new App.Views.RoleAddPage()
    this.swap(view)

  rolesEdit: (id) ->
    view = new App.Views.RoleEditPage({id: id})
    this.swap(view)

  rolesDelete: (id) ->
    view = new App.Views.RoleDeletePage({id: id})
    this.swap(view)

  # Permissions

  permissionsIndex: (page, field, order)->
    if page == "1" && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Permission.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.PermissionsIndexPage({page: page, field: field, order: order})
    this.swap(view)

  permissionsShow: (id) ->
    view = new App.Views.PermissionShowPage({id: id})
    this.swap(view)

  permissionsAdd: () ->
    view = new App.Views.PermissionAddPage()
    this.swap(view)

  permissionsEdit: (id) ->
    view = new App.Views.PermissionEditPage({id: id})
    this.swap(view)

  permissionsDelete: (id) ->
    view = new App.Views.PermissionDeletePage({id: id})
    this.swap(view)

  # Translations

  translationsIndex: (page, field, order)->
    if page == "1" && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Translation.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.TranslationsIndexPage({page: page, field: field, order: order})
    this.swap(view)

  translationsDump: () ->
    view = new App.Views.TranslationsDumpPage()
    this.swap(view)

  translationsImport: () ->
    view = new App.Views.TranslationsImportPage()
    this.swap(view)

  # Users

  usersIndex: (page, field, order)->
    if page == "1" && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.User.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.UsersIndexPage({page: page, field: field, order: order})
    this.swap(view)

  usersBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.UsersBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  usersShow: (id) ->
    view = new App.Views.UserShowPage({id: id})
    this.swap(view)

  usersAdd: () ->
    view = new App.Views.UserAddPage()
    this.swap(view)

  usersEdit: (id) ->
    view = new App.Views.UserEditPage({id: id})
    this.swap(view)

  usersDelete: (id) ->
    view = new App.Views.UserDeletePage({id: id})
    this.swap(view)

  # Sections

  sectionsIndex: (page, field, order)->
    if page == "1" && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Section.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.SectionsIndexPage({page: page, field: field, order: order})
    this.swap(view)

  sectionsShow: (id) ->
    view = new App.Views.SectionShowPage({id: id})
    this.swap(view)

  sectionsAdd: () ->
    view = new App.Views.SectionAddPage()
    this.swap(view)

  sectionsEdit: (id) ->
    view = new App.Views.SectionEditPage({id: id})
    this.swap(view)

  sectionsDelete: (id) ->
    view = new App.Views.SectionDeletePage({id: id})
    this.swap(view)

  # Categories

  categoriesIndex: (sectionSlug, page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.CategoriesIndexPage({sectionSlug: sectionSlug, page: page, field: field, order: order})
    this.swap(view)

  categoriesStructure: (sectionSlug)->
    view = new App.Views.CategoriesStructurePage({sectionSlug: sectionSlug})
    this.swap(view)

  categoriesShow: (sectionSlug, id) ->
    view = new App.Views.CategoryShowPage({sectionSlug: sectionSlug, id: id})
    this.swap(view)

  categoriesAdd: (sectionSlug) ->
    view = new App.Views.CategoryAddPage({sectionSlug: sectionSlug})
    this.swap(view)

  categoriesEdit: (sectionSlug, id) ->
    view = new App.Views.CategoryEditPage({sectionSlug: sectionSlug, id: id})
    this.swap(view)

  categoriesDelete: (sectionSlug, id) ->
    view = new App.Views.CategoryDeletePage({sectionSlug: sectionSlug, id: id})
    this.swap(view)

  # MaxFiles

  imagesIndex: (page, field, order)->
    if page == "1" && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Image.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.ImagesIndexPage({page: page, field: field, order: order})
    this.swap(view)

  imagesShow: (id) ->
    view = new App.Views.ImageShowPage({id: id})
    this.swap(view)

  imagesAdd: () ->
    view = new App.Views.ImageAddPage()
    this.swap(view)

  imagesEdit: (id) ->
    view = new App.Views.ImageEditPage({id: id})
    this.swap(view)

  # Pages

  pagesIndex: (page, field, order)->
    if page == "1" && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Page.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.PagesIndexPage({page: page, field: field, order: order})
    this.swap(view)

  pagesBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.PagesBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  pagesShow: (id) ->
    view = new App.Views.PageShowPage({id: id})
    this.swap(view)

  pagesAdd: () ->
    view = new App.Views.PageAddPage()
    this.swap(view)

  pagesEdit: (id) ->
    view = new App.Views.PageEditPage({id: id})
    this.swap(view)

  pagesDelete: (id) ->
    view = new App.Views.PageDeletePage({id: id})
    this.swap(view)

  # Settings

  settingsIndex: ()->
    view = new App.Views.SettingsIndexPage()
    this.swap(view)

  settingsStructure: ()->
    view = new App.Views.SettingsStructurePage()
    this.swap(view)

  settingsSystem: ()->
    view = new App.Views.SettingsSystemPage()
    this.swap(view)

  settingsMessages: ()->
    view = new App.Views.SettingsMessagesPage()
    this.swap(view)

  settingsVisualizationLabels: ()->
    view = new App.Views.SettingsVisualizationLabelsPage()
    this.swap(view)

  settingsSeo: ()->
    @swap(new App.Views.SettingsSeoPage())

  settingsCalc: () ->
    @swap(new App.Views.SettingsCalcPage())

  settingsOther: ()->
    view = new App.Views.SettingsOtherPage()
    this.swap(view)

  # Blocks

  blocksIndex: (page, field, order)->
    if page == "1" && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Block.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.BlocksIndexPage({page: page, field: field, order: order})
    this.swap(view)

  blocksBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.BlocksBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  blocksShow: (id) ->
    view = new App.Views.BlockShowPage({id: id})
    this.swap(view)

  blocksAdd: () ->
    view = new App.Views.BlockAddPage()
    this.swap(view)

  blocksEdit: (id) ->
    view = new App.Views.BlockEditPage({id: id})
    this.swap(view)

  blocksDelete: (id) ->
    view = new App.Views.BlockDeletePage({id: id})
    this.swap(view)

  # Menus

  menusIndex: (page, field, order)->
    if page == "1" && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Menu.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.MenusIndexPage({page: page, field: field, order: order})
    this.swap(view)

  menusShow: (id) ->
    view = new App.Views.MenuShowPage({id: id})
    this.swap(view)

  menusAdd: () ->
    view = new App.Views.MenuAddPage()
    this.swap(view)

  menusEdit: (id) ->
    view = new App.Views.MenuEditPage({id: id})
    this.swap(view)

  menusDelete: (id) ->
    view = new App.Views.MenuDeletePage({id: id})
    this.swap(view)

  # Links

  linksIndex: (menuSlug)->
    view = new App.Views.LinksIndexPage({menuSlug: menuSlug})
    this.swap(view)

  linksShow: (menuSlug, id) ->
    view = new App.Views.LinkShowPage({menuSlug: menuSlug, id: id})
    this.swap(view)

  linksAdd: (menuSlug) ->
    view = new App.Views.LinkAddPage({menuSlug: menuSlug})
    this.swap(view)

  linksEdit: (menuSlug, id) ->
    view = new App.Views.LinkEditPage({menuSlug: menuSlug, id: id})
    this.swap(view)

  linksDelete: (menuSlug, id) ->
    view = new App.Views.LinkDeletePage({menuSlug: menuSlug, id: id})
    this.swap(view)

  # Facilities

  facilitiesIndex: (page, field, order) ->
    if page == "1" && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Facility.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.FacilitiesIndexPage({page: page, field: field, order: order})
    this.swap(view)

  facilitiesBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.FacilitiesBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  facilitiesShow: (id) ->
    view = new App.Views.FacilityShowPage({id: id})
    this.swap(view)

  facilitiesAdd: () ->
    view = new App.Views.FacilityAddPage()
    this.swap(view)

  facilitiesEdit: (id) ->
    view = new App.Views.FacilityEditPage({id: id})
    this.swap(view)

  facilitiesDelete: (id) ->
    view = new App.Views.FacilityDeletePage({id: id})
    this.swap(view)

  # Scenes

  scenesIndex: (page, field, order) ->
    if page == "1" && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Scene.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.ScenesIndexPage({page: page, field: field, order: order})
    this.swap(view)

  scenesBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.ScenesBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  scenesShow: (id) ->
    view = new App.Views.SceneShowPage({id: id})
    this.swap(view)

  scenesAdd: () ->
    view = new App.Views.SceneAddPage()
    this.swap(view)

  scenesEdit: (id) ->
    view = new App.Views.SceneEditPage({id: id})
    this.swap(view)

  scenesDelete: (id) ->
    view = new App.Views.SceneDeletePage({id: id})
    this.swap(view)

  # MaxFiles

  maxFilesIndex: (page, field, order)->
    if page == "1" && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.MaxFile.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = "created_at" if _.isUndefined(field)
    order = "desc" if _.isUndefined(order)

    view = new App.Views.MaxFilesIndexPage({page: page, field: field, order: order})
    this.swap(view)

  maxFilesShow: (id) ->
    view = new App.Views.MaxFileShowPage({id: id})
    this.swap(view)

  maxFilesAdd: () ->
    view = new App.Views.MaxFileAddPage()
    this.swap(view)

  maxFilesEdit: (id) ->
    view = new App.Views.MaxFileEditPage({id: id})
    this.swap(view)

  # Projects

  projectsIndex: (page, field, order) ->
    if page == '1' && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Project.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.ProjectsIndexPage({page: page, field: field, order: order})
    this.swap(view)

  projectsBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.ProjectsBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  projectsShow: (id) ->
    view = new App.Views.ProjectShowPage({id: id})
    this.swap(view)

  projectsAdd: () ->
    view = new App.Views.ProjectAddPage()
    this.swap(view)

  projectsEdit: (id) ->
    view = new App.Views.ProjectEditPage({id: id})
    this.swap(view)

  projectsDelete: (id) ->
    view = new App.Views.ProjectDeletePage({id: id})
    this.swap(view)

  

  # Visualizations

  visualizationsIndex: (page, field, order) ->
    if page == '1' && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Visualization.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.VisualizationsIndexPage({page: page, field: field, order: order})
    this.swap(view)

  visualizationsBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.VisualizationsBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  visualizationsShow: (id) ->
    view = new App.Views.VisualizationShowPage({id: id})
    this.swap(view)

  visualizationsAdd: () ->
    view = new App.Views.VisualizationAddPage()
    this.swap(view)

  visualizationsEdit: (id) ->
    view = new App.Views.VisualizationEditPage({id: id})
    this.swap(view)

  visualizationsDelete: (id) ->
    view = new App.Views.VisualizationDeletePage({id: id})
    this.swap(view)

  visualizationsFurniturePlan: (id) ->
    view = new App.Views.VisualizationFurniturePlanPage({id: id})
    this.swap(view)

  visualizationsCeilingPlan: (id) ->
    view = new App.Views.VisualizationCeilingPlanPage({id: id})
    this.swap(view)

  visualizationsLightingPlan: (id) ->
    view = new App.Views.VisualizationLightingPlanPage({id: id})
    this.swap(view)

  visualizationsCoveringsPlan: (id) ->
    view = new App.Views.VisualizationCoveringsPlanPage({id: id})
    this.swap(view)

  visualizationsWallsPlan: (id) ->
    view = new App.Views.VisualizationWallsPlanPage({id: id})
    this.swap(view)

  # FurniturePlans

  furniturePlansIndex: (page, field, order) ->
    if page == '1' && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.FurniturePlan.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.FurniturePlansIndexPage({page: page, field: field, order: order})
    this.swap(view)

  furniturePlansBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.FurniturePlansBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  furniturePlansShow: (id) ->
    view = new App.Views.FurniturePlanShowPage({id: id})
    this.swap(view)

  furniturePlansAdd: () ->
    view = new App.Views.FurniturePlanAddPage()
    this.swap(view)

  furniturePlansEdit: (id) ->
    view = new App.Views.FurniturePlanEditPage({id: id})
    this.swap(view)
  

  # CeilingPlans

  ceilingPlansIndex: (page, field, order) ->
    if page == '1' && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.CeilingPlan.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.CeilingPlansIndexPage({page: page, field: field, order: order})
    this.swap(view)

  ceilingPlansBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.CeilingPlansBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  ceilingPlansShow: (id) ->
    view = new App.Views.CeilingPlanShowPage({id: id})
    this.swap(view)

  ceilingPlansAdd: () ->
    view = new App.Views.CeilingPlanAddPage()
    this.swap(view)

  ceilingPlansEdit: (id) ->
    view = new App.Views.CeilingPlanEditPage({id: id})
    this.swap(view)
  

  # LightingPlans

  lightingPlansIndex: (page, field, order) ->
    if page == '1' && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.LightingPlan.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.LightingPlansIndexPage({page: page, field: field, order: order})
    this.swap(view)

  lightingPlansBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.LightingPlansBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  lightingPlansShow: (id) ->
    view = new App.Views.LightingPlanShowPage({id: id})
    this.swap(view)

  lightingPlansAdd: () ->
    view = new App.Views.LightingPlanAddPage()
    this.swap(view)

  lightingPlansEdit: (id) ->
    view = new App.Views.LightingPlanEditPage({id: id})
    this.swap(view)
  

  # CoveringsPlans

  coveringsPlansIndex: (page, field, order) ->
    if page == '1' && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.CoveringsPlan.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.CoveringsPlansIndexPage({page: page, field: field, order: order})
    this.swap(view)

  coveringsPlansBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.CoveringsPlansBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  coveringsPlansShow: (id) ->
    view = new App.Views.CoveringsPlanShowPage({id: id})
    this.swap(view)

  coveringsPlansAdd: () ->
    view = new App.Views.CoveringsPlanAddPage()
    this.swap(view)

  coveringsPlansEdit: (id) ->
    view = new App.Views.CoveringsPlanEditPage({id: id})
    this.swap(view)
  

  # CoveringsTextures

  coveringsTexturesIndex: (page, field, order) ->
    if page == '1' && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.CoveringsTexture.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.CoveringsTexturesIndexPage({page: page, field: field, order: order})
    this.swap(view)

  coveringsTexturesBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.CoveringsTexturesBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  coveringsTexturesShow: (id) ->
    view = new App.Views.CoveringsTextureShowPage({id: id})
    this.swap(view)

  coveringsTexturesAdd: () ->
    view = new App.Views.CoveringsTextureAddPage()
    this.swap(view)

  coveringsTexturesEdit: (id) ->
    view = new App.Views.CoveringsTextureEditPage({id: id})
    this.swap(view)
  

  # WallsPlans

  wallsPlansIndex: (page, field, order) ->
    if page == '1' && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.WallsPlan.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.WallsPlansIndexPage({page: page, field: field, order: order})
    this.swap(view)

  wallsPlansBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.WallsPlansBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  wallsPlansShow: (id) ->
    view = new App.Views.WallsPlanShowPage({id: id})
    this.swap(view)

  wallsPlansAdd: () ->
    view = new App.Views.WallsPlanAddPage()
    this.swap(view)

  wallsPlansEdit: (id) ->
    view = new App.Views.WallsPlanEditPage({id: id})
    this.swap(view)
  

  # WallsTextures

  wallsTexturesIndex: (page, field, order) ->
    if page == '1' && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.WallsTexture.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.WallsTexturesIndexPage({page: page, field: field, order: order})
    this.swap(view)

  wallsTexturesBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.WallsTexturesBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  wallsTexturesShow: (id) ->
    view = new App.Views.WallsTextureShowPage({id: id})
    this.swap(view)

  wallsTexturesAdd: () ->
    view = new App.Views.WallsTextureAddPage()
    this.swap(view)

  wallsTexturesEdit: (id) ->
    view = new App.Views.WallsTextureEditPage({id: id})
    this.swap(view)
  

  # Discounts

  discountsIndex: (page, field, order) ->
    if page == '1' && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Discount.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.DiscountsIndexPage({page: page, field: field, order: order})
    this.swap(view)

  discountsBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.DiscountsBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  discountsShow: (id) ->
    view = new App.Views.DiscountShowPage({id: id})
    this.swap(view)

  discountsAdd: () ->
    view = new App.Views.DiscountAddPage()
    this.swap(view)

  discountsEdit: (id) ->
    view = new App.Views.DiscountEditPage({id: id})
    this.swap(view)
  
  discountsDelete: (id) ->
    view = new App.Views.DiscountDeletePage({id: id})
    this.swap(view)

  

  # Articles

  articlesIndex: (page, field, order) ->
    if page == '1' && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Article.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.ArticlesIndexPage({page: page, field: field, order: order})
    this.swap(view)

  articlesBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.ArticlesBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  articlesShow: (id) ->
    view = new App.Views.ArticleShowPage({id: id})
    this.swap(view)

  articlesAdd: () ->
    view = new App.Views.ArticleAddPage()
    this.swap(view)

  articlesEdit: (id) ->
    view = new App.Views.ArticleEditPage({id: id})
    this.swap(view)
  
  articlesDelete: (id) ->
    view = new App.Views.ArticleDeletePage({id: id})
    this.swap(view)

  

  # Portfolios

  portfoliosIndex: (page, field, order) ->
    if page == '1' && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Portfolio.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.PortfoliosIndexPage({page: page, field: field, order: order})
    this.swap(view)

  portfoliosBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.PortfoliosBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  portfoliosShow: (id) ->
    view = new App.Views.PortfolioShowPage({id: id})
    this.swap(view)

  portfoliosAdd: () ->
    view = new App.Views.PortfolioAddPage()
    this.swap(view)

  portfoliosEdit: (id) ->
    view = new App.Views.PortfolioEditPage({id: id})
    this.swap(view)
  
  portfoliosDelete: (id) ->
    view = new App.Views.PortfolioDeletePage({id: id})
    this.swap(view)

  

  # Reviews

  reviewsIndex: (page, field, order) ->
    if page == '1' && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Review.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.ReviewsIndexPage({page: page, field: field, order: order})
    this.swap(view)

  reviewsBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.ReviewsBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  reviewsShow: (id) ->
    view = new App.Views.ReviewShowPage({id: id})
    this.swap(view)

  reviewsAdd: () ->
    view = new App.Views.ReviewAddPage()
    this.swap(view)

  reviewsEdit: (id) ->
    view = new App.Views.ReviewEditPage({id: id})
    this.swap(view)
  
  reviewsDelete: (id) ->
    view = new App.Views.ReviewDeletePage({id: id})
    this.swap(view)

  # Sketches

  sketchesIndex: (page, field, order) ->
    if page == '1' && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Sketch.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.SketchesIndexPage({page: page, field: field, order: order})
    this.swap(view)

  sketchesBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.SketchesBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  sketchesShow: (id) ->
    view = new App.Views.SketchShowPage({id: id})
    this.swap(view)

  sketchesAdd: () ->
    view = new App.Views.SketchAddPage()
    this.swap(view)

  sketchesEdit: (id) ->
    view = new App.Views.SketchEditPage({id: id})
    this.swap(view)
  
  sketchesDelete: (id) ->
    view = new App.Views.SketchDeletePage({id: id})
    this.swap(view)

  

  # Slides

  slidesIndex: (page, field, order) ->
    if page == '1' && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Slide.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.SlidesIndexPage({page: page, field: field, order: order})
    this.swap(view)

  slidesBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.SlidesBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  slidesShow: (id) ->
    view = new App.Views.SlideShowPage({id: id})
    this.swap(view)

  slidesAdd: () ->
    view = new App.Views.SlideAddPage()
    this.swap(view)

  slidesEdit: (id) ->
    view = new App.Views.SlideEditPage({id: id})
    this.swap(view)
  
  slidesDelete: (id) ->
    view = new App.Views.SlideDeletePage({id: id})
    this.swap(view)

  

  # People

  peopleIndex: (page, field, order) ->
    if page == '1' && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Person.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.PeopleIndexPage({page: page, field: field, order: order})
    this.swap(view)

  peopleShow: (id) ->
    view = new App.Views.PersonShowPage({id: id})
    this.swap(view)

  peopleAdd: () ->
    view = new App.Views.PersonAddPage()
    this.swap(view)

  peopleEdit: (id) ->
    view = new App.Views.PersonEditPage({id: id})
    this.swap(view)
  
  peopleDelete: (id) ->
    view = new App.Views.PersonDeletePage({id: id})
    this.swap(view)

  

  # Faqs

  faqsIndex: (page, field, order) ->
    if page == '1' && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.Faq.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.FaqsIndexPage({page: page, field: field, order: order})
    this.swap(view)

  faqsBlocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.FaqsBlockedPage({page: page, field: field, order: order})
    this.swap(view)

  faqsShow: (id) ->
    view = new App.Views.FaqShowPage({id: id})
    this.swap(view)

  faqsAdd: () ->
    view = new App.Views.FaqAddPage()
    this.swap(view)

  faqsEdit: (id) ->
    view = new App.Views.FaqEditPage({id: id})
    this.swap(view)
  
  faqsDelete: (id) ->
    view = new App.Views.FaqDeletePage({id: id})
    this.swap(view)

  