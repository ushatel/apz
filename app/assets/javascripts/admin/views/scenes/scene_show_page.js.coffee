class App.Views.SceneShowPage extends App.Views.SharedShowPage

  template: JST['admin/scenes/show_page']

  initialize: ->
    @model = new App.Models.Scene({slug: @id})
    super

  getRecordTabsView: () ->
    new App.Views.BlockedRecordTabsView({model: @model})