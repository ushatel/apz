class App.Views.Scene extends App.Views.BlockableRecord

  template: JST['admin/scenes/blockable_record']

  renderTemplate: () ->
    @$el.addClass(@model.get("slug")).html(@template({
      record: @model
      collectionPath: @model.constructor.getCollectionPath(@model.getCollectionParam(), @model.getAdditionalCollectionParam())
      slug: @model.get(@model.idAttribute)
    }))