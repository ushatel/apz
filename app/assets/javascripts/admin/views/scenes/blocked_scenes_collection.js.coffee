class App.Views.BlockedScenesCollection extends App.Views.BlockableCollection

  template: JST['admin/scenes/blockable_collection']

  id: "scenes"

  initialize: ->
    @itemView = App.Views.BlockedScene
    super

  renderTemplate: () ->
    @$el.html(@template({collection: @collection}))