class App.Views.ScenesCollection extends App.Views.BlockableCollection

  template: JST['admin/scenes/blockable_collection']

  id: "scenes"

  initialize: ->
    @itemView = App.Views.Scene
    super

  renderTemplate: () ->
    @$el.html(@template({collection: @collection}))