class App.Views.BlockedScene extends App.Views.BlockedRecord

  template: JST['admin/scenes/blocked_record']

  renderTemplate: () ->
    @$el.addClass(@model.get("slug")).html(@template({
      record: @model
      collectionPath: @model.constructor.getCollectionPath(@model.getCollectionParam(), @model.getAdditionalCollectionParam())
      slug: @model.get(@model.idAttribute)
    }))