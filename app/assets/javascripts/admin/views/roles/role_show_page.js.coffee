class App.Views.RoleShowPage extends App.Views.SharedShowPage

  initialize: ->
    @model = new App.Models.Role({slug: @id})
    super