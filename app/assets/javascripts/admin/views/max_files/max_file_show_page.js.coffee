class App.Views.MaxFileShowPage extends App.Views.SharedShowPage

  initialize: ->
    @model = new App.Models.MaxFile({slug: @id})
    super

  getRecordTabsView: () ->
    new App.Views.NonDeletableRecordTabsView({model: @model})