class App.Views.ProjectsCollection extends App.Views.BlockableCollection

  template: HoganTemplates['admin/projects/blockable_collection']

  id: "projects"

  initialize: ->
    @itemView = App.Views.Project
    super