class App.Views.BlockedProjectsCollection extends App.Views.BlockableCollection

  template: HoganTemplates['admin/projects/blockable_collection']

  id: "projects"

  initialize: ->
    @itemView = App.Views.BlockedProject
    super