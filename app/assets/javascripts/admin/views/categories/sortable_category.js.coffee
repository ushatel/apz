class App.Views.SortableCategory extends App.Views.SharedRecord

  template: HoganTemplates['admin/shared/sortable_tree_item']
  tagName: 'li'

  id: () ->
    "category_#{@model.id}"