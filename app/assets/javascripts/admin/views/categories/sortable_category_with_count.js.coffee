class App.Views.SortableCategoryWithCount extends App.Views.SharedRecord

  template: HoganTemplates['admin/shared/sortable_tree_item_with_count']
  tagName: 'li'

  id: () ->
    "category_#{@model.id}"