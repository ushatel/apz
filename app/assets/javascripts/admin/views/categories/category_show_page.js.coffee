class App.Views.CategoryShowPage extends App.Views.SharedShowPage

  initialize: (params)->
    @model = new App.Models.Category({sectionSlug: params.sectionSlug, slug: @id})
    super(params)

  getRecordTabsView: () ->
    new App.Views.CategoryRecordTabsView({model: @model})
