class App.Views.CategoriesCollectionTabsView extends App.Views.CollectionTabsView

  getLinks: () ->
    super()

    @links.push(Support.View_Helpers.active_li(I18n.t("words.structure"), "#{@collection.getPath()}/structure"))

    @links