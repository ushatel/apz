class App.Views.TranslationsCollection extends App.Views.SharedCollection

  template: HoganTemplates['admin/translations/collection']

  id: "translations"

  initialize: ->
    @itemView = App.Views.Translation
    super