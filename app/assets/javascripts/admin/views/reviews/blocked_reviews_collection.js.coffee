class App.Views.BlockedReviewsCollection extends App.Views.BlockableCollection

  id: "reviews"

  initialize: ->
    @itemView = App.Views.BlockedReview
    super