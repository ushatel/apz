class App.Views.ReviewsCollection extends App.Views.BlockableCollection

  id: "reviews"

  initialize: ->
    @itemView = App.Views.Review
    super