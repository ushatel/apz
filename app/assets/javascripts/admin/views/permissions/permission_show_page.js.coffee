class App.Views.PermissionShowPage extends App.Views.SharedShowPage

  initialize: ->
    @model = new App.Models.Permission({slug: @id})
    super