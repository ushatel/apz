class App.Views.PermissionsCollection extends App.Views.SharedCollection

  id: "permissions"

  initialize: ->
    @itemView = App.Views.Permission
    super