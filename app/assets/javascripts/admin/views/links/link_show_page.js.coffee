class App.Views.LinkShowPage extends App.Views.SharedShowPage

  initialize: (params)->
    @model = new App.Models.Link({menuSlug: params.menuSlug, slug: @id})
    super(params)

  getRecordTabsView: () ->
    new App.Views.LinkRecordTabsView({model: @model})
