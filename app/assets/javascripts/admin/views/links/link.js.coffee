class App.Views.Link extends App.Views.SharedRecord

  template: HoganTemplates['admin/shared/sortable_tree_item']
  tagName: 'li'

  id: () ->
    "link_#{@model.id}"