class App.Views.MainPage extends App.View

  template: HoganTemplates['admin/main/main_page']

  id: "main-page"

  render: ->
    $(@el).html(@template.render({}, {}))

    current_view = this

    navigationBarView = new App.Views.NavigationBarView()

    current_view.$("#navigation-bar").html(navigationBarView.render().el)

    this