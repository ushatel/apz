class App.Views.DiscountsCollection extends App.Views.SharedCollection

  template: HoganTemplates['admin/discounts/collection']

  id: "discounts"

  initialize: ->
    @itemView = App.Views.Discount
    super