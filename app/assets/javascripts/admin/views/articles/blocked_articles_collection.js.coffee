class App.Views.BlockedArticlesCollection extends App.Views.BlockableCollection

  id: "articles"

  initialize: ->
    @itemView = App.Views.BlockedArticle
    super