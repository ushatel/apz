class App.Views.ArticlesCollection extends App.Views.BlockableCollection

  id: "articles"

  initialize: ->
    @itemView = App.Views.Article
    super