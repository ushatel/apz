class App.Views.MenuShowPage extends App.Views.SharedShowPage

  initialize: ->
    @model = new App.Models.Menu({slug: @id})
    super

  getRecordTabsView: () ->
    new App.Views.MenuRecordTabsView({model: @model})
