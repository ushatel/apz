class App.Views.SectionsCollection extends App.Views.SharedCollection

  id: "sections"

  initialize: ->
    @itemView = App.Views.Section
    super