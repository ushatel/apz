class App.Views.SectionShowPage extends App.Views.SharedShowPage

  initialize: ->
    @model = new App.Models.Section({slug: @id})
    super

  getRecordTabsView: () ->
    new App.Views.SectionRecordTabsView({model: @model})
