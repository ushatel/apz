class App.Views.ImageShowPage extends App.Views.SharedShowPage

  initialize: ->
    @model = new App.Models.Image({slug: @id})
    super

  getRecordTabsView: () ->
    new App.Views.NonDeletableRecordTabsView({model: @model})