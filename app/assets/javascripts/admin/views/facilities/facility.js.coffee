class App.Views.Facility extends App.Views.BlockableRecord

  template: JST['admin/facilities/blockable_record']

  renderTemplate: () ->
    @$el.addClass(@model.get("slug")).html(@template({
      record: @model
      collectionPath: @model.constructor.getCollectionPath(@model.getCollectionParam(), @model.getAdditionalCollectionParam())
      slug: @model.get(@model.idAttribute)
    }))