class App.Views.FacilityShowPage extends App.Views.SharedShowPage

  template: JST['admin/facilities/show_page']

  initialize: ->
    @model = new App.Models.Facility({slug: @id})
    super

  getRecordTabsView: () ->
    new App.Views.BlockedRecordTabsView({model: @model})