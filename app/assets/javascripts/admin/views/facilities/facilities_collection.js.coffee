class App.Views.FacilitiesCollection extends App.Views.BlockableCollection

  template: JST['admin/facilities/blockable_collection']

  id: "facilities"

  initialize: ->
    @itemView = App.Views.Facility
    super

  renderTemplate: () ->
    @$el.html(@template({collection: @collection}))