class App.Views.BlockedFacilitiesCollection extends App.Views.BlockableCollection

  template: JST['admin/facilities/blockable_collection']

  id: "facilities"

  initialize: ->
    @itemView = App.Views.BlockedFacility
    super

  renderTemplate: () ->
    @$el.html(@template({collection: @collection}))