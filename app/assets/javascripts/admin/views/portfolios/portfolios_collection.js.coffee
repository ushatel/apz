class App.Views.PortfoliosCollection extends App.Views.BlockableCollection

  id: "portfolios"

  initialize: ->
    @itemView = App.Views.Portfolio
    super