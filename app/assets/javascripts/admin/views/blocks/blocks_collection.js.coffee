class App.Views.BlocksCollection extends App.Views.BlockableCollection

  id: "blocks"

  initialize: ->
    @itemView = App.Views.Block
    super