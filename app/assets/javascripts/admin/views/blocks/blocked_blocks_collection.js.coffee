class App.Views.BlockedBlocksCollection extends App.Views.BlockableCollection

  id: "blocks"

  initialize: ->
    @itemView = App.Views.BlockedBlock
    super