class App.Views.PagesCollection extends App.Views.BlockableCollection

  id: "pages"

  initialize: ->
    @itemView = App.Views.Page
    super