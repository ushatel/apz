class App.Views.BlockedPagesCollection extends App.Views.BlockableCollection

  id: "pages"

  initialize: ->
    @itemView = App.Views.BlockedPage
    super