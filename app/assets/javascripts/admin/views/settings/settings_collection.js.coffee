class App.Views.SettingsCollection extends App.Views.SharedCollection

  template: HoganTemplates['admin/settings/collection']

  id: "settings"

  initialize: (params)->
    @part = params.part
    @itemView = App.Views.Setting
    if @part == "structure"
      @itemView = App.Views.SelectSetting
    else if @part == "system"
      @itemView = App.Views.TextareaSetting
    else if @part == "messages"
      @itemView = App.Views.CkeditorSetting
    else if @part == "visualization_labels"
      @itemView = App.Views.CkeditorSetting
    super

  appendItem: (item) =>
    view = new @itemView({model: item, part: @part})
    $(@el).append(view.render().el)