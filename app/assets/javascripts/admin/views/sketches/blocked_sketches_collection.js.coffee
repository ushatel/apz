class App.Views.BlockedSketchesCollection extends App.Views.BlockableCollection

  id: "sketches"

  initialize: ->
    @itemView = App.Views.BlockedSketch
    super