class App.Views.SketchesCollection extends App.Views.BlockableCollection

  id: "sketches"

  initialize: ->
    @itemView = App.Views.Sketch
    super