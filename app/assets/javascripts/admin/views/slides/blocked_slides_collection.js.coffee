class App.Views.BlockedSlidesCollection extends App.Views.BlockableCollection

  id: "slides"

  initialize: ->
    @itemView = App.Views.BlockedSlide
    super