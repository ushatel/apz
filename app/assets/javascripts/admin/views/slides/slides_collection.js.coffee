class App.Views.SlidesCollection extends App.Views.BlockableCollection

  id: "slides"

  initialize: ->
    @itemView = App.Views.Slide
    super