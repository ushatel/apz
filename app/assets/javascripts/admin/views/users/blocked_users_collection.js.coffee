class App.Views.BlockedUsersCollection extends App.Views.BlockableCollection

  id: "users"

  initialize: ->
    @itemView = App.Views.BlockedUser
    super