class App.Views.UsersCollection extends App.Views.BlockableCollection

  id: "users"

  initialize: ->
    @itemView = App.Views.User
    super