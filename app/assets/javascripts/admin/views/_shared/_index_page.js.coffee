class App.Views.SharedIndexPage extends App.View

  template: HoganTemplates['admin/shared/index_page']

  id: "index-page"

  events: {
    "click #add-item": "addItem"
  }

  initialize: (params) ->
    _.bindAll(@)
    @collection.on('reset', @render, @)
    @collection.on('change', @render, @)
    @collection.fetch()

  render: ->
    $(@el).html(@template.render({
      collectionPath: @collection.getPath()
      pluralModelString: @options.pluralModelString
      addModelString: @options.addModelString
      count: @collection.getCount
    }, {}))

    @$("#navigation-bar").html(@getNavigationBarView().render().el)
    @$("#records").html(@getItemsView().render().el)
    @$("#pagination").html(@getPaginationView().render().el)
    @$("#tabs").html(@getTabsView().render().el)

    @

  getNavigationBarView: () ->
    new App.Views.NavigationBarView()

  getTabsView: () ->
    new App.Views.CollectionTabsView({collection: @collection})

  getPaginationView: () ->
    new App.Views.PaginatedView({collection: @collection})

  getItemsView: () ->
    new @collectionView({collection: @collection})

  addItem: (e)->
    e.preventDefault()
    window.router.navigate("#{@collection.getPath()}/new", true)
