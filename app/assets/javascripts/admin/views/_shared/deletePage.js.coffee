class App.Views.SharedDeletePage extends App.View

  template: JST['admin/shared/delete_page']

  events: {
    "click #delete": "deleteRecord"
    "click #cancel": "cancelDelete"
    "click .record a": "clickRecord"
  }

  initialize: (params) ->
    _.bindAll(@)
    _.extend(@, params) unless _.isUndefined(params)
    @model.on("change", @render, @)
    @model.fetch()

  render: ->
    $(@el).html(@template({record: @model}))

    @$("#navigation-bar").html(@getNavigationBarView().render().el)
    @$("#tabs").html(@getRecordTabsView().render().el)

    @

  getNavigationBarView: () ->
    new App.Views.NavigationBarView()

  getRecordTabsView: () ->
    new App.Views.RecordTabsView({model: @model})

  deleteRecord: (e) ->
    e.preventDefault()

    current_view = @
    collectionPath = @model.constructor.getCollectionPath(@model.getCollectionParam(), @model.getAdditionalCollectionParam())

    @model.destroy({
      success: (model, response) ->
        window.router.navigate(collectionPath, true)
        current_view.prepend_and_fade_alert($('.flash-messages'), current_view.afterDeleteMessage, 'success')
    })

  cancelDelete: (e) ->
    e.preventDefault()
    window.router.navigate(@model.constructor.getCollectionPath(@model.getCollectionParam()), true)

  clickRecord: (e) ->
    e.preventDefault()
    window.router.navigate(e.target.pathname, true)

