class App.Views.SharedForm extends App.View

  # Functions:
  #
  # success_callback()
  #
  # executed when model successfully saved, takes no arguments and returns true
  # by default. Can be used for example to add model to a collection
  #
  # Options
  #
  # header - header of the modal
  # success_message - message shown in success alert
  #	error_message - message shown in error alert

  template: JST['admin/shared/form']

  events: {
    'click #submit-form' : 'submit_form'
    'click #cancel' : 'close_form'
  }

  initialize: (params)->
    _.bindAll(this)
    _.extend(@, params) unless _.isUndefined(params)
    @model.on("change", @render, @)

    @options.after_save_callback = () =>
      window.router.navigate(@model.constructor.getCollectionPath(@model.getCollectionParam(), @model.getAdditionalCollectionParam()), true)

    @options.cancel_callback = () =>
      window.router.navigate(@model.constructor.getCollectionPath(@model.getCollectionParam(), @model.getAdditionalCollectionParam()), true)

  render: ->
    @renderTemplate()

    @$("#navigation-bar").html(@getNavigationBarView().render().el)
    @$('#tabs').html(@getRecordTabsView().render().el)
    @$('#form').html(new Backbone.Form({model: @model}).render().el)

    @

  renderTemplate: () ->
    @$el.html(@template({
      header: @options.header
      submitValue: @options.submitValue
      cancelValue: @options.cancelValue
      record: @model
    }))

  getNavigationBarView: () ->
    new App.Views.NavigationBarView()

  getRecordTabsView: () ->
    new App.Views.RecordTabsView({model: @model})

  close_form: (e) ->
    e.preventDefault()
    @options.cancel_callback()
    @leave()

  submit_form: (e) ->
    e.preventDefault()

    _.each CKEDITOR.instances, (instance) ->
      CKEDITOR.instances[instance.name].updateElement()

    attributes = Backbone.Syphon.serialize(@)
    current_view = @

    @model.set('id', attributes.slug)
    delete attributes.slug

    @model.save(attributes, {
      wait: true
      success: (model) =>

        @options.after_save_callback()
        current_view.prepend_and_fade_alert($('.flash-messages'), current_view.options.success_message, 'success')
        current_view.success_callback()

      error: (model, response) =>
        # clear all previous errors
        $('.bbf-error .error').each ->
          $(this).removeClass('error')

        $('.bbf-help').each ->
          $(this).empty()
        # mark all fields with errors and show error text
        all_errors = JSON.parse(response.responseText).errors
        for attribute_name, attribute_errors of all_errors

          attribute_container = @$('input[name=' + attribute_name + ']')
          attribute_container.closest('.bbf-field').addClass('bbf-error')
          attribute_container.closest('.bbf-field').find('.bbf-error').html(attribute_errors.join(', '))

          div_attribute_container = @$('div[name=' + attribute_name + ']')
          div_attribute_container.closest('.bbf-field').addClass('bbf-error')
          div_attribute_container.closest('.bbf-field').find('.bbf-error').html(attribute_errors.join(', '))

        # show corresponding alert
        current_view.prepend_and_fade_alert($('.flash-messages'), current_view.options.error_message, 'error')
    })

    return false

  success_callback: ->
    return true