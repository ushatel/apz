class App.Views.SharedCollection extends App.View

  template: HoganTemplates['admin/shared/collection']

  tagName: "table"

  className: "table table-striped table-bordered"

  events: {
    "click .sort-link": "sortItems"
  }

  initialize: () ->
    @collection.on('reset', @render, @)
    @collection.on('change', @render, @)
    super()

  render: ->
    @renderTemplate()

    @collection.each(@appendItem)

    @

  renderTemplate: () ->
    @$el.html(@template.render({collection: @collection}, {}))

  appendItem: (item) =>
    view = new @itemView(model: item)
    $(@el).append(view.render().el)

  sortItems: (e) ->
    e.preventDefault()
    window.router.navigate(e.currentTarget.pathname, true)




