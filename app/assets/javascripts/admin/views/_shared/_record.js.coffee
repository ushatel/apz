class App.Views.SharedRecord extends App.View

  template: HoganTemplates['admin/shared/record']
  tagName: 'tr'

  events: ->
    'click .show-link' : "showRecord"
    'click .edit-link' : "editRecord"
    'click .delete-link' : "deleteRecord"

  initialize: () ->
    @model.on("change", @render, @)

  render: () ->
    @renderTemplate()

    @

  renderTemplate: () ->
    @$el.addClass(@model.get("slug")).html(@template.render({
      record: @model.toJSON()
      collectionPath: @model.constructor.getCollectionPath(@model.getCollectionParam(), @model.getAdditionalCollectionParam())
      slug: @model.get(@model.idAttribute)
    }, {}))

  showRecord: (e) ->
    e.preventDefault()
    window.router.navigate(@getPath(e), true)

  editRecord: (e) ->
    e.preventDefault()
    window.router.navigate(@getPath(e), true)

  deleteRecord: (e) ->
    e.preventDefault()
    window.router.navigate(@getPath(e), true)

  getPath: (e) ->
    if _.isUndefined(e.target.pathname)
      e.target.parentNode.pathname
    else
      e.target.pathname




