class App.Views.BlockableRecord extends App.Views.SharedRecord

  template: HoganTemplates['admin/shared/blockable_record']

  events: ->
    'click .show-link' : "showRecord"
    'click .edit-link' : "editRecord"
    'click .block-link' : "blockRecord"
    'click .delete-link' : "deleteRecord"

  blockRecord: (e) ->
    e.preventDefault()
    @model.block_it()
    @remove()