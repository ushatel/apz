class App.Views.SharedShowPage extends App.View

  template: JST['admin/shared/show_page']

  initialize: (params) ->
    _.bindAll(@)
    _.extend(@, params) unless _.isUndefined(params)
    @model.on("change", @render, @)
    @model.fetch()

  getNavigationBarView: () ->
    new App.Views.NavigationBarView()

  getRecordTabsView: () ->
    new App.Views.RecordTabsView({model: @model})

  render: ->
    @$el.html(@template({record: @model}))

    @$("#navigation-bar").html(@getNavigationBarView().render().el)
    @$("#tabs").html(@getRecordTabsView().render().el)
    @
