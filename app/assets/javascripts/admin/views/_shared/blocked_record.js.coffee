class App.Views.BlockedRecord extends App.Views.SharedRecord

  template: HoganTemplates['admin/shared/blocked_record']

  events: ->
    'click .show-link' : "showRecord"
    'click .edit-link' : "editRecord"
    'click .unblock-link' : "unblockRecord"
    'click .delete-link' : "deleteRecord"

  unblockRecord: (e) ->
    e.preventDefault()
    @model.unblock_it()