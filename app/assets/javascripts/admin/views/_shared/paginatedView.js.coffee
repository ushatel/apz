class App.Views.PaginatedView extends App.View

  events:
    "click a.servernext": "nextResultPage"
    "click a.serverprevious": "previousResultPage"
    "click a.orderUpdate": "updateSortBy"
    "click a.serverlast": "gotoLast"
    "click a.page": "gotoPage"
    "click a.serverfirst": "gotoFirst"
    "click a.serverpage": "gotoPage"
    "click .serverhowmany a": "changeCount"

  tagName: "aside"

  template: JST["admin/shared/pagination"]

  initialize: (params)->
    _.bindAll(this)
    @collection.on "reset", @render, this
    @collection.on "change", @render, this

  render: ->
    html = @template(_.extend(@collection.info(), {field: @collection.field, order: @collection.order, collectionPath:  @collection.getPath()}))
    @$el.html html
    this

  updateSortBy: (e) ->
    e.preventDefault()
    currentSort = $("#sortByField").val()
    @collection.updateOrder currentSort

  nextResultPage: (e) ->
    e.preventDefault()
    window.router.navigate("#{@collection.getPath()}/page/#{@collection.information.currentPage + 1}/#{@collection.field}/#{@collection.order}", true)
    #    @collection.requestNextPage()

  previousResultPage: (e) ->
    e.preventDefault()
    window.router.navigate("#{@collection.getPath()}/page/#{@collection.information.currentPage - 1}/#{@collection.field}/#{@collection.order}", true)
    #    @collection.requestPreviousPage()


  gotoFirst: (e) ->
    e.preventDefault()
    window.router.navigate("#{@collection.getPath()}/page/#{@collection.information.firstPage}/#{@collection.field}/#{@collection.order}", true)
    #    @collection.goTo @collection.information.firstPage

  gotoLast: (e) ->
    e.preventDefault()
    window.router.navigate("#{@collection.getPath()}/page/#{@collection.information.lastPage}/#{@collection.field}/#{@collection.order}", true)
    #    @collection.goTo @collection.information.lastPage

  gotoPage: (e) ->
    e.preventDefault()
    page = $(e.target).text()
    window.router.navigate("#{@collection.getPath()}/page/#{page}/#{@collection.field}/#{@collection.order}", true)
    #    @collection.goTo page

  changeCount: (e) ->
    e.preventDefault()
    per = $(e.target).text()
    @collection.howManyPer per