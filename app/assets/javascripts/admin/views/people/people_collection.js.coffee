class App.Views.PeopleCollection extends App.Views.SharedCollection

  id: "people"

  initialize: ->
    @itemView = App.Views.Person
    super