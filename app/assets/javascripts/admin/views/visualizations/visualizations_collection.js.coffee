class App.Views.VisualizationsCollection extends App.Views.BlockableCollection

  template: HoganTemplates['admin/visualizations/blockable_collection']

  id: "visualizations"

  initialize: ->
    @itemView = App.Views.Visualization
    super