// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//

//= require jquery
//= require jquery.spin
//= require jquery_ujs
//= require jquery-fileupload/basic
//= require jquery-fileupload/vendor/tmpl
//= require unobtrusive_flash
//= require i18n
//= require i18n/translations
//= require libs/i18n-support
//= require select2
//= require twitter/bootstrap
//= require cocoon
//= require ckeditor/init
//= require js-routes
//= require underscore
//= require backbone
//= require libs/backbone-forms/backbone-forms
//= require libs/backbone-forms/backbone-forms.editors
//= require hogan
//= require libs/backbone.paginator.min
//= require libs/backbone.syphon
//= require libs/backbone.syphon.patch
//= require_tree ./libs
//= require backbone-support
//= require account/app
//= require account/app.lib
//= require_tree ../templates/account
//= require_tree ./account/mixins
//= require_tree ./account/models
//= require_tree ./account/collections
//= require_tree ./account/components
//= require_tree ./account/views
//= require_tree ./account/routers