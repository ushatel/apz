#FileUpload
Backbone.Form.editors.FileUpload = Backbone.Form.editors.Base.extend({

  tagName: "div"

  className: "fileupload"

  events:
    "click .remove": "removeFile"

  initialize: (options) ->
    Backbone.Form.editors.Text::initialize.call this, options
    @uploadUrl = (if (not (options.schema.url?)) then "/api/images" else options.schema.url)
    @multipleValue = (if (not (options.schema.multipleValue?)) then true else options.schema.multipleValue)
    @uploadInputName = (if (not (options.schema.fileInputName?)) then "image[file]" else options.schema.fileInputName)
    @fileTypes = (if (not (options.schema.fileTypes?)) then /(\.|\/)(gif|jpe?g|png)$/i else options.schema.fileTypes)

    @errorMessage = (if (not (options.schema.errorMessage?)) then I18n.t("errors.messsages.not_image_format") else options.schema.errorMessage)

    @$hiddenField = $("<select multiple='multiple' style='display: none' name='#{@key}' />")
    @$fileInput = $("<input type=\"file\" " + ((if @multipleValue then "multiple=\"multiple\"" else "")) + " name=\"" + @uploadInputName + "\" data-url=\"" + @uploadUrl + "\"  />")
    @$errorsContainer = $("<div class=\"alert alert-error\">")
    @$progressBarsContainer = $("<div>")
    @$filesContainer = @getFilesContainer()

    @setValue JSON.stringify(@value) unless @value == null

  getValue: ->
    @value

  setValue: (value) ->
    @value = value

    @$hiddenField.html("")
    _.each $.unique(JSON.parse(@value)), (each_value) =>
      @$hiddenField.append($("<option value='#{each_value}' selected='selected'>#{each_value}</option>"))

  removeValue: (value) ->
    @setValue(JSON.stringify(_.without(JSON.parse(@value), value)))

  addValue: (value) ->
    newValue = []
    newValue = JSON.parse(@getValue()) if (@getValue()?) and (@getValue() isnt "")
    newValue.push value
    @setValue JSON.stringify(newValue)

  getFilesContainer: () ->
    $("<table class=\"table table-bordered table-striped\">")

  appendFileInput: () ->
    @$el.append @$fileInput

  render: (options) ->
    @appendFileInput()
    @$el.append @$hiddenField
    @$el.append @$progressBarsContainer

    @preloadFiles()

    @$fileInput.fileupload({

      dataType: "json"
      formData: @getAdditionalFormData()
      dropZone: @$el

      add: (e, data) =>
        @fileuploadAdd(e, data)

      progress: (e, data) =>
        @fileuploadProgress(e, data)

      done: (e, data) =>
        @fileuploadDone(e, data)

      error: (e, data, message) =>
        @fileuploadError(e, data, message)

      fail: (e, data, message) =>
        @fileuploadFail(e, data, message)

    })
    @

  getAdditionalFormData: () ->
    {}

  fileuploadAdd: (e, data) ->
    types = @fileTypes
    file = data.files[0]
    if types.test(file.type) or types.test(file.name)
      data.context = $("<div class=\"upload\">" + file.name + "<div class=\"progress progress-striped active\"><div class=\"bar\" style=\"width: 0%\"></div></div></div>")
      @$progressBarsContainer.append data.context
      data.submit()
    else
      @$el.append @$errorsContainer  if @$el.find(".alert-error").length is 0
      @$errorsContainer.append $("<div class=\"error\"><strong>" + file.name + "</strong> " + @errorMessage + "</div>")

  fileuploadProgress: (e, data) ->
    if data.context
      progress = parseInt(data.loaded / data.total * 100, 10)
      data.context.find(".bar").css "width", progress + "%"

  fileuploadDone: (e, data) ->
    @addValue data.result.id
    @$el.append @$filesContainer if @$el.find(".table").length is 0
    data.context.find(".progress").removeClass("active").addClass "progress-success"  if data.context
    data.context.remove()  if data.context
    @$filesContainer.append @getFileRow(data.result)
    $("#ajax-loader").animate({top: "-55px"}, 10)

  fileuploadError: (e, data, message) ->
    console.log message

  fileuploadFail: (e, data, message) ->
    data.context.find(".progress").removeClass("active").addClass "progress-danger"  if data.context

  preloadFiles: () ->
    files_key = @key.replace(/_id/, "")

    if _.isArray(@model.get(files_key))

      @$el.append @$filesContainer if @$el.find(".table").length is 0

      _.each @model.get(files_key), (file_item) =>
        @$filesContainer.append @getFileRow(file_item)

    else if !_.isUndefined(@model.get(files_key)) && !_.isNull(@model.get(files_key))

      @$el.append @$filesContainer if @$el.find(".table").length is 0

      @$filesContainer.append @getFileRow(@model.get(files_key))


  getFileRow: (file) ->
    result = $("<tr>")
    previewTd = $("<td>")
    removeTd = $("<td>")
    nameTd = $("<td>")

    previewImage = $("<img src=\"" + file.thumb_url + "\" alt=\"\"/>\"")
    nameTd.text(file.name)
    removeButton = $("<button class=\"btn btn-danger remove\" data-img=\"" + file.id + "\"><i class=\"icon-trash\"></i></button>")

    previewTd.append previewImage
    removeTd.append removeButton
    result.append previewTd
    result.append nameTd
    result.append removeTd
    result

  removeFile: (e) ->
    e.preventDefault()
    model = eval("App.Models.#{_.string.classify(_.string.camelize(@$fileInput.prop("name").replace(/\[.*\]/, "")))}")
    new model(id: $(e.currentTarget).data("img")).destroy()
    $(e.currentTarget).parents("td").parents("tr").remove()
    @removeValue($(e.currentTarget).data("img"))


})
#FileUpload

#SingleImageUpload
Backbone.Form.editors.SingleImageUpload = Backbone.Form.editors.Base.extend({

  tagName: "div"

  className: "fileupload"

  events:
    "click .remove": "removeFile"

  initialize: (options) ->
    Backbone.Form.editors.Text::initialize.call this, options

    @fileTypes = (if (not (options.schema.fileTypes?)) then /(\.|\/)(gif|jpe?g|png)$/i else options.schema.fileTypes)

    @errorMessage = @getErrorMessage(options)

    @$hiddenField = @getHiddenField()
    @$fileInput = @getFileInput()
    @$errorsContainer = $("<div class=\"alert alert-error\">")
    @$progressBarsContainer = $("<div>")
    @$filesContainer = @getFilesContainer()

    @preloadValue()

  getErrorMessage: (options) ->
    if (not (options.schema.errorMessage?)) then I18n.t("errors.messsages.not_image_format") else options.schema.errorMessage

  getHiddenField: () ->
    $("<input type='hidden' name='#{@key}' />")

  getFileInput: () ->
    $("<input type='file' name='image[file]' data-url='/api/images'  />")

  preloadValue: () ->
    @setValue(@value) unless @value == null

  getValue: ->
    @$hiddenField.val()

  setValue: (value) ->
    @$hiddenField.val(value)

  removeValue: (value) ->
    @setValue(null)

  addValue: (value) ->
    @setValue(value)

  getFilesContainer: () ->
    $("<table class=\"table table-bordered table-striped\">")

  appendFileInput: () ->
    @$el.append @$fileInput

  render: (options) ->
    @appendFileInput()
    @$el.append @$hiddenField
    @$el.append @$progressBarsContainer

    @preloadFiles()

    @$fileInput.fileupload({

      dataType: "json"
      formData: @getAdditionalFormData()
      dropZone: @$el

      add: (e, data) =>
        @fileuploadAdd(e, data)

      progress: (e, data) =>
        @fileuploadProgress(e, data)

      done: (e, data) =>
        @fileuploadDone(e, data)

      error: (e, data, message) =>
        @fileuploadError(e, data, message)

      fail: (e, data, message) =>
        @fileuploadFail(e, data, message)

    })
    @

  getAdditionalFormData: () ->
    {}

  fileuploadAdd: (e, data) ->
    types = @fileTypes
    file = data.files[0]
    if types.test(file.type) or types.test(file.name)
      data.context = $("<div class=\"upload\">" + file.name + "<div class=\"progress progress-striped active\"><div class=\"bar\" style=\"width: 0%\"></div></div></div>")
      @$progressBarsContainer.append data.context
      data.submit()
    else
      @$el.append @$errorsContainer  if @$el.find(".alert-error").length is 0
      @$errorsContainer.append $("<div class=\"error\"><strong>" + file.name + "</strong> " + @errorMessage + "</div>")

  fileuploadProgress: (e, data) ->
    if data.context
      progress = parseInt(data.loaded / data.total * 100, 10)
      data.context.find(".bar").css "width", progress + "%"

  fileuploadDone: (e, data) ->
    @addValue data.result.id
    @$el.append @$filesContainer if @$el.find(".table").length is 0
    data.context.find(".progress").removeClass("active").addClass "progress-success"  if data.context
    data.context.remove()  if data.context
    @$filesContainer.append @getFileRow(data.result)
    $("#ajax-loader").animate({top: "-55px"}, 10)

  fileuploadError: (e, data, message) ->
    console.log message

  fileuploadFail: (e, data, message) ->
    data.context.find(".progress").removeClass("active").addClass "progress-danger"  if data.context

  preloadFiles: () ->
    files_key = @key.replace(/_id/, "")

    if !_.isUndefined(@model.get(files_key)) && !_.isNull(@model.get(files_key))

      @$el.append @$filesContainer if @$el.find(".table").length is 0
      @$filesContainer.append @getFileRow(@model.get(files_key))


  getFileRow: (file) ->
    result = $("<tr>")
    previewTd = $("<td>")
    removeTd = $("<td>")
    nameTd = $("<td>")

    previewImage = $("<img src=\"" + file.file.thumb.url + "\" alt=\"\"/>\"")
    nameTd.text(file.name)
    removeButton = $("<button class=\"btn btn-danger remove\" data-img=\"" + file.id + "\"><i class=\"icon-trash\"></i></button>")

    previewTd.append previewImage
    removeTd.append removeButton
    result.append previewTd
    result.append nameTd
    result.append removeTd
    result

  removeFile: (e) ->
    e.preventDefault()
    model = eval("App.Models.#{_.string.classify(_.string.camelize(@$fileInput.prop("name").replace(/\[.*\]/, "")))}")
    new model(id: $(e.currentTarget).data("img")).destroy()
    $(e.currentTarget).parents("td").parents("tr").remove()
    @removeValue($(e.currentTarget).data("img"))

})
#SingleImageUpload

Backbone.Form.editors.SingleStaticImageUpload = Backbone.Form.editors.SingleImageUpload.extend({

  getFileInput: () ->
    $("<input type='file' name='static_image[file]' data-url='/api/static-images'  />")

  removeFile: (e) ->
    e.preventDefault()
    console.log "App.Models.#{_.string.classify(_.string.camelize(@$fileInput.prop("name").replace(/\[.*\]/, "")))}"
    model = eval("App.Models.#{_.string.classify(_.string.camelize(@$fileInput.prop("name").replace(/\[.*\]/, "")))}")
    new model(id: $(e.currentTarget).data("img")).destroy()
    $(e.currentTarget).parents("td").parents("tr").remove()
    @removeValue($(e.currentTarget).data("img"))

})

Backbone.Form.editors.SinglePersonImageUpload = Backbone.Form.editors.SingleImageUpload.extend({

  getFileInput: () ->
    $("<input type='file' name='person_image[file]' data-url='/api/person-images'  />")

  removeFile: (e) ->
    e.preventDefault()
    console.log "App.Models.#{_.string.classify(_.string.camelize(@$fileInput.prop("name").replace(/\[.*\]/, "")))}"
    model = eval("App.Models.#{_.string.classify(_.string.camelize(@$fileInput.prop("name").replace(/\[.*\]/, "")))}")
    new model(id: $(e.currentTarget).data("img")).destroy()
    $(e.currentTarget).parents("td").parents("tr").remove()
    @removeValue($(e.currentTarget).data("img"))

})

Backbone.Form.editors.MiniFileUpload = Backbone.Form.editors.FileUpload.extend({

  getFilesContainer: () ->
    $("<div class='files-container'>")

  appendFileInput: () ->
    fileInputButton = $("<div class='fileinput-button'>")
    fileInputButton.append($("<span class='fileinput-text'>#{@schema.title}</span>"))
    fileInputButton.append(@$fileInput)
    @$el.append fileInputButton

  getFileRow: (file) ->
    result = $("<div class='file'>")
    nameDiv = $("<div class='file-name'>")
    removeDiv = $("<div class='file-remove'>")

    nameDiv.text(file.name)
    removeButton = $("<button class='remove' data-img='#{file.id}'/>")

    removeDiv.append removeButton
    result.append nameDiv
    result.append removeDiv
    result

  removeFile: (e) ->
    Backbone.Form.editors.FileUpload::removeFile.call this, e
    $(e.currentTarget).parents(".file").remove()

})

Backbone.Form.editors.MiniFileUploadImmediateSave = Backbone.Form.editors.MiniFileUpload.extend({

  initialize: (options) ->
    Backbone.Form.editors.MiniFileUpload::initialize.call this, options

    @parentIdFieldKey = options.schema.parentIdFieldKey

  getAdditionalFormData: () ->
    result = Backbone.Form.editors.MiniFileUpload::getAdditionalFormData.call this

    result[@parentIdFieldKey] = @model.get('id')
    result

  appendFileInput: () ->
    Backbone.Form.editors.MiniFileUpload::appendFileInput.call this
    hiddenParentIdField = $("<input type='hidden' name='#{@parentIdFieldKey}' value='#{@model.get('id')}' />")
    @$el.append(hiddenParentIdField)

})

Backbone.Form.editors.BigFileUpload = Backbone.Form.editors.FileUpload.extend({

  getFilesContainer: () ->
    $("<div class='files-container'>")

  appendFileInput: () ->
    fileInputButton = $("<div class='fileinput-button'>")
    fileInputButton.append(@$fileInput)
    @$el.append fileInputButton

  getFileRow: (file) ->
    result = $("<div class='file'>")
    previewDiv = $("<div class='file-preview'>")
    nameDiv = $("<div class='file-name'>")
    removeDiv = $("<div class='file-remove'>")

    previewDiv.append($("<img src='#{file.big_thumb_url}' alt=''/>"))
    nameDiv.text(file.name)
    removeButton = $("<button class='remove' data-img='#{file.id}'/>")

    nameAndRemoveWrapper = $("<div class='file-name-remove-wrapper'>")
    nameAndRemoveWrapper.append nameDiv
    nameAndRemoveWrapper.append removeDiv

    removeDiv.append removeButton
    result.append previewDiv
    result.append nameAndRemoveWrapper

    result

  removeFile: (e) ->
    Backbone.Form.editors.FileUpload::removeFile.call this, e
    $(e.currentTarget).parents(".file").remove()

  fileuploadDone: (e, data) ->
    Backbone.Form.editors.FileUpload::fileuploadDone.call this, e, data
    @$(".fileinput-button").remove()

})

# CollapseRadio
Backbone.Form.editors.CollapseRadio = Backbone.Form.editors.Radio.extend({

  events: {
    "change input[type=radio]": 'toggleInput'
  }

  initialize: (options) ->
    Backbone.Form.editors.Radio::initialize.call this, options
    @$hiddenField = @getHiddenField()

  getHiddenField: () ->
    $("<input type='hidden' name='#{@key}' value=#{@getValue()} />")

  getValue: () ->
    @$('input[type=radio]:checked').val()

  setValue: (value) ->
    @$('input[type=radio]').val([value])
    @$hiddenField.val(value)

  toggleInput: (e) ->
    @setValue($(e.currentTarget).val())

  renderOptions: (options) ->
    @collection = options
    html = @renderItems(options)

    @$el.html(html)
    @$el.append(@$hiddenField)

    #Select correct option
    @setValue(@value)

  renderItems: (items, viewed = []) ->
    self = @

    output = $("<div id='accordion2' class='accordion'>")
    _.each items, (item) =>

      if ($.inArray(item, viewed) == -1) # not in viewed
        viewed.push(item)

        childItems = []
        _.each @collection, (childItem) =>

          childItems.push(childItem) if childItem.parent_id == item.val

        if _.size(childItems) > 0
          view = $("<div class='accordion-group'>")
          collapseId = item.slug
          view.append($("<div class='accordion-heading'>").append($("<a href='#collapse#{collapseId}' data-toggle='collapse' data-parent='#accordion2'>#{item.label}</a>")))
        else
          view = $("<div class='child-category'>#{@getMainControl(self.id, item.val)}<label for='#{self.id}'>#{item.label}</label></div>")

        if _.size(childItems) > 0
          itemsHTML = @renderItems(childItems, viewed)
          show_in = false
          show_in = true if viewed.length < 10
          _.each childItems, (childItem) ->
            show_in = true if childItem.val == self.value
          html = view.append($("<div id='collapse#{collapseId}' class='accordion-body collapse #{'in' if show_in}'><div class='accordion-inner'>#{itemsHTML.html()}</div></div>"))
          output.append html
        else
          output.append view

    output

  getMainControl: (self_id, item_val) ->
    "<input type='radio' name='#{self_id}' value='#{item_val}'/>"

})
# CollapseRadio

Backbone.Form.editors.CollapseCheckboxes = Backbone.Form.editors.CollapseRadio.extend({

  events: {
    "change input[type=checkbox]": 'toggleInput'
  }

  getHiddenField: () ->
    $("<select multiple='multiple' style='display: none' name='#{@key}' />")

  getMainControl: (self_id, item_val) ->
    "<input type='checkbox' name='#{self_id}' value='#{item_val}'/>"

  toggleInput: (e) ->

    values = []
    @$("input[type=checkbox]:checked").each (index, checked_input) ->
      values.push($(checked_input).val())

    @setValue(values)

  setValue: (value) ->

    @value = if _.isObject(value) then JSON.stringify(value) else value

    @$hiddenField.html("")

    all_values = JSON.parse(@value)

    if all_values?
      _.each $.unique(all_values), (each_value) =>
        @$hiddenField.append($("<option value='#{each_value}' selected='selected'>#{each_value}</option>"))
        @$("input[value='#{each_value}']").attr('checked', true) if _.isUndefined(@$("input[value='#{each_value}']").attr('checked'))

})

Backbone.Form.editors.TreeCheckboxes = Backbone.Form.editors.CollapseRadio.extend({

  renderOptions: (options) ->
    @collection = options
    html = @renderItems(options)

    @$el.html(html)
    @$el.append(@$hiddenField)


    @setValue(@value)

  renderItems: (items, viewed = []) ->
    self = @

    output = $("<div class='tree-checkboxes'>")
    _.each items, (item) =>

      if ($.inArray(item, viewed) == -1) # not in viewed
        viewed.push(item)

        childItems = []
        _.each @collection, (childItem) =>

          childItems.push(childItem) if childItem.parent_id == item.val

        if _.size(childItems) > 0
          view = $("<div class='tree-checkboxes-group'>")
          collapseId = item.slug
          view.append($("<div class='tree-checkboxes-heading'>").text(item.label))
        else
          view = $("<div class='tree-checkboxes-item'><input type='checkbox' name='#{self.id}' value='#{item.val}'/><label for='#{self.id}'>#{item.label}</label></div>")

        if _.size(childItems) > 0
          itemsHTML = @renderItems(childItems, viewed)
          show_in = false
          show_in = true if viewed.length < 10
          _.each childItems, (childItem) ->
            show_in = true if childItem.val == self.value
          html = view.append(itemsHTML.html())
          output.append html
        else
          output.append view

    output

})

# CustomRadios
Backbone.Form.editors.CustomRadios = Backbone.Form.editors.Radio.extend({

  events: {
    "change input[type=radio]": 'toggleInput'
  }

  initialize: (options) ->
    Backbone.Form.editors.Radio::initialize.call this, options
    @$hiddenField = $("<input type='hidden' name='#{@key}' value=#{@getValue()} />")
    @$el.removeAttr('name')

  setValue: (value) ->
    @$('input[type=radio]').val([value])
    @$hiddenField.val(value)

  toggleInput: (e) ->
    @setValue($(e.currentTarget).val())

  renderOptions: (options) ->
    Backbone.Form.editors.Radio::renderOptions.call this, options
    @$el.append(@$hiddenField)

    #Select correct option
    @setValue(@value)

})
# CustomRadios

# CustomCheckboxes
Backbone.Form.editors.CustomCheckboxes = Backbone.Form.editors.Checkboxes.extend({

  events: {
    "change input[type=checkbox]": 'toggleInput'
  }

  initialize: (options) ->
    Backbone.Form.editors.Checkboxes::initialize.call this, options
    @$hiddenField = $("<select multiple='multiple' style='display: none' name='#{@key}' />")
    @$el.removeAttr('name')

  toggleInput: (e) ->

    values = []
    @$("input[type=checkbox]:checked").each (index, checked_input) ->
      values.push($(checked_input).val())

    @setValue(values)

  getValue: ->
    @value


  setValue: (value) ->
    @value = JSON.stringify(value)

    @$hiddenField.html("")
    all_values = JSON.parse(@value)

    if all_values?
      _.each $.unique(all_values), (each_value) =>
        @$hiddenField.append($("<option value='#{each_value}' selected='selected'>#{each_value}</option>"))
        @$("input[value='#{each_value}']").attr('checked', true) if _.isUndefined(@$("input[value='#{each_value}']").attr('checked'))

  render: (options) ->
    Backbone.Form.editors.Checkboxes::render.call this, options

    @$el.append @$hiddenField
    @

})
# CustomCheckboxes

#ColorCheckboxes
Backbone.Form.editors.ColorCheckboxes = Backbone.Form.editors.CustomCheckboxes.extend({

  _arrayToHtml: (array) ->
    html = []
    self = this
    _.each array, (option, index) ->
      itemHtml = "<li>"
      if _.isObject(option)
        val = (if (option.val or option.val is 0) then option.val else "")
        itemHtml += ("<input type=\"checkbox\" name=\"" + self.id + "\" value=\"" + val + "\" id=\"" + self.id + "-" + index + "\" />")
        itemHtml += ("<label for='#{self.id}' class='color' style='background: #{option.label}'></span>")
      else
        itemHtml += ("<input type=\"checkbox\" name=\"" + self.id + "\" value=\"" + option + "\" id=\"" + self.id + "-" + index + "\" />")
        itemHtml += ("<label for='#{self.id}' class='color' style='background: #{option.label}'></span>")
      itemHtml += "</li>"
      html.push itemHtml
      return

    html.join ""

})
#ColorCheckboxes

Backbone.Form.editors.Select2 = Backbone.Form.editors.Select.extend({

  render: (options) ->
    Backbone.Form.editors.Select::render.call this, options

    @

})

Backbone.Form.editors.Select2FromCategories = Backbone.Form.editors.Text.extend({

  initialize: (options) ->
    Backbone.Form.editors.Text::initialize.call this, options

    @section_slug = (if (not (options.schema.section_slug?)) then (throw 'section_slug must be specified') else options.schema.section_slug)
    @collection = (if (not (options.schema.collection?)) then (new App.Collections.CategoriesPaginatedCollection({sectionSlug: @section_slug})) else options.schema.collection)

    @$el.select2({
      ajax: {
        dataType: 'json'

        url: @collection.getPath()

        data: (term, page) ->
          {q: term, page_limit: 10, page: page}

        results: (data, page) ->
          more = (page * 10) < data.total
          {results: data.items, more: more}
      }

      id: (item) ->
        item._id unless (typeof(item) == 'undefined')

      formatResult: (category) ->
        markup = "<table class='category-result'><tr>"
        markup += "<td class='category-info'><div class='category-name'>#{category.name}</div></td>"
        markup += "</tr></table>"
        markup

      formatSelection: (category) ->
        category.name
    })

})

Backbone.Form.editors.TextAreaPreview = Backbone.Form.editors.TextArea.extend({

  tagName: "div"

  initialize: (options) ->
    Backbone.Form.editors.TextArea::initialize.call this, options

    @$pencil = $("<div class='pencil' style='display: none;'>")
    @$textArea = $("<textarea name='#{@key}' class='textarea'>#{@value}</textarea>")
    @$valueContainer = $("<div class='value-container' style='display: none'>")

    @$textArea.on "blur", () =>
      @toggleTextarea(@)

    @$pencil.on "click", () =>
      @toggleTextarea(@)

  toggleTextarea: (viewModel) =>
    viewModel.$textArea.toggle()
    viewModel.$valueContainer.toggle()
    viewModel.$valueContainer.text(viewModel.getValue())
    viewModel.$pencil.toggle()

  setValue: (value) ->
    @$textArea.val(value)

  getValue: () ->
    @$textArea.val()

  render: () ->
    @setValue(@value)

    @$el.append(@$pencil)
    @$el.append(@$textArea)
    @$el.append(@$valueContainer)

    @

})

Backbone.Form.editors.TextPreview = Backbone.Form.editors.Text.extend({

  tagName: "div"

  initialize: (options) ->
    Backbone.Form.editors.Text::initialize.call this, options

    @$pencil = $("<div class='pencil' style='display: none;'>")
    @$textField = $("<input type='text' name='#{@key}' class='#{@key}'/>")
    @$valueContainer = $("<div class='value-container' style='display: none'>")

    @$textField.on "blur", () =>
      @toggleTextField(@)

    @$pencil.on "click", () =>
      @toggleTextField(@)

  toggleTextField: (viewModel) =>
    viewModel.$textField.toggle()
    viewModel.$valueContainer.toggle()
    viewModel.$valueContainer.text(viewModel.getValue())
    viewModel.$pencil.toggle()

  setValue: (value) ->
    @$textField.val(value)

  getValue: () ->
    @$textField.val()

  render: () ->
    @setValue(@value)

    @$el.append(@$textField)
    @$el.append(@$valueContainer)
    @$el.append(@$pencil)

    @

})

Backbone.Form.editors.Select2FromCollection = Backbone.Form.editors.Select.extend({

  tagName: "div"

  initialize: (options) ->
    Backbone.Form.editors.Select::initialize.call this, options

    @options = options

    @url = if _.isUndefined(@options.schema.url) then @options.schema.options.paginator_core.url else @options.schema.url
    @placeholder = if _.isUndefined(@options.schema.placeholder) then @options.schema.title else @options.schema.placeholder

    @$inputHidden = @getInputHidden()
    @$addButton = $("<div class='add-button'>")

    @$addButton.on "click", () =>
      @showCreateRecordForm(@)

  getInputHidden: () ->
    $("<input type='hidden' value='#{@value}' name='#{@key}'/>")

  getValue: () ->
    null

  setValue: (value) ->
    null

  render: (options) ->

    @$el.append(@$inputHidden)
    @$el.append(@$addButton) if _.isUndefined(@options.schema.add)

    @$inputHidden.select2(

      ajax: {
        url: @url
        dataType: 'json'

        data: (term, page) ->
          {q: term, per: 10, page: page}

        results: (data, page) ->
          more = (page * 10) < data.total
          {results: data.items, more: more}
      }
      placeholder: @placeholder
      allowClear: true

      initSelection: (element, callback) =>
        id = @$(element).val()

        if (id != "") && (id != "null") && (id != null)
          $.ajax("#{@url}/#{id}", {dataType: "json"}).done (data) -> callback(data)

      formatResult: (record) ->
        markup = "<table class='record-result'><tr>"
        markup += "<td class='record-info'><div class='tag-name'>#{record.name}</div>"
        markup += "</td></tr></table>"
        markup

      formatSelection: (record) ->
        record.name

      id: (record) ->
        record.id
    )

    @

  showCreateRecordForm: (currentView) ->

    class CreateRecordFormView extends App.View

      className: "add-record-form-wrapper"

      events: {
        "click .add-record": "addRecordButtonClick"
      }

      addRecordButtonClick: () ->
        attributes = Backbone.Syphon.serialize(@)
        model = new currentView.options.schema.options.model

        model.sectionSlug = currentView.options.schema.options.sectionSlug unless _.isUndefined(currentView.options.schema.options.sectionSlug)

        model.save(attributes, {
          wait: true
          success: (model) =>
            @prepend_and_fade_alert($('.flash-messages'), I18n.t("words.record_created"), 'success')
            $.colorbox.close()

          error: (model, response) =>
            # clear all previous errors
            $('.bbf-error .error').each ->
              $(this).removeClass('error')

            $('.bbf-help').each ->
              $(this).empty()
            # mark all fields with errors and show error text
            all_errors = JSON.parse(response.responseText).errors
            for attribute_name, attribute_errors of all_errors

              attribute_container = @$('input[name=' + attribute_name + ']')
              attribute_container.closest('.bbf-field').addClass('bbf-error')
              attribute_container.closest('.bbf-field').find('.bbf-error').html(attribute_errors.join(', '))

              div_attribute_container = @$('div[name=' + attribute_name + ']')
              div_attribute_container.closest('.bbf-field').addClass('bbf-error')
              div_attribute_container.closest('.bbf-field').find('.bbf-error').html(attribute_errors.join(', '))

            # show corresponding alert
            @prepend_and_fade_alert($('.flash-messages'), I18n.t("words.record_creating_errors"), 'error')
        })

      render: () ->
        form = new Backbone.Form({model: new currentView.options.schema.options.model}).render().el
        addRecordButton = $("<button class='btn btn-success add-record'>#{I18n.t("words.add")}</button>")

        @$el.append(form)
        @$el.append(addRecordButton)

        @

    $.colorbox({html: new CreateRecordFormView().render().el, width: "50%"})

})

Backbone.Form.editors.HABTMSelect2FromCollection = Backbone.Form.editors.Select2FromCollection.extend({

  getInputHidden: () ->
    $("<input>", {type: 'hidden', value: @value, name: @key, multiple: 'multiple'})

  render: (options) ->

    @$el.append(@$inputHidden)
    @$el.append(@$addButton) if _.isUndefined(@options.schema.add)

    @$inputHidden.select2(

      multiple: true

      ajax: {
        url: @url
        dataType: 'json'

        data: (term, page) ->
          {q: term, per: 10, page: page}

        results: (data, page) ->
          more = (page * 10) < data.total
          {results: data.items, more: more}
      }
      placeholder: @placeholder
      allowClear: true

      initSelection: (element, callback) =>
        data = []
        _.each @model.get(@key.replace('_ids', '') + 's'), (eachValue) => data.push(eachValue)
        callback(data)

      formatResult: (record) ->
        markup = "<table class='record-result'><tr>"
        markup += "<td class='record-info'><div class='tag-name'>#{record.name}</div>"
        markup += "</td></tr></table>"
        markup

      formatSelection: (record) ->
        console.log record
        record.name

      id: (record) ->
        record.id
    )

    @



})