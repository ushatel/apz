I18n.t = (scope, options) ->
  val = I18n.translate(scope, options)


  if /\["(.*)"]/.test(val)
    val.replace(/\["(.*)"]/, '$1')
  else
    val