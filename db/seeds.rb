# encoding: utf-8

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#anonymous_role = Role.create(:name => "Anonymous")
#
#read_all_permission = Permission.create(:action => "read", :subject_class => "all")
#anonymous_role.permissions << read_all_permission
#anonymous_role.save
#
#user = User.new(:login => "vpolotskiy", :password => "123654", :email => "vpolotskiy@gmail.com")
#
#admin_role = Role.create(:name => "Admin")
#manage_all_permission = Permission.create(:action => "manage", :subject_class => "all")
#admin_access_permission = Permission.create(:action => "admin_access", :subject_class => "Admin")
#admin_role.permissions << manage_all_permission
#admin_role.permissions << admin_access_permission
#admin_role.save
#user.roles << admin_role
#user.save
#user.confirm!
#
#Settings.anonymous_role_id = anonymous_role.id
#Settings.authorized_role_id = Role.create(:name => "Authorized").id
#Settings.notify_delay_time = 5000
#Settings.base_seo_settings = {}
#Settings.date_time_format = "%d/%m/%Y %H:%M"
#
#Settings.notify_delay_time = 3000

#Settings.vk_id = 3261496
#Settings.vk_comments_count = 20


#Settings.devise_confirmation_instructions_message = "<p>\tДобро пожаловать, @email !</p><p>\tВы можете активировать свой аккаунт по следующей ссылке:</p><p>\t@link(Активировать аккаунт)</p>"
#Settings.devise_reset_password_instructions_message = "<p>\r\n\tЗдравствуйте, @email !</p>\r\n<p>\r\n\tБыл получен запрос на изменение пароля. Вы можете это сделать пройдя по следующей ссылке:</p>\r\n<p>\r\n\t@link(Сменить пароль)</p>\r\n<p>\r\n\tЕсли вы не подавали такой запрос, проигнорируйте это письмо.</p>\r\n<p>\r\n\tВаш пароль не будет изменен, пока вы не воспользуетесь этой ссылкой.</p>\r\n"
#Settings.devise_unlock_instructions_message = "<p>\r\n\tЗдравствуйте, @email !</p>\r\n<p>\r\n\tВаш аккаунт был заблокирован.</p>\r\n<p>\r\n\tНажмите на следующую ссылку, чтобы разблокировать ваш аккаунт:</p>\r\n<p>\r\n\t@link(Разблокировать аккаунт)</p>\r\n"



I18n.locale = :ru
#
#
#embedded = [EmbeddedImage]
#nested = [Image]
#nodes = [Node, Article, Novelty, Page]
#structure = [Block, Category, Link, Menu, Section]
#users = [Permission, Role, User]
#other = [FeedBack, Message]
#
#all = embedded + nested + nodes + structure + users + other


I18n.locale = :ru
models = [Node, Page, Portfolio, Faq, Person, Slide, Article, Facility, Scene, Project, Visualization, FurniturePlan, CeilingPlan, LightingPlan, CoveringsPlan, CoveringsTexture, WallsPlan, WallsTexture, Image, MaxFile, Menu, Link, Section, Category, Block, Permission, Role, User, Discount]
models.each do |model|
  %w"all plural my new add added add_error edit update updated update_error deleted".each do |value|
    if I18n.t("models.#{model.to_s.underscore.pluralize}.#{value}") =~ /translation/
      puts I18n.t("models.#{model.to_s.underscore.pluralize}.#{value}")
    end
  end
end

#Portfolio.all.each_with_index do |img, index|
#  puts index
#  puts img.id
#  puts img.file_url
#
#  begin
#    img.file.recreate_versions!
#  rescue
#    puts "#{index} #{img.id} - Failed"
#  end
#end
#
#TextureFile.all.each_with_index do |img, index|
#  puts index
#  puts img.file.class
#
#  begin
#    img.file.recreate_versions!
#  rescue
#    puts "#{index} #{img.id} - Failed"
#  end
#end

#[Facility, Project, Visualization].each do |model|
#  model.all.each do |record|
#    serializer = record.active_model_serializer.new record
#    # This wil cache the JSON and the hash it's generated from
#    serializer.to_json
#  end
#end

#Benchmark.measure do
#  500.times do
#    key = (0...50).map{ ('a'..'z').to_a[rand(26)] }.join
#    tag = (0...50).map{ ('a'..'z').to_a[rand(26)] }.join
#    tag2 = (0...50).map{ ('a'..'z').to_a[rand(26)] }.join
#    Cashier.store_fragment(key, tag, tag2)
#  end
#end

#[Facility, Project, Visualization].each do |model|
#  model.all.each do |record|
#    CacheWorker.perform_async(model.name.to_s, record.id.to_s)
#  end
#end


path = "/Users/vpolotskiy/www/archivizer.com.local/trasnlations_file.json"
#path = "/var/www/ruby/data/www/sweb-design.com/trasnlations_file.json"

#File.open(path, "r") do |infile|
#  while (line = infile.gets)
#    pieces = line.split(":====:")
#    key = pieces[0]
#    value = pieces[1]
#    puts key
#    puts value
#    #puts JSON.parse(value).first
#    I18n.backend.store_translations("ru", {key => value.gsub(/\r\n$/, "")})
#    #I18n.backend.store_translations("ru", Settings.get_hash_form_key(key, value.gsub(/\r\n$/, "")))
#  end
#end
#
#File.open("/Users/vpolotskiy/www/archivizer.com.local/trasnlations.json", "r") do |infile|
#  index = 1
#  while (line = infile.gets)
#    unless line =~ /"value":"\[\\"(.*)\\"]"/
#      puts "#{index} - #{line}"
#      index = index + 1
#    end
#  end
#end

#MaxFile.all.each_with_index do |max_file, index|
#  unless max_file.file.nil?
#    max_file.compute_hash
#    max_file.save
#  end
#
#  puts "#{index} - #{max_file.id.to_s} - #{max_file.md5hash.to_s}"
#end

#Portfolio.all.each_with_index do |portfolio, index|
#  unless portfolio.file.nil?
#    begin
#      portfolio.file.recreate_versions!
#    rescue
#
#    end
#  end
#
#  puts "#{index} - #{portfolio.id.to_s}"
#end

#PlanFile.all.each_with_index do |image, index|
#  unless image.file.nil?
#    begin
#      image.file.recreate_versions!
#    rescue
#
#    end
#  end
#
#  puts index
#end

#[Project, User, Visualization, Image].each do |model_name|
#
#  model_name.all.each do |record|
#    begin
#      record.touch
#    rescue
#      puts "#{record} - #{record.id.to_s}"
#    end
#  end
#end

#section = Section.create(:name => "Назначения помещений")
#
#section.categories << Category.new(:name => "Гостиная")
#section.categories << Category.new(:name => "Столовая")
#section.categories << Category.new(:name => "Кухня")
#section.categories << Category.new(:name => "Студия")
#section.categories << Category.new(:name => "Спальня")
#section.categories << Category.new(:name => "Детская")
#section.categories << Category.new(:name => "Хол")
#section.categories << Category.new(:name => "Коридор")
#section.categories << Category.new(:name => "Санузел")
#section.categories << Category.new(:name => "Ванная комната")
#section.categories << Category.new(:name => "Кабинет")
#section.categories << Category.new(:name => "Лестница")
#section.categories << Category.new(:name => "Гардероб")
#
#section.save

#client = Twitter::Tweet.new do |config|
#  config.consumer_key        = "a2hF3mieS5gdRtx3K54Mg"
#  config.consumer_secret     = "3qFVzI77u3YEPRlJnJ356LX3mjqu6gXXSpGKNXbyZY"
#  config.access_token        = "56631606-mecQJQmict9XbkiY7KBXM0y9kuw8karXoGKALqHbH"
#  config.access_token_secret = "RDIjYZDTbtc4NnKifny51rwZkEmC9PeMMzJW9z8c1GeU4"
#end
#client = Twitter::REST::Client.new do |config|
#  config.consumer_key    = "a2hF3mieS5gdRtx3K54Mg"
#  config.consumer_secret = "3qFVzI77u3YEPRlJnJ356LX3mjqu6gXXSpGKNXbyZY"
#end

#puts client.statuses.user_timeline


#require 'flickraw'
#
#FlickRaw.api_key = "16eb344403c951a5e43c3ac0b101f141"
#FlickRaw.shared_secret = "d044f5c836bbb8a7"
#
#photos = flickr.photos.search(user_id: flickr.people.findByUsername(username: 'archivizer_com').nsid)
#
#puts FlickRaw.url_b(photos[0])


#list   = flickr.photos.archivizer
#
#id     = list[0].id
#secret = list[0].secret
##info = flickr.photos.getInfo :photo_id => id, :secret => secret
#info = flickr.photos.getInfo(:photo_id => id)
#
##puts info.title           # => "PICT986"
##puts info.dates.taken     # => "2006-07-06 15:16:18"
#puts FlickRaw.url_b(info)

# User.all.each do |user|
#
#   puts "#{user.name} - #{user.surname}"
#
# end

Task.all.each do |task|

  puts task.id
  puts task.users_in_discussions.size

  task.discussions.each do |discussion|
    discussion.discussion_messages.each do |discussion_message|
      discussion_message.save
    end
  end

  puts task.users_in_discussions.size

end

#dates = {}
#
#0.upto(20) do |i|
#
#  c = Curl::Easy.new("https://bitbucket.org/api/2.0/repositories/vpolotskiy/apz/commits?page=#{i}")
#  c.http_auth_types = :basic
#  c.username = 'vpolotskiy'
#  c.password = 'pFO20JdxT0NTDj7Ldp5X'
#  c.perform
#
#  response = JSON.parse(c.body_str)
#
#  unless response['values'].nil?
#    response['values'].each do |commit|
#      dates[DateTime.parse(commit['date']).to_date] ||= []
#      dates[DateTime.parse(commit['date']).to_date] << DateTime.parse(commit['date']).to_i
#    end
#  end
#end
#
#dates.each do |each_date, values|
#  #puts "#{each_date} - #{values}"
#  #puts each_date
#  #puts each_date.class
#  #puts values
#  puts "#{Time.at(values.min).strftime(Settings.date_time_format)} \t #{Time.at(values.max).strftime(Settings.date_time_format)} \t #{((Time.at(values.max) - Time.at(values.min)) / 1.hour).round}"
#end

# Scene.all.each_with_index do |facility, facility_index|
#
#   if facility.images.size > 0
#     facility.images.each do |image|
#       puts "#{facility.id} - #{facility_index}"
#
#       begin
#         image.file.recreate_versions!
#       rescue
#         puts "#{facility_index} #{image.id} - Failed"
#       end
#     end
#   end
#
# end


