# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "mongoid-app_settings"
  s.version = "1.1.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Marten Veldthuis"]
  s.date = "2013-07-23"
  s.description = "Mongoid::AppSettings allows you to store settings in MongoDB, and access them easily"
  s.email = "marten@veldthuis.com"
  s.files = [".document", ".gitignore", ".rspec", ".travis.yml", "Appraisals", "Gemfile", "Gemfile.lock", "LICENSE.txt", "README.markdown", "Rakefile", "gemfiles/mongoid2.gemfile", "gemfiles/mongoid2.gemfile.lock", "gemfiles/mongoid3.gemfile", "gemfiles/mongoid3.gemfile.lock", "lib/mongoid-app_settings.rb", "lib/mongoid/app_settings/version.rb", "mongoid-app_settings.gemspec", "spec/mongoid-app_settings_spec.rb", "spec/mongoid2.yml", "spec/mongoid3.yml", "spec/spec_helper.rb"]
  s.homepage = "http://github.com/marten/mongoid-app_settings"
  s.licenses = ["MIT"]
  s.require_paths = ["lib"]
  s.rubygems_version = "1.8.25"
  s.summary = "Store settings for your application in MongoDB"
  s.test_files = ["spec/mongoid-app_settings_spec.rb", "spec/mongoid2.yml", "spec/mongoid3.yml", "spec/spec_helper.rb"]

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<mongoid>, [">= 2.0"])
      s.add_runtime_dependency(%q<activesupport>, [">= 0"])
      s.add_development_dependency(%q<database_cleaner>, ["~> 0.9"])
      s.add_development_dependency(%q<rspec>, ["~> 2.11"])
      s.add_development_dependency(%q<appraisal>, [">= 0"])
    else
      s.add_dependency(%q<mongoid>, [">= 2.0"])
      s.add_dependency(%q<activesupport>, [">= 0"])
      s.add_dependency(%q<database_cleaner>, ["~> 0.9"])
      s.add_dependency(%q<rspec>, ["~> 2.11"])
      s.add_dependency(%q<appraisal>, [">= 0"])
    end
  else
    s.add_dependency(%q<mongoid>, [">= 2.0"])
    s.add_dependency(%q<activesupport>, [">= 0"])
    s.add_dependency(%q<database_cleaner>, ["~> 0.9"])
    s.add_dependency(%q<rspec>, ["~> 2.11"])
    s.add_dependency(%q<appraisal>, [">= 0"])
  end
end
