# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "carrierwave"
  s.version = "0.8.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Jonas Nicklas"]
  s.date = "2013-07-23"
  s.description = "Upload files in your Ruby applications, map them to a range of ORMs, store them on different backends."
  s.email = ["jonas.nicklas@gmail.com"]
  s.extra_rdoc_files = ["README.md"]
  s.files = ["lib/generators", "lib/generators/templates", "lib/generators/templates/uploader.rb", "lib/generators/uploader_generator.rb", "lib/carrierwave.rb", "lib/carrierwave", "lib/carrierwave/storage", "lib/carrierwave/storage/abstract.rb", "lib/carrierwave/storage/fog.rb", "lib/carrierwave/storage/file.rb", "lib/carrierwave/test", "lib/carrierwave/test/matchers.rb", "lib/carrierwave/error.rb", "lib/carrierwave/mount.rb", "lib/carrierwave/uploader.rb", "lib/carrierwave/compatibility", "lib/carrierwave/compatibility/paperclip.rb", "lib/carrierwave/uploader", "lib/carrierwave/uploader/default_url.rb", "lib/carrierwave/uploader/url.rb", "lib/carrierwave/uploader/extension_blacklist.rb", "lib/carrierwave/uploader/callbacks.rb", "lib/carrierwave/uploader/versions.rb", "lib/carrierwave/uploader/store.rb", "lib/carrierwave/uploader/serialization.rb", "lib/carrierwave/uploader/cache.rb", "lib/carrierwave/uploader/configuration.rb", "lib/carrierwave/uploader/mountable.rb", "lib/carrierwave/uploader/proxy.rb", "lib/carrierwave/uploader/extension_whitelist.rb", "lib/carrierwave/uploader/remove.rb", "lib/carrierwave/uploader/download.rb", "lib/carrierwave/uploader/processing.rb", "lib/carrierwave/version.rb", "lib/carrierwave/orm", "lib/carrierwave/orm/activerecord.rb", "lib/carrierwave/sanitized_file.rb", "lib/carrierwave/storage.rb", "lib/carrierwave/locale", "lib/carrierwave/locale/en.yml", "lib/carrierwave/validations", "lib/carrierwave/validations/active_model.rb", "lib/carrierwave/processing", "lib/carrierwave/processing/rmagick.rb", "lib/carrierwave/processing/mime_types.rb", "lib/carrierwave/processing/mini_magick.rb", "lib/carrierwave/processing.rb", "README.md"]
  s.homepage = "https://github.com/jnicklas/carrierwave"
  s.rdoc_options = ["--main"]
  s.require_paths = ["lib"]
  s.rubyforge_project = "carrierwave"
  s.rubygems_version = "1.8.25"
  s.summary = "Ruby file upload library"

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<activesupport>, [">= 3.2.0"])
      s.add_runtime_dependency(%q<activemodel>, [">= 3.2.0"])
      s.add_development_dependency(%q<mysql2>, [">= 0"])
      s.add_development_dependency(%q<rails>, [">= 3.2.0"])
      s.add_development_dependency(%q<cucumber>, ["~> 1.1.4"])
      s.add_development_dependency(%q<json>, [">= 0"])
      s.add_development_dependency(%q<rspec>, ["~> 2.12.0"])
      s.add_development_dependency(%q<sham_rack>, [">= 0"])
      s.add_development_dependency(%q<timecop>, [">= 0"])
      s.add_development_dependency(%q<fog>, [">= 1.3.1"])
      s.add_development_dependency(%q<mini_magick>, [">= 0"])
      s.add_development_dependency(%q<rmagick>, [">= 0"])
    else
      s.add_dependency(%q<activesupport>, [">= 3.2.0"])
      s.add_dependency(%q<activemodel>, [">= 3.2.0"])
      s.add_dependency(%q<mysql2>, [">= 0"])
      s.add_dependency(%q<rails>, [">= 3.2.0"])
      s.add_dependency(%q<cucumber>, ["~> 1.1.4"])
      s.add_dependency(%q<json>, [">= 0"])
      s.add_dependency(%q<rspec>, ["~> 2.12.0"])
      s.add_dependency(%q<sham_rack>, [">= 0"])
      s.add_dependency(%q<timecop>, [">= 0"])
      s.add_dependency(%q<fog>, [">= 1.3.1"])
      s.add_dependency(%q<mini_magick>, [">= 0"])
      s.add_dependency(%q<rmagick>, [">= 0"])
    end
  else
    s.add_dependency(%q<activesupport>, [">= 3.2.0"])
    s.add_dependency(%q<activemodel>, [">= 3.2.0"])
    s.add_dependency(%q<mysql2>, [">= 0"])
    s.add_dependency(%q<rails>, [">= 3.2.0"])
    s.add_dependency(%q<cucumber>, ["~> 1.1.4"])
    s.add_dependency(%q<json>, [">= 0"])
    s.add_dependency(%q<rspec>, ["~> 2.12.0"])
    s.add_dependency(%q<sham_rack>, [">= 0"])
    s.add_dependency(%q<timecop>, [">= 0"])
    s.add_dependency(%q<fog>, [">= 1.3.1"])
    s.add_dependency(%q<mini_magick>, [">= 0"])
    s.add_dependency(%q<rmagick>, [">= 0"])
  end
end
