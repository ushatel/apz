# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "smt_rails"
  s.version = "0.2.4"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Alexey Vasiliev", "Alexander Chaplinsky"]
  s.date = "2013-07-23"
  s.description = "Shared mustache templates for rails 3"
  s.email = ["contacts@railsware.com"]
  s.extra_rdoc_files = ["LICENSE", "README.md"]
  s.files = [".gitignore", "Gemfile", "LICENSE", "README.md", "Rakefile", "lib/generators/smt_rails/helpers.rb", "lib/generators/smt_rails/install/install_generator.rb", "lib/smt_rails.rb", "lib/smt_rails/config.rb", "lib/smt_rails/engine.rb", "lib/smt_rails/mustache.rb", "lib/smt_rails/tilt.rb", "lib/smt_rails/version.rb", "smt_rails.gemspec", "vendor/assets/javascripts/mustache.js"]
  s.homepage = "https://github.com/railsware/smt_rails"
  s.rdoc_options = ["--charset=UTF-8"]
  s.require_paths = ["lib"]
  s.rubygems_version = "1.8.25"
  s.summary = "Shared mustache templates for rails 3"

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rails>, [">= 3.1.0"])
      s.add_runtime_dependency(%q<tilt>, [">= 1.3.3"])
      s.add_runtime_dependency(%q<sprockets>, [">= 2.0.3"])
      s.add_runtime_dependency(%q<mustache>, [">= 0.99.4"])
    else
      s.add_dependency(%q<rails>, [">= 3.1.0"])
      s.add_dependency(%q<tilt>, [">= 1.3.3"])
      s.add_dependency(%q<sprockets>, [">= 2.0.3"])
      s.add_dependency(%q<mustache>, [">= 0.99.4"])
    end
  else
    s.add_dependency(%q<rails>, [">= 3.1.0"])
    s.add_dependency(%q<tilt>, [">= 1.3.3"])
    s.add_dependency(%q<sprockets>, [">= 2.0.3"])
    s.add_dependency(%q<mustache>, [">= 0.99.4"])
  end
end
