class CacheGenerator < Rails::Generators::Base

  def generate_cache_facilities
    Facility.all.each do |facility|
      CacheWorker.perform_async(Facility, facility.id.to_s)
    end

  end


  private

  def model
    model_name.singularize.underscore
  end

  def models
    model_name.pluralize.underscore
  end

  def up_model
    model_name.singularize.camelize
  end

  def up_models
    model_name.pluralize.camelize
  end

  def l_up_model
    model_name.underscore.camelize(:lower)
  end

  def l_up_models
    model_name.pluralize.underscore.camelize(:lower)
  end

end
