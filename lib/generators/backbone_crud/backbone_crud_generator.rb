class BackboneCrudGenerator < Rails::Generators::Base
  source_root File.expand_path('../templates', __FILE__)

  argument :model_name, :type => :string
  argument :scope_name, :type => :string, :default => "admin"

  class_option :ids, :type => :boolean, :default => true, :desc => "Include ids model."
  class_option :blockable, :type => :boolean, :default => true, :desc => "Include blockable model."
  class_option :deletable, :type => :boolean, :default => true, :desc => "Include deletable model."

  def generate_collection
    template "collection.js.coffee.erb", "app/assets/javascripts/#{scope_name}/collections/#{models}.js.coffee"
    template "blocked_collection.js.coffee.erb", "app/assets/javascripts/#{scope_name}/collections/blocked_#{models}.js.coffee" if options.blockable?
  end

  def generate_model
    template "model.js.coffee.erb", "app/assets/javascripts/#{scope_name}/models/#{model}.js.coffee.erb"
    template "ids_model.js.coffee.erb", "app/assets/javascripts/#{scope_name}/models/ids_#{model}.js.coffee.erb" if options.ids?
  end

  def generate_views
    template "views/model.js.coffee.erb", "app/assets/javascripts/#{scope_name}/views/#{models}/#{model}.js.coffee"
    template "views/models_collection.js.coffee.erb", "app/assets/javascripts/#{scope_name}/views/#{models}/#{models}_collection.js.coffee"
    template "views/blocked_model.js.coffee.erb", "app/assets/javascripts/#{scope_name}/views/#{models}/blocked_#{model}.js.coffee" if options.blockable?
    template "views/blocked_models_collection.js.coffee.erb", "app/assets/javascripts/#{scope_name}/views/#{models}/blocked_#{models}_collection.js.coffee" if options.blockable?

    template "views/models_index_page.js.coffee.erb", "app/assets/javascripts/#{scope_name}/views/#{models}/#{models}_index_page.js.coffee.erb"
    template "views/models_blocked_page.js.coffee.erb", "app/assets/javascripts/#{scope_name}/views/#{models}/#{models}_blocked_page.js.coffee.erb" if options.blockable?

    template "views/model_add_page.js.coffee.erb", "app/assets/javascripts/#{scope_name}/views/#{models}/#{model}_add_page.js.coffee.erb"
    template "views/model_show_page.js.coffee.erb", "app/assets/javascripts/#{scope_name}/views/#{models}/#{model}_show_page.js.coffee.erb"
    template "views/model_edit_page.js.coffee.erb", "app/assets/javascripts/#{scope_name}/views/#{models}/#{model}_edit_page.js.coffee.erb"
    template "views/model_delete_page.js.coffee.erb", "app/assets/javascripts/#{scope_name}/views/#{models}/#{model}_delete_page.js.coffee.erb" if options.deletable?
  end

  def generate_controllers
    template "controllers/models_controller.rb.erb", "app/controllers/api/#{models}_controller.rb"
    template "controllers/model_ids_controller.rb.erb", "app/controllers/api/#{model}_ids_controller.rb" if options.ids?
  end

  def add_methods_to_router
    append_to_file "app/assets/javascripts/#{scope_name}/routers/#{scope_name}_router.js.coffee" do
  result =
  "

  # #{up_models}

  #{l_up_models}Index: (page, field, order) ->
    if page == '1' && _.isUndefined(field) && _.isUndefined(order)
      window.router.navigate(App.Models.#{up_model}.getCollectionPath(), true)
      return
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.#{up_models}IndexPage({page: page, field: field, order: order})
    this.swap(view)

  #{l_up_models}Blocked: (page, field, order)->
    page = parseInt(page)
    page = 1 if isNaN(page)
    field = 'created_at' if _.isUndefined(field)
    order = 'desc' if _.isUndefined(order)

    view = new App.Views.#{up_models}BlockedPage({page: page, field: field, order: order})
    this.swap(view)

  #{l_up_models}Show: (id) ->
    view = new App.Views.#{up_model}ShowPage({id: id})
    this.swap(view)

  #{l_up_models}Add: () ->
    view = new App.Views.#{up_model}AddPage()
    this.swap(view)

  #{l_up_models}Edit: (id) ->
    view = new App.Views.#{up_model}EditPage({id: id})
    this.swap(view)
  "

  if options.deletable?

  result +=
  "
  #{l_up_models}Delete: (id) ->
    view = new App.Views.#{up_model}DeletePage({id: id})
    this.swap(view)

  "
  end
    result
    end
  end

  private

  def model
    model_name.singularize.underscore
  end

  def models
    model_name.pluralize.underscore
  end

  def up_model
    model_name.singularize.camelize
  end

  def up_models
    model_name.pluralize.camelize
  end

  def l_up_model
    model_name.underscore.camelize(:lower)
  end

  def l_up_models
    model_name.pluralize.underscore.camelize(:lower)
  end

end
