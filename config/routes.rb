require 'sidekiq/web'

Archivizer::Application.routes.draw do

  mount Ckeditor::Engine => '/ckeditor'
  mount Sidekiq::Web, at: '/sidekiq'

  match '/uploads/grid/uploads/:model/file/:id/:filename' => 'gridfs#big_thumb_570x200', constraints: { filename: /big_thumb_570x200.*/ }
  match '/uploads/grid/uploads/:model/file/:id/:filename' => 'gridfs#big_thumb_270x430', constraints: { filename: /big_thumb_270x430.*/ }
  match '/uploads/grid/uploads/:model/file/:id/:filename' => 'gridfs#big_thumb_270x200', constraints: { filename: /big_thumb_270x200.*/ }
  match '/uploads/grid/uploads/:model/file/:id/:filename' => 'gridfs#big_thumb_270x170', constraints: { filename: /big_thumb_270x170.*/ }
  match '/uploads/grid/uploads/:model/file/:id/:filename' => 'gridfs#big_thumb_538x435', constraints: { filename: /big_thumb_538x435.*/ }
  match '/uploads/grid/uploads/:model/file/:id/:filename' => 'gridfs#big_thumb_file', constraints: { filename: /big_thumb.*/ }
  match '/uploads/grid/uploads/:model/file/:id/:filename' => 'gridfs#mini_thumb_24x24', constraints: { filename: /mini_thumb_24x24.*/ }
  match '/uploads/grid/uploads/:model/file/:id/:filename' => 'gridfs#mini_thumb_30x30', constraints: { filename: /mini_thumb_30x30.*/ }
  match '/uploads/grid/uploads/:model/file/:id/:filename' => 'gridfs#mini_thumb_70x70', constraints: { filename: /mini_thumb_70x70.*/ }
  match '/uploads/grid/uploads/:model/file/:id/:filename' => 'gridfs#middle_thumb_173x182', constraints: { filename: /middle_thumb_173x182.*/ }
  match '/uploads/grid/uploads/:model/file/:id/:filename' => 'gridfs#middle_thumb_142x163', constraints: { filename: /middle_thumb_142x163.*/ }
  match '/uploads/grid/uploads/:model/file/:id/:filename' => 'gridfs#middle_thumb_182x182', constraints: { filename: /middle_thumb_182x182.*/ }
  match '/uploads/grid/uploads/:model/file/:id/:filename' => 'gridfs#middle_thumb', constraints: { filename: /middle_thumb.*/ }
  match '/uploads/grid/uploads/:model/file/:id/:filename' => 'gridfs#thumb_file', constraints: { filename: /thumb.*/ }
  match '/uploads/grid/uploads/:model/file/:id/:filename' => 'gridfs#full_screen_thumb_1000x850', constraints: { filename: /full_screen_thumb_1000x850.*/ }
  match '/uploads/grid/uploads/:model/file/:id/:filename' => 'gridfs#full_screen_thumb', constraints: { filename: /full_screen_thumb.*/ }
  match '/uploads/grid/uploads/:model/file/:id/:filename' => 'gridfs#file', constraints: { filename: /.*/ }

  devise_for :users, :controllers => {:registrations => "registrations", :sessions => "sessions", :passwords => "passwords", :confirmations => "confirmations"}

  resources :search, :only => [:index, :show] do
    collection do
      post "index"
    end
  end

  resources :feed_backs, :path => "feed-back", :only => [:create] do
    post 'subscribe', :on => :collection
  end

  resources :faqs, :only => [:index], :path => 'faq'
  resources :people, :only => [:index]
  resources :reviews, :only => [:index]
  resources :pages, :only => [:show]
  resources :portfolios,  :only => [:index, :show], :path => 'portfolio' do
    get 'page/:page', :action => :index, :on => :collection
    get 'page/:page', :action => :show, :on => :member
  end
  resources :articles, :only => [:show]
  #resources :testings # Only for debugging

  namespace :api do

    resources :feed_backs, :path => "feed-back"
    resources :settings, :only => [:index, :show, :create, :update]
    resources :users do
      resources :inbox_messages, :path => "inbox-messages"
      resources :sent_messages, :path => "sent-messages"
      collection do
        get "blocked"
        get 'current'
      end
    end
    resources :user_ids, :path => "user-ids", :only => [:show, :destroy]

    resources :roles
    resources :permissions

    resources :translations, :only => [:index, :create] do
      collection do
        get "dump"
        post "import"
      end
    end
    resources :blocks do
      collection do
        get "blocked"
      end
    end
    resources :block_ids, :path => "block-ids", :only => [:show, :destroy]

    resources :sections do
      resources :categories
      member do
        get 'root_categories', :path => "root-categories"
        post 'rebuild'
      end
    end
    resources :menus do
      resources :links
      member do
        post 'rebuild'
      end
    end

    resources :faqs
    resources :people
    resources :comments
    resources :slides
    resources :portfolios
    resources :images
    resources :static_images, :path => 'static-images'
    resources :person_images, :path => 'person-images'
    resources :slideshow_images, :path => 'slideshow-images'
    resources :file_attachments
    resources :result_file_attachments
    resources :max_files
    resources :sample_project_styles
    resources :furniture_plans
    resources :ceiling_plans
    resources :lighting_plans
    resources :coverings_plans
    resources :coverings_textures
    resources :walls_plans
    resources :walls_textures
    resources :selected_facilities do
      collection do
        post 'empty'
      end
    end
    resources :selected_facility_textures
    resources :selected_scenes do
      collection do
        post 'empty'
      end
    end
    resources :selected_scene_textures
    resources :modeling_tasks
    resources :modeling_task_images
    resources :modeling_task_textures

    resources :sketches  do
      collection do
        get "blocked"
      end
    end
    resources :sketch_ids, :path => "sketch-ids", :only => [:show, :destroy]

    resources :reviews  do
      collection do
        get "blocked"
      end
    end
    resources :review_ids, :path => "review-ids", :only => [:show, :destroy]

    resources :discounts  do
      collection do
        get "blocked"
      end
    end
    resources :discount_ids, :path => "discount-ids", :only => [:show, :destroy]

    resources :nodes
    resources :pages  do
      collection do
        get "blocked"
      end
    end
    resources :page_ids, :path => "page-ids", :only => [:show, :destroy]
    resources :articles  do
      collection do
        get "blocked"
      end
    end
    resources :article_ids, :path => "article-ids", :only => [:show, :destroy]

    resources :facilities  do
      collection do
        get "blocked"
      end
    end
    resources :facility_ids, :path => "facility-ids", :only => [:show, :destroy]

    resources :scenes  do
      collection do
        get "blocked"
      end
    end
    resources :scene_ids, :path => "scene-ids", :only => [:show, :destroy]

    resources :facilities_sets, :path => "facilities-sets" do
      member do
        get "zip"
      end
    end
    resources :scenes_sets, :path => "scenes-sets" do
      member do
        get "zip"
      end
    end
    resources :tasks do
      resources :discussions
      member do
        get "zip"
        get "file_attachments_zip"
        get "result_file_attachments_zip"
        get "selected_facilities_zip"
        get "selected_scenes_zip"
        post "set"
        post "toggle_selected"
      end
    end

    resources :discussion_messages

    resources :projects  do
      collection do
        get "blocked"
      end
    end
    resources :project_ids, :path => "project-ids", :only => [:show, :destroy]

    resources :visualizations  do
      collection do
        get "blocked"
      end
      member do
        get "zip"
        get "sketch"
        post "order"
      end
    end
    resources :visualization_ids, :path => "visualization-ids", :only => [:show, :destroy]

  end

  match 'account', to: 'account#index'
  match 'account/*path', to: 'account#index'
  match 'admin', to: 'admin#index'
  match 'admin/*path', to: 'admin#index'

  get 'task_set', :to => 'welcome#task_set'
  get 'about', :to => 'welcome#about'
  get 'service', :to => 'welcome#service'
  get 'sitemap(.:format)', to: 'welcome#sitemap'
  get 'robots(.:format)', to: 'welcome#robots'
  get 'login', :to => 'welcome#login'
  get 'client-register', :to => 'welcome#client_register'
  get 'worker-register', :to => 'welcome#worker_register'

  root :to => 'welcome#index'

end
