# Be sure to restart your server when you modify this file.

#Archivizer::Application.config.session_store :cookie_store, key: '_archivizer_session'
#Archivizer::Application.config.session_store :redis_store

Rails.application.config.session_store ActionDispatch::Session::CacheStore, :expire_after => 20.minutes

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rails generate session_migration")
# Archivizer::Application.config.session_store :active_record_store
