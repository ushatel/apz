#!/usr/bin/env puma

if File.directory? '/home/archivizer/www/apz'

  directory '/home/archivizer/www/apz'
  bind "unix:///tmp/puma.apz.archivizer.com.app.sock"
  pidfile "/tmp/puma.apz.archivizer.com.pid"
  state_path '/tmp/apz.archivizer.com.puma.state'
  stdout_redirect '/tmp/apz_stdout', '/tmp/apz_stderr', true
  threads 32, 128
  workers 5
  environment 'production'
  daemonize true

else

  directory '/Users/vpolotskiy/www/archivizer.com.local'
  bind "unix:///tmp/puma.apz.archivizer.com.app.sock"
  pidfile "/tmp/puma.apz.archivizer.com.pid"
  state_path '/tmp/apz.archivizer.com.puma.state'
  stdout_redirect '/tmp/apz_stdout', '/tmp/apz_stderr', true
  threads 4, 16
  workers 3
  environment 'development'
  daemonize true

end
