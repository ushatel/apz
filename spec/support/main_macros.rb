module MainMacros

  def admin_email
    "vpolotskiy@gmail.com"
  end

  def admin_password
    "123654"
  end

  def uncomfirmed_user_email
    new_user = Fabricate.build(:user)
    new_user.save
    new_user.email
  end

  def user_email_and_password
    new_user = Fabricate.build(:user)
    new_user_password = "123654"
    new_user.password = new_user_password
    new_user.save
    new_user.confirm!
    {email: new_user.email, password: new_user_password}
  end

  def user_email
    new_user = Fabricate.build(:user)
    new_user.save
    new_user.confirm!
    new_user.email
  end

  def user_password
    "123654"
  end

  def login_as_admin!
    visit new_user_session_path
    page.should have_content(I18n.t('login.links.authorization'))
    fill_in "user_email", :with => admin_email
    fill_in "user_password", :with => admin_password
    click_on I18n.t('forms.fields.sign_in')
    sleep(2)
  end

  def login_as_user!
    visit new_user_session_path
    page.should have_content(I18n.t('login.links.authorization'))
    fill_in "user_email", :with => user_email
    fill_in "user_password", :with => user_password
    click_on I18n.t('forms.fields.sign_in')
    sleep(2)
  end

  def generate_password
    rand 111111..99999999
  end

end