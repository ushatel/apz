# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'rspec/autorun'
require 'capybara/rspec'
require 'capybara/rails'
require 'capybara/email/rspec'
require "email_spec"
require "selenium/client"
require 'capybara/mechanize'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

RSpec.configure do |config|
  # ## Mock Framework
  #
  # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  ## config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  #config.use_transactional_fixtures = true

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false
  
  config.include(MailerMacros)
  config.include(MainMacros)
  config.include(ViewsMacros)
  #config.before(:each) { reset_email }

  config.include(EmailSpec::Helpers)
  config.include(EmailSpec::Matchers)

  Capybara.javascript_driver = :selenium
end

#Capybara.register_driver :rack_test do |app|
#  Capybara::RackTest::Driver.new(app, :browser => :chrome)
#end

#Capybara.app_host = "http://get-date.com"
Capybara.default_driver = :selenium