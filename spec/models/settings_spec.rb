require 'spec_helper'

describe Settings do

  before(:all) do
    @site_name = Settings.site_name
  end

  after(:all) do
    Settings.site_name = @site_name
  end

  it 'self.set_value' do
    word = Faker::Lorem.sentence(1)
    Settings.set_value(:site_name, word)
    Settings.site_name.should eq(word)
  end
end
