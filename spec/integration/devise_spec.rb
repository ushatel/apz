require 'spec_helper'

describe 'Devise' do

  before(:each) do
    I18n.locale = :ru
  end

  it 'registering', :js => true do
    visit new_user_registration_path
    fill_in "user_name", :with => Faker::Internet.user_name
    user_email = Faker::Internet.email
    fill_in "user_email", :with => user_email
    password = generate_password
    fill_in "user_password", :with => password
    fill_in "user_password_confirmation", :with => password
    click_on I18n.t('words.register')
    page.should have_content(I18n.t("devise.registrations.signed_up_but_unconfirmed"))
    #last_email.to.should include(user_email)
  end

  it 'authorizing admin', :js => true do
    visit new_user_session_path
    page.should have_content(I18n.t('login.links.authorization'))
    fill_in "user_email", :with => admin_email
    fill_in "user_password", :with => admin_password
    find(:css, ".actions .submit").click
    page.should have_content(I18n.t('devise.sessions.signed_in'))
  end

  it 'authorizing user', :js => true do
    new_user = user_email_and_password
    visit new_user_session_path
    page.should have_content(I18n.t('login.links.authorization'))
    fill_in "user_email", :with => new_user[:email]
    fill_in "user_password", :with => new_user[:password]
    find(:css, ".actions .submit").click
    page.should have_content(I18n.t('devise.sessions.signed_in'))

    authorized_user = User.where(:email => new_user[:email]).first
    visit polymorphic_path(authorized_user)
    page.should have_content(authorized_user.get_name)
  end

  it 'reminds password', :js => true do
    visit new_user_password_path
    page.should have_content(I18n.t('login.links.forgot'))
    fill_in "user_email", :with => user_email
    click_on I18n.t('forms.fields.send_new_password')
    page.should have_content(I18n.t('devise.passwords.send_instructions'))
  end

  it 'sends confirmation', :js => true do
    visit new_user_confirmation_path
    page.should have_content(I18n.t('login.links.resend_confirmation'))
    fill_in "user_email", :with => uncomfirmed_user_email
    click_on I18n.t("login.links.resend_confirmation")
    page.should have_content(I18n.t('devise.confirmations.send_instructions'))
  end

end