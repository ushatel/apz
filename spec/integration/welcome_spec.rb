require 'spec_helper'

describe 'welcome page' do

  before(:each) do
    visit root_path
  end

  it 'shows last product on main page', :js => true do
    page.should have_content(Product.on_index_page.first.get_name)
  end

end