require 'spec_helper'

describe 'products page' do

  before(:each) do
    visit root_path
  end

  it 'shows product page', :js => true do
    last_product = Product.on_index_page.first
    page.should have_content(last_product.get_name)
    click_on last_product.get_name
    page.should have_content(last_product.sku)
  end

end