require 'spec_helper'

describe 'cart page' do

  before(:each) do
    visit root_path
    I18n.locale = :ru
  end

  it 'adds product to cart', :js => true do
    last_product = Product.on_index_page.first
    products_count = 55
    page.should have_content(last_product.get_name)
    click_on last_product.get_name
    page.should have_content(last_product.sku)
    find(:css, ".products-count").set products_count
    find(:css, ".add-to-cart").click
    page.should have_content(I18n.t("cart.added"))
    visit cart_index_path
    find(:css, ".product-count").value.should eq products_count.to_s
  end

  it 'adds products to cart', :js => true do
    last_products = Product.on_index_page
    products_count = 55
    page.should have_content(last_products[0].get_name)

    find(:css, ".products-count").set products_count
    find(:css, ".add-to-cart").click
    page.should have_content(I18n.t("cart.added"))
    visit cart_index_path
    find(:css, ".product-count").value.should eq products_count.to_s
  end

end