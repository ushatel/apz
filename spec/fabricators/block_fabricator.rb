Fabricator(:block) do

  type { Block.get_types.first.last.to_s }
  region { Block.get_regions.first.last.to_s }

  name { Faker::Name.name.gsub(/\./, "") }
  title { Faker::Name.name }
  value { Faker::Lorem.paragraph }

end