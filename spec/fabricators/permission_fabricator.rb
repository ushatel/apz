Fabricator(:permission) do

  action { Faker::Internet.user_name }
  models = [User, Role, Permission]
  subject_class { models[rand(models.count - 1)] }
  description { Faker::Lorem.paragraph }

end