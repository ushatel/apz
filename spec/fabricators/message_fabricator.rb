Fabricator(:message) do

  topic { Faker::Name.name.gsub(/\./, "") }
  body { Faker::Lorem.paragraph }
  sender { User.find_by_slug("vpolotskiy") }
  recipient { User.find_by_slug("margret_howe") }

end