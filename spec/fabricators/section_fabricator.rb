Fabricator(:section) do

  name { Faker::Name.name.gsub(/\./, "") }

end