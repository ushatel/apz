Fabricator(:link) do

  name { Faker::Name.name.gsub(/\./, "") }
  title { Faker::Name.name }
  href { Faker::Internet.domain_name }
  rel { Faker::Name.name }
  class_string { Faker::Name.name }
  nofollow { (rand(0..1) == 1) }
  noindex { (rand(0..1) == 1) }
  menu { Menu.all.last }

end