Fabricator(:user) do

  login { Faker::Internet.user_name.gsub(/\./, "") }
  email { Faker::Internet.email }
  name { Faker::Name.name }
  password { rand(111111..99999999) }

  city { Faker::Name.name }
  studio { Faker::Name.name }

end