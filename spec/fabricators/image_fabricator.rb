Fabricator(:image) do

  name { Faker::Name.name.gsub(/\./, "") }
  remote_file_url { "http://phototraff.ru/apple/#{rand(1..15)}.png" }

end