Fabricator(:role) do

  name { Faker::Name.name.gsub(/\./, "") }

end