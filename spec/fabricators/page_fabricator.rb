Fabricator(:page) do

  name { Faker::Name.name.gsub(/\./, "") }
  title { Faker::Name.name }
  teaser { Faker::Lorem.paragraph }
  body { Faker::Lorem.paragraph }
  seo_keywords { Faker::Lorem.paragraph }
  seo_description { Faker::Lorem.paragraph }
  seo_present_in_xml_sitemap { rand(0..1) == 1 }
  seo_robots { Faker::Name.name }

  user { User.all[User.all.count - 1] }

end