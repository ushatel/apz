Fabricator(:category) do

  name { Faker::Name.name.gsub(/\./, "") }
  section { Section.all.last }

end