Fabricator(:menu) do

  name { Faker::Name.name.gsub(/\./, "") }
  description { Faker::Lorem.paragraph }

end